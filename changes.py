#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

__doc__ = """
	2.18
		Fixed for Android 8.0

	2.17
		New java-python interaction (via call/called-signals)
		Fixed window freezing after onResume
		Fixed banner hiding/showing
		New type of asking for permissions implemented

	2.16
		Cols: new game
		Dict: shows plain content if failed to parse as HTML
		Dict: dictionary name under each article

	2.15
		Cards: JPG-format of background images was added
		Cards: timeout and opacity for guessed cards
		Fix for selection of multiple empty background categories
		Saving selected marks
		Performance improvements

	2.14
		Fix for downloading of large dictionaries (message about virus check)

	2.13
		Dict: additional undo/redo for words
		Fix for downloading of large dictionaries (message about virus check)

	2.12
		<b>Custom shared storages</b> for vocabularies (Google Drive)
		List: button to manual insertion
		New buttons in tree view for selection and download
		List: correct number of lines if searching
		Fix for failed downloading from shared storage

	2.11
		Speech: shortenings for [ab, axb] -> a(x)b and for [axb, ayb] -> a(x|y)b
		Fix for removing just created vocabulary (without data)

	2.10
		Fix for StopIteration if selected vocabulary is empty

	2.09
		<b>New game</b> "Speech": reads aloud selected vocabulary
		Insignificant part: opened but not necessarily closed brackets

	2.08
		Keep place for signs if dictionary variants are showing
		Dialog for release notes
		Feedback through email
		Fix for broken html-attributes in html-formatted dictionary
		Fix for ReadTimeoutError if loading shared source
		Fix for exception if removing a dictionary
		Fix for exception if removing a vocabulary
		Fix for getmtime if filesystem supports it only for directories
		Fix for background images with zero size
		Minor bugfixes and improvements

	2.06
		<b>Desktop widget</b>: random vocabulary pair, hours after last training
		New vertical bar, font selection for punctuation signs
		Restyled settings
		Search input in list of vocabulary pairs
		Dict: do not clear after saving vocabulary pair, but show tree of vocabularies
		Nicer icons, restyled inputs and tree selectors
		List: newer vocabulary pair on top
		Dict: fixed undo/redo if 'show in dictionary'-button pressed
		Bugfixes and improvements

	2.05
		Fix for "Failed QtBroadcastReceiver"
		Bugfixes and improvements

	2.04
		Fix for unexpected "Loading..." dialog
		Nice splash picture instead of green background
		Bugfixes and improvements

	2.03
		Fixed fonts, font dialogs, small images (hdpi)
		Accelerated search of variants
		Optimized access to compressed dictionaries
		Fixed delayed call and observable
		Smarter word/sign toggle with translation field
		Energy efficient workers
		Added buttons "show in dictionary"
		Nicer selector for vocabulary / dictionaries
		Bugfixes and improvements

	2.02
		Nicer selector for vocabulary/dictionaries tree
		Bugfixes and improvements

	2.01
		Compact design
		New types for background placement (cards)
		A lot of new options and customizations
		Bugfixes and improvements

	2.00
		Completely rewritten on Qt5, a lot of major changes

	1.29
		Support for user's Google Drive directories
		Text copy & paste
		Bugfixes and performance improvements

	1.21
		New shared storage
		Bugfixes and performance improvements

	1.15
		Increasing/decreasing the font size

	1.12
		New icons
		Splash screen

	1.11
		Helping message if no vocabularies

	1.10
		Fixes for interface

	1.09
		Font size customization
		Google Analytics

	1.08
		Shared storage
		New popup menu

	1.05
		Welcome screen, etc.

	1.04
		Export to txt
"""

import collections
import sys
import textwrap

# Fills CHANGES from module doc-string
CHANGES = collections.OrderedDict()
key = None
for line in [x for x in [xx.rstrip() for xx in textwrap.dedent(sys.modules[__name__].__doc__).splitlines()] if x]:
	if not line.startswith('\t'):
		key = line
	else:
		CHANGES.setdefault(key, []).append(line.lstrip())
# version = CHANGES.keys()[0]
version = next(iter(CHANGES.keys()))
