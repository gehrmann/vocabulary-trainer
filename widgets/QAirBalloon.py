#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
import datetime
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Inserts application modules path
	sys.path.insert(0, (os.path.dirname(__file__) or '.') + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)

if 'MODULE' in globals():
	# PyQt uic plugin interface, returns plugin type
	try:
		pluginType = MODULE
	except NameError:
		pass

	def moduleInformation():
		"""PyQt uic plugin interface, returns list of available widgets"""
		return 'widgets.QAirBalloon', ['QAirBalloon']  # Specific function call, only hard-coded values

else:
	from PyQt import QtCore, QtGui, QtWidgets, uic

	class QAirBalloon(QtWidgets.QLabel):
		def __init__(self, parent, position=None, size=None, border=0, color=None):
			super(QAirBalloon, self).__init__(parent)
			if position is not None:
				self.move(*position)
			if size is not None:
				self.resize(*size)

			self.angle = 0
			self.color = color

			self._border = border
			if border:
				self._border_pen = QtGui.QPen(QtGui.QColor('#66660000'), 1)
				self._border_brush = QtCore.Qt.NoBrush

			self._default_pen = QtCore.Qt.NoPen

			self._fire_pen = QtCore.Qt.NoPen

			self._rope_debug_pen = QtGui.QPen(QtGui.QColor('#FFFF0000'), 2)
			self._rope_brush = QtCore.Qt.NoBrush

			self._pixmap = None

		""" Model's event handlers """

		""" View's event handlers """

		def paintEvent(self, event):
			with QtGui.QPainter(self) as painter:  # Paints onto screen
				painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
				painter.setRenderHint(QtGui.QPainter.SmoothPixmapTransform)

				# Draws border
				if self._border:
					painter.setPen(self._border_pen)
					painter.setBrush(self._border_brush)
					painter.drawRect(0, 0, self.width(), self.height())

				painter.translate(self.width() / 2, self.height() / 2)
				painter.scale(.9, .9)
				painter.rotate(self._angle)
				painter.translate(- self.width() / 2, - self.height() / 2)
				# painter.drawRect(0, 0, self.width(), self.height())

				# painter.drawRect(0, 0, self.width(), self.height())
				pixmap = self._get_pixmap()

				# pixmap = pixmap.scaled(1 / self._scale, 1 / self._scale, QtCore.Qt.IgnoreAspectRatio, QtCore.Qt.SmoothTransformation)
				painter.drawPixmap(QtCore.QPointF(0, 0), pixmap)

		""" Helpers """

		@property
		def angle(self):
			return self._angle

		@angle.setter
		def angle(self, angle):
			self._angle = angle
			self.update()
		setAngle = angle.fset  # Property setter from *.ui
		angle = QtCore.pyqtProperty(float, angle.fget, angle.fset)

		@property
		def color(self):
			return self._color

		@color.setter
		def color(self, color):
			self._color = color
			self._default_brush = default_brush = QtGui.QBrush(QtGui.QColor(color or '#00000000'), QtCore.Qt.SolidPattern)
			self._rope_pen = QtGui.QPen(QtGui.QColor(color or '#00000000'), 1)
			fire_color = QtGui.QColor('#FF9966')
			fire_color.setAlpha(3 * default_brush.color().alpha())
			self._fire_brush = fire_brush = QtGui.QBrush(fire_color, QtCore.Qt.SolidPattern)
			self.update()
		setColor = color.fset  # Property setter from *.ui
		color = QtCore.pyqtProperty(unicode, color.fget, color.fset)

		def _get_pixmap(self):
			# if pixmap.isNull():
			if self._pixmap is None:
				# Draws pixmap
				self._pixmap = pixmap = QtGui.QPixmap(self.width(), self.height())

				pixmap.fill(QtCore.Qt.transparent)
				# pixmap.setMask(pixmap.createHeuristicMask())
				with QtGui.QPainter(pixmap) as painter:  # Paints onto pixmap
					painter.setRenderHint(QtGui.QPainter.Antialiasing, True)

					diameter = min(pixmap.width(), pixmap.height()) * .7
					side = diameter / 5
					fire_width, fire_height = side / 3, side

					# Draws fire
					painter.setPen(self._fire_pen)
					painter.setBrush(self._fire_brush)
					painter.drawEllipse(QtCore.QRectF(pixmap.width() / 2 - fire_width / 2, diameter, fire_width, fire_height))

					# Draws ball
					painter.setPen(self._default_pen)
					painter.setBrush(self._default_brush)
					painter.drawEllipse(QtCore.QRectF((pixmap.width() - diameter) / 2, 0, diameter, diameter))

					# Draws basket
					painter.setPen(self._default_pen)
					painter.setBrush(self._default_brush)
					painter.drawRect(QtCore.QRectF((pixmap.width() - side) / 2, pixmap.height() - side, side, side))

					# Draws ropes
					painter.setPen(self._rope_pen)
					# painter.setPen(self._rope_debug_pen)
					painter.setBrush(self._rope_brush)
					path = QtGui.QPainterPath()
					for i in range(-3, 4, 2):
						path.moveTo(QtCore.QPointF(pixmap.width() / 2 + .8 * diameter / 6 * i, - 0.0245 * diameter * i * i + 1.0 * diameter))
						path.lineTo(QtCore.QPointF(pixmap.width() / 2 + .8 * side / 6 * i, pixmap.height() - side))
					painter.drawPath(path)
			# logging.getLogger(__name__).warning('pixmap.isNull()=' + '%s', pixmap.isNull())
			return self._pixmap


def run_init():
	# info = moduleInformation()
	# logging.getLogger(__name__).info('info=' + '%s', info)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	parent = QtWidgets.QDialog()
	parent.move(100, 100)
	parent.resize(800, 420)
	parent.setLayout(QtWidgets.QVBoxLayout())
	parent.show()

	w = QAirBalloon(
		parent,
		position=(10, 10),
		# size=(75, 75),
		size=(400, 400),
		# size=(750, 750),
		border=1,
		color='#6611223d',
		# color='#F05223',
	)
	# w.angle = 15
	w.show()

	from controllers.abstract import ControllerMixture
	duration = 1
	# curve = 'OutCirc'
	# curve = 'SineCurve'
	# ControllerMixture._animate_sequential(w, key='angle', curve=curve, count=9999, animations=(
	#     dict(from_value=-15, to_value=15, duration=duration),
	# )).start()
	ControllerMixture._animate_parallel(w, animations=(
		dict(key='angle', curve='SineCurve', from_value=-5, to_value=5, duration=5., count=-1),
		# dict(key='pos', curve='InCirc', from_value=(-100, 50), to_value=(1600, -15), duration=18.),
	)).start()

	app.exec_()


def _main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	_main()
