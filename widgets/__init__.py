#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

# PyQt uic plugin interface, returns plugin type
try:
	pluginType = MODULE
except NameError:
	pass


def moduleInformation():
	"""PyQt uic plugin interface, returns list of available widgets"""
	return 'widgets', []
