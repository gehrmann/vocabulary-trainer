package com.gehrmann;

import __INSERT_PACKAGE_NAME_HERE__.R;

public class DesktopWidgetReceiver extends android.appwidget.AppWidgetProvider {
	private static final String PREFIX = "Java: DesktopWidgetReceiver.";

	@Override
	public void onUpdate(android.content.Context context, android.appwidget.AppWidgetManager widgets_manager, int[] widgets_ids) {
System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ____________________");

		// Initializes python: loads libraries
		QtHelper.load_libraries(context);
		// Initializes python: sets environment variables
		QtHelper.init_env(context);
		// Re-loads controllers-module from file
		QtHelper.pyeval("import controllers; reload(controllers)");
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ___");


		// For every widget
		for (int widget_id: widgets_ids) {
System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": widget_id=" + widget_id);

			android.os.Bundle options = widgets_manager.getAppWidgetOptions(widget_id);
			int min_width = options.getInt(android.appwidget.AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH);
			int max_width = options.getInt(android.appwidget.AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH);
			int min_height = options.getInt(android.appwidget.AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT);
			int max_height = options.getInt(android.appwidget.AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT);
			boolean is_portrait = context.getResources().getConfiguration().orientation == context.getResources().getConfiguration().ORIENTATION_PORTRAIT;
			long width = java.lang.Math.round(is_portrait? 1. * min_width: 1. * max_width);
			long height = java.lang.Math.round(is_portrait? 1. * max_height: 1. * min_height);
System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": portrait=" + is_portrait + " :" + min_width + ":" + width + ":" + max_width + " :: " + min_height + ":" + height + ":" + max_height);

			String image_path = QtHelper.pyrun("controllers", "get_desktop_widget_image", "{" + "u'id': " + widget_id + ", u'width': " + width + ", u'height': " + height + "}");
System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": image_path=" + image_path);

			android.widget.RemoteViews remote_layout = new android.widget.RemoteViews(context.getPackageName(), R.layout.desktop_widget_layout);
			// Updates widget image
			if (!image_path.isEmpty()) {
				// Generates uri to share image between processes
				android.net.Uri image_uri = android.support.v4.content.FileProvider.getUriForFile(context, context.getPackageName() + ".fileprovider", new java.io.File(image_path));
				// Grants uri-permission for every process found (can not get the name of process used for widget)
				android.content.Intent launchers_intent = new android.content.Intent(android.content.Intent.ACTION_MAIN, null);
				java.util.ArrayList<String> permitted_packages_cache = new java.util.ArrayList<String>();
				for (android.content.pm.ResolveInfo info: context.getPackageManager().queryIntentActivities(launchers_intent, 0)) {
					String package_name = info.activityInfo.packageName;
					if (!permitted_packages_cache.contains(package_name)) {
						permitted_packages_cache.add(package_name);
						context.grantUriPermission(package_name, image_uri, android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION);
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": allow access to " + info.activityInfo.packageName);
					}
				}
				// Sets image uri
				remote_layout.setImageViewUri(R.id.picture, image_uri);
			}
			// Binds FLAG_UPDATE_CURRENT to reload-button
			android.content.Intent intent = new android.content.Intent(context, DesktopWidgetReceiver.class);
			intent.setAction(android.appwidget.AppWidgetManager.ACTION_APPWIDGET_UPDATE);
			intent.putExtra(android.appwidget.AppWidgetManager.EXTRA_APPWIDGET_IDS, widgets_ids);
			remote_layout.setOnClickPendingIntent(R.id.reload_button, android.app.PendingIntent.getBroadcast(context, 0, intent, android.app.PendingIntent.FLAG_UPDATE_CURRENT));
			//
			widgets_manager.updateAppWidget(widget_id, remote_layout);
		}

System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ____________________");
	}

    public void onAppWidgetOptionsChanged(android.content.Context context, android.appwidget.AppWidgetManager widgets_manager, int widget_id, android.os.Bundle options) {
System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ____________________");
		onUpdate(context, widgets_manager, new int[] {widget_id});
    }
}
