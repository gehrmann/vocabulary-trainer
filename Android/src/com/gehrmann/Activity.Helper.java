public interface Helper {
	// private static final String PREFIX = "Java: Helper.";
	static final String PREFIX = "Java: Helper.";

	// static {
	//     // System.loadLibrary("gnustl_shared");
	//     // System.loadLibrary("Qt5Core");
	//     // System.loadLibrary("Qt5Gui");
	//     // System.loadLibrary("Qt5Widgets");
	//     // System.loadLibrary("python2.7");
	//     System.loadLibrary("vocabulary_trainer_trunk");
	// }

	public native void called(String arguments);
	// public native void called(String key, String value);
	// public static native void setenv(String key, String value);
	// public static native void pyeval(String script);
	// public static native String pyrun(String module, String function, String argument);

	public void
	onResume() {
		super.onResume();

		java.util.Map<String, String> result = new java.util.HashMap<String, String>();
		result.put("event", "onResume");
		called((new org.json.JSONObject(result)).toString());

		// if (org.qtproject.qt5.android.bindings.MyActivity.m_delegateObject != null && org.qtproject.qt5.android.bindings.MyActivity.m_delegateMethods.containsKey("setFullScreen")) {
		//     org.qtproject.qt5.android.bindings.MyActivity.invokeDelegateMethod(org.qtproject.qt5.android.bindings.MyActivity.m_delegateMethods.get("setFullScreen").get(0), true);
		//     org.qtproject.qt5.android.bindings.MyActivity.invokeDelegateMethod(org.qtproject.qt5.android.bindings.MyActivity.m_delegateMethods.get("setFullScreen").get(0), false);
		// }
		//     QtNative.updateWindow();
		//     updateFullScreen(); // Suspending the app clears the immersive mode, so we need to set it again.
		// }
	}

	String
	log(String serialized_kwargs) {
		java.util.Map<String, String> result = new java.util.HashMap<String, String>();

		try {
			org.json.JSONObject kwargs = new org.json.JSONObject(serialized_kwargs);
			final String level = kwargs.has("level")? kwargs.getString("level"): "";
			final String message = kwargs.has("message")? kwargs.getString("message"): "";

			System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + "LOG [" + level + "]  " + message);
		} catch (org.json.JSONException e) {
			result.put("error", "Exception \"" + e.getMessage() + "\"");
		}

		return (new org.json.JSONObject(result)).toString();
	}

	void dump_resources(String name, java.lang.reflect.Field[] fields) {
		// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "RESOURCE SCANNER(): ----------------------------------------");
		for (java.lang.reflect.Field field: fields) {
			try {
				System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "dump_resources(): " + name + "=" + field.getName());
			} catch (Exception e) {
				continue;
			}
		}
		// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "RESOURCE SCANNER(): ----------------------------------------");
	}

//     void init_env(android.content.Context context) {
//         String package_name = context.getPackageName();
//         String application_name = context.getApplicationInfo().nonLocalizedLabel.toString();
//         String application_basename = application_name.split("\\s*\\(")[0];

//         String version;
//         try {
//             version = context.getPackageManager().getPackageInfo(package_name, 0).versionName;
//         } catch (android.content.pm.PackageManager.NameNotFoundException e) {
//             version = "undefined";
//         }

//         String version_code;
//         try {
//             version_code = "" + context.getPackageManager().getPackageInfo(package_name, 0).versionCode;
//         } catch (android.content.pm.PackageManager.NameNotFoundException e) {
//             version_code = "undefined";
//         }

//         setenv("DENSITY", "" + context.getResources().getDisplayMetrics().density);
//         setenv("USER_AGENT", System.getProperty("http.agent"));
//         setenv("PROCESS_NAME", context.getApplicationInfo().processName);
//         setenv("APK_PATH", context.getApplicationInfo().sourceDir);
//         setenv("PACKAGE_NAME", package_name);
//         setenv("APPLICATION_FILES", "/data/data/" + package_name + "/files");
//         setenv("APPLICATION_NAME", application_name);
//         setenv("APPLICATION_BASENAME", application_basename);
//         setenv("APPLICATION_HOME", android.os.Environment.getExternalStorageDirectory() + "/" + application_basename);
//         // setenv("INSTALL_REFERRER", ...);
//         setenv("OS_VERSION", android.os.Build.VERSION.RELEASE);
//         setenv("OS_VERSION_CODE", android.os.Build.VERSION.INCREMENTAL);
//         setenv("APPLICATION_VERSION", version);
//         setenv("APPLICATION_VERSION_CODE", version_code);



// //         setenv("QT_USE_ANDROID_NATIVE_DIALOGS", "1");
// //         setenv("QT_ANDROID_THEME", "Theme_DeviceDefault_Light/");
// //         setenv("QT_ANDROID_THEME_DISPLAY_DPI", "240");
// //         setenv("QT_BLOCK_EVENT_LOOPS_WHEN_SUSPENDED", "1");
// //         setenv("QT_USE_ANDROID_NATIVE_STYLE", "1");
// //         setenv("MINISTRO_ANDROID_STYLE_PATH", "/data/data/gehrmann.vocabulary.learning.game.box.debug.pro/qt-reserved-files/android-style/240/");
// //         setenv("QT_ANDROID_THEMES_ROOT_PATH", "/data/data/gehrmann.vocabulary.learning.game.box.debug.pro/qt-reserved-files/android-style/");
// //         setenv("QML2_IMPORT_PATH", "/data/data/gehrmann.vocabulary.learning.game.box.debug.pro/qt-reserved-files//qml");
// //         setenv("QML_IMPORT_PATH", "/data/data/gehrmann.vocabulary.learning.game.box.debug.pro/qt-reserved-files//imports");
// //         setenv("QT_PLUGIN_PATH", "/data/data/gehrmann.vocabulary.learning.game.box.debug.pro/qt-reserved-files//plugins");

// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": _____");
// //         load_library("libjnigraphics.so");
// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": _____");
// //         load_library("libandroid.so");
// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": _____");
// //         load_library("libEGL.so");
// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": _____");
// //         load_library("libGLESv2.so");

// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": _____");
// //         dalvik.system.DexClassLoader classLoader = new dalvik.system.DexClassLoader(
// //                 "/data/data/" + package_name + "/lib/QtAndroid-bundled.jar",
// //                 context.getDir("outdex", android.content.Context.MODE_PRIVATE).getAbsolutePath(),
// //                 null, // libs folder (if exists)
// //                 context.getClassLoader()
// //                 );
// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": _____");

// //         load_library("/data/data/" + package_name + "/qt-reserved-files/plugins/platforms/android/libqtforandroid.so");


//     }

//     void load_libraries(android.content.Context context) {
//         String package_name = context.getPackageName();

//         java.util.ArrayList<String> libraries_paths = new java.util.ArrayList<String>();
//         libraries_paths.add("/data/data/" + package_name + "/lib/libgnustl_shared.so");
//         libraries_paths.add("/data/data/" + package_name + "/lib/libQt5Core.so");
//         libraries_paths.add("/data/data/" + package_name + "/lib/libQt5Gui.so");
//         libraries_paths.add("/data/data/" + package_name + "/lib/libQt5Widgets.so");
//         libraries_paths.add("/data/data/" + package_name + "/lib/libQt5AndroidExtras.so");
//         // libraries_paths.add("/data/data/" + package_name + "/qt-reserved-files/plugins/platforms/android/libqtforandroid.so");
//         libraries_paths.add("/data/data/" + package_name + "/lib/libpython2.7.so");
//         libraries_paths.add("/data/data/" + package_name + "/lib/libvocabulary_trainer_trunk.so");
//         for (String library_path: libraries_paths) {
//             load_library(library_path);
//         }
//     }

//     void load_library(String library_path) {
// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": library_path=" + library_path);
//         try {
//             java.io.File file = new java.io.File(library_path);
//             if (file.exists()) {
// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": library_path=" + library_path);
//                 System.load(library_path);
// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ___");
//             }
//         } catch (SecurityException e) {
//             android.util.Log.i(PREFIX, "Can't load '" + library_path + "'", e);
//         } catch (Exception e) {
//             android.util.Log.i(PREFIX, "Can't load '" + library_path + "'", e);
//         }
//     }

	// default void copy(java.io.File src, java.io.File dst) throws IOException {
	//     java.io.InputStream src = new java.io.FileInputStream(src);
	//     java.io.OutputStream dst = new java.io.FileOutputStream(dst);

	//     // Transfer bytes from src to dst
	//     byte[] buf = new byte[1024];
	//     int len;
	//     while ((len = src.read(buf)) > 0) {
	//         dst.write(buf, 0, len);
	//     }
	//     src.close();
	//     dst.close();
	// }
	void move(String src_path, String dst_path) {
		java.io.InputStream src = null;
		java.io.OutputStream dst = null;
		try {
			src = new java.io.FileInputStream(src_path);
			dst = new java.io.FileOutputStream(dst_path);
			byte[] buffer = new byte[1024];
			int read;
			while ((read = src.read(buffer)) != -1) {
				dst.write(buffer, 0, read);
			}
			src.close();
			src = null;

			// Writes to dst_path
			dst.flush();
			dst.close();
			dst = null;

			// Removes src_path
			new java.io.File(src_path).delete();

		} catch (java.io.FileNotFoundException e) {
			android.util.Log.i(PREFIX, "Can't move '" + src_path + "' to '" + dst_path + "': " + e.getMessage());

		} catch (Exception e) {
			android.util.Log.i(PREFIX, "Can't move '" + src_path + "' to '" + dst_path + "': " + e.getMessage());
		}
	}

	void print_packages_by_categories(android.content.Context context) {
System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ======================");
		for (String category: new String[] {
				android.content.Intent.CATEGORY_DEFAULT,
				android.content.Intent.CATEGORY_BROWSABLE,
				android.content.Intent.CATEGORY_TAB,
				android.content.Intent.CATEGORY_ALTERNATIVE,
				android.content.Intent.CATEGORY_SELECTED_ALTERNATIVE,
				android.content.Intent.CATEGORY_LAUNCHER,
				android.content.Intent.CATEGORY_INFO,
				android.content.Intent.CATEGORY_HOME,
				android.content.Intent.CATEGORY_PREFERENCE,
				android.content.Intent.CATEGORY_TEST,
				android.content.Intent.CATEGORY_CAR_DOCK,
				android.content.Intent.CATEGORY_DESK_DOCK,
				android.content.Intent.CATEGORY_LE_DESK_DOCK,
				android.content.Intent.CATEGORY_HE_DESK_DOCK,
				android.content.Intent.CATEGORY_CAR_MODE,
				android.content.Intent.CATEGORY_APP_MARKET,
		}) {
System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- " + category);
			android.content.Intent intent = new android.content.Intent(android.content.Intent.ACTION_MAIN, null);
			intent.addCategory(category);
			for (android.content.pm.ResolveInfo info: context.getPackageManager().queryIntentActivities(intent, 0)) {
System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": package " + info.activityInfo.packageName);
			}
		}
System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + "<<< PROFILE >>> " + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ======================");
	}

	String
	show_rate_application(String kwargs) {
		java.util.Map<String, String> result = new java.util.HashMap<String, String>();

// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- ");
		// if (m_activity == null) {
		//     result.put("error", "m_activity is null");

		// } else {
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- ");
			android.content.Intent open_market_intent = new android.content.Intent(android.content.Intent.ACTION_VIEW, android.net.Uri.parse("market://details?id=__INSERT_PACKAGE_NAME_HERE__"));
			open_market_intent.addFlags(android.content.Intent.FLAG_ACTIVITY_NO_HISTORY | android.content.Intent.FLAG_ACTIVITY_NEW_DOCUMENT | android.content.Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
			try {
				this.startActivity(open_market_intent);
			} catch (android.content.ActivityNotFoundException e) {
				this.startActivity(new android.content.Intent(android.content.Intent.ACTION_VIEW, android.net.Uri.parse("http://play.google.com/store/apps/details?id=__INSERT_PACKAGE_NAME_HERE__")));
			}
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- ");
		// }
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- ");

		return (new org.json.JSONObject(result)).toString();
	}

	String
	show_feedback_email(String serialized_kwargs) {
		java.util.Map<String, String> result = new java.util.HashMap<String, String>();

		try {
			org.json.JSONObject kwargs = new org.json.JSONObject(serialized_kwargs);
			final String to_email = kwargs.has("to")? kwargs.getString("to"): "";
			final String subject = kwargs.has("subject")? kwargs.getString("subject"): "";
			final String message = kwargs.has("message")? kwargs.getString("message"): "";

// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- ");
		// if (m_activity == null) {
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- ");
			// result.put("error", "m_activity is null");

		// } else {
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- ");
			// java.util.ArrayList<String> body = new java.util.ArrayList<String>();
			// body.add(message);
			String[] attachments_paths = new String[] {};
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- ");

			try {
				final android.content.Context context = this.getApplicationContext();
				android.content.Intent email_intent = new android.content.Intent(android.content.Intent.ACTION_SEND_MULTIPLE);
				//email_intent.setType("plain/text");
				email_intent.setType("message/rfc822");
				email_intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{to_email});
				email_intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
				email_intent.putExtra(android.content.Intent.EXTRA_TEXT, message);
				// Attachments
				java.util.ArrayList<android.net.Uri> attachments_uris = new java.util.ArrayList<android.net.Uri>();
				for (String attachment_path: attachments_paths) {
					java.io.File attachment = new java.io.File(attachment_path);
					if (!attachment.exists()) {
						result.put("error", "Attachment \"" + attachment_path + "\" does not exist");
						this.runOnUiThread(new Runnable() {
							public void run() {
								try {
									android.widget.Toast.makeText(context, "Attachment does not exist", android.widget.Toast.LENGTH_SHORT).show();
								} catch (java.lang.Exception e) {
									System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": Toast exception '" + e.getMessage() + "'.");
								}
							}
						});
					} else if (!attachment.canRead()) {
						result.put("error", "Can not read attachment \"" + attachment_path + "\"");
						this.runOnUiThread(new Runnable() {
							public void run() {
								try {
									android.widget.Toast.makeText(context, "Can not read attachment", android.widget.Toast.LENGTH_SHORT).show();
								} catch (java.lang.Exception e) {
									System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": Toast exception '" + e.getMessage() + "'.");
								}
							}
						});
					} else {
						//email_intent.putExtra(android.content.Intent.EXTRA_STREAM, android.net.Uri.fromFile(attachment));
						attachments_uris.add(android.net.Uri.fromFile(attachment));
					}
				}
				email_intent.putParcelableArrayListExtra(android.content.Intent.EXTRA_STREAM, attachments_uris);

				// Send
				try {
					android.content.Intent intent = android.content.Intent.createChooser(email_intent, "Sending email...");
					intent.addFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
				} catch (android.content.ActivityNotFoundException e) {
					System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": There are no email-clients installed.");
					result.put("error", "Email-clients are not found");
				}
			} catch (java.lang.Exception e) {
				System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": Exception '" + e.getMessage() + "'.");
				result.put("error", "Exception \"" + e.getMessage() + "\"");
			}

		} catch (org.json.JSONException e) {
			result.put("error", "Exception \"" + e.getMessage() + "\"");
		}

// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- ");
		// }
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": ----- ");

		return (new org.json.JSONObject(result)).toString();
	}

	String
	get_locales(String serialized_kwargs) {
		java.util.Map<String, String> result = new java.util.HashMap<String, String>();

		for (java.util.Locale locale: java.util.Locale.getAvailableLocales()) {
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": LOCALE=" + locale.getLanguage() + "_" + locale.getCountry() + " [" + locale.getDisplayName() + "]");
			result.put(locale.getLanguage() + (locale.getCountry() != ""? "_" + locale.getCountry(): ""), locale.getDisplayName());
		}

		return (new org.json.JSONObject(result)).toString();
	}

	// private static android.speech.tts.TextToSpeech _text_to_speech = null;
	// private static boolean _text_to_speech_ready = false;
	android.speech.tts.TextToSpeech _text_to_speech = null;
	boolean _text_to_speech_ready = false;

	private void
	_init_text_to_speech() {
		_text_to_speech = new android.speech.tts.TextToSpeech(this, (new android.speech.tts.TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
				_text_to_speech_ready = true;
			}
		}));
	}

	String
	get_text_to_speech_languages(String serialized_kwargs) {
		java.util.Map<String, String> result = new java.util.HashMap<String, String>();

		if (_text_to_speech == null) {
			_init_text_to_speech();
			result.put("error", "Text-to-speech is not ready");

		} else if (!_text_to_speech_ready) {
			result.put("error", "Text-to-speech is not ready");

		} else {
//             try {
//                 for (android.speech.tts.Voice voice: _text_to_speech.getVoices()) {
// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": LOCALE=" + voice.getLocale().getLanguage() + "_" + voice.getLocale().getCountry() + " [" + voice.getLocale().getDisplayName() + "]");
//                     result.put(voice.getLocale().getLanguage() + (voice.getLocale().getCountry() != ""? "_" + voice.getLocale().getCountry(): ""), voice.getLocale().getDisplayName());
//                 }
//                 result.put("debug", "1");
//             // } catch (NoSuchMethodError e1) {
//             } catch (IllegalArgumentException e1) {
//                 try {
//                     for (java.util.Locale locale: _text_to_speech.getAvailableLanguages()) {
// // System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": LOCALE=" + locale.getLanguage() + "_" + locale.getCountry() + " [" + locale.getDisplayName() + "]");
//                         result.put(locale.getLanguage() + (locale.getCountry() != ""? "_" + locale.getCountry(): ""), locale.getDisplayName());
//                     }
//                     result.put("debug", "2");
//                 } catch (IllegalArgumentException e2) {
//                 // } catch (NoSuchMethodError e2) {
					try {
						// Best way to get list of available text-to-speech-locales
						for (java.util.Locale locale: java.util.Locale.getAvailableLocales()) {
							try {
								if (_text_to_speech.isLanguageAvailable(locale) == android.speech.tts.TextToSpeech.LANG_AVAILABLE) {
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": LOCALE=" + locale.getLanguage() + "_" + locale.getCountry() + " [" + locale.getDisplayName() + "]");
									result.put(locale.getLanguage() + (locale.getCountry() != ""? "_" + locale.getCountry(): ""), locale.getDisplayName());
								}
							} catch (IllegalArgumentException e3) {
							}
						}
						// result.put("debug", "3");

					} catch (IllegalArgumentException e3) {
						result.put("error", "Illegal argument");

					} catch (NoSuchMethodError e3) {
						result.put("error", "No method");
					}
//                 }
//             }
		}

		// called((new org.json.JSONObject(result)).toString());

		return (new org.json.JSONObject(result)).toString();
	}

	String
	say(String serialized_kwargs) {
		java.util.Map<String, String> result = new java.util.HashMap<String, String>();

		try {
			org.json.JSONObject kwargs = new org.json.JSONObject(serialized_kwargs);
			final String text = kwargs.has("text")? kwargs.getString("text"): "";
			// final java.util.Locale locale = new java.util.Locale((kwargs.has("locale")? kwargs.getString("locale"): "en"), "", "");
			final java.util.Locale locale = new java.util.Locale((kwargs.has("locale")? kwargs.getString("locale"): "en"));
			final boolean has_flush = kwargs.has("flush") && kwargs.getBoolean("flush");
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": \t\t\t\t\tLOCALE=" + locale + ", TEXT=" + text);

			if (_text_to_speech == null) {
				_init_text_to_speech();
				result.put("error", "Text-to-speech is not ready");

			} else if (!_text_to_speech_ready) {
				result.put("error", "Text-to-speech is not ready");

			// } else if (_text_to_speech.isLanguageAvailable(locale) < android.speech.tts.TextToSpeech.LANG_AVAILABLE) {
			// if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
			} else if (_text_to_speech.isLanguageAvailable(locale) == android.speech.tts.TextToSpeech.LANG_NOT_SUPPORTED) {
				result.put("error", "Locale \"" + locale + "\" is not supported");

			} else if (_text_to_speech.isLanguageAvailable(locale) == android.speech.tts.TextToSpeech.LANG_MISSING_DATA) {
				result.put("error", "Missing data for locale \"" + locale + "\"");

			} else {
				_text_to_speech.setLanguage(locale);
				_text_to_speech.speak(text, (has_flush? android.speech.tts.TextToSpeech.QUEUE_FLUSH: android.speech.tts.TextToSpeech.QUEUE_ADD), null);
// System.err.println(new java.text.SimpleDateFormat("HH:mm:ss.SSS ").format(new java.util.Date()) + PREFIX + Thread.currentThread().getStackTrace()[2].getMethodName() + "():" + Thread.currentThread().getStackTrace()[2].getLineNumber() + ": \t\t\t\t\tFLUSH=" + (has_flush? "YES": "NO"));
			}

		} catch (org.json.JSONException e) {
			result.put("error", "Exception \"" + e.getMessage() + "\"");
		}

		return (new org.json.JSONObject(result)).toString();
	}

	String
	get_text_to_speech_state(String serialized_kwargs) {
		java.util.Map<String, String> result = new java.util.HashMap<String, String>();

		if (_text_to_speech == null) {
			result.put("error", "Text-to-speech is not initialized");

		} else if (!_text_to_speech_ready) {
			result.put("error", "Text-to-speech is not ready");

		} else if (_text_to_speech.isSpeaking()) {
			result.put("error", "Text-to-speech is speaking");
		}

		return (new org.json.JSONObject(result)).toString();
	}

	String
	open_url(String serialized_kwargs) {
		// Arguments:
		// * url -- URL to open
		// Returns:
		// * error -- contains error message if appeared
		java.util.Map<String, String> result = new java.util.HashMap<String, String>();

		try {
			org.json.JSONObject kwargs = new org.json.JSONObject(serialized_kwargs);
			final String url = kwargs.has("url")? kwargs.getString("url"): "";

			// Opens URL in browser
			android.content.Intent intent = new android.content.Intent(android.content.Intent.ACTION_VIEW, android.net.Uri.parse(url));
			this.startActivity(intent);

		} catch (org.json.JSONException e) {
			result.put("error", "Exception \"" + e.getMessage() + "\"");
		}

		return (new org.json.JSONObject(result)).toString();
	}

	String
	get_language(String serialized_kwargs) {
		// Arguments: no
		// Returns: system language
		java.util.Map<String, String> result = new java.util.HashMap<String, String>();

		result.put("result", java.util.Locale.getDefault().getDisplayLanguage());

		return (new org.json.JSONObject(result)).toString();
	}
}
