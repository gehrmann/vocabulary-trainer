#!/usr/bin/env bash

#set -x

SDK="/home/vlad/-install/-programming/-android/sdk"
DST="/storage/emulated/0"

rm -f stat_profile.prof*

SRC_PATHS=$(${SDK}/platform-tools/adb shell 'ls ${DST}/Vocabulary\ Trainer/stat_profile.prof* 2>&-' | sed 's/\r//g')

if test -n "${SRC_PATHS}"; then
	IFS=$'\n'  # Some nonsense as a delimiter
	for SRC_PATH in $SRC_PATHS; do
		echo PATH=$SRC_PATH
		${SDK}/platform-tools/adb pull ${SRC_PATH} ./
		${SDK}/platform-tools/adb shell rm \'${SRC_PATH}\'
	done

	gvim -o stat_profile.prof*
fi
