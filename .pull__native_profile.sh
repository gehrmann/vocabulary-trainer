#!/usr/bin/env bash

SDK="/home/vlad/-install/-programming/-android/sdk"
DST="/storage/emulated/0"

#${SDK}/platform-tools/adb pull ${DST}/Addigo\ Servicereports/native_profile.prof ./; runsnake native_profile.prof &
#${SDK}/platform-tools/adb shell rm ${DST}/Addigo\ Servicereports/native_profile.prof
rm -f native_profile.prof
${SDK}/platform-tools/adb pull ${DST}/Vocabulary\ Trainer/native_profile.prof ./; runsnake native_profile.prof &
${SDK}/platform-tools/adb shell rm ${DST}/Vocabulary\ Trainer/native_profile.prof
