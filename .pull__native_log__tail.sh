#!/usr/bin/env bash

SDK="/home/vlad/-install/-programming/-android/sdk"
DST="/storage/emulated/0"
#DELAY=0.2
DELAY=1

watch -n ${DELAY} "${SDK}/platform-tools/adb shell cat \"${DST}/Vocabulary\ Trainer/native.txt\" | grep -v -e \"after connection broken by\" -e \"Max retries exceeded with url\" -e \"Connection pool is full\" | tail -n ${@:-45}"
