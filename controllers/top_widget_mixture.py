#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

import logging
import os
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Sets default timeout for network requests
	socket.setdefaulttimeout(10)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtGui, QtWidgets, uic

from helpers.caller import Caller
from models.abstract import ObservableSet


class TopWidgetMixture(object):
	def __init__(
			self,
			parent_view=None,
			with_reload_button=False,
			with_change_button=False,
			with_undo_redo_buttons=False,
			with_double_undo_redo_buttons=False,
			with_marks_buttons=False,
			with_dictionaries_button=False,
			with_backgrounds_button=False,
			with_vocabularies_button=False,
			with_save_button=False,
	):

		# Models
		""" Current-State-Model for selected view's data"""
		state_model = self.state_model
		state_model.selected_marks = ObservableSet([])
		state_model.new_count = ''
		state_model.bad_count = ''
		state_model.fair_count = ''
		state_model.good_count = ''
		state_model.tips = ''

		self.__with_vocabularies_button = with_vocabularies_button

		# Views
		self.top_widget = view = self._load_ui('views/top_widget.ui')  # 220ms
		parent_view.addWidget(view)

		view.vocabulary_button.clicked.connect(self.__on_vocabulary_button_clicked)
		if not with_vocabularies_button:
			view.vocabulary_button.setParent(None)
		if not with_dictionaries_button:
			view.dictionaries_button.setParent(None)
		if not with_backgrounds_button:
			view.backgrounds_button.setParent(None)
		if not with_change_button:
			view.change_button.setParent(None)
		if not with_undo_redo_buttons:
			view.undo_button.setParent(None)
			view.redo_button.setParent(None)
		if not with_double_undo_redo_buttons:
			view.double_undo_button.setParent(None)
			view.double_redo_button.setParent(None)
		if not with_marks_buttons:
			view.new_checkbox.setParent(None)
			view.bad_checkbox.setParent(None)
			view.fair_checkbox.setParent(None)
			view.good_checkbox.setParent(None)
		else:
			view.new_checkbox.toggled.connect(self.__on_new_checkbox_toggled)
			view.bad_checkbox.toggled.connect(self.__on_bad_checkbox_toggled)
			view.fair_checkbox.toggled.connect(self.__on_fair_checkbox_toggled)
			view.good_checkbox.toggled.connect(self.__on_good_checkbox_toggled)
			view.new_checkbox.paintEvent = (lambda widget, previous_callback: (lambda event: (self.__on_checkbox_painting(event, widget, previous_callback))))(view.new_checkbox, view.new_checkbox.paintEvent)
			view.bad_checkbox.paintEvent = (lambda widget, previous_callback: (lambda event: (self.__on_checkbox_painting(event, widget, previous_callback))))(view.bad_checkbox, view.bad_checkbox.paintEvent)
			view.fair_checkbox.paintEvent = (lambda widget, previous_callback: (lambda event: (self.__on_checkbox_painting(event, widget, previous_callback))))(view.fair_checkbox, view.fair_checkbox.paintEvent)
			view.good_checkbox.paintEvent = (lambda widget, previous_callback: (lambda event: (self.__on_checkbox_painting(event, widget, previous_callback))))(view.good_checkbox, view.good_checkbox.paintEvent)
		if not with_reload_button:
			view.reload_button.setParent(None)
		if not with_save_button:
			view.save_button.setParent(None)
		view.settings_button.clicked.connect(self._on_settings_button_clicked)
		# Resizes widget to its content
		if hasattr(view.tips_label, 'setHtml'):  # Only for QWebView, not for QLabel
			view.tips_label.loadFinished.connect(lambda url: (view.tips_label.setMinimumSize(view.tips_label.page().mainFrame().contentsSize())))
		view.wizard_button.clicked.connect(self.__on_wizard_button_clicked)

		# if 1:
		#     # Blinks with a settings button (in order to detect if main window freezes)
		#     def call(__state=dict(index=0)):
		#         __state['index'] += 1
		#         if not __state['index'] % 60 == 1:
		#             view.window().showFullScreen()
		#         if not __state['index'] % 60 == 3:
		#             view.window().showNormal()
		#         view.settings_button.setEnabled(not view.settings_button.isEnabled())
		#         # logging.getLogger(__name__).warning('CALLED')
		#         Caller.call_once_after(1., call)
		#     call()

		# from PyQt5 import QtQuick
		# view = QtQuick.QQuickView()
		# view.setSource(QtCore.QUrl.fromLocalFile('views/_test.qml'))
		# view.show()

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug(self.__class__, object.__repr__(model), previous and previous, current and current)
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		data_model = self.data_model
		state_model = self.state_model
		settings_model = self.settings_model
		view = self.top_widget

		if model is state_model:
			# if previous[0] is not None and 'vocabulary' in previous[0]:
			#     if self.__with_vocabularies_button:
			#         if previous[0]['vocabulary'] is not None:
			#             previous[0]['vocabulary'].changed.unbind(state_model.changed)  # Unbind from previous[0] vocabulary

			if current[0] is None or 'vocabulary' in current[0] and previous[0] != current[0]:
				if self.__with_vocabularies_button:
					tips = ''
					if state_model.vocabulary is None:
						# Show tip: how to select dictinaries
						tips += 'Click &nbsp;<img width="{0}" height="{0}" src="qrc:/images/vocabularies-dark.png">&nbsp; to create / select a vocabulary.'.format(int(3 * view.physicalDpiX() / 25.4))
					state_model.tips = tips

					if state_model.vocabulary is not None:
						# state_model.vocabulary.changed.bind(state_model.changed)  # Bind to current vocabulary
						state_model.vocabulary.new  # Fix: force pre-loading of the vocabulary in order to prevent needless double-calling of self._fill

					Caller.call_once_after(0, self._fill)

			if current[0] is None or 'vocabulary' in current[0] and previous[:3] != current[:3]:
				Caller.call_once_after(.2, self._update_marks_counts)

			if current[0] is None or 'selected_marks' in current[0] and previous[:2] != current[:2]:
				view.new_checkbox.setChecked('new' in state_model.selected_marks)
				view.good_checkbox.setChecked('good' in state_model.selected_marks)
				view.fair_checkbox.setChecked('fair' in state_model.selected_marks)
				view.bad_checkbox.setChecked('bad' in state_model.selected_marks)

				Caller.call_once_after(0, self._fill)

			if current[0] is None or 'new_count' in current[0] and previous[0] != current[0]:
				if view.new_checkbox.parent() is not None:
					view.new_checkbox.setVisible(bool(state_model.new_count.strip('0')))
					view.new_checkbox.setText(state_model.new_count)

			if current[0] is None or 'bad_count' in current[0] and previous[0] != current[0]:
				if view.bad_checkbox.parent() is not None:
					view.bad_checkbox.setVisible(bool(state_model.bad_count.strip('0')))
					view.bad_checkbox.setText(state_model.bad_count)

			if current[0] is None or 'fair_count' in current[0] and previous[0] != current[0]:
				if view.fair_checkbox.parent() is not None:
					view.fair_checkbox.setVisible(bool(state_model.fair_count.strip('0')))
					view.fair_checkbox.setText(state_model.fair_count)

			if current[0] is None or 'good_count' in current[0] and previous[0] != current[0]:
				if view.good_checkbox.parent() is not None:
					view.good_checkbox.setVisible(bool(state_model.good_count.strip('0')))
					view.good_checkbox.setText(state_model.good_count)

			if current[0] is None or 'tips' in current[0] and previous[0] != current[0]:
				getattr(view.tips_widget, 'show' if state_model.tips else 'hide')()
				if hasattr(view.tips_label, 'setHtml'):  # If QWebView
					view.tips_label.setHtml(str(state_model.tips))
				else:
					view.tips_label.setText(state_model.tips.replace('qrc:', ':'))
				if state_model.tips:
					self._animate_wizard_button()

		# if self.__with_vocabularies_button:
		#     if model is state_model.vocabulary:
		#         if current[0] is None or 'new' in current[0] and previous[0] != current[0]:
		#             if 'new' in state_model.selected_marks:
		#                 Caller.call_once_after(0, self._fill)

	""" View's event handlers """

	def __on_checkbox_painting(self, event, widget, previous_callback):
		from styles import styles

		previous_callback(event)

		if event.rect() == widget.rect():
			if widget.text():  # Draw only if text is not empty
				# logging.getLogger(__name__).debug('Painting: text: %s', widget.text())
				with QtGui.QPainter(widget) as painter:
					painter.setRenderHint(painter.Antialiasing, True)
					default_pen = painter.pen()
					default_brush = painter.brush()

					# # Draw rounded rectangle
					# painter.drawRoundedRect(event.rect().x() - .5, event.rect().y() - .5, event.rect().width(), event.rect().height(), 0, 0)

					# Calculate size of text
					painter.setPen(QtGui.QPen(QtCore.Qt.transparent))
					text_rect = painter.drawText(event.rect().x(), event.rect().y(), event.rect().width() - styles.mm2px(1.), event.rect().height() - styles.mm2px(.5), QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom, widget.text())

					# Draw rounded rectangle
					painter.setPen(QtCore.Qt.NoPen)
					# painter.setBrush(QtGui.QBrush(QtGui.QColor('#ffcc66'), QtCore.Qt.SolidPattern))
					painter.setBrush(QtGui.QBrush(QtGui.QColor(240, 240, 240, 200), QtCore.Qt.SolidPattern))
					painter.drawRoundedRect(text_rect.x() - styles.mm2px(.4), text_rect.y() - styles.mm2px(.3), text_rect.width() + styles.mm2px(.8), text_rect.height() + styles.mm2px(.4), styles.mm2px(.6), styles.mm2px(.6))

					# Draw text
					painter.setPen(default_pen)
					painter.setBrush(default_brush)
					painter.drawText(text_rect, QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom, widget.text())

	def __on_vocabulary_button_clicked(self, event):
		def on_vocabulary_renamed(item, name):
			item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

		from controllers.abstract import VocabulariesDialog
		VocabulariesDialog(
			headers=['Vocabulary'],
			settings_model=self.settings_model,
			data_model=self.data_model.vocabularies,
			model=self.state_model,
			key='vocabulary',
			path_attribute='path',
			path_prefix='vocabularies/',
			is_expanded=True,
			is_container_selectable=False,
			renamed=on_vocabulary_renamed,
			selected=(self._on_vocabulary_selected if hasattr(self, '_on_vocabulary_selected') else None),
		).show()

	def __on_new_checkbox_toggled(self):
		if self.top_widget.new_checkbox.isChecked() != ('new' in self.state_model.selected_marks):
			getattr(self.state_model.selected_marks, 'add' if self.top_widget.new_checkbox.isChecked() else 'remove')('new')

	def __on_bad_checkbox_toggled(self):
		if self.top_widget.bad_checkbox.isChecked() != ('bad' in self.state_model.selected_marks):
			getattr(self.state_model.selected_marks, 'add' if self.top_widget.bad_checkbox.isChecked() else 'remove')('bad')

	def __on_fair_checkbox_toggled(self):
		if self.top_widget.fair_checkbox.isChecked() != ('fair' in self.state_model.selected_marks):
			getattr(self.state_model.selected_marks, 'add' if self.top_widget.fair_checkbox.isChecked() else 'remove')('fair')

	def __on_good_checkbox_toggled(self):
		if self.top_widget.good_checkbox.isChecked() != ('good' in self.state_model.selected_marks):
			getattr(self.state_model.selected_marks, 'add' if self.top_widget.good_checkbox.isChecked() else 'remove')('good')

	def __on_wizard_button_clicked(self, event):
		from controllers.first_start_wizard import FirstStartWizardController
		Caller.call_once_after(.1, FirstStartWizardController(settings_model=self.settings_model).show)

	""" Helpers """

	def _fill(self):
		pass

	def _update_marks_counts(self):
		state_model = self.state_model
		vocabulary = self.state_model.vocabulary

		if vocabulary is not None:
			state_model.new_count = "{}".format(len(vocabulary.new))
			state_model.bad_count = "{}".format(len(vocabulary.bad))
			state_model.fair_count = "{}".format(len(vocabulary.fair))
			state_model.good_count = "{}".format(len(vocabulary.good))

	def _animate_wizard_button(self, state=dict(activated=set(), counter=dict())):
		view = self.top_widget

		if view.wizard_button not in state['activated']:
			state['activated'].add(view.wizard_button)

			def animate():
				if state['counter'].setdefault(view.wizard_button, 0) < 5:
					state['counter'][view.wizard_button] += 1

					self._animate_sequential(widget=view.wizard_button, key='opacity', effect='opacity', duration=1., curve='OutCirc', animations=(
						dict(from_value=1., to_value=.4),
						dict(from_value=.4, to_value=1., finished=animate),
					)).start()
			animate()


def run_init():
	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	controller = MainController()
	# controller._view.setGeometry(850, 50, 480, 640)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
