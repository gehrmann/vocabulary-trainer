#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a list-tab

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.SIG_DFL)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtWidgets

from controllers.abstract import ControllerMixture, HashableQItem, VocabulariesDialog
from controllers.list_settings import ListSettingsController
from controllers.top_widget_mixture import TopWidgetMixture
from helpers.caller import Caller
from helpers.timer import Timer
from models.abstract import ObservableAttrDict  # , UnhashableKeyDict  # , DiffUndoRedo
from models.dictionary import DictdDictionary


class _State(ObservableAttrDict):
	pass


class ListController(ControllerMixture, TopWidgetMixture):
	def __init__(self, parent_view=None, test=False, data_model=None, settings_model=None):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# Models
			""" Current-State-Model for selected view's data"""
			state_model = self.state_model
			state_model.vocabulary = None
			state_model.search = ''

			self._cell_to_pairs_and_pair = dict()  # Relation between views and models
			self._item_to_edit = None

			# """ Undo-Redo-Model saves Current-State-Model with .save() and restores previous saved states with .undo() and .redo() """
			# self.undo_redo_model = DiffUndoRedo(state_model.vocabulary)

			self.data_model = data_model

			self.settings_model = settings_model

			# Views
			self._view = view = self._load_ui('views/list_view.ui')
			if parent_view is not None:
				parent_view.addWidget(view)

			TopWidgetMixture.__init__(self, parent_view=view.top_widget, with_vocabularies_button=True, with_marks_buttons=True)

			view.clear_search_button.clicked.connect(self.__on_clear_search_button_clicked)
			view.search_input.textEdited.connect(self.__on_search_input_edited)
			if hasattr(view.pairs_table.horizontalHeader(), 'setSectionResizeMode'):
				view.pairs_table.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
			else:
				view.pairs_table.horizontalHeader().setResizeMode(QtWidgets.QHeaderView.Stretch)
			# view.pairs_table.resizeEvent = (lambda old_method: (lambda event: (self._on_pairs_table_resized(event), old_method(event))[-1]))(view.pairs_table.resizeEvent)
			view.pairs_table.itemChanged.connect(self.__on_pairs_table_item_changed)
			view.dictionary_button.clicked.connect(self.__on_dictionary_button_clicked)
			view.add_button.clicked.connect(self.__on_add_button_clicked)
			# view.undo_button.clicked.connect(self.__on_undo_button_clicked)
			# view.redo_button.clicked.connect(self.__on_redo_button_clicked)
			view.erase_button.clicked.connect(self.__on_erase_button_clicked)
			view.move_button.clicked.connect(self.__on_move_button_clicked)
			view.copy_button.clicked.connect(self.__on_copy_button_clicked)

			if parent_view is None:
				self._restore_window_geometry()
				view.closeEvent = self._on_close
				view.keyPressEvent = self._on_key_pressed
				view.show()

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)
			data_model.changed.bind(self._on_model_updated)
			state_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)
			self._on_model_updated(data_model)
			self._on_model_updated(state_model)

			if '--test-list' in sys.argv:
				import ipdb; ipdb.set_trace()  # FIXME: must be removed

			# import _helpers
			# _helpers.show_banner('ca-app-pub-9181265559842913/2733995986')

			# import jnius

			# class QtActivity(jnius.JavaClass):
			#     """ [ V=void | I=int | Z=bool | Ljava/lang/String;=str ] """

			#     __metaclass__ = jnius.MetaJavaClass
			#     __javaclass__ = 'org.kde.necessitas.origo.QtActivity'
			# print("QtActivity,", QtActivity, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed

			# class QtBanner(jnius.JavaClass):
			#     """ [ V=void | I=int | Z=bool | Ljava/lang/String;=str ] """

			#     __metaclass__ = jnius.MetaJavaClass
			#     __javaclass__ = 'org.kde.necessitas.origo.QtBanner'
			# print("QtBanner,", QtBanner, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed

			#     # get_date_dialog_value = jnius.jnius.JavaStaticMethod('()[I')
			#     # get_dpi = jnius.jnius.JavaStaticMethod('()I')
			#     # hide_banner = jnius.jnius.JavaStaticMethod('()V')
			#     # show_banner = jnius.jnius.JavaStaticMethod('(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;)V')
			#     # show_confirmation_dialog = jnius.jnius.JavaStaticMethod('(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V')
			#     # show_date_dialog = jnius.jnius.JavaStaticMethod('(IIILjava/lang/String;)V')
			#     # set_input_type = jnius.jnius.JavaStaticMethod('(Ljava/lang/String;)V')
			#     # send_email = jnius.jnius.JavaStaticMethod('(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;')
			#     # show_application_in_market = jnius.jnius.JavaStaticMethod('(Ljava/lang/String;)V')
			#     # start_profiler = jnius.jnius.JavaStaticMethod('(Ljava/lang/String;)V')
			#     # stop_profiler = jnius.jnius.JavaStaticMethod('()V')
			#     # dump_memory = jnius.jnius.JavaStaticMethod('()Ljava/lang/String;')

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# print(self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80], file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		TopWidgetMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		data_model = self.data_model
		state_model = self.state_model
		view = self._view

		if model is settings_model:
			# if current[0] is None or 'list_vocabulary' in current[0] and previous[0] != current[0]:
			#     state_model.vocabulary = data_model.vocabularies.get(settings_model.list_vocabulary, None)

			if current[0] is None or 'list_selected_marks' in current[0] and previous[0] != current[0]:
				state_model.selected_marks = settings_model.list_selected_marks

			if current[0] is None or 'list_font' in current[0] and previous[0] != current[0]:
				if settings_model.list_font is not None:
					from styles import styles
					font = settings_model.list_font
					styles.replace(view.pairs_table, {'* {font: ': self._font_to_style(font)[7:]})

		if model is data_model:
			if current[0] is None or 'vocabularies' in current[0] and previous[:2] != current[:2]:
				state_model.vocabulary = data_model.vocabularies.get(settings_model.list_vocabulary, None)

		if model is state_model:
			# if current[0] is None or 'vocabulary' in current[0] and previous[0] != current[0]:
			#     # if state_model.vocabulary is None:
			#     #     self.undo_redo_model = None
			#     # else:
			#     #     self.undo_redo_model = DiffUndoRedo(state_model.vocabulary)  # Start new UndoRedo-list every time if another vocabulary is selected

			#     if state_model.vocabulary is not None:
			#         settings_model.list_vocabulary = state_model.vocabulary.path if state_model.vocabulary is not None else None

			if current[0] is None or 'vocabulary' in current[0] and previous[:3] != current[:3]:
				# Updates list after delay in order to avoid freeze
				Caller.call_once_after(.5, self._fill)

			if current[0] is None or 'selected_marks' in current[0] and previous[0] != current[0]:
				settings_model.list_selected_marks = state_model.selected_marks

			if current[0] is None or 'search' in current[0] and previous[0] != current[0]:
				# Updates search_input
				if view.search_input.text() != state_model.search:
					view.search_input.setText(state_model.search)

				Caller.call_once_after(.4, self._fill)

		# if state_model.vocabulary is not None:
		#     if model is state_model.vocabulary:
		#         Caller.call_once_after(0, self._fill)

	""" View's event handlers """

	def _on_vocabulary_selected(self, vocabulary):
		self.settings_model.list_vocabulary = vocabulary.path if vocabulary is not None else None

	def __on_menu_pressed(self, event):
		ListSettingsController(settings_model=self.settings_model).show()

	def __on_clear_search_button_clicked(self, event=None):
		self.state_model.search = ''

	def __on_search_input_edited(self, text):
		self.state_model.search = str(text)

	def __on_pairs_table_item_changed(self, cell):

		(pairs, pair) = self._cell_to_pairs_and_pair[HashableQItem(cell)]

		if pair[cell.column()] != cell.text():
			new_pair = pair[:]
			new_pair[cell.column()] = str(cell.text())
			if any(new_pair):
				pairs[pairs.index(pair)] = new_pair  # Replace with new object in order to invoke 'changed'-event
			else:
				pairs.remove(pair)

			# self.undo_redo_model.save()

	def _on_settings_button_clicked(self, event):
		ListSettingsController(settings_model=self.settings_model).show()

	def __on_dictionary_button_clicked(self, checked):
		keywords = next((str(cell.text()) for cell in self._view.pairs_table.selectedItems() if cell.text()), None) or ''

		if keywords:
			self._show_in_dict_tab(keywords)

	def __on_add_button_clicked(self, checked):
		state_model = self.state_model
		vocabulary = state_model.vocabulary

		if vocabulary is not None and not state_model.search:
			if not vocabulary.new[-1:] or any(vocabulary.new[-1]):
				vocabulary.new.append(['', ''])
			else:
				Caller.call_once_after(0, self._fill)
			self._item_to_edit = vocabulary.new[-1]

	# def __on_undo_button_clicked(self, checked):
	#     if self.undo_redo_model is not None:
	#         self.undo_redo_model.undo()

	# def __on_redo_button_clicked(self, checked):
	#     if self.undo_redo_model is not None:
	#         self.undo_redo_model.redo()

	def __on_erase_button_clicked(self, checked):
		result = QtWidgets.QMessageBox.question(QtWidgets.qApp.activeWindow(), 'Confirmation needed', 'Clear selected cells?', QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel, QtWidgets.QMessageBox.Cancel)
		if result == QtWidgets.QMessageBox.Yes:

			# Gathers pairs which fields are being erased
			row_to_data = dict()
			for cell in self._view.pairs_table.selectedItems():
				(pairs, pair) = self._cell_to_pairs_and_pair[HashableQItem(cell)]
				row_to_data.setdefault(cell.row(), (pairs, pair, [pair[0], pair[1]]))[2][cell.column()] = ''

			# Updates/removes changed pairs
			for pairs, pair, (new_left_field, new_right_field) in row_to_data.values():
				if new_left_field or new_right_field:
					pairs[pairs.index(pair)] = [new_left_field, new_right_field]
				else:
					pairs.remove(pair)

			# self.undo_redo_model.save()

	def __on_move_button_clicked(self, checked):
		def on_vocabulary_selected(another_vocabulary):
			for cell in self._view.pairs_table.selectedItems():
				(pairs, pair) = self._cell_to_pairs_and_pair[HashableQItem(cell)]

				if another_vocabulary is not None:
					if pair not in another_vocabulary.new:
						if pair in pairs:
							pairs.remove(pair)
						another_vocabulary.new.insert(0, pair)

			# self.undo_redo_model.save()

		def on_vocabulary_renamed(item, name):
			item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

		VocabulariesDialog(
			headers=['Vocabulary'],
			settings_model=self.settings_model,
			data_model=self.data_model.vocabularies,
			path_attribute='path',
			path_prefix='vocabularies/',
			is_expanded=True,
			is_container_selectable=False,
			has_icons=False,
			selected=on_vocabulary_selected,
			renamed=on_vocabulary_renamed,
		).show()

	def __on_copy_button_clicked(self, checked):
		def on_vocabulary_selected(another_vocabulary):
			for cell in self._view.pairs_table.selectedItems():
				(pairs, pair) = self._cell_to_pairs_and_pair[HashableQItem(cell)]

				if another_vocabulary is not None:
					if pair not in another_vocabulary.new:
						another_vocabulary.new.insert(0, pair[:])

			# self.undo_redo_model.save()

		def on_vocabulary_renamed(item, name):
			item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

		VocabulariesDialog(
			headers=['Vocabulary'],
			settings_model=self.settings_model,
			data_model=self.data_model.vocabularies,
			path_attribute='path',
			path_prefix='vocabularies/',
			is_expanded=True,
			is_container_selectable=False,
			has_icons=False,
			selected=on_vocabulary_selected,
			renamed=on_vocabulary_renamed,
		).show()

	""" Helpers """

	def _fill(self):
		TopWidgetMixture._fill(self)

		state_model = self.state_model
		vocabulary = state_model.vocabulary
		normalized_search = DictdDictionary.normalize(state_model.search)
		view = self._view
		pairs_table = view.pairs_table

		self._cell_to_pairs_and_pair.clear()

		pairs_table.clear()
		pairs_table.setRowCount(sum(len(vocabulary[mark]) for mark in state_model.selected_marks) if state_model.selected_marks and vocabulary is not None else 0)
		pairs_table.setColumnCount(2)
		pairs_table.setHorizontalHeaderLabels(['◀ a-z', 'a-z ▶'])

		entry_to_edit = None
		if vocabulary is not None:
			# t = time.time()

			QTableWidgetItem = QtWidgets.QTableWidgetItem
			cell_to_pairs_and_pair = self._cell_to_pairs_and_pair
			pairs_table_setItem = pairs_table.setItem
			pairs_table_item = pairs_table.item
			DictdDictionary_normalize = DictdDictionary.normalize
			row_index = 0
			pairs_table.setSortingEnabled(False)
			pairs_table.itemChanged.disconnect(self.__on_pairs_table_item_changed)  # Temporarily unbind during bulk-insertion
			for mark in state_model.selected_marks:
				pairs = vocabulary[mark]
				for pair in reversed(pairs):
					if normalized_search in DictdDictionary_normalize(pair[0]) or normalized_search in DictdDictionary_normalize(pair[1]):
						pairs_table_setItem(row_index, 0, QTableWidgetItem(pair[0]))
						pairs_table_setItem(row_index, 1, QTableWidgetItem(pair[1]))
						cell_to_pairs_and_pair[HashableQItem(pairs_table_item(row_index, 0))] = (pairs, pair)
						cell_to_pairs_and_pair[HashableQItem(pairs_table_item(row_index, 1))] = (pairs, pair)

						if self._item_to_edit is not None and self._item_to_edit == pair:
							self._item_to_edit = None
							entry_to_edit = pairs_table.item(row_index, 0)

						row_index += 1
			pairs_table.setRowCount(row_index)  # Decreases number of lines
			# pairs_table.setSortingEnabled(True)
			pairs_table.itemChanged.connect(self.__on_pairs_table_item_changed)  # Bind again

			# print("time.time() - t,", time.time() - t, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed

		if entry_to_edit is not None:
			pairs_table.editItem(entry_to_edit)

	def _test_google_drive(self):
		"""Code for Google Drive"""

		code = None

		import pydrive.auth
		import pydrive.drive

		# Authenticate the client.
		google_auth = pydrive.auth.GoogleAuth()
		if code is None:
			import oauth2client._helpers
			import oauth2client.tools
			import six.moves
			import socket
			import webbrowser

			def _get_code():
				state = _State()
				state.query_params = None

				class ClientRedirectHandler(six.moves.BaseHTTPServer.BaseHTTPRequestHandler):
					"""A handler for OAuth 2.0 redirects back to localhost.

					Waits for a single request and parses the query parameters
					into the servers query_params and then stops serving.
					"""

					def do_GET(self):
						"""Handle a GET request.

						Parses the query parameters and prints a message
						if the flow has completed. Note that we can't detect
						if an error occurred.
						"""
						self.send_response(six.moves.http_client.OK)
						self.send_header('Content-type', 'text/html')
						self.end_headers()
						parts = six.moves.urllib.parse.urlparse(self.path)
						query = oauth2client._helpers.parse_unique_urlencoded(parts.query)
						# self.server.query_params = query
						state.query_params = query
						self.wfile.write(b'<html><head><title>Authentication Status</title></head>')
						self.wfile.write(b'<body><p>The authentication flow has completed!</p>')
						self.wfile.write(b'</body></html>')

					def log_message(self, format, *args):
						"""Do not log messages to stdout while running as cmd. line program."""

				host = 'localhost'
				for port in range(8080, 8090):
					try:
						httpd_server = oauth2client.tools.ClientRedirectServer((host, port), ClientRedirectHandler)
					except socket.error as e:
						pass
					else:
						authorize_url = google_auth.GetAuthUrl()
						google_auth.flow.redirect_uri = 'http://%s:%s/' % (host, port)
						print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'authorize_url=', authorize_url, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

						# Opens url with authorization form in browser
						try:
							result = self.java.call('open_url', url=authorize_url, ignore_wrong_platform_exception=False)
						except self.WrongPlatformException:
							status = webbrowser.open(authorize_url, new=1, autoraise=True)
							if not status:
								raise Exception('Can not open browser')
						else:
							if 'error' in result:
								raise Exception('Error: {}'.format(result['error']))
						print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'Browser was opened', file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

						# Waits for result
						for attempt in range(10):
							print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'TRYING...', file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
							httpd_server.handle_request()
							# if not httpd_server.query_params:  # Got empty params (not selected up till now or browser/tab was closed)
							if not state.query_params:  # Got empty params (not selected up till now or browser/tab was closed)
								continue
							break
						# if 'error' in httpd_server.query_params:
						if 'error' in state.query_params:
							raise Exception('User rejected authentication')
						# if 'code' not in httpd_server.query_params:
						if 'code' not in state.query_params:
							raise Exception('No code found in redirect')

						# return httpd_server.query_params['code']
						return state.query_params['code']
				else:
					raise Exception('Failed to start a local web server.')
			# code = google_auth.LocalWebserverAuth()
			code = _get_code()
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'google_auth=', google_auth, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		google_auth.Auth(code)
		google_drive = pydrive.drive.GoogleDrive(google_auth)

		# Create a file, set content, and upload.
		test_file = google_drive.CreateFile()
		test_file_content = 'Generic, non-exhaustive\n ASCII test string.'
		test_file.SetContentString(test_file_content)
		# {'convert': True} triggers conversion to a Google Drive document.
		test_file.Upload({'convert': True})

		# Download the file.
		test_file_2 = google_drive.CreateFile({'id': test_file['id']})

		# Print content before download.
		print('Original text:')
		print(bytes(test_file_content.encode('unicode-escape')))
		print('Number of chars: %d' % len(test_file_content))
		print('')
		# Original text:
		# Generic, non-exhaustive\n ASCII test string.
		# Number of chars: 43

		# Download document as text file WITH the BOM and print the contents.
		content_with_bom = test_file_2.GetContentString(mimetype='text/plain')
		print('Content with BOM:')
		print(bytes(content_with_bom.encode('unicode-escape')))
		print('Number of chars: %d' % len(content_with_bom))
		print('')
		# Content with BOM:
		# \ufeffGeneric, non-exhaustive\r\n ASCII test string.
		# Number of chars: 45

		# Download document as text file WITHOUT the BOM and print the contents.
		content_without_bom = test_file_2.GetContentString(mimetype='text/plain', remove_bom=True)
		print('Content without BOM:')
		print(bytes(content_without_bom.encode('unicode-escape')))
		print('Number of chars: %d' % len(content_without_bom))
		print('')
		# Content without BOM:
		# Generic, non-exhaustive\r\n ASCII test string.
		# Number of chars: 44

		# *NOTE*: When downloading a Google Drive document as text file, line-endings
		# are converted to the Windows-style: \r\n.

		# Delete the file as necessary.
		test_file.Delete()


def run_init():
	from models.abstract import ObservableAttrDict, ObservableSet

	class Vocabularies(ObservableSet):
		def get(self, key, default=None):
			return self[key]

		def __getitem__(self, key):
			# if key.__class__ in (str, unicode):
			if key.__class__ in (str, ):
				return next((x for x in self if x._path == key), None)
			raise KeyError('Vocabulary with path="{}" does not exist'.format(key))
			# return super(_Vocabularies, self).__getitem__(key)

		def __contains__(self, key):
			if key.__class__ == int and len(self) > key:
				return True
			return super(Vocabularies, self).__contains__(key)

	class Vocabulary(ObservableAttrDict):
		def __init__(self, name, *args, **kwargs):
			self._name = name
			self._path = name
			super(Vocabulary, self).__init__(*args, **kwargs)

		def __str__(self):
			return self._name

		@property
		def path(self):
			return 'root/subdirectory/' + self._name

	data_model = ObservableAttrDict(
		vocabularies=Vocabularies([
			Vocabulary('Körperteile', new=[['Glied', 'конечность'], ['Gelenk', 'сустав'], ['Knochen, Bein', 'кость'], ['Haut', 'кожа'], ['Nagel', 'ноготь'], ['Stirn', 'лоб']], good=[], fair=[], bad=[]),
		]),
	)
	settings_model = ObservableAttrDict(
		list_vocabulary='Körperteile',
		list_font=None,
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles; styles  # Load once the styles for QApplication

	controller = ListController(data_model=data_model, settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
