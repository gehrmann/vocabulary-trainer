#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a speech-tab

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import itertools
import logging
import os
import random
import re
import signal
import string
import threading
import sys
import time

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtWidgets, uic

from controllers.abstract import ControllerMixture, Network, BackgroundsCategoriesDialog
from controllers.speech_settings import SpeechSettingsController
from controllers.top_widget_mixture import TopWidgetMixture
from helpers.caller import Caller
from helpers.timer import Timer
from models.abstract import ObservableAttrDict, ObservableSet


class _State(ObservableAttrDict):
	pass


class SpeechController(ControllerMixture, TopWidgetMixture):
	def __init__(self, parent_view=None, test=False, data_model=None, settings_model=None):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# Models
			""" Current-State-Model for selected view's data"""
			state_model = self.state_model
			state_model.vocabulary = None
			state_model.is_playing = False
			state_model.locales = []
			state_model.play_generator = None
			state_model.current_value = None
			self._pairs = []

			self.data_model = data_model

			self.settings_model = settings_model

			# Views
			self._view = view = self._load_ui('views/speech_view.ui')
			if parent_view is not None:
				parent_view.addWidget(view)

			TopWidgetMixture.__init__(self, parent_view=view.top_widget, with_vocabularies_button=True, with_marks_buttons=True, with_reload_button=True)
			self.top_widget.reload_button.clicked.connect(self.__on_reload_button_clicked)

			view.left_field_locale_dropdown.activated.connect(self.__on_left_field_locale_dropdown_changed)
			view.right_field_locale_dropdown.activated.connect(self.__on_right_field_locale_dropdown_changed)
			view.play_button.toggled.connect(self.__on_play_button_toggled)

			if parent_view is None:
				self._restore_window_geometry()
				view.closeEvent = self._on_close
				view.keyPressEvent = self._on_key_pressed
				view.show()

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)
			data_model.changed.bind(self._on_model_updated)
			state_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)
			self._on_model_updated(data_model)
			self._on_model_updated(state_model)

			if '--test-speech' in sys.argv:
				import ipdb; ipdb.set_trace()  # FIXME: must be removed

			Caller.call_once_after(.5, self._fill_locales)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), self.__class__, object.__repr__(model), previous and previous, current and current, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# print(self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80], file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		TopWidgetMixture._on_model_updated(self, model=model, previous=previous, current=current)

		data_model = self.data_model
		state_model = self.state_model
		settings_model = self.settings_model
		view = self._view
		top_widget = self.top_widget

		if model is settings_model:
			if current[0] is None or 'speech_selected_marks' in current[0] and previous[0] != current[0]:
				state_model.selected_marks = settings_model.speech_selected_marks

			if current[0] is None or 'speech_font' in current[0] and previous[0] != current[0]:
				if settings_model.speech_font is not None:
					from styles import styles
					font = settings_model.speech_font
					styles.replace(view.left_field_label, {'* {font: ': self._font_to_style(font)[7:]})
					styles.replace(view.right_field_label, {'* {font: ': self._font_to_style(font)[7:]})

			if current[0] is None or 'speech_pairs_count' in current[0] and previous[0] != current[0]:
				if settings_model.speech_pairs_count != len(self._pairs):
					Caller.call_once_after(0, self._fill)

			if current[0] is None or 'speech_left_field_locale' in current[0] and previous[0] != current[0]:
				view.left_field_locale_dropdown.setCurrentIndex(view.left_field_locale_dropdown.findText(str(settings_model.speech_left_field_locale)))

			if current[0] is None or 'speech_right_field_locale' in current[0] and previous[0] != current[0]:
				view.right_field_locale_dropdown.setCurrentIndex(view.right_field_locale_dropdown.findText(str(settings_model.speech_right_field_locale)))

		if model is data_model:
			if current[0] is None or 'vocabularies' in current[0] and previous[:2] != current[:2]:
				state_model.vocabulary = data_model.vocabularies.get(settings_model.speech_vocabulary, None)

		if model is state_model:
			if current[0] is None or 'selected_marks' in current[0] and previous[0] != current[0]:
				settings_model.speech_selected_marks = state_model.selected_marks

			if current[0] is None or 'locales' in current[0] and previous[0] != current[0]:
				# view.left_field_locale_dropdown.activated.disconnect(self.__on_left_field_locale_dropdown_changed)  # Temporarily unbind during bulk-insertion
				view.left_field_locale_dropdown.clear()
				view.left_field_locale_dropdown.addItems(state_model.locales)
				view.left_field_locale_dropdown.setCurrentIndex(view.left_field_locale_dropdown.findText(str(settings_model.speech_left_field_locale)))
				# view.left_field_locale_dropdown.activated.connect(self.__on_left_field_locale_dropdown_changed)  # Temporarily unbind during bulk-insertion

				view.right_field_locale_dropdown.clear()
				view.right_field_locale_dropdown.addItems(state_model.locales)
				view.right_field_locale_dropdown.setCurrentIndex(view.right_field_locale_dropdown.findText(str(settings_model.speech_right_field_locale)))

			if current[0] is None or 'current_value' in current[0] and previous[0] != current[0]:
				if state_model.current_value is None:
					view.left_field_label.setText('')
					view.right_field_label.setText('')

				elif state_model.current_value.__class__ in (tuple, list):
					pair, text, locale = state_model.current_value
					view.left_field_label.setText(pair[0])
					view.right_field_label.setText(pair[1])

			if current[0] is None or 'is_playing' in current[0] and previous[0] != current[0]:
				if not state_model.is_playing:
					# Initializes and stops/flushes Text-to-speech-queue
					Caller.call_never(self._play)
					if not self._check_if_text_to_speech_is_ready():
						self._say(text='', locale='en', flush=True)

				else:
					# Caller.call_once_after(0, self._fill)
					Caller.call_once_after(0, self._play)

					Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Action Play ' + self.__class__.__name__))

			if current[0] is None or 'play_generator' in current[0] and previous[0] != current[0]:
				view.play_button.setEnabled(state_model.play_generator is not None)

	""" View's event handlers """

	def _on_vocabulary_selected(self, vocabulary):
		self.settings_model.speech_vocabulary = vocabulary.path if vocabulary is not None else None

	def __on_menu_pressed(self, event):
		SpeechSettingsController(settings_model=self.settings_model).show()

	def _on_settings_button_clicked(self, event):
		SpeechSettingsController(settings_model=self.settings_model).show()

	def __on_reload_button_clicked(self, event):
		# Stop previous
		if not self._check_if_text_to_speech_is_ready():
			self._say(text='', locale='en', flush=True)

		Caller.call_once_after(0, self._fill)

	def __on_left_field_locale_dropdown_changed(self, current_index):
		self.settings_model.speech_left_field_locale = str(self._view.left_field_locale_dropdown.currentText())

	def __on_right_field_locale_dropdown_changed(self, current_index):
		self.settings_model.speech_right_field_locale = str(self._view.right_field_locale_dropdown.currentText())

	def __on_play_button_toggled(self, event):
		self.state_model.is_playing = self._view.play_button.isChecked()

	""" Helpers """

	def _fill(self):
		# logging.getLogger(__name__).debug('calling _fill()')

		TopWidgetMixture._fill(self)

		state_model = self.state_model
		data_model = self.data_model
		settings_model = self.settings_model
		vocabulary = self.state_model.vocabulary
		view = self._view

		if vocabulary is not None:
			with Timer('fill'):
				pairs = [pair for mark in state_model.selected_marks for pair in vocabulary[mark]]
				self._pairs = pairs = random.sample(pairs, min(len(pairs), settings_model.speech_pairs_count))

				def __get_play_generator():
					for pair in itertools.cycle(self._pairs):
						for index in range(settings_model.speech_repeat_pair_count):
							for field, locale in zip(
									pair,
									[x.split('(')[1].split(')')[0] for x in (settings_model.speech_left_field_locale, settings_model.speech_right_field_locale)],
							):
								yield (pair, field, locale)
								yield settings_model.speech_pause_between_fields
						yield max(0, settings_model.speech_pause_between_pairs - settings_model.speech_pause_between_fields)
				state_model.play_generator = play_generator = __get_play_generator() if pairs else None
				state_model.current_value = next(play_generator) if pairs else None

	def _fill_locales(self):
		with Timer('get locales'):
			try:
				# locales = self.java.call('get_locales')
				while True:
					result = self.java.call('get_text_to_speech_languages')
					if 'error' in result:
						logging.getLogger(__name__).debug('Error: %s', result['error'])
						time.sleep(.2)
						continue
					# debug = result.pop('debug', None)
					result.pop('debug', None)
					# logging.getLogger(__name__).debug(debug)
					locales = result
					break

			except self.WrongPlatformException:
				locales = {'ms': 'Malay', 'en_BE': 'English (Belgium)', 'tr': 'Turkish', 'ja': 'Japanese', 'sk': 'Slovak', 'fi': 'Finnish', 'en_BW': 'English (Botswana)', 'eu': 'Basque', 'el': 'Greek', 'en_SG': 'English (Singapore)', 'gl_ES': 'Galician (Spain)', 'th_TH': 'Thai (Thailand)', 'en_BZ': 'English (Belize)', 'ru_UA': 'Russian (Ukraine)', 'es_VE': 'Spanish (Venezuela)', 'nl': 'Dutch', 'pt': 'Portuguese', 'it': 'Italian', 'es_AR': 'Spanish (Argentina)', 'en_NA': 'English (Namibia)', 'sw': 'Swahili', 'zh_HK': 'Chinese (Hong Kong SAR China)', 'hr_HR': 'Croatian (Croatia)', 'fil_PH': 'Filipino (Philippines)', 'pt_BR': 'Portuguese (Brazil)', 'cs_CZ': 'Czech (Czech Republic)', 'tr_TR': 'Turkish (Turkey)', 'en_IE': 'English (Ireland)', 'en_IN': 'English (India)', 'en_RH': 'English (Zimbabwe)', 'fr_LU': 'French (Luxembourg)', 'en_JM': 'English (Jamaica)', 'en_ZA': 'English (South Africa)', 'hi': 'Hindi', 'eu_ES': 'Basque (Spain)', 'ps': 'Pashto', 'am': 'Amharic', 'en_ZW': 'English (Zimbabwe)', 'in_ID': 'Indonesian (Indonesia)', 'da': 'Danish', 'es_UY': 'Spanish (Uruguay)', 'es_PR': 'Spanish (Puerto Rico)', 'sr_ME': 'Serbian (Montenegro)', 'fr_FR': 'French (France)', 'fi_FI': 'Finnish (Finland)', 'sr_RS': 'Serbian (Serbia)', 'es_PY': 'Spanish (Paraguay)', 'nl_NL': 'Dutch (Netherlands)', 'es_PE': 'Spanish (Peru)', 'en_US': 'English (United States,Computer)', 'lv_LV': 'Latvian (Latvia)', 'es_PA': 'Spanish (Panama)', 'sv_SE': 'Swedish (Sweden)', 'ro_RO': 'Romanian (Romania)', 'iw_IL': 'Hebrew (Israel)', 'nb': 'Norwegian Bokm\xe5l', 'es_CO': 'Spanish (Colombia)', 'rm': 'Romansh', 'es_CL': 'Spanish (Chile)', 'es_CR': 'Spanish (Costa Rica)', 'sr_LATN': 'Serbian (Serbia,YU)', 'de_AT': 'German (Austria)', 'ru_RU': 'Russian (Russia)', 'en_CA': 'English (Canada)', 'da_DK': 'Danish (Denmark)', 'lt': 'Lithuanian', 'et': 'Estonian', 'zu': 'Zulu', 'en_TT': 'English (Trinidad and Tobago)', 'gl': 'Galician', 'sr': 'Serbian', 'bg': 'Bulgarian', 'sr_CYRL': 'Serbian (Serbia,YU)', 'pl': 'Polish', 'es': 'Spanish', 'es_HN': 'Spanish (Honduras)', 'zh_MO': 'Chinese (Macau SAR China)', 'ru': 'Russian', 'nb_NO': 'Norwegian Bokm\xe5l (Norway)', 'es_EC': 'Spanish (Ecuador)', 'uk': 'Ukrainian', 'pt_PT': 'Portuguese (Portugal)', 'vi_VN': 'Vietnamese (Vietnam)', 'en_NZ': 'English (New Zealand)', 'ko': 'Korean', 'sr_YU': 'Serbian (Serbia)', 'fr': 'French', 'sr_CS': 'Serbian (Serbia and Montenegro)', 'zh_SG': 'Chinese (Singapore)', 'de_CH': 'German (Switzerland)', 'hu_HU': 'Hungarian (Hungary)', 'ca_ES': 'Catalan (Spain)', 'zh_HANS': 'Chinese (Singapore,SG)', 'hi_IN': 'Hindi (India)', 'zh_HANT': 'Chinese (Taiwan,TW)', 'ja_JP': 'Japanese (Japan)', 'lt_LT': 'Lithuanian (Lithuania)', 'sl_SI': 'Slovenian (Slovenia)', 'es_ES': 'Spanish (Spain)', 'vi': 'Vietnamese', 'en_GB': 'English (United Kingdom)', 'bg_BG': 'Bulgarian (Bulgaria)', 'sl': 'Slovenian', 'ca': 'Catalan', 'es_SV': 'Spanish (El Salvador)', 'en_MH': 'English (Marshall Islands)', 'de': 'German', 'en_VI': 'English (U.S. Virgin Islands)', 'ro': 'Romanian', 'sr_BA': 'Serbian (Bosnia and Herzegovina)', 'zh': 'Chinese', 'es_NI': 'Spanish (Nicaragua)', u'be': 'Belarusian', 'en_MT': 'English (Malta)', 'sv_FI': 'Swedish (Finland)', 'ga': 'Irish', 'hr': 'Croatian', 'es_BO': 'Spanish (Bolivia)', 'zh_CN': 'Chinese (China)', 'de_BE': 'German (Belgium)', 'it_CH': 'Italian (Switzerland)', 'hu': 'Hungarian', 'it_IT': 'Italian (Italy)', 'nl_BE': 'Dutch (Belgium)', 'uk_UA': 'Ukrainian (Ukraine)', 'cs': 'Czech', 'af': 'Afrikaans', 'el_GR': 'Greek (Greece)', 'en_HK': 'English (Hong Kong SAR China)', 'fr_BE': 'French (Belgium)', 'en': 'English', 'ga_IE': 'Irish (Ireland)', 'es_GT': 'Spanish (Guatemala)', 'fil': 'Filipino', 'en_AU': 'English(Australia)', 'fr_MC': 'French (Monaco)', 'lv': 'Latvian', 'th': 'Thai', 'en_PH': 'English (Philippines)', 'en_PK': 'English (Pakistan)', 'zh_TW': 'Chinese (Taiwan)', 'es_US': 'Spanish (United States)', 'ko_KR': 'Korean (South Korea)', 'de_LI': 'German (Liechtenstein)', 'de_LU': 'German (Luxembourg)', 'sv': 'Swedish', 'in': 'Indonesian', 'fr_CH': 'French (Switzerland)', 'de_DE': 'German (Germany)', 'es_MX': 'Spanish (Mexico)', 'fr_CA': 'French (Canada)', 'es_DO': 'Spanish (Dominican Republic)', 'iw': 'Hebrew', 'pl_PL': 'Polish (Poland)', 'sk_SK': 'Slovak (Slovakia)'}

			# locales = sorted(['{1} ({0})'.format(k, v) for k, v in locales.iteritems()])
			# locales = sorted(['{1} ({0})'.format(k, v) for k, v in locales.iteritems() if '_' not in k])
			locales = sorted(['{1} ({0})'.format(k, v) for k, v in locales.items() if '_' not in k])

			self.state_model.locales = locales

	def _play(self):
		state_model = self.state_model
		settings_model = self.settings_model

		if state_model.play_generator is not None:
			value = state_model.current_value

			if not self._check_if_text_to_speech_is_ready():
				Caller.call_once_after(.2, self._play)

			else:
				if value.__class__ in (int, float):
					state_model.current_value = next(state_model.play_generator)
					Caller.call_once_after(value, self._play)

				elif value.__class__ in (tuple, list):
					pair, field, locale = value
					# logging.getLogger(__name__).debug('Text=%s', field)
					field = self._substitute(field, skip_braces=(settings_model.speech_left_field_skip_braces if pair.index(field) == 0 else settings_model.speech_right_field_skip_braces))
					# logging.getLogger(__name__).debug('Text=%s', field)
					self._say(locale=locale, text=field)

					state_model.current_value = next(state_model.play_generator)
					Caller.call_once_after(0, self._play)

	def _check_if_text_to_speech_is_ready(self):
		# Waits until Text-to-speech is ready
		status = True
		try:
			result = self.java.call('get_text_to_speech_state')
			if 'error' in result:
				logging.getLogger(__name__).debug('Error: %s', result['error'])
				status = False

		except self.WrongPlatformException:
			pass

		return status

	def _say(self, locale, text, flush=False):
		# logging.getLogger(__name__).debug('Calling _say(): locale=%s, text=%s, flush=%s', locale, text, flush)

		# Sends langunage and text to Text-to-speech
		def __say():
			with Timer('say'):
				try:
					result = self.java.call('say', locale=locale, text=text, flush=flush)
					if 'error' in result:
						logging.getLogger(__name__).debug('Error: %s', result['error'])

				except self.WrongPlatformException:
					languages_to_voices = {
						'en': dict(name='Joey', rate='x-slow', pause_between_sentences_in_ms=500, pause_between_paragraphs_in_ms=700),
						# 'en': dict(name='Salli', rate='x-slow', pause_between_sentences_in_ms=500, pause_between_paragraphs_in_ms=700),
						# 'de': dict(name='Hans', rate='x-slow', pause_between_sentences_in_ms=500, pause_between_paragraphs_in_ms=700),
						'de': dict(name='Marlene', rate='x-slow', pause_between_sentences_in_ms=500, pause_between_paragraphs_in_ms=700),
						'ru': dict(name='Maxim', rate='slow', pause_between_sentences_in_ms=500, pause_between_paragraphs_in_ms=700),
						# 'ru': dict(name='Tatyana', rate='x-slow', pause_between_sentences_in_ms=500, pause_between_paragraphs_in_ms=700),
					}
					import pyvona
					if not hasattr(self, '_voice'):
						self._voice = pyvona.create_voice("GDNAICTDMLSLU5426OAA", "2qUFTF8ZF9wqy7xoGBY+YXLEu+M2Qqalf/pSrd9m")
					try:
						voice = self._voice
						voice.voice_name = languages_to_voices[locale]['name']
						voice.speech_rate = languages_to_voices[locale]['rate']
					except KeyError as e:
						print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), repr(e), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
					else:
						try:
							voice.speak(text)
						except (pyvona.PyvonaException, Exception) as e:
							print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), repr(e), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		# __say()
		# self.thread = elf.make_breakpoint(threading.Thread(target=send))
		thread = threading.Thread(target=__say)
		thread.setDaemon(True)
		thread.start()

	@staticmethod
	def _split(src):
		if src:

			state = None
			_state = None
			dst = []
			for char in src:
				if char in string.whitespace or char in string.punctuation:
					_state = _state
				else:
					try:
						char.encode('iso-8859-1')
						_state = 'de'
					except UnicodeEncodeError:
						_state = 'ru'
				# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "char,", char, _state, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

				if state is None or state == _state:
					dst.append(char)
				else:
					if dst:
						# sys.stderr.write('[2=>' + ''.join(dst) + ']'); sys.stderr.flush()  # FIXME: must be removed/commented
						yield [state, ''.join(dst)]
					dst[:] = [char]
				state = _state
			if dst:
				# sys.stderr.write('[2=>' + ''.join(dst) + ']'); sys.stderr.flush()  # FIXME: must be removed/commented
				yield [state, ''.join(dst)]

	@staticmethod
	def _substitute(field, skip_braces=False):

		# Unfolds ...(...|...)...
		def _expand_parentheses(sentence):
			matches = re.match(r'([^(]*)\(([^)]*)\)(.*)', sentence)
			if matches is not None and (matches.group(1) or matches.group(3)):
				yield [matches.group(1)]
				yield (('' if '|' in matches.group(2) else '|') + matches.group(2)).split('|')
				for x in _expand_parentheses(matches.group(3)):
					yield x
			else:
				yield [sentence]
		field = ' '.join([' / '.join([''.join(xx) for xx in itertools.product(*_expand_parentheses(x))]) for x in field.split(' ')])

		# Makes replacements
		for match, substitute in (
				[
					(r'\bA\b', r'Akkusativ'),
					(r'\bD\b', r'Dativ'),
					(r'\bDA\b', r'Dativ Akkusativ'),
					(r'vor \bJ\b', r'vor jemandem'),
					(r'vor \bE\b', r'vor etwas'),
				] +
				([
					(r'\([^)]+\.\)', r' '),
					(r'\([^)]+\)', r' '),
				] if skip_braces else [])
				# # (r'\([^)]+\.\)', r'(....)'),
				# # (r'\([^)]+\)', r'(...)'),
				# # (r'(\w+) \((\w+), ge-en\)', r'\1 (\2. ge\1)'),
				# # ¬ u00ac
				# #  ͡ u0361
		):
			field = re.sub(match, substitute, field)

		return field


def run_init():
	app = QtWidgets.QApplication(sys.argv)
	from styles import Styles  # Load once the styles for QApplication

	from models.abstract import ObservableAttrDict
	settings_model = ObservableAttrDict(
		speech_font=None,
		speech_pairs_count=5,
		speech_left_field_locale='en',
		speech_right_field_locale='ru',
		speech_vocabulary=None,
	)

	class Vocabulary(ObservableAttrDict):
		def __init__(self, name, *args, **kwargs):
			self._name = name
			super(Vocabulary, self).__init__(*args, **kwargs)

		def __str__(self):
			return self._name

		@property
		def path(self):
			return 'root/subdirectory/' + self._name

	data_model = ObservableAttrDict(
		vocabularies={
			0: Vocabulary('Körperteile', new=[['Glied', 'конечность'], ['Gelenk', 'сустав'], ['Knochen, Bein', 'кость'], ['Haut', 'кожа'], ['Nagel', 'ноготь'], ['Stirn', 'лоб']], good=[], fair=[], bad=[]),
		},
	)

	controller = SpeechController(data_model=data_model, settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def run_split():
	src = 'быть раскрытым, дозволенным, ABC, не закрытым (перед J)'
	for line in SpeechController._split(src):
		print(line)


def run_substitute():
	# src = 'под(рас|за)крыва(x|y)ть (рас|за)крывать под(рас|за), быть раскрытым (auf A), дозволенным, ABC, не закрытым (тест) (перен.) (vor J) rufen (rief, ge-en)'
	src = '(abc)def под(рас|за)крывать (рас|за)крывать под(рас|за), быть раскрытым (auf A), дозволенным, ABC, не закрытым (тест) (перен.) (vor J) rufen (rief, ge-en)'
	# src = 'aaa(111|222|333)bbb(444|555)ccc(666)ddd xxxyyyzzz'
	# src = 'a(1|2|3)b'
	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'src=', src, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
	dst = SpeechController._substitute(src)
	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'dst=', dst, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
	# import re
	# for src in 'под(рас|за)крыва(x|y)ть (рас|за)крывать под(рас|за)'.split(' '):
	#     matches = re.match(r'^' r'([^(]*)?' + r'(\([^)]*\))?' + r'(.*)?$', src)
	#     print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), '', file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
	#     # print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'matches.groups()=', matches.groups(), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
	#     # print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'matches.group(0)=', matches.group(0), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
	#     # print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'matches.group(1)=', matches.group(1), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
	#     # print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'matches.group(2)=', matches.group(2), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
	#     for index, group in enumerate(matches.groups()):
	#         print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), index, group, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
