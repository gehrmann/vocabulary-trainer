#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a flashcards-tab's settings

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtWidgets, uic

from helpers.timer import Timer
from controllers.abstract import ControllerMixture, CommonSettingsMixture, Network


class FlashSettingsController(ControllerMixture, CommonSettingsMixture):
	def __init__(self, settings_model):
		with Timer('initializing'):
			ControllerMixture.__init__(self)
			CommonSettingsMixture.__init__(self)

			# Models
			self.settings_model = settings_model

			# Views
			self._view = view = self._load_ui('views/flash_settings_widget.ui')
			self.dialog.settings_widget.layout().addWidget(view)

			view.repeat_bad_groupbox.toggled.connect(self.__on_repeat_bad_groupbox_changed)
			view.repeat_bad_from_dropdown.editTextChanged.connect(self.__on_repeat_bad_from_dropdown_changed)
			view.repeat_bad_to_dropdown.editTextChanged.connect(self.__on_repeat_bad_to_dropdown_changed)
			view.repeat_fair_groupbox.toggled.connect(self.__on_repeat_fair_groupbox_changed)
			view.repeat_fair_from_dropdown.editTextChanged.connect(self.__on_repeat_fair_from_dropdown_changed)
			view.repeat_fair_to_dropdown.editTextChanged.connect(self.__on_repeat_fair_to_dropdown_changed)
			view.repeat_good_groupbox.toggled.connect(self.__on_repeat_good_groupbox_changed)
			view.repeat_good_from_dropdown.editTextChanged.connect(self.__on_repeat_good_from_dropdown_changed)
			view.repeat_good_to_dropdown.editTextChanged.connect(self.__on_repeat_good_to_dropdown_changed)
			view.font_button.clicked.connect(self.__on_font_button_clicked)
			view.card_background_color_button.clicked.connect(self.__on_card_background_color_button_clicked)
			view.hidden_card_background_color_button.clicked.connect(self.__on_hidden_card_background_color_button_clicked)

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Show Dialog ' + self.__class__.__name__))

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		CommonSettingsMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		view = self._view

		if model is settings_model:
			if current[0] is None or 'flash_repeat_bad' in current[0] and previous[0] != current[0]:
				view.repeat_bad_groupbox.setChecked(settings_model.flash_repeat_bad or False)
				# if settings_model.flash_repeat_bad:
				#     self.__on_repeat_bad_from_dropdown_changed()
				#     self.__on_repeat_bad_to_dropdown_changed()

			if current[0] is None or 'flash_repeat_bad_from' in current[0] and previous[0] != current[0]:
				view.repeat_bad_from_dropdown.setEditText(str(settings_model.flash_repeat_bad_from or ''))

			if current[0] is None or 'flash_repeat_bad_to' in current[0] and previous[0] != current[0]:
				view.repeat_bad_to_dropdown.setEditText(str(settings_model.flash_repeat_bad_to or ''))

			if current[0] is None or 'flash_repeat_fair' in current[0] and previous[0] != current[0]:
				view.repeat_fair_groupbox.setChecked(settings_model.flash_repeat_fair or False)
				# if settings_model.flash_repeat_fair:
				#     self.__on_repeat_fair_from_dropdown_changed()
				#     self.__on_repeat_fair_to_dropdown_changed()

			if current[0] is None or 'flash_repeat_fair_from' in current[0] and previous[0] != current[0]:
				view.repeat_fair_from_dropdown.setEditText(str(settings_model.flash_repeat_fair_from or ''))

			if current[0] is None or 'flash_repeat_fair_to' in current[0] and previous[0] != current[0]:
				view.repeat_fair_to_dropdown.setEditText(str(settings_model.flash_repeat_fair_to or ''))

			if current[0] is None or 'flash_repeat_good' in current[0] and previous[0] != current[0]:
				view.repeat_good_groupbox.setChecked(settings_model.flash_repeat_good or False)
				# if settings_model.flash_repeat_good:
				#     self.__on_repeat_good_from_dropdown_changed()
				#     self.__on_repeat_good_to_dropdown_changed()

			if current[0] is None or 'flash_repeat_good_from' in current[0] and previous[0] != current[0]:
				view.repeat_good_from_dropdown.setEditText(str(settings_model.flash_repeat_good_from or ''))

			if current[0] is None or 'flash_repeat_good_to' in current[0] and previous[0] != current[0]:
				view.repeat_good_to_dropdown.setEditText(str(settings_model.flash_repeat_good_to or ''))

			if current[0] is None or 'flash_font' in current[0] and previous[0] != current[0]:
				view.font_button.setText(self._font_to_text(settings_model.flash_font))

			if current[0] is None or 'flash_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.card_background_color_button, {'* {background-color: ': '{};}}'.format(settings_model.flash_card_background_color)})

			if current[0] is None or 'flash_hidden_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.hidden_card_background_color_button, {'* {background-color: ': '{};}}'.format(settings_model.flash_hidden_card_background_color)})

	""" View's event handlers """

	def __on_repeat_bad_groupbox_changed(self):
		self.settings_model.flash_repeat_bad = self._view.repeat_bad_groupbox.isChecked()

	def __on_repeat_bad_from_dropdown_changed(self, value=None):
		self.settings_model.flash_repeat_bad_from = self._parse_int(self._view.repeat_bad_from_dropdown.currentText(), default=0)

	def __on_repeat_bad_to_dropdown_changed(self, value=None):
		self.settings_model.flash_repeat_bad_to = self._parse_int(self._view.repeat_bad_to_dropdown.currentText(), default=0)

	def __on_repeat_fair_groupbox_changed(self):
		self.settings_model.flash_repeat_fair = self._view.repeat_fair_groupbox.isChecked()

	def __on_repeat_fair_from_dropdown_changed(self, value=None):
		self.settings_model.flash_repeat_fair_from = self._parse_int(self._view.repeat_fair_from_dropdown.currentText(), default=0)

	def __on_repeat_fair_to_dropdown_changed(self, value=None):
		self.settings_model.flash_repeat_fair_to = self._parse_int(self._view.repeat_fair_to_dropdown.currentText(), default=0)

	def __on_repeat_good_groupbox_changed(self):
		self.settings_model.flash_repeat_good = self._view.repeat_good_groupbox.isChecked()

	def __on_repeat_good_from_dropdown_changed(self, value=None):
		self.settings_model.flash_repeat_good_from = self._parse_int(self._view.repeat_good_from_dropdown.currentText(), default=0)

	def __on_repeat_good_to_dropdown_changed(self, value=None):
		self.settings_model.flash_repeat_good_to = self._parse_int(self._view.repeat_good_to_dropdown.currentText(), default=0)

	def __on_font_button_clicked(self):
		self._show_font_dialog(model=self.settings_model, key='flash_font')

	def __on_card_background_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='flash_card_background_color')

	def __on_hidden_card_background_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='flash_hidden_card_background_color')


def run_init():
	from models.abstract import ObservableAttrDict

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	settings_model = ObservableAttrDict(
		font=dict(family='DejaVu Sans', size=.75 * styles.mm2px(2.6), weight=12, italic=False),
		button_size=6,
		close_on_double_back=True,
		current_tab=0,

		flash_vocabulary=None,
		flash_font=dict(family='Droid Sans', size=.75 * styles.mm2px(3.6), weight=50, italic=False),
		flash_card_background_color='#ddd',
		flash_hidden_card_background_color='#bbb',
		flash_repeat_bad=False,
		flash_repeat_bad_from=None,
		flash_repeat_bad_to=None,
		flash_repeat_fair=False,
		flash_repeat_fair_from=None,
		flash_repeat_fair_to=None,
		flash_repeat_good=False,
		flash_repeat_good_from=None,
		flash_repeat_good_to=None,
	)

	controller = FlashSettingsController(settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)
	controller.show()


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
