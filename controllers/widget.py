#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a widget (on android desktop)

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import random
import signal
import sys
import textwrap
import time

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from helpers.timer import Timer
from models.data import _Vocabularies


class WidgetController(object):
	def __init__(self, id, width, height, mtime_path='.last_trained'):
		import PIL.Image
		import PIL.ImageFont
		import PIL.ImageDraw

		with Timer('drawing shield'):
			# default_font = PIL.ImageFont.load_default()
			# background_color = (0, 108, 0, 127)
			# background_color = (0, 0, 0, 0)
			background_color = (0, 32, 0, 192)
			default_font = PIL.ImageFont.truetype(self._get_font_path('DroidSans.ttf', 'FreeSans.ttf'), 20)
			# shield_background_color = (0, 64, 0, 192)
			days_count_font = PIL.ImageFont.truetype(self._get_font_path('DroidSans.ttf', 'FreeSans.ttf'), 60)
			# card_background_color = (0, 32, 0, 192)
			left_field_color = '#fee'
			left_field_font = PIL.ImageFont.truetype(self._get_font_path('DroidSans-Bold.ttf', 'FreeSansBold.ttf'), 26)
			right_field_color = '#ff6'

			self._image = image = PIL.Image.new('RGBA', (2 * width, 2 * height), background_color)
			drawer = PIL.ImageDraw.Draw(image)

			# Draws shield 'hours without training' on right bottom corner
			try:
				time_delta = time.time() - os.path.getmtime(mtime_path)
			except OSError:
				shield_width, shield_height, hours_count = 0, 0, None
			else:
				margin = 10
				hours_count = int(time_delta // 3600)
				hours_color = '#f00' if hours_count > 24 else '#090'
				data = [
					dict(text='Training'.upper()),
					dict(text='free for'.upper()),
					dict(text='{}'.format(hours_count).upper(), font=days_count_font, color=hours_color, margin_top=-2, margin=6),
					# dict(text='{}'.format(hours_count).upper(), font=days_count_font, color=random.choice(['#f00', '#090']), margin_top=-2, margin=6),
					dict(text='hours'.upper()),
				]
				data, max_width, max_height = self._prepare_centered_text(data, font=default_font)
				shield_width, shield_height = max_width + 2 * margin, max_height + 2 * margin
				x = image.size[0] - max_width - margin
				y = image.size[1] - max_height - margin
				# # Draws green rectangle
				# drawer.rectangle((x - margin + 5, y - margin + 5, image.size[0] - 5, image.size[1] - 5), fill=shield_background_color)
				# Draws text
				self._draw_centered_text(drawer, x, y, width=max_width, height=max_height, data=data, font=default_font, color='#fff')

		# Draws card
		with Timer('drawing card'):
			card_width, card_height = (image.size[0] - shield_width), image.size[1]
			# Shows card only if enough place for it
			if card_width > shield_width:
				vocabularies = _Vocabularies()
				try:
					# Bad pairs if there are some, or new pairs if there are some
					pairs = random.choice([x.bad for x in vocabularies if x.bad] or [x.new for x in vocabularies if x.new])
				except IndexError:
					logging.getLogger(__name__).info('No bad or new pairs')
					# Shows white text 'nothing to learn'
					margin = 20
					data = [dict(text='Nothing to learn'.upper())]
					data, max_width, max_height = self._prepare_centered_text(data, font=default_font, width=card_width - 2 * margin)
					x = (card_width - max_width) // 2
					y = (card_height - max_height) // 2
					self._draw_centered_text(drawer, x, y, width=max_width, height=max_height, data=data, font=default_font, color='#fff')
				else:
					left_field, right_field = random.choice(pairs)
					# # Draws gray rectangle
					# margin = 5
					# drawer.rectangle((margin, margin, card_width, image.size[1] - margin), fill=card_background_color)
					# Draws black horizontal line
					drawer.line((0, card_height // 2, card_width, card_height // 2), '#000')

					# Writes left_field, right_field
					margin = 20
					for offset_y, data in (
							(0, [dict(text=left_field.upper(), font=left_field_font, color=left_field_color)]),
							(card_height // 2, [dict(text=right_field.upper(), color=right_field_color)]),
					):
						data, max_width, max_height = self._prepare_centered_text(data, font=default_font, width=card_width - 2 * margin)
						x = (card_width - max_width) // 2
						y = (card_height // 2 - max_height) // 2 + offset_y
						# # Draws gray rectangle
						# drawer.rectangle((x, y, x + max_width, y + max_height), fill=(64, 32, 0, 192))
						# Draws white left_field
						self._draw_centered_text(drawer, x, y, width=max_width, height=max_height, data=data, font=default_font, color='#fff')
		logging.getLogger(__name__).warning('Widget with size=(%s, %s) is ready: hours=%s', width, height, hours_count)

	""" Helpers """

	def show(self):
		self._image.show()

	def save(self, path):
		logging.getLogger(__name__).debug('Saving image to %s', path)
		self._image.save(path, 'PNG')

	def _prepare_centered_text(self, data, font, width=99999):
		"""Wraps text by width, calculates size for each line, returns needed size, modifies data"""
		dst_data = []
		for value in data:
			value['width'], value['height'] = value.get('font', font).getsize(value['text'])
			if value['width'] <= width:
				dst_data.append(value)
			else:
				previous_value = value
				for text in textwrap.wrap(value['text'], width=int(len(value['text']) * width / value['width'])):
					value = dict(**previous_value)
					value['text'] = text
					value['width'], value['height'] = value.get('font', font).getsize(value['text'])
					dst_data.append(value)
		max_width, max_height = max(x['width'] for x in dst_data), sum(x['height'] + x.get('margin_top', 0) + x.get('margin', 0) for x in dst_data)
		return dst_data, max_width, max_height

	def _draw_centered_text(self, drawer, x, y, width, height, data, font, color):
		for value in data:
			drawer.text(
				(
					x + ((width - value['width']) // 2),
					y + value.get('margin_top', 0),
				),
				value['text'],
				font=value.get('font', font),
				fill=value.get('color', color),
			)
			y += value['height'] + value.get('margin_top', 0) + value.get('margin', 0)

	def _get_font_path(self, *names):
		for directory_path in (
				'/system/fonts',
				'/usr/local/lib/python2.7/dist-packages/matplotlib/mpl-data/fonts/ttf/',
				'/usr/share/fonts/truetype/freefont',
		):
			for name in names:
				path = os.path.join(directory_path, name)
				if os.path.isfile(path):
					return path
		else:
			raise Exception('Fonts {} are not found. Available fonts are: {}'.format(names, list(self._get_available_fonts())))

	@staticmethod
	def _get_available_fonts(fonts_path='/system/fonts'):
		"""Returns avaiable fonts (default path is for android platform)"""
		for font_path in [os.path.join(fonts_path, x) for x in os.listdir(fonts_path)]:
			yield font_path


def run_init():
	controller = WidgetController(id=-1, width=240, height=160, mtime_path='.geometry')
	controller.show()


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
