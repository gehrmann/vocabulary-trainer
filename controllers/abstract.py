#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides abstract base for controllers

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )

Todo:
	* Write docstrings for classes and methods
"""

from colorama import (
	Fore as FG,
)
import datetime
import functools
import logging
import os
import parse
import pickle
import re
import signal
import socket
import ssl
import sys
import threading
import time
import urllib
import urllib.parse
# import urllib2
import urllib3
# import encodings.idna  # Prevent "LookupError: unknown encoding: idna" for urllib3 pool request
import uuid
import weakref
# import wget
import zipfile

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Sets default timeout for network requests
	socket.setdefaulttimeout(10)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtGui, QtWidgets, uic

from helpers.caller import Caller
from helpers.timer import Timer
from models.abstract import AttrDict, ObservableAttrDict  #, UnhashableKeyDict


class HashableQItem(object):
	"""PyQt Q*Item are no more hashable. This wrapper patches the problem"""

	_instances = weakref.WeakValueDictionary()

	def __new__(cls, entry):
		entry_id = id(entry)

		# logging.getLogger(__name__).warning('len(cls._instances)=' + '%s', len(cls._instances))
		if entry_id in cls._instances:
			instance = cls._instances[entry_id]
		else:
			cls._instances[entry_id] = instance = super().__new__(cls)

		return instance

	def __init__(self, entry):
		# logging.getLogger(__name__).warning('(id(entry), entry)=' + '%s', (id(entry), entry))
		self.entry = entry

	def __hash__(self):
		return id(self.entry)

	# def __del__(self):
	#     logging.getLogger(__name__).warning('here')
	#     # from helpers import my_trace; my_trace.print_stack()  # FIXME: must be removed/commented

	# def __eq__(self, entry):
	#     return isinstance(entry, HashableQItem) and id(self.entry) == id(entry)

	def __getattr__(self, name):
		return getattr(self.entry, name)

	# def __getattribute__(self, name):
	#     logging.getLogger(__name__).warning('name=' + '%s', name)
	#     return super(HashableQItem, self).__getattribute__(name)



class Progress(object):
	def __init__(self, message, thread=None, done=None, total=None):
		self.message, self.thread, self.done, self.total = message, thread, done, total


class ProgressMixture(object):

	def __init__(self):
		self._progress_dialog = None

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug(self.__class__, object.__repr__(model), previous and previous, current and current)
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		state_model = self.state_model
		view = self._view

		if model is state_model:
			if current[0] is not None and 'progress' in current[0] and previous[0] != current[0]:
				if state_model.progress is None:
					if self._progress_dialog is None:
						pass
					else:
						self._progress_dialog.close()
						self._progress_dialog = None
				else:
					if self._progress_dialog is None:
						self._progress_dialog = progress_dialog = QtWidgets.QProgressDialog(
							QtCore.QString(state_model.progress.message if hasattr(state_model.progress, 'message') else state_model.progress),
							QtCore.QString(None if QtCore.QT_VERSION_STR.startswith('5.') else ''),  # Cancel-button's title
							(state_model.progress.done if hasattr(state_model.progress, 'done') else 0),
							(state_model.progress.total if hasattr(state_model.progress, 'total') else 0),
							parent=view,
							flags=QtCore.Qt.WindowFlags(0),
						)
						progress_dialog.setWindowModality(QtCore.Qt.WindowModal)
						# progress_dialog.setMinimumDuration(0)
						progress_dialog.setAutoClose(False)
						# progress_dialog.forceShow()
					else:
						self._progress_dialog.setLabelText(state_model.progress.message if hasattr(state_model.progress, 'message') else state_model.progress)
						self._progress_dialog.setValue(state_model.progress.done if hasattr(state_model.progress, 'done') else 0)
						self._progress_dialog.setMaximum(state_model.progress.total if hasattr(state_model.progress, 'total') else 0)
				# progress_dialog.forceShow()
				# for i in range(100):
				#     self._progress_dialog.setValue(i)
				#     time.sleep(1)
				# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "state_model.progress,", state_model.progress, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
				# pass
				# if not state_model.progress:
				#     QtWidgets.QToolTip.hideText()
				# else:
				#     # QtWidgets.QToolTip.setFont(QtGui.QFont('Droid Sans', 8))
				#     # QtWidgets.QToolTip.showText(
				#     #     QtCore.QPoint(
				#     #         max(0, (dialog.x() + (dialog.width() - len(state_model.progress) * 6) / 2)),  # Centered
				#     #         dialog.y() + dialog.height() * 5 / 6,  # Almost bottom
				#     #     ),
				#     #     state_model.progress,
				#     # )
				#     # Caller.call_once_after(2., self.__hide_message)  # Re-set a hiding routine. Must be the same function as previously used

	""" View's event handlers """

	""" Helpers """


class ControllerMixture(ProgressMixture):
	_instance = None

	class _State(ObservableAttrDict):
		pass

	@classmethod
	def init(cls, **kwargs):
		if cls._instance is None:
			cls._instance = cls(**kwargs)

	def __init__(self):
		ProgressMixture.__init__(self)

		self.state_model = self._State()

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug(self.__class__, object.__repr__(model), previous and previous, current and current)
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		ProgressMixture._on_model_updated(self, model=model, previous=previous, current=current)

	""" View's event handlers """

	def _on_key_pressed(self, event):
		if event.key() == QtCore.Qt.Key_Escape:
			self._view.close()

	def _on_close(self, event=None):
		# dst_path = '{}/.{}-geometry'.format(os.path.expanduser('~'), os.path.basename(sys.argv[0]))
		dst_path = '{}/.geometry'.format(os.getcwd())

		print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), f'Saving geometry to {dst_path}', file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		with open(dst_path, 'wb') as dst:
			value = self._view.saveGeometry()
			pickle.dump(value, dst)

		# with open('{}/.{}-state'.format(os.path.expanduser('~'), os.path.basename(sys.argv[0])), 'w') as dst:
		#     print(self._view.saveState(), file=dst);

	""" Helpers """

	def _restore_window_geometry(self):
		# src_path = '{}/.{}-geometry'.format(os.path.expanduser('~'), os.path.basename(sys.argv[0]))
		src_path = '{}/.geometry'.format(os.getcwd())

		print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), f'Loading geometry from {src_path}', file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		try:
			# with open('{}/.{}-geometry'.format(os.path.expanduser('~'), os.path.basename(sys.argv[0]))) as storage:
			with open(src_path, 'rb') as src:
				value = pickle.load(src)
				self._view.restoreGeometry(value)

			# FIXME: does not work
			# with open('{}/.{}-state'.format(os.path.expanduser('~'), os.path.basename(sys.argv[0]))) as src:
			#     self._view.restoreState(src.read())
		except (IOError, EOFError):
			pass

	# @staticmethod
	# def _find_file(directories_paths, filename):
	#     for directory_path in directories_paths:
	#         path = directory_path + filename
	#         if os.path.exists(path):
	#             return path
	#     else:
	#         raise Exception("Can not find file {} in {}".format(filename, directories_paths))

	@staticmethod
	def _load_ui(ui_path, widget_name='QWidget', __state=dict(initialized=False)):
		import images_rc; images_rc  # Make images be accessed via qt-resources-style [url(":/...")], or in QWebView: [qrc:/...]

		if not __state['initialized']:
			# Inserts path to custom application widgets
			uic.widgetPluginPath.insert(0, (os.path.dirname(__file__) or '.') + '/../widgets')

		with Timer('loading ' + ui_path):
			# Loads *.py
			#
			# # Compiles .ui into .py
			# with open(src_path) as src:
			#     dst_path = src_path.replace('.ui', '.py')
			#     logging.getLogger(__name__).warning('dst_path=' + '%s', dst_path)
			#     with open(dst_path, 'w') as dst:
			#         uic.compileUi(uifile=src, pyfile=dst)
			#
			py_module_name = ui_path.replace('/', '.').replace('.ui', '')
			# logging.getLogger(__name__).warning('py_module_name=' + '%s', py_module_name)
			try:
				module = __import__(py_module_name)
			except ImportError as e:
				logging.getLogger(__name__).info('%s, trying to load from ui...', e)
			else:
				for key in py_module_name.split('.')[1:]:
					module = getattr(module, key)
				# logging.getLogger(__name__).warning('module=' + '%s', module)
				# logging.getLogger(__name__).warning('dir(module)=' + '%s', dir(module))

				cls_name = next(x for x in dir(module) if x.startswith('Ui_'))
				# logging.getLogger(__name__).warning('cls_name=' + '%s', cls_name)
				cls = getattr(module, cls_name)

				widget = getattr(QtWidgets, widget_name)()
				view = cls()
				view.setupUi(widget)
				widget.__dict__.update(view.__dict__)
				return widget

			# Loads *.ui
			def _load(path, open=open):
				with open(path) as src:
					return uic.loadUi(src)
			for path in sys.path:
				src_path = path + os.sep + ui_path
				if os.path.exists(src_path):
					return _load(path=src_path)
				elif os.path.isfile(path) and zipfile.is_zipfile(path):
					with zipfile.ZipFile(path) as archive:
						return _load(ui_path, open=archive.open)
			else:
				raise Exception("Can not find {} in {}".format(ui_path, sys.path))

	_font_weights_to_names = {getattr(QtGui.QFont, key): key for key in vars(QtGui.QFont) if getattr(QtGui.QFont, key).__class__ == QtGui.QFont.Normal.__class__}
	_font_weights_to_names[12] = _font_weights_to_names[25]

	@classmethod
	def _font_to_text(cls, font):
		size, family, weight_name = font.get('size', 12), font['family'], cls._font_weights_to_names[font['weight']]
		return ("{0}, {1}pt, ".format(family, size) + weight_name + (', italic' if font['italic'] else '')) if font is not None else ''

	@classmethod
	def _font_to_style(cls, font):
		size, family, weight, style = font.get('size', 12), font['family'], (8 * font['weight']), ('italic' if font['italic'] else 'normal')
		return '{{font: {0}pt "{1}"; font-weight: {2}; font-style: {3};}}'.format(size, family, weight, style)

	@classmethod
	def _show_font_dialog(cls, model, key):
		# dialog = cls._load_ui('views/common_multiwindow_dialog.ui', widget_name='QDialog')

		if model.get(key, None) is not None:
			font = QtGui.QFont(model[key]['family'], int(model[key].get('size', 12)), model[key]['weight'], model[key]['italic'])
			font_dialog = QtWidgets.QFontDialog(font)
		elif hasattr(model, key):
			font = QtGui.QFont(getattr(model, key)['family'], int(getattr(model, key).get('size', 12)), getattr(model, key)['weight'], getattr(model, key)['italic'])
			font_dialog = QtWidgets.QFontDialog(font)
		else:
			font_dialog = QtWidgets.QFontDialog()
		if hasattr(QtCore.Qt, 'MaximizeUsingFullscreenGeometryHint'):  # In PyQt5
			font_dialog.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)
		# subwindow = dialog.mdiArea.addSubWindow(font_dialog)
		# subwindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
		# subwindow.showMaximized()

		# def on_font_selected(result):
		#     # dialog.close()
		#     setattr(model, key, {
		#         x: value
		#         for x in ('family', 'pointSize', 'weight', 'italic')
		#         for value in [getattr(font_dialog.currentFont(), x)()]
		#     })
		# font_dialog.finished.connect(on_font_selected)

		selected = font_dialog.exec_()
		# dialog.exec_()

		if selected:
			# dialog.close()
			font = font_dialog.currentFont()
			setattr(model, key, dict(family=str(font.family()), size=font.pointSize(), weight=font.weight(), italic=font.italic()))

	@classmethod
	def _show_color_dialog(cls, model, key):
		# dialog = cls._load_ui('views/common_multiwindow_dialog.ui', widget_name='QDialog')

		color_dialog = QtWidgets.QColorDialog(*([QtGui.QColor(model[key])] if model.get(key, None) is not None else []))
		if hasattr(QtCore.Qt, 'MaximizeUsingFullscreenGeometryHint'):  # In PyQt5
			color_dialog.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)
		# subwindow = dialog.mdiArea.addSubWindow(color_dialog)
		# subwindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
		# subwindow.showMaximized()

		# def on_color_selected(result):
		#     # dialog.close()
		#     setattr(model, key, str(color_dialog.currentColor().name()))
		# color_dialog.finished.connect(on_color_selected)

		selected = color_dialog.exec_()
		# dialog.exec_()

		if selected:
			# dialog.close()
			setattr(model, key, str(color_dialog.currentColor().name()))

	@staticmethod
	def _parse_int(value, default):
		try:
			return int(value)
		except ValueError:
			return default

	# Java proxy/
	class WrongPlatformException(Exception):
		pass

	class Java(object):
		def __init__(self):
			try:
				import QtJavaCaller
			except ImportError:
				message = 'Can not import "QtJavaCaller", available only on Android-platform'
				# if not ignore_wrong_platform_exception:
				#     # logging.getLogger(__name__).warning(message)
				#     raise cls.WrongPlatformException(message)
				_Java = object
			else:
				_Java = QtJavaCaller.QtJavaCaller

			self._java = java = _Java()

			if hasattr(java, 'called'):
				self.called = java.called
			else:
				self.called = lambda: None
				self.called.connect = lambda x: None

			def on_called(serialized_kwargs):
				logging.getLogger(__name__).warning('on_called(%s)', serialized_kwargs)
				import json
				kwargs = json.loads(serialized_kwargs)
			self.called.connect(on_called)

		@staticmethod
		def called_wrapper(function):
			def wrapper(serialized_kwargs):
				import json
				kwargs = json.loads(serialized_kwargs)
				return function(**kwargs)
			return wrapper

		def call(self, method, ignore_wrong_platform_exception=True, **kwargs):
			java = self._java

			import json
			serialized_kwargs = json.dumps(kwargs)

			if not hasattr(java, 'call'):
				message = 'Can not call java.call, available only on Android-platform'
				if not ignore_wrong_platform_exception:
					# logging.getLogger(__name__).warning(message)
					raise ControllerMixture.WrongPlatformException(message)
				serialized_result = '{}'
			else:
				serialized_result = java.call(method, serialized_kwargs)

			result = json.loads(serialized_result)

			return result
	java = Java()
	# /Java proxy

	@staticmethod
	def _convert_previous_data():
		"""
		Check if there is data to convert and show messages how to do it.
		"""

		previous_path = os.path.join(os.getcwd(), '..', 'Vocabulary Learning Game-Box')
		previous_data_path = os.path.realpath(previous_path + os.sep + 'data.db')

		if os.path.exists(previous_path) and os.path.exists(previous_data_path) and not os.path.exists(previous_path + os.sep + 'data.db.converted'):
			parent_window = QtWidgets.qApp.activeWindow()

			QtWidgets.QMessageBox.information(parent_window, 'Congratulations!', 'The App was <b>completely rewritten!</b>')

			result = QtWidgets.QMessageBox.question(parent_window, 'Conversion needed', 'The problem is, that your vocabularies <b>must be converted</b>.', QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Discard, QtWidgets.QMessageBox.Ok)

			if result == QtWidgets.QMessageBox.Discard:
				# Forget about it
				with open(previous_path + os.sep + 'data.db.converted', 'w'):
					pass

			elif result == QtWidgets.QMessageBox.Ok:
				QtWidgets.QMessageBox.information(
					parent_window,
					'How to convert',
					(
						'Email me to <b>{0}</b><br/>'
						'with that file attached:<br/>'
						'<b>{1}</b><br/>'
						'I will send you converted data back.<br/>'
						.format(
							'gehrmann.apps@gmail.com',
							previous_data_path,
						)
					),
				)

	@staticmethod
	def _cut_insignificant_keywords_parts(keywords):
		"""Cuts insignificant parts of keywords (like text written in brackets), returns a modified result"""
		for match, substitute in (
				(r'\([^)]*\)?', r''),  # Opened (not necessarily closed) parentheses
		):
			keywords = re.sub(match, substitute, keywords)
		keywords = keywords.strip()
		return keywords

	def _show_in_dict_tab(self, keywords):
		"""Opens keywords in dict-tab"""
		self.settings_model.dict_left_field = self.settings_model.dict_variants_keywords = keywords
		self.settings_model.current_tab = 0  # Open dict-tab

		# Increment undo-redo
		from controllers.dict import DictController
		if DictController._instance is not None:
			Caller.call_once_after(.03, DictController._instance.undo_redo_model.save)

	def _update_last_training_time(self):
		"""Updates last modification time of .last_trained"""
		with open('.last_trained', 'w') as dst:
			pass

	""" Animation """

	# def _animate_color(self, widget, key, from_value, to_value, duration, curve, reset=False):

	#     class ColorAnimator(QtCore.QObject):
	#         def __init__(self, widget, color):
	#             QtCore.QObject.__init__(self)
	#             self._widget = widget
	#             self._color = color
	#             self._previous_stylesheet = widget.styleSheet()

	#         @QtCore.pyqtProperty(QtWidgets.QColor)
	#         def color(self):
	#             return self._color

	#         @color.setter
	#         def color(self, value):
	#             stylesheet = "QLabel { %s: rgba(%d, %d, %d, %d); }" % (key, value.red(), value.green(), value.blue(), value.alpha())
	#             self._widget.setStyleSheet(stylesheet)

	#         def clear_styles(self):
	#             self._widget.setStyleSheet(self._previous_stylesheet)

	#     color_animator = ColorAnimator(widget, QtWidgets.QColor(0, 0, 0, 0))

	#     animation = QtCore.QPropertyAnimation(color_animator, bytes('color'), widget)
	#     animation.setEasingCurve(getattr(QtCore.QEasingCurve, curve))
	#     animation.setDuration(int(duration * 1000))
	#     # animation.setStartValue(0)
	#     # animation.setStartValue(QtWidgets.QColor(100, 100, 100, 255))
	#     animation.setStartValue(QtWidgets.QColor(*from_value) if from_value.__class__ in (tuple, list) else QtWidgets.QColor(from_value))
	#     # animation.setKeyValueAt(.5, QtWidgets.QColor(255, 0, 0, 100))
	#     # animation.setEndValue(10)
	#     animation.setEndValue(QtWidgets.QColor(*to_value) if to_value.__class__ in (tuple, list) else QtWidgets.QColor(to_value))
	#     # animation.setEndValue(QtWidgets.QColor(0, 0, 0, 255))
	#     # animation.setLoopCount(2)
	#     if reset:
	#         animation.finished.connect(color_animator.clear_styles)
	#     animation.start()

	@classmethod
	def _animate(cls, widget, key, effect=None, from_value=1., to_value=0., duration=1., count=1, curve='Linear', finished=None):
		"""Animates widget's properties.

		If effect is set as:
		* opacity:		key = [ opacity ]
		* blur:			key = [ blurRadius ]
		* dropShadow:	key = [ blurRadius | color | offset | xOffset | yOffset ]
		* colorize:		key = [ color | strength ]
		Key:
		(only for QtWidgets.QPropertyAnimation-objects)
		* scale
		* rotation
		Returns animation.
		Can not animate two or more different graphic effects.
		Call .start() to animate.
		"""

		if effect is not None:
			effect_name = 'QGraphics' + effect[0].upper() + effect[1:] + 'Effect'
			effect_class = getattr(QtWidgets, effect_name)
			if widget.graphicsEffect().__class__ != effect_class:
				widget.setGraphicsEffect(effect_class())

		# animation = QtCore.QPropertyAnimation(widget.graphicsEffect() if effect is not None else widget, bytes(key), widget)
		animation = QtCore.QPropertyAnimation(widget.graphicsEffect() if effect is not None else widget, bytes(key, 'utf-8'), widget)
		animation.setEasingCurve(getattr(QtCore.QEasingCurve, curve))
		animation.setStartValue(
			QtCore.QPoint(*from_value) if from_value.__class__ in (tuple, list) and len(from_value) == 2 else (
				QtCore.QRect(*from_value) if from_value.__class__ in (tuple, list) and len(from_value) == 4 else (
					from_value
				)
			)
		)
		animation.setEndValue(
			QtCore.QPoint(*to_value) if to_value.__class__ in (tuple, list) and len(to_value) == 2 else (
				QtCore.QRect(*to_value) if to_value.__class__ in (tuple, list) and len(to_value) == 4 else (
					to_value
				)
			)
		)
		animation.setDuration(int(duration * 1000))
		animation.setLoopCount(count)
		if finished is not None:
			animation.finished.connect(finished)

		return animation

	@classmethod
	def _animate_parallel(cls, widget, animations, **kwargs):
		""" returns animation group. Call .start() to animate.  """

		animation_group = QtCore.QParallelAnimationGroup(widget)
		for animation in animations:
			if isinstance(animation, (int, float)):
				animation_group.addPause(int(animation * 1000))
			elif isinstance(animation, QtCore.QPropertyAnimation):
				animation_group.addAnimation(animation)
			else:
				animation_group.addAnimation(cls._animate(**(dict(kwargs, widget=widget, **animation))))

		return animation_group

	@classmethod
	def _animate_sequential(cls, widget, animations, **kwargs):
		""" returns animation group. Call .start() to animate.  """

		animation_group = QtCore.QSequentialAnimationGroup(widget)
		for animation in animations:
			if isinstance(animation, (int, float)):
				animation_group.addPause(int(animation * 1000))
			elif isinstance(animation, QtCore.QPropertyAnimation):
				animation_group.addAnimation(animation)
			else:
				animation_group.addAnimation(cls._animate(**(dict(kwargs, widget=widget, **animation))))

		return animation_group


class CommonTreeDialog(ControllerMixture):
	def __init__(
			self,
			parent_view=None,
			headers=None,
			settings_model=None,
			data_model=None,
			model=None,
			key=None,
			selected=None,
			renamed=None,
			preloading=None,
			path_attribute=None,
			path_separator=None,
			path_prefix=None,
			is_expanded=False,
			has_icons=False,
			icons={},
			# icons={'item': ':/images/checked.png'},
			buttons=('Close', ),
			has_multiselection=False,
			is_container_selectable=True,
			with_add_button=False,
			add_button_clicked=None,
			with_delete_button=False,
			with_shared_button=False,
			with_proceed_selected_button=False,
			proceed_selected_button_clicked=None,
			with_download_selected_button=False,
			download_selected_button_clicked=None,
	):
		ControllerMixture.__init__(self)

		self.key = key
		self.path_attribute = path_attribute
		self.path_separator = path_separator
		self.path_prefix = path_prefix
		self.selected = selected  # selected-callback
		self.renamed = renamed  # renamed-callback
		self.preloading = preloading  # preloading-callback
		self.is_expanded = is_expanded
		self.has_icons = has_icons
		self.icons = icons
		self.has_multiselection = has_multiselection
		self.is_container_selectable = is_container_selectable

		# Models
		self.settings_model = settings_model
		self.data_model = data_model
		self.model = model
		""" Current-State-Model for selected view's data"""
		state_model = self.state_model
		state_model.headers = headers
		state_model.progress = None
		state_model.tips = ''

		self._entries_to_data = dict()  # Relation between views and models
		self._entries_to_paths = dict()  # Relation between views and models
		self._paths_to_entries = dict()  # Relation between models and views
		self._expanded_paths = set()
		self._item_to_edit = None
		self._checked_sign = ' ✔ '

		# Views
		self._dialog = dialog = self._load_ui('views/common_multiwindow_dialog.ui', widget_name='QDialog')
		if hasattr(QtCore.Qt, 'MaximizeUsingFullscreenGeometryHint'):  # In PyQt5
			dialog.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)

		self._view = view = self._load_ui('views/common_tree_dialog.ui', widget_name='QDialog')
		subwindow = dialog.mdiArea.addSubWindow(view)
		subwindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
		subwindow.showMaximized()

		if not with_add_button:
			view.add_button.setParent(None)
		else:
			view.add_button.clicked.connect(add_button_clicked if add_button_clicked is not None else self.__on_add_button_clicked)
		if not with_delete_button:
			view.delete_button.setParent(None)
		else:
			view.delete_button.clicked.connect(self.__on_delete_button_clicked)
		if not with_shared_button:
			view.shared_button.setParent(None)
		self.with_proceed_selected_button = with_proceed_selected_button
		self.proceed_selected_button_clicked = proceed_selected_button_clicked
		self.with_download_selected_button = with_download_selected_button
		self.download_selected_button_clicked = download_selected_button_clicked
		view.tree.itemChanged.connect(self.__on_tree_item_changed)
		view.tree.itemSelectionChanged.connect(self.__on_tree_selection_changed)
		view.tree.itemExpanded.connect(self.__on_tree_item_expanded)
		view.tree.itemCollapsed.connect(self.__on_tree_item_collapsed)
		view.tree.itemDoubleClicked.connect(self.__on_tree_item_double_clicked)
		if has_multiselection:
			view.tree.setSelectionMode(QtWidgets.QTreeWidget.MultiSelection)
		view.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.StandardButtons(functools.reduce(int.__or__, [getattr(QtWidgets.QDialogButtonBox, x) for x in buttons])))
		view.finished.connect(self.__on_view_finished)
		dialog.closeEvent = self.__on_dialog_closed

		""" Observe models by view """
		if hasattr(data_model, 'changed'):
			data_model.changed.bind(self._on_model_updated)
		state_model.changed.bind(self._on_model_updated, invoke_in_main_thread=True)  # Invoke in main because the progress is running from thread

		""" Fill blank view by models """
		self._on_model_updated(data_model)
		self._on_model_updated(state_model)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug(self.__class__, object.__repr__(model), previous and previous, current and current)
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)

		data_model = self.data_model
		state_model = self.state_model
		tree_view = self._view.tree
		view = self._view

		if model is data_model:
			Caller.call_once_after(0, self.__fill)

		if model is state_model:
			if current[0] is None or 'tips' in current[0] and previous != current:
				getattr(view.tips_widget, 'show' if state_model.tips else 'hide')()
				if hasattr(view.tips_label, 'setHtml'):  # If QWebView
					view.tips_label.setHtml(str(state_model.tips))
				else:
					view.tips_label.setText(state_model.tips.replace('qrc:', ':'))

			if current[0] is None or 'headers' in current[0] and previous != current:
				for index, header in enumerate(state_model.headers):
					tree_view.headerItem().setText(index, header)

	""" View's event handlers """

	def __on_view_finished(self, result):
		self._dialog.close()

	def __on_dialog_closed(self, event=None):
		# values = (self._entries_to_data[x] for x in self._view.tree.selectedItems())
		# value = next(values, None)
		values = [self._entries_to_data[HashableQItem(x)] for x in self._view.tree.selectedItems()]
		value = values[0] if values else None

		if self.model is not None and self.key is not None:
			setattr(self.model, self.key, (values if self.has_multiselection else value))
		if self.selected is not None:
			self.selected((values if self.has_multiselection else value))

	def __on_tree_item_changed(self, entry):
		if HashableQItem(entry) in self._entries_to_data:
			data = self._entries_to_data[HashableQItem(entry)]
			if not entry.text(0):  # Restore name if empty
				entry.setText(0, self.__get_path(data)[-1])
			else:
				if self.renamed:
					text = str(entry.text(0))
					# text = self._remove_selection_signs(text)
					path = self.__get_path(data)
					if path is not None and text != path[-1]:
						self.renamed(data, text)  # Call renamed-callback

			self._update_selection_widget()

	def __on_tree_selection_changed(self):
		self._update_selection_widget()

	def __on_tree_item_expanded(self, entry):
		path = self._entries_to_paths[HashableQItem(entry)]
		self._expanded_paths.add(path)

		if HashableQItem(entry) in self._entries_to_data:
			data = self._entries_to_data[HashableQItem(entry)]
			if not entry.childCount() and self.preloading:
				Caller.call_once_after(.1, self.preloading, data)  # Call preloading-callback

	def __on_tree_item_collapsed(self, entry):
		path = self._entries_to_paths[HashableQItem(entry)]
		self._expanded_paths.remove(path)

	def __on_tree_item_double_clicked(self, entry, column):
		tree_view = self._view.tree

		if entry.flags() & QtCore.Qt.ItemIsEditable:
			# Removes item widget before editing because QLineEdit will be inserted there (implicit)
			if tree_view.itemWidget(entry, 0).__class__ == QtWidgets.QWidget:
				tree_view.removeItemWidget(entry, 0)

			# Inserts QLineEdit (implicit)
			tree_view.editItem(entry, column)

	def __on_add_button_clicked(self, event):
		# Get path of selected or first top-level directory
		entry = self._view.tree.currentItem() or self._view.tree.topLevelItem(0)

		directory_path = self.path_prefix
		try:
			if HashableQItem(entry) not in self._entries_to_paths:  # Get its directory if it is not a directory
				entry = entry.parent()
			directory_path += self._entries_to_paths[HashableQItem(entry)]
		except KeyError:
			pass
		except AttributeError:
			pass

		if directory_path is not None:
			if not os.path.isdir(directory_path):
				os.makedirs(directory_path)

		# Create new item
		base_path = os.path.join(directory_path, 'New ' + (self.key or 'item'))
		for index in range(9999):
			path = base_path + (' #{}'.format(index) if index else '')
			try:
				item = self.data_model.add(path)
			except NameError:
				continue
			break
		self._item_to_edit = item  # Activate edition of its entry after refilling tree items

		Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Action Create ' + self.__class__.__name__, label=path))

	def __on_delete_button_clicked(self, event):
		# Get selected item
		entry = self._view.tree.currentItem()

		if entry is None:
			pass

		elif HashableQItem(entry) in self._entries_to_paths:  # Is a directory
			pass  # Not implemented

		else:  # Is a file
			data = self._entries_to_data[HashableQItem(entry)]

			result = QtWidgets.QMessageBox.question(QtWidgets.qApp.activeWindow(), 'Confirmation needed', 'Remove focused?', QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel, QtWidgets.QMessageBox.Cancel)
			if result == QtWidgets.QMessageBox.Yes:
				# Remove item
				self.data_model.remove(data)
				self._entries_to_data.pop(HashableQItem(entry))

				Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Action Remove ' + self.__class__.__name__, label=data))

	""" Helpers """

	def show(self):
		self._dialog.exec_()

	def __get_path(self, data):
		path = getattr(data, self.path_attribute) if self.path_attribute is not None else str(data)
		if path is None:
			return path
		if self.path_prefix:
			try:
				if path.startswith(self.path_prefix):
					path = path[len(self.path_prefix):]
			except AttributeError as e:
				# raise e.__class__, e.__class__(str(e) + ' [DEBUG: {}]'.format(dict(cls=self.__class__.__name__, path_attribute=self.path_attribute, path_prefix=self.path_prefix, data=data, path=path))), sys.exc_info()[2]
				raise e.__class__(str(e) + ' [DEBUG: {}]'.format(dict(cls=self.__class__.__name__, path_attribute=self.path_attribute, path_prefix=self.path_prefix, data=data, path=path))) from e
		return path.split(self.path_separator if self.path_separator is not None else os.sep)

	def _update_selection_widget(self):
		tree_view = self._view.tree

		for entry in (x.entry for x in self._entries_to_data):
			# Shows/hides selection widget with checked text (and maybe buttons)
			if not entry.isSelected():
				tree_view.removeItemWidget(entry, 0)
			else:
				entry_widget = self._load_ui('views/common_tree_entry_with_buttons_view.ui')  # Qt-resources have to be imported already
				entry_widget.label.setText('{}{}'.format(self._checked_sign, str(entry.text(0))))

				if not self.with_proceed_selected_button:
					entry_widget.proceed_button.setParent(None)
				elif self.proceed_selected_button_clicked is not None:
					entry_widget.proceed_button.clicked.connect(self.proceed_selected_button_clicked)

				if not self.with_download_selected_button:
					entry_widget.download_button.setParent(None)
				elif self.download_selected_button_clicked is not None:
					entry_widget.download_button.clicked.connect(self.download_selected_button_clicked)

				tree_view.setItemWidget(entry, 0, entry_widget)

	def __fill(self):
		tree_view = self._view.tree

		if self.has_icons:
			empty_icon = QtGui.QIcon()
			item_icon = QtGui.QIcon(self.icons['item'])

		class TreeWidgetItem(QtWidgets.QTreeWidgetItem):
			pass
			# def __lt__(item_1, item_2):
			#     """Compares items without selection signs"""
			#     return self._remove_selection_signs(str(item_1.text(0))) < self._remove_selection_signs(str(item_2.text(0)))

		# Re-create tree items
		tree_view.blockSignals(True)
		entry_to_edit = None
		tree_view.clear()
		self._entries_to_data.clear()
		self._entries_to_paths.clear()
		self._paths_to_entries.clear()
		for data in self.data_model:
			_path = []
			names = self.__get_path(data)
			directories_names, file_name = names[:-1], names[-1]

			parent_directory_path = ''
			directory_path = ''
			for directory_name in directories_names:
				directory_path += ((self.path_separator if self.path_separator is not None else os.sep) if directory_path else '') + directory_name

				if directory_path not in self._paths_to_entries:
					entry = TreeWidgetItem([directory_name])
					if self.has_icons:
						entry.setIcon(0, empty_icon)
					# if self.has_checkboxes:
					#     entry.setCheckState(0, QtCore.Qt.Unchecked)
					#     entry.setCheckState(0, QtCore.Qt.Checked if str(entry.text(0)) in ... else QtCore.Qt.Unchecked)
					entry.setChildIndicatorPolicy(TreeWidgetItem.ShowIndicator)  # Force showing 'expand'-button
					# entry.setExpanded(self.is_expanded)
					# entry.setFlags(QtCore.Qt.ItemIsEnabled)
					entry.setFlags(entry.flags() ^ (0 if self.is_container_selectable else QtCore.Qt.ItemIsSelectable))

					# if file_name and (self.is_expanded or directory_path in self._expanded_paths):
					if self.is_expanded or directory_path in self._expanded_paths:
						self._expanded_paths.add(directory_path)
						# entry.setExpanded(True)
						# Caller.call_once_after(0, entry.setExpanded, True)

					(self._paths_to_entries[parent_directory_path].addChild if parent_directory_path else tree_view.addTopLevelItem)(entry)

					self._paths_to_entries[directory_path] = entry
					self._entries_to_paths[HashableQItem(entry)] = directory_path

				parent_directory_path = directory_path

			if directories_names and not file_name:
				self._entries_to_data[HashableQItem(entry)] = data  # Attach data to last directory in hierarchy

			elif file_name:
				entry = TreeWidgetItem([file_name])
				if self.has_icons:
					entry.setIcon(0, item_icon)
				# if self.has_checkboxes:
				#     entry.setCheckState(0, QtCore.Qt.Unchecked)
				#     entry.setCheckState(0, QtCore.Qt.Checked if str(entry.text(0)) in ... else QtCore.Qt.Unchecked)
				entry.setFlags(entry.flags() | (QtCore.Qt.ItemIsEditable if self.renamed is not None else 0))
				(self._paths_to_entries[parent_directory_path].addChild if parent_directory_path else tree_view.addTopLevelItem)(entry)

				if self.model is not None and self.key is not None:
					entry.setSelected(((data in self.model[self.key]) if self.has_multiselection else (data == self.model[self.key])))  # Select only after addition to parent

				if self._item_to_edit is not None and self._item_to_edit == data:  # Activate interactive edition of this entry
					self._item_to_edit, entry_to_edit = None, entry

				self._entries_to_data[HashableQItem(entry)] = data

		# Re-expand re-created but previously expanded tree containers
		for directory_path in self._expanded_paths:
			if directory_path in self._paths_to_entries:
				entry = self._paths_to_entries[directory_path]
				entry.setExpanded(True)

		tree_view.sortItems(0, QtCore.Qt.AscendingOrder)

		self._update_selection_widget()

		tree_view.blockSignals(False)

		if entry_to_edit is not None:
			tree_view.editItem(entry_to_edit, 0)
			# tree_view.openPersistentEditor(entry_to_edit, 0)


class CommonSharedTreeDialog(CommonTreeDialog):
	def __init__(self, *args, **kwargs):
		CommonTreeDialog.__init__(self, *args, with_download_selected_button=True, download_selected_button_clicked=self.__on_download_button_clicked, **kwargs)

		self.state_model.alert = None

		Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Show Dialog ' + self.__class__.__name__))

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug(self.__class__, object.__repr__(model), previous and previous, current and current)
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		super(CommonSharedTreeDialog, self)._on_model_updated(model, previous, current)

		data_model = self.data_model
		state_model = self.state_model
		view = self._view

		if model is data_model:
			tips = ''
			if not data_model:
				# Show tip: why is no shared entries there
				tips = 'Check your Internet connection'
			state_model.tips = tips

		if model is state_model:
			if current[0] is None or 'progress' in current[0] and previous[0] != current[0]:
				# view.download_button.setDisabled(bool(state_model.progress))
				view.tree.setDisabled(bool(state_model.progress))

			if current[0] is not None and 'alert' in current[0] and previous[0] != current[0]:
				if state_model.alert is not None:
					QtWidgets.QMessageBox.information(QtWidgets.qApp.activeWindow(), 'Info', state_model.alert)
					state_model.alert = None

	""" View's event handlers """

	def __on_download_button_clicked(self, event):
		# Get selected item
		entry = self._view.tree.currentItem()

		if entry is None:
			pass

		elif HashableQItem(entry) in self._entries_to_paths:  # Is a directory
			pass  # Not implemented

		else:  # Is a file
			data = self._entries_to_data[HashableQItem(entry)]

			# Download item
			def download():
				for progress in self.data_model.download(data):
					self.state_model.progress = progress
				time.sleep(1.)
				# self.state_model.alert = 'Download completed'
				self.state_model.alert = self.state_model.progress
				self.state_model.progress = None

			# Caller.call_once_after(.1, download)
			# thread = elf.make_breakpoint(threading.Thread(target=send))
			thread = threading.Thread(target=download)
			thread.setDaemon(True)
			thread.start()

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Action Download ' + self.__class__.__name__, label=data))

	""" Helpers """


class SharedVocabulariesStoragesDialog(CommonTreeDialog):
	def __init__(self, *args, **kwargs):
		CommonTreeDialog.__init__(
			self,
			*args,
			with_add_button=True,
			add_button_clicked=self.__on_add_button_clicked,
			with_delete_button=True,
			with_proceed_selected_button=True,
			proceed_selected_button_clicked=self.__on_proceed_button_clicked,
			**kwargs
		)

	""" Model's event handlers """

	# def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
	#     # logging.getLogger(__name__).debug(self.__class__, object.__repr__(model), previous and previous, current and current)

	#     super(VocabulariesDialog, self)._on_model_updated(model, previous, current)

	#     data_model = self.data_model
	#     view = self._view

	#     if model is data_model:
	#         if current[0] is None:  # Only once when view is being filled
	#             tips = ''
	#             if not data_model:
	#                 # Show tip: how to add vocabularies
	#                 tips += (
	#                     'Click &nbsp;<img width="{0}" height="{0}" src="qrc:/images/shared.png">&nbsp; to download from shared source<br/>'
	#                     '<b>or</b> click &nbsp;<img width="{0}" height="{0}" src="qrc:/images/add.png">&nbsp; to create your own'
	#                     .format(int(3 * view.physicalDpiX() / 25.4))
	#                 )
	#             state_model.tips = tips

	""" View's event handlers """

	def __on_add_button_clicked(self, event):
		# Get path of selected or first top-level directory
		entry = self._view.tree.currentItem() or self._view.tree.topLevelItem(0)

		result = self._show_add_dialog()
		if result is not None:
			# Create new item
			base_name = 'New shared storage'
			for index in range(9999):
				name = base_name + (' #{}'.format(index) if index else '')
				if any([x.name == name for x in self.data_model]):
					continue
				break

			item = ObservableAttrDict(name=name, shared_id=result.shared_id)
			self.data_model.add(item)

			self._item_to_edit = item  # Activate edition of its entry after refilling tree items

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Action Create ' + self.__class__.__name__, label=repr(item)))

	@classmethod
	def _show_add_dialog(cls, **kwargs):
		class _State(ObservableAttrDict):
			pass

		class _AddSharedVocabulariesStorageDialog(ControllerMixture):
			def __init__(self, shared_id=''):
				ControllerMixture.__init__(self)

				state_model = self.state_model
				state_model.shared_id = shared_id
				state_model.shared_id_is_correct = False
				state_model.tips = ''

				self._view = dialog = cls._load_ui('views/add_shared_vocabularies_storage_dialog.ui', widget_name='QDialog')
				if hasattr(QtCore.Qt, 'MaximizeUsingFullscreenGeometryHint'):  # In PyQt5
					dialog.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)

				# dialog.keyPressEvent = self.__on_key_pressed
				dialog.keyPressEvent = (lambda previous_callback: (lambda event: (self.__on_key_pressed(event, previous_callback))))(dialog.keyPressEvent)
				# dialog.shared_id_entry.textChanged.connect(self.__shared_id_entry_text_changed)
				dialog.shared_id_entry.textEdited.connect(self.__shared_id_entry_text_edited)

				movie = QtGui.QMovie(':images/google-drive-example.gif')
				dialog.example_label.setMovie(movie)
				movie.start()

				""" Observe models by view """
				state_model.changed.bind(self._on_model_updated, invoke_in_main_thread=True)  # Invoke in main because the progress is running from thread

				""" Fill blank view by models """
				self._on_model_updated(state_model)

			""" Model's event handlers """

			def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
				# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
				# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

				ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)

				state_model = self.state_model
				view = self._view

				if model is state_model:
					if current[0] is None or 'shared_id' in current[0] and previous[0] != current[0]:
						if str(view.shared_id_entry.text()) != state_model.shared_id:
							view.shared_id_entry.setText(state_model.shared_id)
						state_model.shared_id_is_correct = False
						Caller.call_once_after(.5, self._check_shared_id)

					if current[0] is None or 'shared_id_is_correct' in current[0] and previous[0] != current[0]:
						view.ok_button.setEnabled(state_model.shared_id_is_correct)

					if current[0] is None or 'tips' in current[0] and previous[0] != current[0]:
						getattr(view.tips_widget, 'show' if state_model.tips else 'hide')()
						if hasattr(view.tips_label, 'setHtml'):  # If QWebView
							view.tips_label.setHtml(str(state_model.tips))
						else:
							view.tips_label.setText(state_model.tips.replace('qrc:', ':'))

			""" View's event handlers """

			def __on_key_pressed(self, event, previous_callback):
				# Ignores return-key in order to prevent from closing dialog
				if event.key() in (QtCore.Qt.Key_Enter, QtCore.Qt.Key_Return):
					pass
				else:
					previous_callback(event)

			def __shared_id_entry_text_edited(self, text):
				self.state_model.shared_id = str(text)

			""" Helpers """

			def show(self):
				return self._view.exec_()

			def _check_shared_id(self):
				# def _check_shared_id():
				state_model = self.state_model

				tips = ''
				if state_model.shared_id:
					try:
						listing = Network.get_shared_listing(shared_id=state_model.shared_id, cache_interval=60, raise_exceptions=True)
					except Exception as e:
						if 'No connection?' in str(e):
							tips += 'Check your Internet connection'
					else:
						state_model.shared_id_is_correct = True

		dialog = _AddSharedVocabulariesStorageDialog(**kwargs)
		status = dialog.show()
		if status == QtWidgets.QDialog.Accepted:
			return dialog.state_model

	def __on_proceed_button_clicked(self, event):
		# Get selected item
		entry = self._view.tree.currentItem()
		data = self._entries_to_data[HashableQItem(entry)]

		from models.data import _SharedVocabularies
		data_model = _SharedVocabularies(shared_id=data.get('shared_id', None))

		Caller.call_once_after(.1, data_model.preload_root, repeat=2.)

		SharedVocabulariesDialog(
			headers=['Shared vocabularies'],
			settings_model=self.settings_model,
			data_model=data_model,
			is_container_selectable=False,
			# has_multiselection=True,
			preloading=lambda item: (data_model.preload_children(item)),
		).show()

	""" Helpers """


class VocabulariesDialog(CommonTreeDialog):
	def __init__(self, *args, **kwargs):
		CommonTreeDialog.__init__(self, *args, with_add_button=True, with_delete_button=True, with_shared_button=True, **kwargs)

		self._view.shared_button.clicked.connect(self.__on_shared_button_clicked)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug(self.__class__, object.__repr__(model), previous and previous, current and current)

		super(VocabulariesDialog, self)._on_model_updated(model, previous, current)

		data_model = self.data_model
		state_model = self.state_model
		view = self._view

		if model is data_model:
			if current[0] is None:  # Only once when view is being filled
				tips = ''
				if not data_model:
					# Show tip: how to add vocabularies
					tips += (
						'Click &nbsp;<img width="{0}" height="{0}" src="qrc:/images/shared.png">&nbsp; to download from my/your shared storages (Google Drive)<br/>'
						'<b>or</b> click &nbsp;<img width="{0}" height="{0}" src="qrc:/images/add.png">&nbsp; to create your own'
						.format(int(3 * view.physicalDpiX() / 25.4))
					)
				state_model.tips = tips

	""" View's event handlers """

	# def __on_shared_button_clicked(self, event):

	#     from models.data import _SharedVocabularies
	#     data_model = _SharedVocabularies()

	#     Caller.call_once_after(.1, data_model.preload_root, repeat=2.)

	#     SharedVocabulariesDialog(
	#         headers=['Shared vocabularies'],
	#         settings_model=self.settings_model,
	#         data_model=data_model,
	#         is_container_selectable=False,
	#         # has_multiselection=True,
	#         preloading=lambda item: (data_model.preload_children(item)),
	#     ).show()

	#     self.data_model.refill()  # Reload the list of local vocabularies

	def __on_shared_button_clicked(self, event):
		def _renamed(item, name):
			if item.name:
				item.name = name

		SharedVocabulariesStoragesDialog(
			headers=['Shared storages of vocabularies'],
			settings_model=self.settings_model,
			data_model=self.settings_model.shared_vocabularies_storages,
			path_attribute='name',
			is_container_selectable=False,
			renamed=_renamed,
		).show()

		# self.data_model.refill()  # Reload the list of local vocabularies
		Caller.call_once_after(.5, self.data_model.refill)  # Reload the list of local vocabularies

	""" Helpers """


class SharedVocabulariesDialog(CommonSharedTreeDialog):
	pass


class DictionariesDialog(CommonTreeDialog):
	def __init__(self, *args, **kwargs):
		CommonTreeDialog.__init__(self, *args, with_delete_button=True, with_shared_button=True, **kwargs)

		self._view.shared_button.clicked.connect(self.__on_shared_button_clicked)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug(self.__class__, object.__repr__(model), previous and previous, current and current)

		super(DictionariesDialog, self)._on_model_updated(model, previous, current)

		data_model = self.data_model
		state_model = self.state_model
		view = self._view

		if model is data_model:
			if current[0] is None:  # Only once when view is being filled
				tips = ''
				if not data_model:
					# Show tip: how to add dictionaries
					tips += (
						'Click &nbsp;<img width="{0}" height="{0}" src="qrc:/images/shared.png">&nbsp; to download from shared source<br/>'
						'<b>or</b> copy your own set into:<br/>'
						'<font size="-1">{1}</font>'
						.format(int(3 * view.physicalDpiX() / 25.4), os.getcwd() + os.sep + 'dictionaries')
					)
				state_model.tips = tips

	""" View's event handlers """

	def __on_shared_button_clicked(self, event):

		from models.data import _SharedDictionaries
		data_model = _SharedDictionaries()

		Caller.call_once_after(.1, data_model.preload_root, repeat=2.)

		SharedDictionariesDialog(
			headers=['Shared dictionaries'],
			settings_model=self.settings_model,
			data_model=data_model,
			is_container_selectable=False,
			# has_multiselection=True,
			preloading=lambda item: (data_model.preload_children(item)),
		).show()

		self.data_model.refill()  # Reload the list of local dictionaries

	""" Helpers """


class SharedDictionariesDialog(CommonSharedTreeDialog):
	pass


class BackgroundsCategoriesDialog(CommonTreeDialog):
	def __init__(self, *args, **kwargs):
		CommonTreeDialog.__init__(self, *args, with_delete_button=True, with_shared_button=True, **kwargs)

		self._view.shared_button.clicked.connect(self.__on_shared_button_clicked)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug(self.__class__, object.__repr__(model), previous and previous, current and current)

		super(BackgroundsCategoriesDialog, self)._on_model_updated(model, previous, current)

		data_model = self.data_model
		state_model = self.state_model
		view = self._view

		if model is data_model:
			if current[0] is None:  # Only once when view is being filled
				tips = ''
				if not data_model:
					# Show tip: how to add vocabularies
					tips += (
						'Click &nbsp;<img width="{0}" height="{0}" src="qrc:/images/shared.png">&nbsp; to download from shared source<br/>'
						'<b>or</b> copy your own set into:<br/>'
						'<font size="-1">{1}</font>'
						.format(int(3 * view.physicalDpiX() / 25.4), os.getcwd() + os.sep + 'backgrounds')
					)
				state_model.tips = tips

	""" View's event handlers """

	def __on_shared_button_clicked(self, event):

		from models.data import _SharedBackgroundsCategories
		data_model = _SharedBackgroundsCategories()

		Caller.call_once_after(.1, data_model.preload_root, repeat=2.)

		SharedBackgroundsCategoriesDialog(
			headers=['Shared backgrounds'],
			settings_model=self.settings_model,
			data_model=data_model,
			is_container_selectable=False,
			# has_multiselection=True,
			preloading=lambda item: (data_model.preload_children(item)),
		).show()

		self.data_model.refill()  # Reload the list of local backgrounds categories

	""" Helpers """


class SharedBackgroundsCategoriesDialog(CommonSharedTreeDialog):
	pass


class CommonSettingsMixture(object):
	def __init__(self):

		# Views
		self.dialog = view = self._load_ui('views/common_settings_dialog.ui', widget_name='QDialog')
		if hasattr(QtCore.Qt, 'MaximizeUsingFullscreenGeometryHint'):  # In PyQt5
			view.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)

		view.unfreeze_button.clicked.connect(self.__on_unfreeze_button_clicked)
		view.show_banner_after_button.clicked.connect(self.__on_show_banner_after_button_clicked)
		view.font_button.clicked.connect(self.__on_font_button_clicked)
		view.button_size_dropdown.activated.connect(self.__on_button_size_dropdown_changed)
		view.close_on_double_back_checkbox.stateChanged.connect(self.__on_close_on_double_back_checkbox_changed)
		view.info_button.clicked.connect(self.__on_info_button_clicked)
		view.release_notes_button.clicked.connect(self.__on_release_notes_button_clicked)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug(self.__class__, object.__repr__(model), previous and previous, current and current)

		settings_model = self.settings_model
		view = self.dialog

		if model is settings_model:
			if current[0] is None or 'font' in current[0] and previous[0] != current[0]:
				font = settings_model.font
				if font is not None:
					from styles import styles
					styles.replace(view.window(), {'* {font: ': self._font_to_style(font)[7:]})
					view.font_button.setText(self._font_to_text(font))

			if current[0] is None or 'button_size' in current[0] and previous[0] != current[0]:
				view.button_size_dropdown.setCurrentIndex(view.button_size_dropdown.findText(str(settings_model.button_size)))

			if current[0] is None or 'close_on_double_back' in current[0] and previous[0] != current[0]:
				view.close_on_double_back_checkbox.setChecked(settings_model.close_on_double_back)

	""" View's event handlers """

	def __on_unfreeze_button_clicked(self):
		self.settings_model.unfreeze = True

	def __on_show_banner_after_button_clicked(self):
		self.settings_model.show_banner_after = 300.

	def __on_info_button_clicked(self):
		from controllers.info import InfoController
		InfoController(settings_model=self.settings_model).show()

	def __on_release_notes_button_clicked(self):
		from controllers.release_notes import ReleaseNotesController
		ReleaseNotesController(settings_model=self.settings_model).show()

	def __on_font_button_clicked(self):
		self._show_font_dialog(model=self.settings_model, key='font')

	def __on_button_size_dropdown_changed(self, current_index):
		self.settings_model.button_size = int(self.dialog.button_size_dropdown.currentText())

	def __on_close_on_double_back_checkbox_changed(self):
		self.settings_model.close_on_double_back = self.dialog.close_on_double_back_checkbox.isChecked()

	""" Helpers """

	def show(self):
		self.dialog.exec_()


class Network(object):
	from urllib3 import exceptions

	# urllib2.install_opener(urllib2.build_opener(urllib2.HTTPHandler(debuglevel=1)))  # urllib2 verbose
	# httplib.HTTPConnection.debuglevel = 1  # urllib3 verbose
	# pool = urllib3.PoolManager(timeout=5., **(dict(ca_certs=cls._find_file(sys.path, '/ca-certificates.crt')) if sys.platform == 'android' else dict()))
	# pool = urllib3.PoolManager(timeout=5.)
	# pool = urllib3.PoolManager(timeout=5., cert_reqs=ssl.CERT_OPTIONAL)
	pool = urllib3.PoolManager(timeout=5., cert_reqs=ssl.CERT_NONE)

	_ga_resource_id = 'UA-42404419-3'

	_previous_session = None
	# _ip = None

	@classmethod
	def send_for_google_analytics(cls, settings_model, session=None, event=None, warning=None, exception=None):
		google_analytics_enabled = not os.path.exists(os.path.join(sys.path[0], 'disable_google_analytics'))
		if not google_analytics_enabled:
			return

		if cls._ga_resource_id is None:
			raise Exception('Google Analytics resource id is not set.')

		# Resolve client's external IP
		# if cls._ip is None:
		#     s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		#     s.connect(('8.8.8.8', 53))
		#     cls._ip = s.getsockname()[0]
		#     s.close()

		# Description of the protocol: https://developers.google.com/analytics/devguides/vocabulary/protocol/v1/parameters?hl=ru
		query = '&'.join((
			't=' + ('exception' if warning is not None or exception is not None else ('event' if event is not None else 'screenview')),  # [pageview, screenview, event, transaction, item, social, exception, timing]
			# 't=' + ('exception' if warning is not None or exception is not None else ('event' if event is not None else 'pageview')),  # [pageview, screenview, event, transaction, item, social, exception, timing]
			('sc=' + session['value']) if session is not None and cls._previous_session is None else '',  # session control [, start, end]
			('cd=' + session['key']) if session is not None else '',  # screen location (for screenview)
			# ('dh=myapp') if session is not None else '',  # screen location (for screenview)
			# ('dp=' + session['key']) if session is not None else '',  # screen location (for screenview)
			('linkid=' + cls._previous_session['key']) if session is not None and cls._previous_session is not None else '',  # previous location
			('ec=' + urllib.parse.quote(str(event['category']))) if event is not None else '',  # event category
			('ea=' + urllib.parse.quote(str(event['action']))) if event is not None else '',  # event action
			('el=' + urllib.parse.quote(str(event['label']))) if event is not None and 'label' in event else '',  # event label
			('ev=' + urllib.parse.quote(str(event['value']))) if event is not None and 'value' in event else '',  # event value
			'v=1',  # GA-protocol version
			'tid=' + cls._ga_resource_id,
			'ds=application',  # data source, for ex.: web, app, call center
			'cid=' + urllib.parse.quote(settings_model.setdefault('user-id', lambda: (str(uuid.uuid1())))),  # unique client id (from cookies)
			# 'uid=' + urllib.parse.quote(settings_model.setdefault('user-id', lambda: (str(uuid.uuid1())))),  # client name
			# 'uip=' + cls._ip,  # user ip
			'dr=' + urllib.parse.quote(settings_model.get('referrer', ''), safe=''),  # referrer url
			'cs=' + urllib.parse.quote(settings_model.get('referrer_source', ''), safe=''),  # referrer source
			'ci=' + urllib.parse.quote(settings_model.get('referrer_campaign_id', ''), safe=''),  # referrer campaign id
			# 'cn=' + urllib.parse.quote(settings_model.get('referrer-campaign-name', ''), safe=''),  # referrer campaign name
			# 'cm=' + urllib.parse.quote(settings_model.get('referrer-campaign-channel', ''), safe=''),  # referrer campaign channel
			'ck=' + urllib.parse.quote(settings_model.get('referrer_campaign_keywords', ''), safe=''),  # referrer campaign keywords
			# 'cc=' + urllib.parse.quote(settings_model.get('referrer_campaign_content', ''), safe=''),  # referrer campaign content
			'sr={0}x{1}'.format(QtWidgets.QDesktopWidget().screenGeometry().width(), QtWidgets.QDesktopWidget().screenGeometry().height()),
			'ul=' + str(QtCore.QLocale.languageToString(QtCore.QLocale.system().language())),
			'ni=0',  # not interaction
			# 'dl=' + key,  # document location
			# 'dh=',  # document host
			# 'dp=' + key,  # document path
			# 'dt=' + key,  # document title
			'an=' + urllib.parse.quote(os.environ.get('APP_NAME', ''), safe=''),
			'aid=' + urllib.parse.quote(os.environ.get('PACKAGE', ''), safe=''),
			'av=' + os.environ.get('VERSION', ''),
			# 'aiid=',  # installer name
			# 'ti=',  # transaction id
			# 'ta=',  # transaction affiliate
			# 'tr=',  # transaction rate
			# ...
			('plt={:d}'.format(int(session['loading_time'] * 1000))) if session is not None and session.get('loading_time', False) else '',  # page loading time (ms)
			# ...
			('exd=' + urllib.parse.quote(warning or exception, safe='') if warning is not None or exception is not None else ''),  # exception description
			('exf=' + ('0' if warning is not None else '1') if warning is not None or exception is not None else ''),  # exception unrecoverable [0, 1]
			'cd1=' + urllib.parse.quote(settings_model.setdefault('from_version', os.environ.get('VERSION', ''))),  # custom parameter 1, max 20
			'cd2=' + urllib.parse.quote(settings_model.setdefault('theme', 'LightTheme')),  # custom parameter 2
			# 'cm1=',  # custom value 1 [1..200]
			# 'xid=',  # experiment id (always with var)
			# 'xvar=',  # experiment var
		))

		if session is not None:
			cls._previous_session = session

		logging.getLogger(__name__).info(FG.CYAN + '--- Sending to GA ---' + FG.RESET)

		def send():
			time.sleep(.010)  # Write in log a little bit later in order not to mix logs
			try:
				# Check request for errors
				if settings_model.get('DEBUG_GOOGLE_ANALYTICS_REQUESTS', False):
					# response = urllib2.urlopen('http://www.google-analytics.com/debug/collect?' + query)
					# message = response.read()
					response = cls.pool.request('GET', 'http://www.google-analytics.com/debug/collect?' + query)
					message = response.data
					if '"valid": false' in message:
						raise Exception('Wrong GA-Request. Query="{}". Response="{}"'.format(*[x.replace(cls._ga_resource_id, 'xxxxxx') for x in (query, message)]))

				# Send statistics to GA
				# urllib2.urlopen(urllib2.Request(url='http://www.google-analytics.com/collect?' + query, data=None, headers={'User-agent': os.environ.get('USER_AGENT', urllib2.URLopener.version)} if 'USER_AGENT' in os.environ else {}))
				cls.pool.request('GET', 'http://www.google-analytics.com/collect?' + query)

			except IOError:  # Skip if no connection
				pass

			except Network.exceptions.MaxRetryError as e:
				logging.getLogger(__name__).warning('%s', e)
				pass

		# thread = elf.make_breakpoint(threading.Thread(target=send))
		thread = threading.Thread(target=send)
		thread.setDaemon(True)
		thread.start()

	@classmethod
	def send_postback_to_referrer(cls, settings_model, referrer):
		referrer = referrer.replace('&equals;', '=')
		# referrer = "source=gowide.com&campaign=testcamp&keywords=testkeys&transaction=testtrans"
		logging.getLogger(__name__).debug('Referrer is %s', referrer)

		if referrer and not settings_model.get('postback_is_sent', False):
			settings_model.postback_is_sent = True

			logging.getLogger(__name__).info('Preparing postback...')
			settings_model.referrer = referrer
			try:
				source, campaign, keywords, transaction = parse.parse('source={}&campaign={}&keywords={}&transaction={}', referrer)
			except TypeError:
				logging.getLogger(__name__).warning('Can not parse referrer')
			else:
				settings_model.referrer_source = source
				settings_model.referrer_campaign_id = campaign
				settings_model.referrer_campaign_keywords = keywords
				logging.getLogger(__name__).debug('Source is %s', source)

				url = None
				if source == 'gowide.com':
					url = 'http://track.comboapp.com/aff_lsr?security_token=a947eeb881ab21e8752384fdbde7ba2c&transaction_id={}'.format(transaction)

				try:
					from helpers import sh
					url = sh.do(source, cls.pool, cls._shared_download_url, url)
				except ImportError:
					pass

				logging.getLogger(__name__).debug('Postback URL is %s', url)
				if url is not None and url:
					def send():
						try:
							# urllib2.urlopen(url)
							cls.pool.request('GET', url)
							# response = urllib2.urlopen(url)
							# logging.getLogger(__name__).debug('Response: %s', response.info())
						except IOError:
							pass

					# thread = elf.make_breakpoint(threading.Thread(target=send))
					thread = threading.Thread(target=send)
					thread.setDaemon(True)
					thread.start()

	@classmethod
	def _send_logs_to_pastebin(cls):
		# import urllib

		with open('native.txt') as f:
			log = f.read()

		post_data = dict(
			api_dev_key='8ade3b45a044fc537f4e59e01d04afa3',
			api_user_key='gehrmann',
			api_option='paste',
			api_paste_private='1',  # 0 - public, 1 - unlisted, 2 - private
			api_paste_format='text',
			api_paste_expire_date='10M',  # 10M - 10 minutes, 1H - 1 hour
			api_paste_name='Logs from ' + datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3],
			api_paste_code=log,
		)

		message = None
		try:
			response = cls.pool.request('POST', 'http://pastebin.com/api/api_post.php', fields=post_data)
			message = response.data.replace('http://pastebin.com/', '')
		except Exception as e:
			logging.getLogger(__name__).warning('Can not send to pastebin: %s', e)

		return message

	# googledrive.com/host/<page_id>/


	_shared_listing_url = "http://drive.google.com/embeddedfolderview?id={shared_id}"
	# _shared_listing_url = "http://drive.google.comp/embeddedfolderview?id={shared_id}"


	_shared_download_url = "http://drive.google.com/uc?id={shared_id}"

	_shared_listing_cache = dict()

	@classmethod
	def get_shared_listing(cls, shared_id, cache_interval=None, raise_exceptions=False):

		# if True:  # Test for SSL-module and send error if something occurs
		#     try:
		#         import _ssl
		#         import ssl
		#     except (ImportError, AttributeError):
		#         sys.excepthook(*sys.exc_info())
		#         if hasattr(sys, 'exception_callback'):
		#             sys.exception_callback()

		if cache_interval is not None and shared_id in cls._shared_listing_cache and cls._shared_listing_cache[shared_id].updated + cache_interval > time.time():
			listing = cls._shared_listing_cache[shared_id].listing
		else:
			try:
				prefix = 'https://drive.google.com/drive/folders/'
				if shared_id.startswith(prefix):
					shared_id = shared_id[len(prefix):].replace('?', '&')
				# if 'drive.google.com' in shared_id:
				#     shared_id
				#     for x in re.findall(r'<a href="https://drive.google.com/(drive/folders|file/d)/([^"]+).*?flip-entry-title">([^<]+)', response_data)

				url = cls._shared_listing_url.format(shared_id=shared_id)
				logging.getLogger(__name__).warning('url=' + '%s', url)
				response = Network.pool.request('GET', url)
				response_data = response.data.decode('UTF-8')
			except (IOError, Network.exceptions.ConnectTimeoutError, Network.exceptions.MaxRetryError, Network.exceptions.ReadTimeoutError) as e:
				logging.getLogger(__name__).warning('Can not get shared listing: %s', e)
				if raise_exceptions:
					# raise e.__class__, str(e) + ' [DEBUG: {}]'.format(dict(cause='No connection?')), sys.exc_info()[2]
					# raise (Exception, '{}: {} [DEBUG: {}]'.format(e.__class__, e, dict(cause='No connection?')), sys.exc_info()[2])
					# raise Exception, '{}: {} [DEBUG: {}]'.format(e.__class__, e, dict(cause='No connection?')), sys.exc_info()[2]
					# raise e.__class__('{}: {} [DEBUG: {}]'.format(e.__class__, e, dict(cause='No connection?'))) from e
					raise Exception(str(e) + '{}: {} [DEBUG: {}]'.format(e.__class__, e, dict(cause='No connection?'))) from e
					# raise
				listing = None
			else:
				# if '<div class="flip-entries">' not in response_data:
				if response.status != 200:
					try:
						tmp, message, tmp = parse.parse('{}<title>{}</title>{}', response_data)
					except:
						message = 'unknown'
					logging.getLogger(__name__).warning('Response error: %s', message)
					if raise_exceptions:
						raise Exception(message)
					listing = None
				else:
					listing = [
						[x[1].replace('?', '&').replace('/view&usp=drive_web&amp;', '&'), x[2].replace('&lt;', '<').replace('&gt;', '>').replace('&#39;', '\'')]
						for x in re.findall(r'<a href="https://drive.google.com/(drive/folders|file/d)/([^"]+).*?flip-entry-title">([^<]+)', response_data)
					]

					# Marks title with leading '×' if bad url (does not contain resourcekey)
					for data in listing:
						if '&resourcekey=' not in data[0]:
							data[1] = '× ' + data[1]

					# logging.getLogger(__name__).warning('data=' + '%s', data)
					# logging.getLogger(__name__).warning('listing=' + '%s', listing)
					# if not listing:
					# logging.getLogger(__name__).warning('response_data=' + '%s', response_data)

					cls._shared_listing_cache[shared_id] = AttrDict(listing=listing, updated=time.time())
		return listing

	@classmethod
	def download_shared_file(cls, shared_id, dst_path, headers=None, autocreate_directories=False, replace=False, raise_exceptions=True):
		url = cls._shared_download_url.format(shared_id=shared_id)
		logging.getLogger(__name__).warning('url=' + '%s', url)

		if dst_path.__class__ in (tuple, list):
			dst_path = os.path.join(*dst_path)

		if replace:
			try:
				os.unlink(dst_path)
			except OSError:
				pass

		if autocreate_directories:
			try:
				os.makedirs(os.path.join(*os.path.split(dst_path)[:-1]))
			except OSError:
				pass

		# try:
		#     yield 'Downloading {}'.format(dst_path)
		#     # wget.download(url=url, out=dst_path, bar=lambda *args, **kwargs: (None))  # Fetch
		#     urllib.urlretrieve(url=url, filename=dst_path)
		#     yield 'Done'
		# except IOError as e:
		#     yield 'Exception: {}'.format(e)
		#     if raise_exceptions:
		#         raise
		#     logging.getLogger(__name__).warning('Error to download shared resource: %s', e)
		yield 'Downloading {}'.format(dst_path)
		try:
			response = Network.pool.request('GET', url, preload_content=False, headers=headers if headers is not None else None)
		except (IOError, Network.exceptions.ConnectTimeoutError, Network.exceptions.MaxRetryError, Network.exceptions.ReadTimeoutError) as e:
			logging.getLogger(__name__).warning('Can not download file: %s', e)
			yield 'Exception: {}'.format(e)
			if raise_exceptions:
				# raise e.__class__, str(e) + ' [DEBUG: {}]'.format(dict(cause='No connection?')), sys.exc_info()[2]
				# raise (Exception, '{}: {} [DEBUG: {}]'.format(e.__class__, e, dict(url=url, cause='No connection?')), sys.exc_info()[2])
				# raise Exception, '{}: {} [DEBUG: {}]'.format(e.__class__, e, dict(url=url, cause='No connection?')), sys.exc_info()[2]
				raise Exception(str(e) + '{}: {} [DEBUG: {}]'.format(e.__class__, e, dict(url=url, cause='No connection?'))) from e
				# raise
		else:
			if response.status != 200:
				try:
					tmp, message, tmp = parse.parse('{}<title>{}</title>{}', response_data)
				except:
					message = 'unknown'
				e = Exception(message)
				yield 'Exception: {}'.format(e)
				if raise_exceptions:
					raise e
				logging.getLogger(__name__).warning('Error to download shared resource: %s', e)

				yield 'Failed'
			else:

				try:
					# If HTML found in response
					if 'text/html' in response.getheader('content-type'):

						# Downloads data
						data = []
						while True:
							chunk = response.read(1024 * 1024)
							if not chunk:
								break
							data.append(chunk)
						data = ''.join(data)

						# Looks for redirection link
						for url in re.findall(r'<a id="uc-download-link" .*? href="(.*?)">', data):
							additional_query_parameters = ''.join(''.join(url.replace('&amp;', '&').split('?')[1:]).split('&id=' + shared_id)[:])

							# Repeats request with additional parameters
							for message in cls.download_shared_file(
									shared_id=(shared_id + '&' + additional_query_parameters),
									dst_path=dst_path,
									headers={'cookie': response.getheader('set-cookie')},
									autocreate_directories=autocreate_directories,
									replace=replace,
									raise_exceptions=raise_exceptions,
							):
								yield message

							break

						# If no redirection links found
						else:
							if raise_exceptions:
								raise Exception('Found HTML in response, but redirection link was not found')
							logging.getLogger(__name__).warning('Found HTML instead of file content')

					else:
						with open(dst_path, 'wb') as dst:
							while True:
								chunk = response.read(1024 * 1024)
								if not chunk:
									break
								dst.write(chunk)

						yield 'Done'

				finally:
					response.release_conn()

	@classmethod
	def download_shared_directory(cls, shared_id, dst_path, autocreate_directories=False, replace=False, raise_exceptions=True):
		items = cls.get_shared_listing(shared_id=shared_id)
		logging.getLogger(__name__).warning('items=' + '%s', items)
		if items is not None:
			try:
				for index, (id, title) in enumerate(items):
					# yield 'Downloading...\n{} of {}'.format(index, len(items))
					yield Progress('Downloading...', thread=threading.current_thread(), done=index, total=len(items))
					for progress in cls.download_shared_file(shared_id=id, dst_path=dst_path + os.sep + title, autocreate_directories=autocreate_directories, replace=replace):  # Fetch
						pass
				yield 'Done'
			except IOError as e:
				yield 'Exception: {}'.format(str(e))
				if raise_exceptions:
					raise
				logging.getLogger(__name__).warning('Error to download shared directory: %s', e)


def run_show_vocabularies_dialog():
	# from models.abstract import ObservableAttrDict, ObservableSet
	# settings_model = ObservableAttrDict(
	#     shared_vocabularies_storages=ObservableSet((
	#         ObservableAttrDict(name=1, path=2),
	#     )),
	# )
	os.chdir(os.environ['HOME'] + '/Vocabulary Trainer')

	from models.data import _Vocabularies
	data_model = _Vocabularies()
	# state_model = ObservableAttrDict(
	#     vocabulary=None,
	# )

	def on_vocabulary_renamed(item, name):
		item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	from models.settings import Settings
	settings_model = Settings()

	VocabulariesDialog(
		headers=['Vocabulary'],
		settings_model=settings_model,
		data_model=data_model,
		path_attribute='path',
		path_prefix='vocabularies/',
		is_expanded=True,
		is_container_selectable=False,


		has_multiselection=True,


		renamed=on_vocabulary_renamed,
	).show()


def run_show_shared_vocabularies_storages_dialog():
	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	from models.settings import Settings
	settings_model = Settings()

	SharedVocabulariesStoragesDialog(
		headers=['Shared storages of vocabularies'],
		settings_model=settings_model,
		data_model=settings_model.shared_vocabularies_storages,
		path_attribute='name',
		renamed=lambda item, value: (None),
	).show()



def run_show_add_shared_vocabularies_storage_dialog():
	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	from models.settings import Settings
	settings_model = Settings()

	result = SharedVocabulariesStoragesDialog._show_add_dialog(
		# shared_id='0B_7-aHx_p3QTaUkzaTJyOTNQUWs-',
	)
	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'result=', result, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented


def run_show_dictionaries_dialog():
	from models.abstract import ObservableAttrDict
	settings_model = ObservableAttrDict(
	)
	from models.data import _Dictionaries
	data_model = _Dictionaries()
	# state_model = ObservableAttrDict(
	#     dictionaries=[],
	# )

	def on_dictionary_renamed(item, name):
		item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	DictionariesDialog(
		headers=['Dictionary'],
		settings_model=settings_model,
		data_model=data_model,
		path_attribute='decorated_path',
		path_separator='\\',  # Disable splitting by '/'
		path_prefix='dictionaries/',
		is_expanded=True,
		is_container_selectable=False,
		has_multiselection=True,
		# renamed=on_dictionary_renamed,
	).show()


def run_show_backgrounds_categories_dialog():
	from models.abstract import ObservableAttrDict
	settings_model = ObservableAttrDict(
	)
	from models.data import _BackgroundsCategories
	data_model = _BackgroundsCategories()
	# state_model = ObservableAttrDict(
	#     backgrounds_categories=[],
	# )

	def on_backgrounds_category_renamed(item, name):
		item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	BackgroundsCategoriesDialog(
		headers=['Backgrounds'],
		settings_model=settings_model,
		data_model=data_model,
		path_attribute='path',
		# path_separator='\\',  # Disable splitting by '/'
		path_prefix='backgrounds/',
		is_expanded=True,
		is_container_selectable=False,
		has_multiselection=True,
		# renamed=on_backgrounds_category_renamed,
	).show()


def run_show_shared_vocabularies_dialog():
	from models.abstract import ObservableAttrDict
	settings_model = ObservableAttrDict(
	)
	# data_model = [
	#     'abc/def',
	#     'xxx/',
	# ]
	from models.data import _SharedVocabularies
	data_model = _SharedVocabularies()

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	Caller.call_once_after(.2, data_model.preload_root, repeat=2.)

	SharedVocabulariesDialog(
		headers=['Shared vocabularies'],
		settings_model=settings_model,
		data_model=data_model,
		is_container_selectable=False,
		# has_multiselection=True,
		preloading=lambda item: (data_model.preload_children(item)),
	).show()


def run_show_shared_dictionaries_dialog():
	from models.abstract import ObservableAttrDict
	settings_model = ObservableAttrDict(
	)
	# # data_model = [
	# #     'abc/def',
	# #     'xxx/',
	# # ]
	from models.data import _SharedDictionaries
	data_model = _SharedDictionaries()

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	Caller.call_once_after(.2, data_model.preload_root, repeat=2.)

	SharedDictionariesDialog(
		headers=['Shared dictionaries'],
		settings_model=settings_model,
		data_model=data_model,
		is_container_selectable=False,
		# has_multiselection=True,
		preloading=lambda item: (data_model.preload_children(item)),
	).show()


def run_show_shared_backgrounds_categories_dialog():
	from models.abstract import ObservableAttrDict
	settings_model = ObservableAttrDict(
	)
	# # data_model = [
	# #     'abc/def',
	# #     'xxx/',
	# # ]
	from models.data import _SharedBackgroundsCategories
	data_model = _SharedBackgroundsCategories()

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	Caller.call_once_after(.2, data_model.preload_root, repeat=2.)

	SharedBackgroundsCategoriesDialog(
		headers=['Shared backgrounds'],
		settings_model=settings_model,
		data_model=data_model,
		is_container_selectable=False,
		# has_multiselection=True,
		preloading=lambda item: (data_model.preload_children(item)),
	).show()


def run_show_font_dialog():
	from models.abstract import ObservableAttrDict

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	settings_model = ObservableAttrDict(
		font=dict(family='DejaVu Sans', size=.75 * styles.mm2px(2.6), weight=12, italic=False),
	)

	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'settings_model=', settings_model, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
	ControllerMixture._show_font_dialog(model=settings_model, key='font')
	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'settings_model=', settings_model, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented


def run_progress():
	from models.abstract import ObservableAttrDict
	settings_model = ObservableAttrDict(
	)
	data_model = ObservableAttrDict()

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	def _show_progress():
		dialog = QtWidgets.QProgressDialog('Copying', 'Abort copy', 0, 10)
		dialog.setWindowModality(QtCore.Qt.WindowModal)
		dialog.forceShow()
		for i in range(10):
			logging.getLogger(__name__).debug('i=%s', i)
			dialog.setValue(i)
			time.sleep(1)

	Caller.call_once_after(.2, _show_progress)

	SharedDictionariesDialog(
		headers=['Shared dictionaries'],
		data_model=data_model,
		is_container_selectable=False,
		# has_multiselection=True,
		preloading=lambda item: (data_model.preload_children(item)),
	).show()


def run_balloon():
	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	# QtWidgets.QBalloonTip.balloon()
	QtWidgets.QToolTip.showText(QtCore.QPoint(0, 0), '<font size="+4">test</font><img src="images/bad-on.png"')

	sys.exit(app.exec_())


def run_tray():
	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	# tray = QtWidgets.QSystemTrayIcon(QtGui.QIcon(':/images/dictionaries.png'), app)
	# # tray_menu = QtWidgets.QMenu()
	# # tray_exit_button = tray_menu.addAction("Exit")
	# # tray.setContextMenu(tray_menu)
	# tray.show()
	# tray.showMessage('abc', 'def')

	# QtWidgets.QMessageBox.critical(QtWidgets.qApp.activeWindow(), 'abc', 'def <b>ghi</b> <img src="qrc:/images/dictionaries.png" />')

	# dialog = QtWidgets.QDialog()
	# QtCore.pyqtRemoveInputHook(); import ipdb; ipdb.set_trace()  # FIXME: must be removed/commented
	# dialog.show()

	# label = QtWidgets.QLabel('test')
	# label.show()

	sys.exit(app.exec_())


def run_message_logger():
	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	QtCore.QMessageLogger().fatal('test')

	sys.exit(app.exec_())


def run_message_logger():
	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	QtCore.QMessageLogger().fatal('test')

	sys.exit(app.exec_())


def run_send_postback_to_referrer():
	from models.abstract import ObservableAttrDict
	settings_model = ObservableAttrDict()
	referrer = 'source&equals;gowide.com&campaign&equals;gowide_1&keywords&equals;no&transaction&equals;{transaction_id}'

	Network.send_postback_to_referrer(settings_model=settings_model, referrer=referrer)
	logging.getLogger(__name__).debug('Settings model: %s', settings_model)


def run_download_shared_file():

	# ID of a large english dictionary thats downloading shows warning
	shared_id = '0B_7-aHx_p3QTdnVMS1p0RXNXOUE'

	# Creates temporary directory
	import tempfile
	dst_directory = tempfile.mkdtemp()
	dst_path = os.path.join(dst_directory, 'file.txt')

	for progress in Network.download_shared_file(shared_id=shared_id, dst_path=dst_path):
		print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'progress=', progress, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

	# Reads content of downloaded file
	with open(dst_path) as src:
		for line in src:
			print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'line=', line, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

	# Removes temporary directory
	import shutil
	shutil.rmtree(dst_directory)


def run_edit_shared_id():
	shared_id = 'https://drive.google.com/drive/folders/1B_CtETEkurN833nNZqBoXyMDA8CwBiAZ?usp=drive_link'
	prefix = 'https://drive.google.com/drive/folders/'
	if shared_id.startswith(prefix):
		shared_id = shared_id[len(prefix):].replace('?', '&')
	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'shared_id:', shared_id, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		# for x in re.findall(r'<a href="https://drive.google.com/(drive/folders|file/d)/([^"]+).*?flip-entry-title">([^<]+)', response_data)


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', required=True, choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
