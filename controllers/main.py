#!/bin/sh

# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides main controller

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import ast
import datetime
import logging
import os
import signal
import sys
import time
import traceback

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.SIG_DFL)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtWidgets  # +200ms

from controllers.abstract import ControllerMixture, Network  # 300ms
# from helpers.my_trace import print_stack
from helpers.caller import Caller
from helpers.timer import Timer
from models.abstract import ObservableAttrDict, ObservableList
from models.data import Data
from models.settings import Settings


class MainController(ControllerMixture):
	def __init__(self):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# def check_organization_domain():
			#     script = QtWidgets.qApp.organizationDomain()
			#     if script:
			#         QtWidgets.qApp.setOrganizationDomain('')
			#         print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "script,", script, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			#     Caller.call_once_after(1., check_organization_domain)
			# check_organization_domain()

			# def test_slot(*args, **kwargs):
			#     print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "test_slot():", args, kwargs, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			# # QtWidgets.qApp.test_slot = QtCore.pyqtSlot(str)(test_slot)
			# QtWidgets.QApplication.test_slot = QtCore.pyqtSlot()(test_slot)
			# QtWidgets.qApp.test_slot = QtCore.pyqtSlot()(test_slot)

			# ->findChild<QPushButton*>("Button name");

			# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "installing native_invoked()" , file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			# def native_invoked(*args, **kwargs):
			#     print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "native_invoked(): ", args, kwargs, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			# QtWidgets.QApplication.native_invoked = native_invoked

			# from PyQt5 import QtBluetooth
			# agent = QtBluetooth.QBluetoothDeviceDiscoveryAgent()
			# def device_discovered(*args, **kwargs): print("device_discovered", args, kwargs, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			# agent.deviceDiscovered.connect(device_discovered)
			# def errored(*args, **kwargs): print("errored", args, kwargs, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			# agent.error.connect(errored)
			# def finished(*args, **kwargs): print("finished", args, kwargs, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			# agent.finished.connect(finished)
			# # QtCore.pyqtRemoveInputHook(); import ipdb; ipdb.set_trace()  # FIXME: must be removed/commented
			# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "starting bluetooth agent...", , file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			# agent.start()
			# print("DONE" , file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			# # print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "agent,", agent, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			# # device.coreConfigurations() & QBluetoothDeviceInfo::LowEnergyCoreConfiguration
			# # device.name()
			# # device.address().toString()
			# def test():
			#     print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "agent.isActive(),", agent.isActive(), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			#     print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "agent.error(),", agent.error(), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			#     print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "agent.errorString(),", agent.errorString(), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			#     Caller.call_once_after(1., test)  # Check if there is some data to convert
			# test()
			# return

			# QAndroidJniObject javaMessage = QAndroidJniObject::fromString(msg);
			# QAndroidJniObject::callStaticMethod<void>("org/qtproject/example/testandroidextras/TestAndroidClient", "test", "(Ljava/lang/String;)V", javaMessage.object<jstring>());

			# Models
			self.data_model = data_model = Data(fill_after_sec=1.)
			# self.data_model = data_model = Data(fill_after_sec=None)
			self.settings_model = settings_model = Settings(path='settings.txt')

			self._banner = None

			# self.with_cols_tab = settings_model.get('with_cols_tab', False) or os.environ.get('with_cols_tab'.upper(), False)

			# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), ''.join(['\n\t{} => {}'.format(x, os.environ[x]) for x in sorted(os.environ.keys())]), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

			sys.displayhook = lambda etype, value, tb: (
				# print_stack(),
				sys.__displayhook__(etype, value, tb),
				Network.send_for_google_analytics(settings_model=settings_model, exception=''.join(["ua={{{}}}\n".format(os.environ.get('USER_AGENT', ''))] + traceback.format_exception(etype, value, tb))),
			)  # On exception: send it to Google Analytics

			sys.excepthook = lambda etype, value, tb: (
				# print_stack(),
				sys.__excepthook__(etype, value, tb),
				Network.send_for_google_analytics(settings_model=settings_model, exception=''.join(["ua={{{}}}\n".format(os.environ.get('USER_AGENT', ''))] + traceback.format_exception(etype, value, tb))),
			)  # On exception: send it to Google Analytics

			QtCore.qInstallMessageHandler(lambda mode, context, message: (
				# print_stack(),
				# sys.stderr.write("QT: {0.file}:{0.line}: [{1}] {2}\n".format(context, {QtCore.QtInfoMsg: 'INFO', QtCore.QtWarningMsg: 'WARNING', QtCore.QtCriticalMsg: 'CRITICAL', QtCore.QtFatalMsg: 'FATAL', QtCore.QtDebugMsg: 'DEBUG'}[mode], message)),
				{QtCore.QtInfoMsg: QtCore.qDebug, QtCore.QtWarningMsg: QtCore.qWarning, QtCore.QtCriticalMsg: QtCore.qCritical, QtCore.QtFatalMsg: QtCore.qFatal, QtCore.QtDebugMsg: QtCore.qDebug}[mode]("QT: {0.file}:{0.line}: [{1}] {2}\n".format(context, mode, message)),
				# sys.stderr.write("QT: {0.file}:{0.line}: [{1}] {2}\n".format(context, , message)),
				Network.send_for_google_analytics(settings_model=settings_model, exception="ua={{{}}}\n".format(os.environ.get('USER_AGENT', '')) + "QT: {0.file}:{0.line}: [{1}] {2}\n".format(context, {QtCore.QtInfoMsg: 'INFO', QtCore.QtWarningMsg: 'WARNING', QtCore.QtCriticalMsg: 'CRITICAL', QtCore.QtFatalMsg: 'FATAL', QtCore.QtDebugMsg: 'DEBUG'}[mode], message)),
				None,
			)[-1])  # On qt-exception: send it to Google Analytics

			self._latest_back_pressed_time = 0

			# Views
			self._view = view = self._load_ui('views/main_view.ui', widget_name='QMainWindow')  # 200ms

			self._restore_window_geometry()
			view.closeEvent = self._on_close
			view.keyPressEvent = self._on_key_pressed
			# view.py_eval_widget.linkActivated.connect(self.__on_py_eval_widget_link_activated)
			view.tabs.currentChanged.connect(self.__on_tab_is_changed)
			# if not self.with_cols_tab:
			#     view.tabs.removeTab(next(i for i in range(view.tabs.count()) if view.tabs.widget(i) == view.cols_tab))
			view.show()  # 50ms

			# DictController.init(parent_view=view.dict_tab.layout(), data_model=data_model)
			# ListController.init(parent_view=view.list_tab.layout(), data_model=data_model)
			# FlashController.init(parent_view=view.flash_tab.layout(), data_model=data_model)
			# PairsController.init(parent_view=view.pairs_tab.layout(), data_model=data_model)
			# CardsController.init(parent_view=view.cards_tab.layout(), data_model=data_model)
			# ColsController.init(parent_view=view.cols_tab.layout(), data_model=data_model)
			# SpeechController.init(parent_view=view.speech_tab.layout(), data_model=data_model)

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)

			self.__init_tabs()

			Caller.call_once_after(.1, self._convert_previous_data)  # Check if there is some data to convert

			self.__start_google_analytics_session_loop()

			Caller.call_once_after(.1, self.__show_release_notes_dialog)
			# Caller.call_once_after(.1, self.__show_first_start_wizard_dialog)
			# Caller.call_once_after(1., self._apply_unfreeze_fix)

			# Caller.call_once_after(.3, self._show_banner)
			Caller.call_once_after(.4, self.__show_interstitial_banner)

			@self.java.called_wrapper
			def java_called(event=None):
				if event == 'onResume':
					# Forces main window re-drawing
					self._apply_unfreeze_fix()
			self.java.called.connect(java_called)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		view = self._view

		if model is settings_model:
			if current[0] is None or 'current_tab' in current[0] and previous[0] != current[0]:
				if settings_model.current_tab != view.tabs.currentIndex():
					view.tabs.setCurrentIndex(settings_model.current_tab)
					self.__init_tabs()

			if current[0] is None or 'unfreeze' in current[0] and previous[0] != current[0]:
				if getattr(settings_model, 'unfreeze', False):
					# Forces main window re-drawing
					self._apply_unfreeze_fix()

					settings_model.unfreeze = None  # Resets value and removes value from settings

			if current[0] is None or 'show_banner_after' in current[0] and previous[0] != current[0]:
				if settings_model.show_banner_after is not None:
					self._hide_banner()
					# Shows banner after N seconds
					Caller.call_once_after(float(settings_model.show_banner_after), self._show_banner)

					settings_model.show_banner_after = None  # Resets value and removes value from settings

			if current[0] is None or 'font' in current[0] and previous[0] != current[0]:
				from styles import styles
				font = settings_model.font
				with Timer('update of button_size'):
					styles.replace(QtWidgets.qApp, {'* {font: ': self._font_to_style(font)[7:]})

			if current[0] is None or 'button_size' in current[0] and previous[0] != current[0]:
				from styles import styles
				with Timer('update of button_size'):
					styles.set_button_size(settings_model.button_size)

	""" View's event handlers """

	def _on_key_pressed(self, event):
		super(MainController, self)._on_key_pressed(event)

		if event.key() == QtCore.Qt.Key_Back:
			event.accept()
			# Check if back-button is double-clicked
			if self._latest_back_pressed_time + .3 < time.time():
				self._latest_back_pressed_time = time.time()
			else:
				if self.settings_model.close_on_double_back:
					# Exit the application
					if QtWidgets.qApp.activeWindow() is not None:
						QtWidgets.qApp.activeWindow().close()
						QtWidgets.QApplication.quit()
						self._view.close()
						QtWidgets.qApp.quit()
						# sys.exit(0)

		if event.modifiers() == QtCore.Qt.NoModifier and event.key() == QtCore.Qt.Key_Menu:
			current_tab = self._view.tabs.currentWidget()
			try:
				current_tab.findChild((QtWidgets.QPushButton, QtWidgets.QToolButton), 'settings_button').click()
			except AttributeError:
				pass

		if event.modifiers() == (QtCore.Qt.ControlModifier | QtCore.Qt.AltModifier) and event.key() == QtCore.Qt.Key_V:
			current_tab = self._view.tabs.currentWidget()
			try:
				current_tab.findChild((QtWidgets.QPushButton, QtWidgets.QToolButton), 'vocabulary_button').click()
			except AttributeError:
				pass

	# def __on_py_eval_widget_link_activated(self, text):
	#     """Receives data from Java-wrapper (Java PyEval() => Native C *_PyEval => <here>).

	#     PyEval can be used ONLY after initialized python! Use delayed calls if needed!

	#     Usage: QtActivity.PyEval("{'INSTALL_REFERRER': '" + referrer.replaceAll("'", "\\'") + "'}");
	#     """
	#     # logging.getLogger(__name__).debug('PyEval(): %s', text)
	#     if text:
	#         parameters = ast.literal_eval(text)
	#         for key in parameters:
	#             # logging.getLogger(__name__).debug('\t%s => %s', key, parameters[key])
	#             if key == 'INSTALL_REFERRER':
	#                 Caller.call_once_after(1., Network.send_postback_to_referrer, settings_model=self.settings_model, referrer=parameters[key])
	#                 Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Got INSTALL_REFERRER', label=parameters[key]))
	#             elif key.lower() == 'debug':
	#                 logging.getLogger(__name__).debug('%s', parameters[key])
	#             elif key.lower() == 'info':
	#                 logging.getLogger(__name__).info('%s', parameters[key])
	#             elif key.lower() == 'warning':
	#                 logging.getLogger(__name__).warning('%s', parameters[key])
	#             elif key.lower() == 'error':
	#                 logging.getLogger(__name__).error('%s', parameters[key])

	def __on_tab_is_changed(self):
		self.__init_tabs()

	""" Helpers """

	def __start_google_analytics_session_loop(self):
		# Re-send every 60s a 'session activity' with the name of current tab
		def _update_google_analytics_session():
			tab_name = str(self._view.tabs.currentWidget().objectName())

			# Increment usage statistics
			self.settings_model[tab_name + '_usage_statistics'] = self.settings_model.get(tab_name + '_usage_statistics', 0) + 1

			# Send session activity
			Network.send_for_google_analytics(settings_model=self.settings_model, session=dict(key=tab_name, value='start'))
			Caller.call_once_after(60, _update_google_analytics_session)
		_update_google_analytics_session()

	def __show_release_notes_dialog(self):
		"""Shows release notes dialog if new version was installed"""
		settings_model = self.settings_model

		from changes import CHANGES, version
		previous_version = settings_model.get('showed_release_notes_version', None)

		# Shows release notes if application was updated
		if previous_version is None:
			settings_model['showed_release_notes_version'] = version

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Run Application First'))

		elif previous_version is not None and previous_version != version:
			settings_model['showed_release_notes_version'] = version

			from controllers.release_notes import ReleaseNotesController
			Caller.call_once_after(.1, ReleaseNotesController(settings_model=self.settings_model).show)

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Run Application Next (updated)'))

		else:
			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Run Application Next'))

	def __show_first_start_wizard_dialog(self):
		pass
		from controllers.first_start_wizard import FirstStartWizardController
		Caller.call_once_after(.1, FirstStartWizardController(settings_model=self.settings_model).show)

	def _hide_banner(self):
		widget = self._view.banner_widget
		banner = self._banner

		if banner is not None:
			banner.setVisible(False)
			widget.setMinimumSize(0, 0)

	def _show_banner(self):
		widget = self._view.banner_widget
		banner = self._banner

		# Caller.call_once_after(10., self._hide_banner)

		logging.getLogger(__name__).info('Showing banner...')
		try:
			import QtAdMobBanner
		except ImportError:
			logging.getLogger(__name__).warning('Can not import "QtAdMobBanner"')
		else:
			def hide_banner():
				banner.setVisible(False)
				widget.setMinimumSize(0, 0)

			def show_banner():
				screen_y_offset = 0
				screen_y_offset = self._get_screen_y_offset()

				# Sets banner position (in current widget)
				x, y = widget.x(), widget.y() + screen_y_offset
				banner.setPosition(QtCore.QPoint(x, y))

				# Sets widget size (by current banner size)
				size = banner.sizeInPixels()
				widget.setMinimumSize(size.width(), size.height())

				# Makes banner visible
				banner.setVisible(True)

			if banner is None:
				self._banner = banner = QtAdMobBanner.QmlAdMobBanner()
				banner.addTestDevice('A5CE5C5520DA3A67A0E3776681CB7BDA')
				banner.addTestDevice('DD8796DB59AC46A4958D799670A05F01')
				banner.setSize(banner.Banner)
				banner.setUnitId('ca-app-pub-9181265559842913/2733995986')
			else:
				show_banner()

			def _banner_loading():
				hide_banner()
			banner.loading.connect(_banner_loading)

			def _banner_loaded():
				show_banner()
			banner.loaded.connect(_banner_loaded)

			# Fix for banner over dialogs
			def _window_activity_changed():
				if widget.minimumSize().height():
					is_active = widget.window().windowHandle().isActive()
					if is_active:
						if banner.isLoaded() and not banner.visible():
							banner.setVisible(True)
					else:
						if banner.visible():
							banner.setVisible(False)
			widget.window().windowHandle().activeChanged.connect(_window_activity_changed)

			def _banner_clicked():
				pass
			banner.clicked.connect(_banner_clicked)

	def __show_interstitial_banner(self):
		widget = self._view.banner_widget

		try:
			import QtAdMobInterstitial
		except ImportError:
			logging.getLogger(__name__).warning('Can not import "QtAdMobInterstitial"')
		else:
			logging.getLogger(__name__).info('Configuring interstitial...')

			self._interstitial = interstitial = QtAdMobInterstitial.QmlAdMobInterstitial()
			interstitial.addTestDevice('A5CE5C5520DA3A67A0E3776681CB7BDA')
			interstitial.addTestDevice('DD8796DB59AC46A4958D799670A05F01')
			interstitial.setUnitId('ca-app-pub-9181265559842913/1469402370')

			def _interstitial_loading():
				pass
			interstitial.loading.connect(_interstitial_loading)

			def _interstitial_loaded():
				# interstitial.setVisible(True)  # Makes interstitial visible
				pass
			interstitial.loaded.connect(_interstitial_loaded)

			def _interstitial_will_present():
				pass
			interstitial.willPresent.connect(_interstitial_will_present)

			def _interstitial_clicked():
				pass
			interstitial.clicked.connect(_interstitial_clicked)

	def __init_tabs(self):
		view = self._view
		data_model = self.data_model
		settings_model = self.settings_model
		force_init = False

		settings_model.current_tab = view.tabs.currentIndex()  # Store in settings latest selected tab
		current_tab = view.tabs.currentWidget()

		""" Lazy init of active tab """
		if force_init or current_tab == view.dict_tab:
			from controllers.dict import DictController
			DictController.init(parent_view=view.dict_tab.layout(), data_model=data_model, settings_model=settings_model)
		if force_init or current_tab == view.list_tab:
			from controllers.list import ListController
			ListController.init(parent_view=view.list_tab.layout(), data_model=data_model, settings_model=settings_model)
		if force_init or current_tab == view.flash_tab:
			from controllers.flash import FlashController
			FlashController.init(parent_view=view.flash_tab.layout(), data_model=data_model, settings_model=settings_model)
		if force_init or current_tab == view.pairs_tab:
			from controllers.pairs import PairsController
			PairsController.init(parent_view=view.pairs_tab.layout(), data_model=data_model, settings_model=settings_model)
		if force_init or current_tab == view.cards_tab:
			from controllers.cards import CardsController
			CardsController.init(parent_view=view.cards_tab.layout(), data_model=data_model, settings_model=settings_model)
		# if self.with_cols_tab:
		if force_init or current_tab == view.cols_tab:
			from controllers.cols import ColsController
			ColsController.init(parent_view=view.cols_tab.layout(), data_model=data_model, settings_model=settings_model)
		if force_init or current_tab == view.speech_tab:
			from controllers.speech import SpeechController
			SpeechController.init(parent_view=view.speech_tab.layout(), data_model=data_model, settings_model=settings_model)

	def _get_screen_y_offset(self):
		# Calculates absolute offset from the top of the screen
		value = 0
		try:
			# value = QtWidgets.qApp.desktop().availableGeometry().y()
			value = QtWidgets.qApp.desktop().geometry().height() - QtWidgets.qApp.desktop().availableGeometry().height()
			logging.getLogger(__name__).warning('value=' + '%s', value)
		except AttributeError as e:
			logging.getLogger(__name__).warning('Exception: %s', e)
		return value

	def _apply_unfreeze_fix(self):
		view = self._view

		logging.getLogger(__name__).warning('Doing trick with fullscreen toggling...')

		screen_y_offset = self._get_screen_y_offset()
		logging.getLogger(__name__).warning('screen_y_offset=' + '%s', screen_y_offset)

		# Appends an offset to the top (protects from oversizing after toggling full screen mode)
		previous_margins = view.contentsMargins()
		view.setContentsMargins(previous_margins.left(), previous_margins.top() + screen_y_offset, previous_margins.right(), previous_margins.bottom())

		# Does the thick with full screen mode toggling
		view.window().showFullScreen()
		view.window().showNormal()

		# Resets margins
		view.setContentsMargins(previous_margins.left(), previous_margins.top(), previous_margins.right(), previous_margins.bottom())


def run_init():
	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	controller = MainController()
	# controller._view.setGeometry(850, 50, 480, 640)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def run_apply_on_resume_fix():
	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	controller = MainController()
	# controller._view.setGeometry(850, 50, 480, 640)

	Caller.call_once_after(1., controller._apply_unfreeze_fix)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
