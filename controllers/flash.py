#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a flashcards-tab

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import random
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.SIG_DFL)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtWidgets

from controllers.abstract import ControllerMixture
from controllers.flash_settings import FlashSettingsController
from controllers.top_widget_mixture import TopWidgetMixture
from helpers.caller import Caller
from helpers.timer import Timer
from models.abstract import ObservableAttrDict


class _State(ObservableAttrDict):
	pass


class FlashController(ControllerMixture, TopWidgetMixture):
	def __init__(self, parent_view=None, test=False, data_model=None, settings_model=None):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# Models
			""" Current-State-Model for selected view's data"""
			state_model = self.state_model
			state_model.vocabulary = None
			state_model.mark = None
			state_model.previous_pairs = []
			# state_model.left_fields_are_hidden = False
			# state_model.right_fields_are_hidden = False
			state_model.left_field_is_hidden = False
			state_model.right_field_is_hidden = False

			self.data_model = data_model

			self.settings_model = settings_model

			# Views
			self._view = view = self._load_ui('views/flash_view.ui')
			if parent_view is not None:
				parent_view.addWidget(view)

			TopWidgetMixture.__init__(self, parent_view=view.top_widget, with_vocabularies_button=True, with_marks_buttons=True)

			view.left_field_dictionary_button.clicked.connect(self.__on_left_field_dictionary_button_clicked)
			view.previous_button.clicked.connect(self.__on_previous_button_clicked)
			view.right_field_dictionary_button.clicked.connect(self.__on_right_field_dictionary_button_clicked)
			view.bad_button.clicked.connect(self.__on_bad_button_clicked)
			view.fair_button.clicked.connect(self.__on_fair_button_clicked)
			view.good_button.clicked.connect(self.__on_good_button_clicked)
			view.very_good_button.clicked.connect(self.__on_very_good_button_clicked)
			view.left_field_visibility_button.toggled.connect(self.__on_left_field_visibility_button_toggled)
			view.right_field_visibility_button.toggled.connect(self.__on_right_field_visibility_button_toggled)
			view.left_field_label.mousePressEvent = self.__on_left_field_label_clicked
			view.right_field_label.mousePressEvent = self.__on_right_field_label_clicked

			if parent_view is None:
				self._restore_window_geometry()
				view.closeEvent = self._on_close
				view.keyPressEvent = self._on_key_pressed
				view.show()

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)
			data_model.changed.bind(self._on_model_updated)
			state_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)
			self._on_model_updated(data_model)
			self._on_model_updated(state_model)

			if '--test-flash' in sys.argv:
				import ipdb; ipdb.set_trace()  # FIXME: must be removed

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# print(self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80], file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		TopWidgetMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		data_model = self.data_model
		state_model = self.state_model
		view = self._view

		if model is settings_model:
			# if current[0] is None or 'flash_vocabulary' in current[0] and previous[0] != current[0]:
			#     state_model.vocabulary = data_model.vocabularies.get(settings_model.flash_vocabulary, None)

			if current[0] is None or 'flash_selected_marks' in current[0] and previous[0] != current[0]:
				state_model.selected_marks = settings_model.flash_selected_marks

			if current[0] is None or 'flash_left_fields_are_hidden' in current[0] and previous[0] != current[0]:
				view.left_field_visibility_button.setChecked(settings_model.flash_left_fields_are_hidden)
				state_model.left_field_is_hidden = settings_model.flash_left_fields_are_hidden

			if current[0] is None or 'flash_right_fields_are_hidden' in current[0] and previous[0] != current[0]:
				view.right_field_visibility_button.setChecked(settings_model.flash_right_fields_are_hidden)
				state_model.right_field_is_hidden = settings_model.flash_right_fields_are_hidden

			if current[0] is None or 'flash_font' in current[0] and previous[0] != current[0]:
				if settings_model.flash_font is not None:
					from styles import styles
					font = settings_model.flash_font
					styles.replace(view.card_container, {'* {font: ': self._font_to_style(font)[7:]})

			if current[0] is None or 'flash_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.left_field_label, {'QLabel {background-color': 'QLabel {{background-color: {0};}}'.format(settings_model.flash_card_background_color)})
				styles.replace(view.right_field_label, {'QLabel {background-color': 'QLabel {{background-color: {0};}}'.format(settings_model.flash_card_background_color)})

			if current[0] is None or 'flash_hidden_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.left_field_label, {'QLabel[hidden=true] {background-color': 'QLabel[hidden=true] {{background-color: {0};}}'.format(settings_model.flash_hidden_card_background_color)})
				styles.replace(view.right_field_label, {'QLabel[hidden=true] {background-color': 'QLabel[hidden=true] {{background-color: {0};}}'.format(settings_model.flash_hidden_card_background_color)})

		if model is data_model:
			if current[0] is None or 'vocabularies' in current[0] and previous[:2] != current[:2]:
				state_model.vocabulary = data_model.vocabularies.get(settings_model.flash_vocabulary, None)

		if model is state_model:
			# # if current[0] is None or 'vocabulary' in current[0] and previous[0] != current[0]:
			# #     settings_model.flash_vocabulary = state_model.vocabulary.path if state_model.vocabulary is not None else None

			# if current[0] is None or 'left_fields_are_hidden' in current[0] and previous[0] != current[0]:
			#     view.left_field_visibility_button.setChecked(state_model.left_fields_are_hidden)
			#     state_model.left_field_is_hidden = state_model.left_fields_are_hidden

			# if current[0] is None or 'right_fields_are_hidden' in current[0] and previous[0] != current[0]:
			#     view.right_field_visibility_button.setChecked(state_model.right_fields_are_hidden)
			#     state_model.right_field_is_hidden = state_model.right_fields_are_hidden

			# # if current[0] is None or 'left_field' in current[0] and previous[0] != current[0]:
			# #     view.left_field_label.setText(state_model.left_field)
			# #     state_model.left_field_is_hidden = state_model.left_fields_are_hidden

			# # if current[0] is None or 'right_field' in current[0] and previous[0] != current[0]:
			# #     view.right_field_label.setText(state_model.right_field)
			# #     state_model.right_field_is_hidden = state_model.right_fields_are_hidden

			if current[0] is None or 'vocabulary' in current[0] and previous[0] != current[0]:
				state_model.previous_pairs[:] = []

				Caller.call_once_after(0., self._next)

			if current[0] is None or 'selected_marks' in current[0] and previous != current:
				settings_model.flash_selected_marks = state_model.selected_marks

				state_model.previous_pairs[:] = []

				Caller.call_once_after(0., self._next)

			if current[0] is None or 'vocabulary' in current[0] and previous != current or 'mark' in current[0] and previous[0] != current[0]:
				left_field, right_field = self._get_current_pair() or ('', '')

				state_model.left_field_is_hidden = settings_model.flash_left_fields_are_hidden
				state_model.right_field_is_hidden = settings_model.flash_right_fields_are_hidden

				view.left_field_label.setText(left_field)
				view.right_field_label.setText(right_field)

			# if current[0] is None or 'pair' in current[0] and previous[0] != current[0]:
			#     # state_model.left_field_is_hidden = state_model.left_fields_are_hidden
			#     # state_model.right_field_is_hidden = state_model.right_fields_are_hidden

			#     state_model.left_field_is_hidden = settings_model.flash_left_fields_are_hidden
			#     state_model.right_field_is_hidden = settings_model.flash_right_fields_are_hidden

			if current[0] is None or 'left_field_is_hidden' in current[0] and previous[0] != current[0]:
				if view.left_field_label.property('hidden') != state_model.left_field_is_hidden:
					view.left_field_label.setProperty('hidden', state_model.left_field_is_hidden)
					view.left_field_label.style().unpolish(view.left_field_label)
					view.left_field_label.style().polish(view.left_field_label)

			if current[0] is None or 'right_field_is_hidden' in current[0] and previous[0] != current[0]:
				if view.right_field_label.property('hidden') != state_model.right_field_is_hidden:
					view.right_field_label.setProperty('hidden', state_model.right_field_is_hidden)
					view.right_field_label.style().unpolish(view.right_field_label)
					view.right_field_label.style().polish(view.right_field_label)

	""" View's event handlers """

	def _on_vocabulary_selected(self, vocabulary):
		self.settings_model.flash_vocabulary = vocabulary.path if vocabulary is not None else None

	def __on_menu_pressed(self, event):
		FlashSettingsController(settings_model=self.settings_model).show()

	def _on_settings_button_clicked(self, event):
		FlashSettingsController(settings_model=self.settings_model).show()

	def __on_left_field_dictionary_button_clicked(self, checked):
		state_model = self.state_model

		keywords = (self._get_current_pair() or ('', ''))[0]

		self._show_in_dict_tab(keywords)

	def __on_previous_button_clicked(self):
		Caller.call_once_after(0, self._previous)

	def __on_right_field_dictionary_button_clicked(self, checked):
		state_model = self.state_model

		keywords = (self._get_current_pair() or ('', ''))[1]

		self._show_in_dict_tab(keywords)

	def __on_bad_button_clicked(self):
		state_model = self.state_model
		settings_model = self.settings_model

		if state_model.mark is not None:
			if state_model.vocabulary[state_model.mark]:
				self._copy_current_pair_to_previous()
				pair = state_model.vocabulary[state_model.mark].pop(0)
				new_place = random.randint(settings_model.flash_repeat_bad_from, settings_model.flash_repeat_bad_to) if settings_model.flash_repeat_bad else len(state_model.vocabulary['bad'])
				state_model.vocabulary['bad'].insert(new_place, pair)
				self._fill()
				self._next()

	def __on_fair_button_clicked(self):
		state_model = self.state_model
		settings_model = self.settings_model

		if state_model.mark is not None:
			if state_model.vocabulary[state_model.mark]:
				self._copy_current_pair_to_previous()
				pair = state_model.vocabulary[state_model.mark].pop(0)
				new_place = random.randint(settings_model.flash_repeat_fair_from, settings_model.flash_repeat_fair_to) if settings_model.flash_repeat_fair else len(state_model.vocabulary['fair'])
				state_model.vocabulary['fair'].insert(new_place, pair)
				self._fill()
				self._next()

	def __on_good_button_clicked(self):
		state_model = self.state_model
		settings_model = self.settings_model

		if state_model.mark is not None:
			if state_model.vocabulary[state_model.mark]:
				self._copy_current_pair_to_previous()
				pair = state_model.vocabulary[state_model.mark].pop(0)
				new_place = random.randint(settings_model.flash_repeat_good_from, settings_model.flash_repeat_good_to) if settings_model.flash_repeat_good else len(state_model.vocabulary['good'])
				state_model.vocabulary['good'].insert(new_place, pair)
				self._fill()
				self._next()

	def __on_very_good_button_clicked(self):
		state_model = self.state_model
		# settings_model = self.settings_model

		if state_model.mark is not None:
			if state_model.vocabulary[state_model.mark]:
				self._copy_current_pair_to_previous()
				pair = state_model.vocabulary[state_model.mark].pop(0)
				new_place = len(state_model.vocabulary['good'])
				state_model.vocabulary['good'].insert(new_place, pair)
				self._fill()
				self._next()

	def __on_left_field_visibility_button_toggled(self):
		# self.state_model.left_fields_are_hidden = self._view.left_field_visibility_button.isChecked()
		self.settings_model.flash_left_fields_are_hidden = self._view.left_field_visibility_button.isChecked()

	def __on_right_field_visibility_button_toggled(self):
		# self.state_model.right_fields_are_hidden = self._view.right_field_visibility_button.isChecked()
		self.settings_model.flash_right_fields_are_hidden = self._view.right_field_visibility_button.isChecked()

	def __on_left_field_label_clicked(self, event):
		self.state_model.left_field_is_hidden = not self.state_model.left_field_is_hidden

	def __on_right_field_label_clicked(self, event):
		self.state_model.right_field_is_hidden = not self.state_model.right_field_is_hidden

	""" Helpers """

	def _get_current_pair(self):
		state_model = self.state_model

		if state_model.vocabulary is not None and state_model.mark is not None and state_model.vocabulary[state_model.mark]:
			return state_model.vocabulary[state_model.mark][0]

	def _previous(self):
		state_model = self.state_model
		vocabulary = self.state_model.vocabulary

		if state_model.previous_pairs:
			state_model.mark, pair = state_model.previous_pairs.pop()

			# Search and remove current pair from lists
			for mark in vocabulary.marks:
				if pair in vocabulary[mark]:
					vocabulary[mark].remove(pair)

			# Restore current pair backwards
			vocabulary[state_model.mark].insert(0, pair)

		# TopWidgetMixture._fill(self)
		self._fill()

	def _copy_current_pair_to_previous(self):
		state_model = self.state_model

		# Store previous pair
		pair = self._get_current_pair()
		if pair is not None:
			state_model.previous_pairs.append((state_model.mark, pair))

	def __choose_value(self, values, weights):
		random_number = random.uniform(0, sum(weights))
		start = 0
		for value, weight in zip(values, weights):
			if start + weight >= random_number:
				return value
			start += weight
		assert False, "Shouldn't get here"

	def _next(self):
		state_model = self.state_model
		vocabulary = self.state_model.vocabulary

		if vocabulary is not None:
			enabled_marks = [mark for mark in state_model.selected_marks if len(vocabulary[mark])]

			# Set next pair
			if not enabled_marks:
				state_model.mark = None
			else:
				# state_model.mark = mark = random.choice(enabled_marks)

				# Attention: more pairs => more probability!
				state_model.mark = mark = self.__choose_value(enabled_marks, [len(vocabulary[mark]) for mark in enabled_marks])

				Caller.call_once_after(.5, self._update_last_training_time)

	def _fill(self):
		TopWidgetMixture._fill(self)

		# self._next()


def run_init():
	from models.abstract import ObservableAttrDict, ObservableList

	class Vocabularies(ObservableList):
		def __contains__(self, key):
			if key.__class__ == int and len(self) > key:
				return True
			return super(Vocabularies, self).__contains__(key)

	class Vocabulary(ObservableAttrDict):
		def __init__(self, name, *args, **kwargs):
			self._name = name
			super(Vocabulary, self).__init__(*args, **kwargs)

		def __str__(self):
			return self._name

		@property
		def path(self):
			return 'root/subdirectory/' + self._name

	data_model = ObservableAttrDict(
		vocabularies={
			0: Vocabulary('Körperteile', new=[['Glied', 'конечность'], ['Gelenk', 'сустав'], ['Knochen, Bein', 'кость'], ['Haut', 'кожа'], ['Nagel', 'ноготь'], ['Stirn', 'лоб']], good=[], fair=[], bad=[]),
		},
	)
	settings_model = ObservableAttrDict(
		flash_vocabulary=0,
		flash_left_fields_are_hidden=True,
		flash_right_fields_are_hidden=False,
		flash_font=None,
		flash_card_background_color='#ddd',
		flash_hidden_card_background_color='#bbb',
		flash_repeat_bad=False,
		flash_repeat_bad_from=None,
		flash_repeat_bad_to=None,
		flash_repeat_fair=False,
		flash_repeat_fair_from=None,
		flash_repeat_fair_to=None,
		flash_repeat_good=False,
		flash_repeat_good_from=None,
		flash_repeat_good_to=None,
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles; styles  # Load once the styles for QApplication

	controller = FlashController(data_model=data_model, settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
