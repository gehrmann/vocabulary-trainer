#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a cols-tab

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import random
import re
import signal
import sys
import time

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtWidgets, uic

from controllers.abstract import ControllerMixture, Network, BackgroundsCategoriesDialog
from controllers.cols_settings import ColsSettingsController
from controllers.top_widget_mixture import TopWidgetMixture
from helpers.caller import Caller
from helpers.timer import Timer
from models.abstract import ObservableAttrDict, ObservableList, ObservableSet  # , UnhashableKeyDict


class _State(ObservableAttrDict):
	pass


class ColsController(ControllerMixture, TopWidgetMixture):
	def __init__(self, parent_view=None, test=False, data_model=None, settings_model=None):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# Models
			""" Current-State-Model for selected view's data"""
			state_model = self.state_model
			state_model.vocabulary = None
			state_model.trailing_field_is_hidden = False
			state_model.columns_to_fields = ObservableAttrDict()
			state_model.fields = ObservableList()

			self.data_model = data_model

			self.settings_model = settings_model

			self._columns_views = []
			self._columns_to_columns_views = {}
			self._columns_views_to_columns = {}

			# Views
			self._view = view = self._load_ui('views/cols_view.ui')
			if parent_view is not None:
				parent_view.addWidget(view)

			TopWidgetMixture.__init__(self, parent_view=view.top_widget, with_vocabularies_button=True, with_marks_buttons=True, with_reload_button=True)
			self.top_widget.reload_button.clicked.connect(self.__on_reload_button_clicked)
			view.trailing_field_label.mousePressEvent = self.__on_trailing_field_label_clicked

			if parent_view is None:
				self._restore_window_geometry()
				view.closeEvent = self._on_close
				view.keyPressEvent = self._on_key_pressed
				view.show()

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)
			data_model.changed.bind(self._on_model_updated)
			state_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)
			self._on_model_updated(data_model)
			self._on_model_updated(state_model)

			if '--test-cols' in sys.argv:
				import ipdb; ipdb.set_trace()  # FIXME: must be removed

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# print(self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80], file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		TopWidgetMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		data_model = self.data_model
		state_model = self.state_model
		view = self._view
		top_widget = self.top_widget

		if model is settings_model:
			if current[0] is None or 'cols_selected_marks' in current[0] and previous[0] != current[0]:
				state_model.selected_marks = settings_model.cols_selected_marks

			if current[0] is None or 'cols_leading_field_index' in current[0] and previous[0] != current[0]:
				Caller.call_once_after(0, self._fill)

			if current[0] is None or 'cols_font' in current[0] and previous[0] != current[0]:
				if settings_model.cols_font is not None:
					from styles import styles
					font = settings_model.cols_font
					styles.replace(view.cols_container, {'* {font: ': self._font_to_style(font)[7:]})

			if current[0] is None or 'cols_leading_field_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.shortened_field_label, {'QLabel {color: ': '{0};}}'.format(settings_model.cols_leading_field_color)})

			if current[0] is None or 'cols_trailing_field_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.trailing_field_label, {'QLabel {color: ': '{0};}}'.format(settings_model.cols_trailing_field_color)})

			if current[0] is None or 'cols_trailing_field_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.trailing_field_label, {'QLabel {background-color: ': '{0};}}'.format(settings_model.cols_trailing_field_background_color)})

			if current[0] is None or 'cols_hidden_trailing_field_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.trailing_field_label, {'QLabel[hidden=true] {background-color: ': '{0};}}'.format(settings_model.cols_hidden_trailing_field_background_color)})

			# if current[0] is None or 'cols_column_background_color' in current[0] and previous[0] != current[0]:
			#     from styles import styles
			#     styles.replace(view.columns_container, {'#fields {background-color: ': '{0};}}'.format(settings_model.cols_column_background_color)})

		if model is data_model:
			if current[0] is None or 'vocabularies' in current[0] and previous[:2] != current[:2]:
				state_model.vocabulary = data_model.vocabularies.get(settings_model.cols_vocabulary, None)

		if model is state_model:
			if current[0] is None or 'selected_marks' in current[0] and previous[0] != current[0]:
				settings_model.cols_selected_marks = state_model.selected_marks

			if current[0] is None or 'fields' in current[0]:  # and previous[0] != current[0]:
				shortened_field, leading_field, trailing_field = state_model.fields[0] if state_model.fields else ('', '', '')

				state_model.trailing_field_is_hidden = True

				# Updates shortened field label
				getattr(view.shortened_field_label, 'show' if shortened_field else 'hide')()
				view.shortened_field_label.setText(shortened_field)

				# view.leading_field_label.setText(leading_field)

				# Updates trailing field label
				getattr(view.trailing_field_label, 'show' if trailing_field else 'hide')()
				view.trailing_field_label.setText(trailing_field)

			if current[0] is None or 'trailing_field_is_hidden' in current[0] and previous[0] != current[0]:
				if view.trailing_field_label.property('hidden') != state_model.trailing_field_is_hidden:
					view.trailing_field_label.setProperty('hidden', state_model.trailing_field_is_hidden)
					view.trailing_field_label.style().unpolish(view.trailing_field_label)
					view.trailing_field_label.style().polish(view.trailing_field_label)

			if current[0] is None or 'columns_to_fields' in current[0]:
				# Clean the board
				for row in range(view.columns_container.layout().count()):
					column_view = view.columns_container.layout().takeAt(0)
					column_view.widget().deleteLater()

				tips = ''
				if not state_model.columns_to_fields:
					# Shows tip: how to prepare data
					tips += 'Add keys to fields in parentheses,<br/>for Ex.: <font color="#090"><i>Ergebnis <b>(n)</b></i></font><br/>and they will be grouped here.'
				state_model.tips = tips

				if state_model.columns_to_fields:
					self._columns_views[:] = [self._load_ui('views/cols_column_view.ui') for i in range(len(state_model.columns_to_fields))]  # Qt-resources have to be imported already

					# Fills the board with new columns
					columns = sorted(state_model.columns_to_fields.keys())
					self._columns_to_columns_views = {}
					self._columns_views_to_columns = {}
					for column, column_view in zip(columns, self._columns_views):
						column_view.fields.mousePressEvent = (lambda column_view: (lambda event: (self._on_column_clicked(column_view))))(column_view)
						column_view.title.setText(column)
						view.columns_container.layout().addWidget(column_view)
						self._columns_to_columns_views[column] = column_view
						self._columns_views_to_columns[column_view] = column

	""" View's event handlers """

	def _on_vocabulary_selected(self, vocabulary):
		self.settings_model.cols_vocabulary = vocabulary.path if vocabulary is not None else None

	def __on_menu_pressed(self, event):
		ColsSettingsController(settings_model=self.settings_model).show()

	def _on_settings_button_clicked(self, event):
		ColsSettingsController(settings_model=self.settings_model).show()

	def __on_reload_button_clicked(self, event):
		Caller.call_once_after(0, self._fill)

		Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Action Reload ' + self.__class__.__name__))

	def __on_trailing_field_label_clicked(self, event):
		self.state_model.trailing_field_is_hidden = not self.state_model.trailing_field_is_hidden

	def _on_column_clicked(self, column_view):
		settings_model = self.settings_model
		state_model = self.state_model

		# field_separator = ', '
		field_separator = {
			'new line': '<br/>\n',
			'comma': ', ',
		}[settings_model.cols_column_field_separator]

		if state_model.fields:
			shortened_field, leading_field, trailing_field = field = state_model.fields.pop(0)
			clicked_column = self._columns_views_to_columns[column_view]

			right_columns = [column for column, fields in state_model.columns_to_fields.items() if field in fields]

			for column in right_columns:
				column_view = self._columns_to_columns_views[column]
				# Inserts value into column
				# column_view.fields.setText(column_view.fields.text() + '<font color="{color}">{shortened_field}</font><br/>\n'.format(
				column_view.fields.setText(
					column_view.fields.text() +
					(field_separator if column_view.fields.text() else '') +
					'<font color="{color}">{shortened_field}</font>'.format(
						color=(settings_model.cols_guessed_field_color if clicked_column in right_columns else settings_model.cols_failed_field_color),
						**locals()
					)
				)
				# Makes inserted value visible
				column_view.scroll_area.verticalScrollBar().setValue(
					column_view.scroll_area.verticalScrollBar().maximum()
				)

	""" Helpers """

	def _fill(self):
		logging.getLogger(__name__).debug('Calling _fill()...')

		TopWidgetMixture._fill(self)

		settings_model = self.settings_model
		data_model = self.data_model
		state_model = self.state_model
		vocabulary = self.state_model.vocabulary
		view = self._view

		if vocabulary is not None:
			enabled_marks = [mark for mark in state_model.selected_marks if len(vocabulary[mark])]

			leading_field_index, trailing_field_index = dict(left=(0, 1), right=(1, 0))[settings_model.cols_group_by_field]
			group_occurs_at_least_percent = .01 * settings_model.cols_group_filter
			matcher = re.compile(
				{
					'whole field': r'()\((.*?)\)',
					'( ... )': r'(.*)\((.*?)\)',
					'[ ... ]': r'(.*)\[(.*?)\]',
				}[settings_model.cols_group_method],
				flags=re.UNICODE,
			)

			matches = [
				[match.group(1).strip(), match.group(2).strip(), pair[leading_field_index], pair[trailing_field_index]]
				for mark in enabled_marks
				for pair in vocabulary[mark]
				for match in [matcher.match(pair[leading_field_index])]
				if match is not None
			]

			# Gets groups with fields
			columns_to_fields = dict()
			for shortened_field, group, leading_field, trailing_field in matches:
				if shortened_field:  # Avoids empty fields
					columns_to_fields.setdefault(group, []).append((shortened_field, leading_field, trailing_field))

			# Removes groups which occur rarely
			total_fields_count = sum(len(fields) for fields in columns_to_fields.values())
			group_to_percent = {group: (total_fields_count and (float(len(fields)) / total_fields_count)) for group, fields in columns_to_fields.items()}
			for group, percent in group_to_percent.items():
				if percent < group_occurs_at_least_percent:
					columns_to_fields.pop(group)

			state_model.columns_to_fields.clear()
			state_model.columns_to_fields.update(columns_to_fields)

			# Gets fields and shuffles them
			fields = [xx for x in columns_to_fields.values() for xx in x]
			random.shuffle(fields)

			state_model.fields[:] = fields


def run_init():
	from models.abstract import ObservableAttrDict, ObservableList
	from models.data import _BackgroundsCategories, _Vocabulary

	class Vocabularies(ObservableList):
		def __contains__(self, key):
			if key.__class__ == int and len(self) > key:
				return True
			return super(Vocabularies, self).__contains__(key)

	import tempfile
	vocabulary_path = tempfile.mkstemp(prefix='temporary_vocabulary_', suffix='.txt')[1]
	try:
		vocabulary = _Vocabulary(path=vocabulary_path)
		vocabulary.new.extend([['Glied (n)', 'конечность'], ['Gelenk (n)', 'сустав'], ['Botschafter', 'посол'], ['Knochen (m)', 'кость'], ['Haut (f)', 'кожа'], ['Nagel (m)', 'ноготь'], ['Stirn (f)', 'лоб']])

		data_model = ObservableAttrDict(
			backgrounds_categories=_BackgroundsCategories(),
			vocabularies={
				0: vocabulary,
			},
		)
		settings_model = ObservableAttrDict(
			cols_vocabulary=0,
			cols_selected_marks=ObservableSet(['new', 'good', 'fair', 'bad']),
			cols_font=dict(family='Droid Sans', size=.75 * styles.mm2px(2.8), weight=50, italic=False),
			cols_leading_field_color='#000',
			cols_trailing_field_color='#666',
			cols_trailing_field_background_color='#eee',
			cols_hidden_trailing_field_background_color='#999',
			cols_column_background_color='#ffcccc',
			cols_failed_field_color='#c33',
			cols_guessed_field_color='#0c0',
			cols_group_by_field='left',
			cols_group_method='( ... )',
			cols_group_filter=3,
			cols_column_field_separator='new line',
		)

		app = QtWidgets.QApplication(sys.argv)
		from styles import styles  # Load once the styles for QApplication

		controller = ColsController(data_model=data_model, settings_model=settings_model)
		# controller._view.setGeometry(850, 50, 480, 640)

		# import ipdb; ipdb.set_trace()  # FIXME: must be removed
		sys.exit(app.exec_())
	finally:
		os.unlink(vocabulary_path)


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
