#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a pairs-tab's settings

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtWidgets, uic

from helpers.timer import Timer
from controllers.abstract import ControllerMixture, CommonSettingsMixture, Network


class PairsSettingsController(ControllerMixture, CommonSettingsMixture):
	def __init__(self, settings_model):
		with Timer('initializing'):
			ControllerMixture.__init__(self)
			CommonSettingsMixture.__init__(self)

			# Models
			self.settings_model = settings_model

			# Views
			self._view = view = self._load_ui('views/pairs_settings_widget.ui')
			self.dialog.settings_widget.layout().addWidget(view)

			view.vertical_rows_count_dropdown.activated.connect(self.__on_vertical_rows_count_dropdown_activated)
			# view.horizontal_rows_count_dropdown.activated.connect(self.__on_horizontal_rows_count_dropdown_activated)
			view.font_button.clicked.connect(self.__on_font_button_clicked)
			view.card_background_color_button.clicked.connect(self.__on_card_background_color_button_clicked)
			view.hidden_card_background_color_button.clicked.connect(self.__on_hidden_card_background_color_button_clicked)

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Show Dialog ' + self.__class__.__name__))

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		CommonSettingsMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		view = self._view

		if model is settings_model:
			if current[0] is None or 'pairs_vertical_rows_count' in current[0] and previous[0] != current[0]:
				view.vertical_rows_count_dropdown.setCurrentIndex(view.vertical_rows_count_dropdown.findText(str(settings_model.pairs_vertical_rows_count)))

			# if current[0] is None or 'pairs_horizontal_rows_count' in current[0] and previous[0] != current[0]:
			#     view.horizontal_rows_count_dropdown.setCurrentIndex(view.horizontal_rows_count_dropdown.findText(str(settings_model.pairs_horizontal_rows_count)))

			if current[0] is None or 'pairs_font' in current[0] and previous[0] != current[0]:
				if settings_model.pairs_font is not None:
					view.font_button.setText(self._font_to_text(settings_model.pairs_font))

			if current[0] is None or 'pairs_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.card_background_color_button, {'* {background-color: ': '{};}}'.format(settings_model.pairs_card_background_color)})

			if current[0] is None or 'pairs_hidden_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.hidden_card_background_color_button, {'* {background-color: ': '{};}}'.format(settings_model.pairs_hidden_card_background_color)})

	""" View's event handlers """

	def __on_vertical_rows_count_dropdown_activated(self, current_index):
		self.settings_model.pairs_vertical_rows_count = int(self._view.vertical_rows_count_dropdown.currentText())

	# def __on_horizontal_rows_count_dropdown_activated(self, current_index):
	#     self.settings_model.pairs_horizontal_rows_count = int(self._view.horizontal_rows_count_dropdown.currentText())

	def __on_font_button_clicked(self):
		self._show_font_dialog(model=self.settings_model, key='pairs_font')

	def __on_card_background_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='pairs_card_background_color')

	def __on_hidden_card_background_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='pairs_hidden_card_background_color')


def run_init():
	from models.abstract import ObservableAttrDict
	settings_model = ObservableAttrDict(
		font=None,
		button_size=6,
		close_on_double_back=True,
		current_tab=0,
		pairs_vertical_rows_count=None,
		# pairs_horizontal_rows_count=None,
		pairs_font=None,
		pairs_card_background_color='#ddd',
		pairs_hidden_card_background_color='#ccc',
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	controller = PairsSettingsController(settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)
	controller.show()


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
