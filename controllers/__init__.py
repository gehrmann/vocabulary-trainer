#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
import datetime
# FIXME: Freezes the multiprocessing.Queue
# try:
#     from gevent import monkey; monkey.patch_all()  # Enable greenlets instead of threads
# except ImportError:
#     pass
import logging
import os
import random
import signal
import socket
import sys
import time

__doc__ = """

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )

Files to change behavior (can be found in a working directory):
	enable_logging_debug -- with LOGGING or LOGGING_<MODULE> per line
	enable_logging_info -- with LOGGING or LOGGING_<MODULE> per line
	enable_logging_warning -- with LOGGING or LOGGING_<MODULE> per line
	enable_logging_error -- with LOGGING or LOGGING_<MODULE> per line
	enable_logging_critical -- with LOGGING or LOGGING_<MODULE> per line
	enable_profiler
	enable_stat_profiler
	enable_line_profiler
"""

if True:
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Sets default timeout for network requests
	socket.setdefaulttimeout(10)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)

"""Create a file in a current working directory to enable a specific option"""
enable_profiler = os.path.exists('enable_profiler')
enable_stat_profiler = os.path.exists('enable_stat_profiler')
enable_line_profiler = os.path.exists('enable_line_profiler')
for src_path, value in (
	('enable_logging_critical', 'CRITICAL'),
	('enable_logging_error', 'ERROR'),
	('enable_logging_warning', 'WARNING'),
	('enable_logging_info', 'INFO'),
	('enable_logging_debug', 'DEBUG'),
):
	if os.path.exists(src_path):
		if not os.path.getsize(src_path):
			os.environ['LOGGING'] = value
		else:
			with open(src_path) as src:
				lines = [x.strip().upper() for x in src if x.strip()]
				for key in lines:
					# Sets environment variables like LOGGING or LOGGING_<MODULE> to ( DEBUG | INFO | WARNING | ERROR | CRITICAL )
					os.environ[key] = value
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

if enable_profiler:
	from cProfile import Profile
	profiler = Profile()
	profiler.enable()
	logging.getLogger(__name__).info('Profiler is enabled')

if enable_stat_profiler:
	import statprof
	statprof.start()
	logging.getLogger(__name__).info('Stat profiler is enabled')

if enable_line_profiler:
	from helpers import my_trace
	sys.line_profiler = my_trace.start_line_profiler()
	logging.getLogger(__name__).info('Line profiler is enabled')

from helpers.timer import Timer


def get_desktop_widget_image(kwargs):
	"""Generates image for android desktop widget (called from QtDesktopReceiver.java), returns path or empty string if failed"""
	try:
		# Decodes keyword arguments
		import ast
		kwargs = ast.literal_eval(kwargs)
		print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'kwargs=', kwargs, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		if not kwargs['width'] and not kwargs['height']:
			kwargs['width'], kwargs['height'] = 184, 160

		directory_path = os.getcwd()
		filename_prefix = '.widget-'

		# Removes previous widget images
		for image_path in [os.path.join(directory_path, x) for x in os.listdir(directory_path) if x.startswith(filename_prefix)]:
			if os.path.getmtime(image_path) < time.time() - 60:
				os.unlink(image_path)

		# Creates image for widget
		from controllers.widget import WidgetController
		controller = WidgetController(**kwargs)
		image_path = os.path.join(os.path.abspath(directory_path), '{}{}.png'.format(filename_prefix, random.randint(0, 999)))
		with Timer('saving image'):
			controller.save(path=image_path)

	except Exception as e:
		sys.excepthook(*sys.exc_info())
		image_path = ''

	return image_path


def loop(argv):
	from PyQt import QtCore, QtWidgets  # 500ms


	# if logging.getLogger(__name__).level in (logging.DEBUG, logging.INFO):
	#     # # Prints out registered loggers
	#     # import logging_tree
	#     # logging_tree.printout()
	#     from helpers import my_logging
	#     my_logging.init(
	#         logger=logging.getLogger(__name__),
	#         force_stderr=True,
	#         with_android_log=True,
	#     )
	#     logging.getLogger(__name__).warning('--- Python: helpers.my_logging tests/ ---')
	#     print('PYTHON STDOUT')
	#     print('PYTHON STDERR', file=sys.stderr);
	#     logging.getLogger(__name__).warning('PYTHON LOGGING.DEBUG')
	#     logging.getLogger(__name__).warning('PYTHON LOGGING.INFO')
	#     logging.getLogger(__name__).warning('PYTHON LOGGING.WARNING')
	#     QtCore.qDebug('PYTHON QDEBUG')
	#     QtCore.qWarning('PYTHON QWARNING')
	#     logging.getLogger(__name__).warning('--- /Python: helpers.my_logging tests ---')


	logging.getLogger(__name__).info('Loaded "controllers" from %s...', __file__)


	# from PyQt import uic
	# logging.getLogger(__name__).warning('uic.widgetPluginPath=' + '%s', uic.widgetPluginPath)
	# sys.exit()  # FIXME: must be removed/commented


	# Set here sys.argv?

	# app = QtWidgets.QApplication(sys.argv)  # 70ms
	# app = QtWidgets.qApp
	app = QtWidgets.QApplication(argv)  # 70ms
	if hasattr(QtCore.Qt, 'AA_EnableHighDpiScaling'):
		app.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
	from styles import styles

	logging.getLogger(__name__).debug('Running MainController.init()...')
	from controllers.main import MainController  # 400ms
	MainController.init()
	logging.getLogger(__name__).debug('MainController.init() is done')


	# from PyQt5 import QtQuick
	# view = QtQuick.QQuickView()
	# view.setSource(QtCore.QUrl.fromLocalFile('views/_test.qml'))
	# view.show()


	try:
		logging.getLogger(__name__).debug('Running app.exec_()...')
		sys.exit(app.exec_())

	finally:
		logging.getLogger(__name__).debug('app.exec_() is done')

		if enable_profiler:
			logging.getLogger(__name__).info('Profile is dumping into native_profile.prof...')
			profiler.dump_stats('native_profile.prof')
			logging.getLogger(__name__).info('Profile was saved')

		if enable_stat_profiler:
			logging.getLogger(__name__).info('Stopping stat profile...')
			statprof.stop()
			with open('stat_profile.prof-' + time.strftime('%Y-%m-%d_%H:%M:%S'), 'w') as dst:
				logging.getLogger(__name__).info('Stat profile is dumping into stat_profile.prof*...')
				statprof.display(dst)
				logging.getLogger(__name__).info('Stat profile was saved')

		if enable_line_profiler:
			logging.getLogger(__name__).info('Line profile is dumping into native_line_profile_*...')
			my_trace.stop_line_profiler(filename_prefix='native_line_profile_')
			logging.getLogger(__name__).info('Line profile was saved')


def main():
	loop()

if __name__ == '__main__':
	main()
