#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a pairs-tab

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import random
import signal
import sys
import time

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.SIG_DFL)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtWidgets, uic

from controllers.abstract import ControllerMixture, Network
from controllers.pairs_settings import PairsSettingsController
from controllers.top_widget_mixture import TopWidgetMixture
from helpers.caller import Caller
from helpers.timer import Timer
from models.abstract import ObservableAttrDict


class _State(ObservableAttrDict):
	pass


class PairsController(ControllerMixture, TopWidgetMixture):
	def __init__(self, parent_view=None, test=False, data_model=None, settings_model=None):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# Models
			""" Current-State-Model for selected view's data"""
			state_model = self.state_model
			state_model.vocabulary = None
			# state_model.left_fields_are_hidden = False
			# state_model.right_fields_are_hidden = False
			state_model.left_field_is_hidden = ObservableAttrDict()
			# state_model.left_field_is_hidden.changed.bind(lambda model=None, previous=None, current=None: (state_model.changed(state_model, previous={'left_field_is_hidden': previous}, current={'left_field_is_hidden': current})))  # invoke parent's changed-event if is changed
			state_model.right_field_is_hidden = ObservableAttrDict()
			# state_model.right_field_is_hidden.changed.bind(lambda model=None, previous=None, current=None: (state_model.changed(state_model, previous={'right_field_is_hidden': previous}, current={'right_field_is_hidden': current})))  # invoke parent's changed-event if is changed
			self.fields = []
			self.pairs_views = []

			self.data_model = data_model

			self.settings_model = settings_model

			# Views
			self._view = view = self._load_ui('views/pairs_view.ui')
			if parent_view is not None:
				parent_view.addWidget(view)

			TopWidgetMixture.__init__(self, parent_view=view.top_widget, with_vocabularies_button=True, with_marks_buttons=True)

			view.left_field_visibility_button.toggled.connect(self.__on_left_field_visibility_button_toggled)
			view.right_field_visibility_button.toggled.connect(self.__on_right_field_visibility_button_toggled)
			view.reload_button_1.clicked.connect(self.__on_reload_button_clicked)
			view.reload_button_2.clicked.connect(self.__on_reload_button_clicked)

			if parent_view is None:
				self._restore_window_geometry()
				view.closeEvent = self._on_close
				view.keyPressEvent = self._on_key_pressed
				view.show()

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)
			data_model.changed.bind(self._on_model_updated)
			state_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)
			self._on_model_updated(data_model)
			self._on_model_updated(state_model)

			if '--test-pairs' in sys.argv:
				import ipdb; ipdb.set_trace()  # FIXME: must be removed

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# print(self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80], file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		TopWidgetMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		data_model = self.data_model
		state_model = self.state_model
		view = self._view

		if model is settings_model:
			# if current[0] is None or 'pairs_vocabulary' in current[0] and previous[0] != current[0]:
			#     state_model.vocabulary = data_model.vocabularies.get(settings_model.pairs_vocabulary, None)

			if current[0] is None or 'pairs_selected_marks' in current[0] and previous[0] != current[0]:
				state_model.selected_marks = settings_model.pairs_selected_marks

			if current[0] is None or 'pairs_left_fields_are_hidden' in current[0] and previous[0] != current[0]:
				view.left_field_visibility_button.setChecked(settings_model.pairs_left_fields_are_hidden)
				state_model.left_field_is_hidden.update({k: settings_model.pairs_left_fields_are_hidden for k in state_model.left_field_is_hidden})

			if current[0] is None or 'pairs_right_fields_are_hidden' in current[0] and previous[0] != current[0]:
				view.right_field_visibility_button.setChecked(settings_model.pairs_right_fields_are_hidden)
				state_model.right_field_is_hidden.update({k: settings_model.pairs_right_fields_are_hidden for k in state_model.right_field_is_hidden})

			if current[0] is None or 'pairs_vertical_rows_count' in current[0] and previous[0] != current[0]:
				if settings_model.pairs_vertical_rows_count != len(self.fields):
					Caller.call_once_after(0, self._fill)

			if current[0] is None or 'pairs_horizontal_rows_count' in current[0] and previous[0] != current[0]:
				pass

			if current[0] is None or 'pairs_font' in current[0] and previous[0] != current[0]:
				if settings_model.pairs_font is not None:
					from styles import styles
					font = settings_model.pairs_font
					styles.replace(view.pairs_container, {'* {font: ': self._font_to_style(font)[7:]})

			if current[0] is None or 'pairs_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.pairs_container, {'QLabel {background-color: ': '{0};}}'.format(settings_model.pairs_card_background_color)})

			if current[0] is None or 'pairs_hidden_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.pairs_container, {'QLabel[hidden=true] {background-color: ': '{0};}}'.format(settings_model.pairs_hidden_card_background_color)})

		if model is data_model:
			if current[0] is None or 'vocabularies' in current[0] and previous[:2] != current[:2]:
				state_model.vocabulary = data_model.vocabularies.get(settings_model.pairs_vocabulary, None)

		if model is state_model:
			# # if current[0] is None or 'vocabulary' in current[0] and previous[0] != current[0]:
			# #     settings_model.pairs_vocabulary = state_model.vocabulary.path if state_model.vocabulary is not None else None

			# if current[0] is None or 'left_fields_are_hidden' in current[0] and previous[0] != current[0]:
			#     view.left_field_visibility_button.setChecked(state_model.left_fields_are_hidden)
			#     state_model.left_field_is_hidden.update({k: state_model.left_fields_are_hidden for k in state_model.left_field_is_hidden})

			# if current[0] is None or 'right_fields_are_hidden' in current[0] and previous[0] != current[0]:
			#     view.right_field_visibility_button.setChecked(state_model.right_fields_are_hidden)
			#     state_model.right_field_is_hidden.update({k: state_model.right_fields_are_hidden for k in state_model.right_field_is_hidden})

			if current[0] is None or 'selected_marks' in current[0] and previous[0] != current[0]:
				settings_model.pairs_selected_marks = state_model.selected_marks

			if current[0] is None or 'left_field_is_hidden' in current[0] and previous[:2] != current[:2]:
				for pair_view in state_model.left_field_is_hidden:
					if pair_view.left_field_label.property('hidden') != state_model.left_field_is_hidden[pair_view]:
						pair_view.left_field_label.setProperty('hidden', state_model.left_field_is_hidden[pair_view])
						pair_view.left_field_label.style().unpolish(pair_view.left_field_label)
						pair_view.left_field_label.style().polish(pair_view.left_field_label)

			if current[0] is None or 'right_field_is_hidden' in current[0] and previous[:2] != current[:2]:
				for pair_view in state_model.right_field_is_hidden:
					if pair_view.right_field_label.property('hidden') != state_model.right_field_is_hidden[pair_view]:
						pair_view.right_field_label.setProperty('hidden', state_model.right_field_is_hidden[pair_view])
						pair_view.right_field_label.style().unpolish(pair_view.right_field_label)
						pair_view.right_field_label.style().polish(pair_view.right_field_label)

	""" View's event handlers """

	def _on_vocabulary_selected(self, vocabulary):
		self.settings_model.pairs_vocabulary = vocabulary.path if vocabulary is not None else None

	def __on_menu_pressed(self, event):
		PairsSettingsController(settings_model=self.settings_model).show()

	def _on_settings_button_clicked(self, event):
		PairsSettingsController(settings_model=self.settings_model).show()

	def __on_left_field_visibility_button_toggled(self):
		# self.state_model.left_fields_are_hidden = self._view.left_field_visibility_button.isChecked()
		self.settings_model.pairs_left_fields_are_hidden = self._view.left_field_visibility_button.isChecked()

	def __on_right_field_visibility_button_toggled(self):
		# self.state_model.right_fields_are_hidden = self._view.right_field_visibility_button.isChecked()
		self.settings_model.pairs_right_fields_are_hidden = self._view.right_field_visibility_button.isChecked()

	def __on_reload_button_clicked(self):
		Caller.call_once_after(0, self._fill)

		Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Action Reload ' + self.__class__.__name__))

	def __on_left_field_label_clicked(self, pair_view):
		self.state_model.left_field_is_hidden[pair_view] = not self.state_model.left_field_is_hidden[pair_view]

	def __on_right_field_label_clicked(self, pair_view):
		self.state_model.right_field_is_hidden[pair_view] = not self.state_model.right_field_is_hidden[pair_view]

	""" Helpers """

	def _fill(self):
		TopWidgetMixture._fill(self)

		settings_model = self.settings_model
		state_model = self.state_model
		vocabulary = self.state_model.vocabulary
		view = self._view

		if vocabulary is not None:
			fields = [pair for mark in state_model.selected_marks for pair in vocabulary[mark]]
			self.fields = fields = random.sample(fields, min(len(fields), settings_model.pairs_vertical_rows_count))

			for pair_view in self.pairs_views:
				while view.pairs_container.layout().count():
					view.pairs_container.layout().takeAt(0).widget().deleteLater()

			self.pairs_views[:] = [self._load_ui('views/pairs_item_view.ui') for i in range(len(fields))]  # Qt-resources have to be imported already

			for pair, pair_view in zip(fields, self.pairs_views):
				view.pairs_container.layout().addWidget(pair_view)

				pair_view.left_field_label.setText(pair[0] if pair is not None else '')
				pair_view.right_field_label.setText(pair[1] if pair is not None else '')

				pair_view.left_field_label.mousePressEvent = (lambda pair_view: (lambda event: (self.__on_left_field_label_clicked(pair_view))))(pair_view)
				pair_view.right_field_label.mousePressEvent = (lambda pair_view: (lambda event: (self.__on_right_field_label_clicked(pair_view))))(pair_view)

			state_model.left_field_is_hidden.clear()
			state_model.right_field_is_hidden.clear()

			# state_model.left_field_is_hidden.update({k: state_model.left_fields_are_hidden for k in self.pairs_views})
			# state_model.right_field_is_hidden.update({k: state_model.right_fields_are_hidden for k in self.pairs_views})

			state_model.left_field_is_hidden.update({k: settings_model.pairs_left_fields_are_hidden for k in self.pairs_views})
			state_model.right_field_is_hidden.update({k: settings_model.pairs_right_fields_are_hidden for k in self.pairs_views})

			Caller.call_once_after(.5, self._update_last_training_time)


def run_init():
	from models.abstract import ObservableAttrDict, ObservableList

	class Vocabularies(ObservableList):
		def __contains__(self, key):
			if key.__class__ == int and len(self) > key:
				return True
			return super(Vocabularies, self).__contains__(key)

	class Vocabulary(ObservableAttrDict):
		def __init__(self, name, *args, **kwargs):
			self._name = name
			super(Vocabulary, self).__init__(*args, **kwargs)

		def __str__(self):
			return self._name

		@property
		def path(self):
			return 'root/subdirectory/' + self._name

	data_model = ObservableAttrDict(
		vocabularies={
			0: Vocabulary('Körperteile', new=[['Glied', 'конечность'], ['Gelenk', 'сустав'], ['Knochen, Bein', 'кость'], ['Haut', 'кожа'], ['Nagel', 'ноготь'], ['Stirn', 'лоб']], good=[], fair=[], bad=[]),
		},
	)
	settings_model = ObservableAttrDict(
		pairs_vocabulary=0,
		pairs_left_fields_are_hidden=True,
		pairs_right_fields_are_hidden=False,
		pairs_font=None,
		pairs_card_background_color='#ddd',
		pairs_hidden_card_background_color='#bbb',
		pairs_vertical_rows_count=8,
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	controller = PairsController(data_model=data_model, settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
