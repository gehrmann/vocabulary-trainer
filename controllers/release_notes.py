#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module shows dialog with a list of changes in a new version with "Rate me", "Feedback" and "Close" buttons

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtWidgets, uic

from helpers.timer import Timer
from controllers.abstract import ControllerMixture, Network


class ReleaseNotesController(ControllerMixture):
	def __init__(self, settings_model):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# Models
			self.settings_model = settings_model

			# Views
			self.dialog = view = self._load_ui('views/release_notes_dialog.ui', widget_name='QDialog')
			if hasattr(QtCore.Qt, 'MaximizeUsingFullscreenGeometryHint'):
				view.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)
			view.show_rate_application_button.clicked.connect(self.__on_show_rate_application_button_clicked)
			view.show_feedback_email_button.clicked.connect(self.__on_show_feedback_email_button_clicked)

			from changes import CHANGES
			view.release_notes_label.setText(
				''.join(['''<p><b>New in version {}:</b><ul>{}</ul></p>'''.format(
					key,
					''.join(['<li>{}</li>'.format(x) for x in CHANGES[key]]),
				) for key in CHANGES])
			)

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Show Dialog ' + self.__class__.__name__))

	""" View's event handlers """

	def __on_show_rate_application_button_clicked(self):
		"""Opens Google Play Store to rate application"""
		self.java.call('show_rate_application')

		Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Show Dialog Rate Application'))

	def __on_show_feedback_email_button_clicked(self):
		"""Opens email-client to write feedback and send it"""
		self.java.call('show_feedback_email', to='gehrmann.apps@gmail.com', subject='Feedback on ' + os.environ.get('APP_NAME', '<APP_NAME>'))

		Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Show Dialog Feedback Email'))

	""" Helpers """

	def show(self):
		self.dialog.exec_()


def run_init():
	from models.abstract import ObservableAttrDict

	settings_model = ObservableAttrDict(
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	controller = ReleaseNotesController(settings_model=settings_model)
	# controller.dialog.setGeometry(850, 50, 480, 640)
	controller.show()


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
