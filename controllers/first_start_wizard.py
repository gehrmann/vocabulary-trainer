#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module shows dialog with a list of changes in a new version with "Rate me", "Feedback" and "Close" buttons

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtGui, QtWidgets, uic

from helpers.caller import Caller
from helpers.timer import Timer
from controllers.abstract import ControllerMixture, Network


class FirstStartWizardController(ControllerMixture):
	def __init__(self, settings_model):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# Models
			self.settings_model = settings_model

			# Views
			self.dialog = view = self._load_ui('views/first_start_wizard_dialog.ui', widget_name='QDialog')
			if hasattr(QtCore.Qt, 'MaximizeUsingFullscreenGeometryHint'):
				view.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)

			# from changes import CHANGES

			# import PyQt5.QtWebEngineWidgets as we
			# QtCore.pyqtRemoveInputHook(); import ipdb; ipdb.set_trace()  # FIXME: must be removed/commented
			# from PyQt5.QtWebEngineWidgets import QWebEngineView
			# browser = QWebEngineView()

			# view._browser.layout().insertWidget(0, browser)
			# QtCore.pyqtRemoveInputHook(); import ipdb; ipdb.set_trace()  # FIXME: must be removed/commented
			# view.browser.setHtml(
			# browser.setHtml(
			# view.browser.setTextFormat(QtCore.Qt.RichText)
			# view.browser.setText(
			#     ''
			#     # ''.join(['''<p><b>New in version {}:</b><ul>{}</ul></p>'''.format(
			#     #     key,
			#     #     ''.join(['<li>{}</li>'.format(x) for x in CHANGES[key]]),
			#     # ) for key in CHANGES])
			# )

			# try:
			#     result = self.java.call('get_language', ignore_wrong_platform_exception=False)
			# except self.WrongPlatformException:
			#     result = os.environ.get('LANG', None)
			# view.browser.setText('Result: {}'.format(result['result'] if isinstance(result, dict) else result))

			# class CircleWidget(QtWidgets.QWidget):
			#     def __init__(self, parent, color=None):
			#         super(CircleWidget, self).__init__(parent)
			#         self._color = color

			#         self.resize(0, 0)
			#         self.setBackgroundRole(QtGui.QPalette.Base)
			#         self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

			#     def paintEvent(self, event):
			#         with QtGui.QPainter(self) as painter:
			#             painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
			#             painter.setPen(QtGui.QPen(QtGui.QColor(0, 0, 0, 0), 1))
			#             painter.setBrush(QtGui.QBrush(QtGui.QColor(self._color or '#FFFFFFFF'), QtCore.Qt.SolidPattern))

			#             # painter.drawRoundedRect(0, 0, self.width(), self.height(), 1, 1)
			#             # painter.drawRect(0, 0, self.width(), self.height())

			#             diameter = min(self.width(), self.height()) - 2
			#             # painter.translate(self.width() // 2, self.height() // 2)
			#             painter.drawEllipse(1, 1, diameter, diameter)

			# class CurveWidget(QtWidgets.QWidget):
			#     def __init__(self, parent, color=None):
			#         super(CurveWidget, self).__init__(parent)
			#         self._color = color

			#         self.resize(0, 0)
			#         self.setBackgroundRole(QtGui.QPalette.Base)
			#         self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

			#     def paintEvent(self, event):
			#         with QtGui.QPainter(self) as painter:
			#             painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
			#             painter.setPen(QtGui.QPen(QtGui.QColor(0, 0, 0, 0), 1))
			#             painter.setBrush(QtGui.QBrush(QtGui.QColor(self._color or '#FFFFFFFF'), QtCore.Qt.SolidPattern))

			#             # painter.drawRect(0, 0, self.width(), self.height())
			#             # painter.drawRoundedRect(0, 0, self.width(), self.height(), 1, 1)

			#             path = QtGui.QPainterPath()
			#             path.moveTo(0, 0)
			#             path.cubicTo(self.width() // 2, 0, self.width() // 2, self.height(), self.width(), self.height())
			#             # path.lineTo(0, self.height())
			#             # path.lineTo(0, 0)
			#             painter.drawPath(path)

			# duration = 1.
			# delay = 0.
			# for (x, y), size, color in (
			#         ((95, 33), 48, '#F05223'),
			#         ((138, 52), 29, '#34B7D3'),
			#         ((177, 42), 30, '#34B7D3'),
			#         ((191, 80), 33, '#172947'),
			#         ((76, 75), 28, '#172947'),
			#         ((51, 110), 40, '#34b7d3'),
			#         ((86, 141), 35, '#B5B1AB'),
			#         ((139, 111), 63, '#34b7d3'),
			#         ((180, 110), 13, '#34b7d3'),
			#         ((115, 150), 13, '#34b7d3'),
			# ):
			#     delay += 0.1

			#     # Size must be odd number
			#     if not size % 2:
			#         size -= 1

			#     # Creates circles
			#     circle = CircleWidget(view.scrollAreaWidgetContents, color=color)
			#     shift = (size + 3) / 2
			#     # circle.resize(size, size)
			#     self._animate_sequential(circle, key='geometry', curve='OutCirc', animations=(
			#         delay,
			#         dict(from_value=(x, y, 0, 0), to_value=(x - shift, y - shift, size, size), duration=duration),
			#     )).start()

			#     # Creates lines
			#     line = CurveWidget(view.scrollAreaWidgetContents, color=color)
			#     # line.move(x + 0, y + 0)
			#     # line.resize(100, 100)
			#     self._animate_sequential(line, key='geometry', curve='OutCirc', animations=(
			#         delay,
			#         dict(from_value=(x, y, 0, 0), to_value=(x, y, 100, 100), duration=duration),
			#     )).start()
			# # line = CurveWidget(view.scrollAreaWidgetContents, color=color)

			# class DrawWidget(QtWidgets.QLabel):
			#     def __init__(self, parent):
			#         super(DrawWidget, self).__init__(parent)
			#         self.resize(100, 100)
			#         self.setBackgroundRole(QtGui.QPalette.Base)
			#         self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

			#     def paintEvent(self, event):
			#         """
			#         Android:
			#         * QPen works worse, breaks drawRect, must be transparent
			#         * QRect works worse: breaks drawEllipse, must be python types
			#         """
			#         super(DrawWidget, self).paintEvent(event)
			#         with QtGui.QPainter(self) as painter:
			#             # painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
			#             # painter.setPen(QtGui.QPen(QtGui.QColor('#FFFFCC66'), 3))
			#             # painter.setBrush(QtGui.QBrush(QtGui.QColor('#FFFFFFFF'), QtCore.Qt.SolidPattern))
			#             painter.setPen(QtGui.QPen(QtGui.QColor(0, 0, 0, 0), 1))
			#             # painter.setBrush(QtGui.QBrush(QtGui.QColor('#FF66CC'), QtCore.Qt.SolidPattern))
			#             painter.setBrush(QtGui.QBrush(QtGui.QColor(255, 24, 24, 255), QtCore.Qt.SolidPattern))

			#             # painter.drawRoundedRect(0, 0, self.width(), self.height(), 1, 1)
			#             # painter.drawRect(0, 0, self.width(), self.height())
			#             painter.drawRect(60., 60., 20., 20.)

			#             diameter = min(self.width(), self.height()) // 4 - 2
			#             # painter.translate(self.width() // 2, self.height() // 2)
			#             painter.drawEllipse(2, 2, diameter, diameter)
			#             # painter.drawEllipse(QtCore.QRectF(2, 2, diameter, diameter))
			#             # painter.drawEllipse(QtCore.QRect(-diameter / 2, -diameter / 2, diameter, diameter))
			# parent = view.scrollAreaWidgetContents
			# # parent = view.browser
			# self._draw_widget = w = DrawWidget(parent)
			# w.setStyleSheet('border: 1px solid red; background-color: transparent;')
			# # parent.layout().addWidget(w)

			# parent = view.scrollAreaWidgetContents
			# from AnimationWidgets import CircleWidget
			# w = CircleWidget(parent)
			# w.move(100, 100)
			# w.resize(100, 100)
			# # parent.layout().insertWidget(0, w)

			import AnimationWidgets

			# # class _CircleWidget(QtWidgets.QWidget):
			# #     def __init__(self, parent):
			# #         super(_CircleWidget, self).__init__(parent)
			# #         self.resize(50, 50)
			# #         self.move(50, 50)

			# #     # def paintEvent(self, event):
			# #     #     with QtGui.QPainter(self) as painter:
			# #     #         painter.setBrush(QtGui.QBrush(QtGui.QColor('#FF660000'), QtCore.Qt.SolidPattern))

			# #     #         diameter = min(self.width(), self.height()) - 2
			# #     #         painter.drawEllipse(1, 1, diameter, diameter)

			# self._widgets = widgets = []

			# class CircleWidget(AnimationWidgets.CircleWidget):
			#     def __init__(self, parent, width=0, height=0, border=0, color=None, thickness=0, background=None, radius=0):
			#         super(CircleWidget, self).__init__(parent, width, height, border, color, thickness, background, radius)
			#         # self.resize(50, 50)
			#         # self.move(100, 50)

			#     # def paintEvent(self, event):
			#     #     logging.getLogger(__name__).warning('here')
			#     #     pass
			#     #     super(CircleWidget, self).paintEvent(event)
			#     #     with QtGui.QPainter(self) as painter:
			#     #         pass
			#     #         painter.setBrush(QtGui.QBrush(QtGui.QColor('#FF660000'), QtCore.Qt.SolidPattern))

			#     #         diameter = min(self.width(), self.height()) - 2
			#     #         painter.drawEllipse(1, 1, diameter, diameter)

			# # parent = view.scrollAreaWidgetContents
			# # # self._draw_widget = w = AnimationWidgets.CircleWidget(parent)

			# # w = _CircleWidget(view.scrollAreaWidgetContents)
			# # w = CircleWidget(view.scrollAreaWidgetContents)
			# # w.update()
			# # w.repaint()
			# # logging.getLogger(__name__).warning('w.radius=' + '%s', w.radius)

			# duration = 1.
			# delay = 0.
			# for (x, y), radius, color in (
			#         ((95, 33), 22, '#F05223'),
			#         ((138, 52), 14, '#34B7D3'),
			#         ((177, 42), 15, '#34B7D3'),
			#         ((191, 80), 17, '#172947'),
			#         ((76, 75), 14, '#172947'),
			#         ((51, 110), 20, '#34b7d3'),
			#         ((86, 141), 17, '#B5B1AB'),
			#         ((139, 111), 28, '#34b7d3'),
			#         ((180, 110), 6, '#34b7d3'),
			#         ((115, 150), 6, '#34b7d3'),
			# ):
			#     delay += 0.1

			#     # # Radius must be odd number
			#     # if not size % 2:
			#     #     size -= 1

			#     # # Creates circles
			#     diameter = 2 * radius
			#     circle_widget = CircleWidget(view.scrollAreaWidgetContents, width=diameter, height=diameter, border=0, background=color)
			#     self._widgets.append(circle_widget)
			#     circle_widget.move(x - radius, y - radius)
			#     self._animate_sequential(circle_widget, key='radius', curve='OutCirc', animations=(
			#         delay,
			#         dict(from_value=0, to_value=radius, duration=duration),
			#     )).start()

				# # Creates lines
				# path = QtGui.QPainterPath()
				# path.moveTo(0, 0)
				# path.moveTo(1, 2)
				# path.moveTo(QtCore.QPointF(1, 2))
				# path.lineTo(QtCore.QPointF(3, 4))
				# path.cubicTo(0, 0, 0, 0, 0, 0)

				# width, height = 50, 150
				# thickness = 1
				# path_widget = PathWidget(
				#     view.scrollAreaWidgetContents,
				#     width=width, height=height,
				#     border=1,
				#     color=color,
				#     thickness=thickness,
				#     path=path,
				# )
				# self.__dict__.setdefault('_widgets', []).append(path_widget)
				# # path_widget.show()
			#     # path_widget = PathWidget(view.scrollAreaWidgetContents, border=0, color=color, thickness=thickness)
			#     self._widgets.append(path_widget)
			#     #
			#     # path_widget.moveTo(thickness, thickness)
			#     # path_widget.cubicTo(
			#     #     0, 8. * height // 10,
			#     #     10. * width // 10, 10. * height // 10,
			#     #     width - thickness, height - thickness,
			#     # )
			#     #
			#     # self.w.append(path)
			#     # self.w.append(path_widget)
				# self._animate_sequential(path_widget, key='geometry', curve='OutCirc', animations=(
				#     delay,
				#     dict(from_value=(x - thickness, y - thickness, 0, 0), to_value=(x - thickness, y - thickness, width, height), duration=duration),
				# )).start()

			# # Shows green triangle with bezier-curve
			# if True:
			#     class PathWidget(AnimationWidgets.PathWidget):
			#         def __init__(self, parent, width=0, height=0, border=0, color='#660000', thickness=1, background='#006600', path=None):
			#             super(PathWidget, self).__init__(parent, width, height, border, color, thickness, background, path)

			#     # Creates lines
			#     path = QtGui.QPainterPath()
			#     path.moveTo(10, 10)
			#     path.lineTo(90, 50)
			#     path.lineTo(10, 90)
			#     path.lineTo(10, 10)
			#     path.moveTo(QtCore.QPointF(20, 20))
			#     path.lineTo(QtCore.QPointF(50, 50))
			#     path.cubicTo(100, 0, 0, 100, 100, 100)

			#     path_widget = PathWidget(parent=view.scrollAreaWidgetContents, width=100, height=100, border=1, path=path)
			#     path_widget.show()
			#     self.__dict__.setdefault('_widgets', []).append(path_widget)

			# Shows animated air-ballon
			if True:
				def run():
					duration = 18
					# curve = 'OutCirc'
					# curve = 'SineCurve'
					# ControllerMixture._animate_sequential(w, key='angle', curve=curve, count=9999, animations=(
					#     dict(from_value=-15, to_value=15, duration=duration),
					# )).start()
					# self._animate_parallel(view.air_balloon, animations=(
					#     dict(key='angle', curve='SineCurve', from_value=-15, to_value=15, duration=1., count=9999),
					#     dict(key='pos', curve='InCirc', from_value=(-100, 50), to_value=(600, -15), duration=8.),
					# )).start()

					self._animate_sequential(view.air_balloon, animations=(
						dict(key='angle', curve='SineCurve', from_value=-5, to_value=5, duration=5., count=9999),
					)).start()
					self._animate_sequential(view.air_balloon, duration=duration, animations=(
						dict(key='pos', curve='SineCurve', from_value=(-50, 200), to_value=(100, 200)),
						dict(key='pos', curve='InCirc', from_value=(-50, 50), to_value=(view.width(), -15)),
						dict(key='pos', curve='InCirc', from_value=(view.width(), 400), to_value=(-100, -15)),
					)).start()
				view.air_balloon.move(-50, 0)
				Caller.call_once_after(.5, run)

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Show Dialog ' + self.__class__.__name__))

			# self._check_qreal()

	""" View's event handlers """

	""" Helpers """

	def show(self):
		self.dialog.exec_()

	# def _check_qreal(self):
	#     """Append it into __init__ and compile library in a qreal debugging state"""
	#     import AnimationWidgets

	#     class PathWidget(AnimationWidgets.PathWidget):
	#         def __init__(self, parent, width=0, height=0, border=0, color='#FF0000', thickness=1, background='#FF0000', path=0):
	#             # super(PathWidget, self).__init__(parent, width, height, border, color, thickness, background, path)
	#             super(PathWidget, self).__init__(parent, width, height, QtCore.QPointF(1, 1), color, thickness, background, path)

	#     # Creates lines
	#     path = QtGui.QPainterPath()
	#     path.moveTo(0, 0)
	#     path.moveTo(1, 2)
	#     path.moveTo(QtCore.QPointF(1, 2))
	#     path.lineTo(QtCore.QPointF(3, 4))
	#     path.cubicTo(0, 0, 0, 0, 0, 0)
	#     print("Python: path=(len=%d, length=%f)" % (path.elementCount(), path.length()), file=sys.stderr);
	#     for i in range(path.elementCount()):
	#         print("  #%d %d/%d/%d  (%f, %f, %d)" % (
	#             i,
	#             path.elementAt(i).isMoveTo(), path.elementAt(i).isLineTo(), path.elementAt(i).isCurveTo(),
	#             path.elementAt(i).x, path.elementAt(i).y, path.elementAt(i).type,
	#         ), file=sys.stderr)

	#     path = PathWidget(self.dialog, 0, 0, 0, path=path)


def run_init():
	from models.abstract import ObservableAttrDict

	settings_model = ObservableAttrDict(
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	controller = FirstStartWizardController(settings_model=settings_model)
	# controller.dialog.setGeometry(850, 50, 480, 640)
	controller.show()


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
