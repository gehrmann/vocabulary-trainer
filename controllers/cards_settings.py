#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a cards-tab's settings

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtWidgets, uic

from helpers.timer import Timer
from controllers.abstract import ControllerMixture, CommonSettingsMixture, Network


class CardsSettingsController(ControllerMixture, CommonSettingsMixture):
	def __init__(self, settings_model):
		with Timer('initializing'):
			ControllerMixture.__init__(self)
			CommonSettingsMixture.__init__(self)

			# Models
			self.settings_model = settings_model

			# Views
			self._view = view = self._load_ui('views/cards_settings_widget.ui')
			self.dialog.settings_widget.layout().addWidget(view)

			view.vertical_rows_count_dropdown.activated.connect(self.__on_vertical_rows_count_dropdown_changed)
			view.vertical_columns_count_dropdown.activated.connect(self.__on_vertical_columns_count_dropdown_changed)
			view.background_arrangement_dropdown.activated.connect(self.__on_background_arrangement_dropdown_changed)
			# view.horizontal_columns_count_dropdown.activated.connect(self.__on_horizontal_columns_count_dropdown_changed)
			# view.horizontal_rows_count_dropdown.activated.connect(self.__on_horizontal_rows_count_dropdown_changed)
			view.font_button.clicked.connect(self.__on_font_button_clicked)
			view.rounded_corners_checkbox.stateChanged.connect(self.__on_rounded_corners_checkbox_changed)
			view.left_card_color_button.clicked.connect(self.__on_left_card_color_button_clicked)
			view.right_card_color_button.clicked.connect(self.__on_right_card_color_button_clicked)
			view.card_background_color_button.clicked.connect(self.__on_card_background_color_button_clicked)
			view.selected_card_background_color_button.clicked.connect(self.__on_selected_card_background_color_button_clicked)
			view.guessed_card_background_color_button.clicked.connect(self.__on_guessed_card_background_color_button_clicked)
			view.guessed_card_delay_timeout_dropdown.activated.connect(self.__on_guessed_card_delay_timeout_dropdown_changed)
			view.guessed_card_delay_opacity_dropdown.activated.connect(self.__on_guessed_card_delay_opacity_dropdown_changed)

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Show Dialog ' + self.__class__.__name__))

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		CommonSettingsMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		view = self._view

		if model is settings_model:
			if current[0] is None or 'cards_vertical_rows_count' in current[0] and previous[0] != current[0]:
				view.vertical_rows_count_dropdown.setCurrentIndex(view.vertical_rows_count_dropdown.findText(str(settings_model.cards_vertical_rows_count)))

			if current[0] is None or 'cards_vertical_columns_count' in current[0] and previous[0] != current[0]:
				view.vertical_columns_count_dropdown.setCurrentIndex(view.vertical_columns_count_dropdown.findText(str(settings_model.cards_vertical_columns_count)))

			if current[0] is None or 'cards_background_arrangement' in current[0] and previous[0] != current[0]:
				view.background_arrangement_dropdown.setCurrentIndex(view.background_arrangement_dropdown.findText(settings_model.cards_background_arrangement))

			# if current[0] is None or 'cards_horizontal_rows_count' in current[0] and previous[0] != current[0]:
			#     view.horizontal_rows_count_dropdown.setCurrentIndex(view.horizontal_rows_count_dropdown.findText(str(settings_model.cards_horizontal_rows_count)))

			# if current[0] is None or 'cards_horizontal_columns_count' in current[0] and previous[0] != current[0]:
			#     view.horizontal_columns_count_dropdown.setCurrentIndex(view.horizontal_columns_count_dropdown.findText(str(settings_model.cards_horizontal_columns_count)))

			if current[0] is None or 'cards_font' in current[0] and previous[0] != current[0]:
				view.font_button.setText(self._font_to_text(settings_model.cards_font))

			if current[0] is None or 'cards_rounded_corners' in current[0] and previous[0] != current[0]:
				view.rounded_corners_checkbox.setChecked(settings_model.cards_rounded_corners)

			if current[0] is None or 'cards_left_card_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.left_card_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cards_left_card_color)})

			if current[0] is None or 'cards_right_card_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.right_card_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cards_right_card_color)})

			if current[0] is None or 'cards_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.card_background_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cards_card_background_color)})

			if current[0] is None or 'cards_selected_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.selected_card_background_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cards_selected_card_background_color)})

			if current[0] is None or 'cards_guessed_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.guessed_card_background_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cards_guessed_card_background_color)})

			if current[0] is None or 'cards_guessed_card_delay_timeout' in current[0] and previous[0] != current[0]:
				view.guessed_card_delay_timeout_dropdown.setCurrentIndex(view.guessed_card_delay_timeout_dropdown.findText(str(settings_model.cards_guessed_card_delay_timeout)))

			if current[0] is None or 'cards_guessed_card_delay_opacity' in current[0] and previous[0] != current[0]:
				view.guessed_card_delay_opacity_dropdown.setCurrentIndex(view.guessed_card_delay_opacity_dropdown.findText(str(settings_model.cards_guessed_card_delay_opacity)))

	""" View's event handlers """

	def __on_vertical_rows_count_dropdown_changed(self, current_index):
		self.settings_model.cards_vertical_rows_count = int(self._view.vertical_rows_count_dropdown.currentText())

	def __on_vertical_columns_count_dropdown_changed(self, current_index):
		self.settings_model.cards_vertical_columns_count = int(self._view.vertical_columns_count_dropdown.currentText())

	def __on_background_arrangement_dropdown_changed(self, current_index):
		self.settings_model.cards_background_arrangement = str(self._view.background_arrangement_dropdown.currentText())

	# def __on_horizontal_columns_count_dropdown_changed(self, current_index):
	#     self.settings_model.cards_horizontal_columns_count = int(self._view.horizontal_columns_count_dropdown.currentText())

	# def __on_horizontal_rows_count_dropdown_changed(self, current_index):
	#     self.settings_model.cards_horizontal_rows_count = int(self._view.horizontal_rows_count_dropdown.currentText())

	def __on_font_button_clicked(self):
		self._show_font_dialog(model=self.settings_model, key='cards_font')

	def __on_rounded_corners_checkbox_changed(self):
		self.settings_model.cards_rounded_corners = self._view.rounded_corners_checkbox.isChecked()

	def __on_left_card_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cards_left_card_color')

	def __on_right_card_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cards_right_card_color')

	def __on_card_background_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cards_card_background_color')

	def __on_selected_card_background_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cards_selected_card_background_color')

	def __on_guessed_card_background_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cards_guessed_card_background_color')

	def __on_guessed_card_delay_timeout_dropdown_changed(self, current_index):
		self.settings_model.cards_guessed_card_delay_timeout = int(self._view.guessed_card_delay_timeout_dropdown.currentText())

	def __on_guessed_card_delay_opacity_dropdown_changed(self, current_index):
		self.settings_model.cards_guessed_card_delay_opacity = max(0., min(1., float(self._view.guessed_card_delay_opacity_dropdown.currentText())))


def run_init():
	from models.abstract import ObservableAttrDict, ObservableList

	class BackgroundsCategory(ObservableList):
		def __init__(self, name):
			self._name = name
			super(BackgroundsCategory, self).__init__()

		def __str__(self):
			return self._name

		def __repr__(self):
			return object.__repr__(self).replace(' object at ', ' name="{0._name}" object at '.format(self))

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	settings_model = ObservableAttrDict(
		font=dict(family='DejaVu Sans', size=.75 * styles.mm2px(2.6), weight=12, italic=False),
		button_size=6,
		close_on_double_back=True,
		current_tab=0,

		cards_vocabulary=None,
		cards_backgrounds_category='miscellaneous',
		cards_font=dict(family='Droid Sans', size=.75 * styles.mm2px(2.8), weight=50, italic=False),
		cards_left_card_color='#600',
		cards_right_card_color='#060',
		cards_card_background_color='#ddd',
		cards_selected_card_background_color='#bbb',
		cards_rounded_corners=True,
		cards_vertical_rows_count=4,
		cards_vertical_columns_count=3,
		cards_background_arrangement='cover',
		cards_guessed_card_background_color='#bbb',
		cards_guessed_card_delay_timeout=8,
		cards_guessed_card_delay_opacity=.3,
	)

	controller = CardsSettingsController(settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)
	controller.show()


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
