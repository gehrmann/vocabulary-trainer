#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a dict-tab

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )

Todo:
	* Fast implementation of dictd
"""

import datetime
import logging
import os
import re
import signal
import sys
import threading
import urllib
import urllib.parse
from html.parser import HTMLParser

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtGui, QtWidgets

from controllers.abstract import ControllerMixture, Network, VocabulariesDialog, DictionariesDialog
from controllers.dict_settings import DictSettingsController
from controllers.top_widget_mixture import TopWidgetMixture
from helpers.caller import Caller
from helpers.timer import Timer
from models.abstract import ObservableAttrDict, UndoRedo


class DictController(ControllerMixture, TopWidgetMixture):
	def __init__(self, parent_view=None, test=False, data_model=None, settings_model=None):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# Models
			""" Current-State-Model for selected view's data"""
			state_model = self.state_model
			state_model.vocabulary = None
			state_model.dictionaries = []
			state_model.left_field = ''
			state_model.right_field = ''
			state_model.right_field_edited = False
			state_model.variants_keywords = ''
			# state_model, self._states[self._index].vocabularies_to_vocabularies = dict()  # Relation between views and models

			self._punctuation_signs = (';', ',', ' (', '(', ')', '|', ' [', '[', ']', '¬', '⌒')
			self._variants = []
			self._articles = []

			""" Undo-Redo-Model saves Current-State-Model with .save() and restores previous saved states with .undo() and .redo() """
			self.undo_redo_model = UndoRedo(state_model, skip_first=True)
			self.double_undo_redo_model = UndoRedo(state_model, skip_first=True)

			self.data_model = data_model

			self.settings_model = settings_model

			# Views
			self._view = view = self._load_ui('views/dict_view.ui')  # 220ms
			if parent_view is not None:
				parent_view.addWidget(view)

			TopWidgetMixture.__init__(
				self,
				parent_view=view.top_widget,
				with_dictionaries_button=True,
				with_change_button=True,
				with_undo_redo_buttons=True,
				with_double_undo_redo_buttons=True,
				with_save_button=True,
			)
			self.top_widget.dictionaries_button.clicked.connect(self.__on_dictionaries_button_clicked)
			self.top_widget.change_button.clicked.connect(self.__on_change_button_clicked)
			self.top_widget.undo_button.clicked.connect(self.__on_undo_button_clicked)
			self.top_widget.redo_button.clicked.connect(self.__on_redo_button_clicked)
			self.top_widget.double_undo_button.clicked.connect(self.__on_double_undo_button_clicked)
			self.top_widget.double_redo_button.clicked.connect(self.__on_double_redo_button_clicked)
			self.top_widget.save_button.clicked.connect(self.__on_save_button_clicked)

			view.clear_left_field_button.clicked.connect(self.__on_clear_left_field_button_clicked)
			view.left_field_input.textEdited.connect(self.__on_left_field_input_edited)
			view.left_field_input.paintEvent = (lambda widget, previous_callback: (lambda event: (self.__on_left_field_input_painting(event, widget, previous_callback))))(view.left_field_input, view.left_field_input.paintEvent)
			# view.left_field_input.focusInEvent = (lambda old_method: (lambda event: (self.__expand_left(view.left_field_input, view.dictionaries_dropdown), old_method(event))[-1]))(view.left_field_input.focusInEvent)
			# view.dictionaries_dropdown.activated.connect(self.__on_dictionaries_dropdown_activated)
			# view.dictionaries_dropdown.focusInEvent = (lambda old_method: (lambda event: (self.__expand_right(view.left_field_input, view.dictionaries_dropdown), old_method(event))[-1]))(view.dictionaries_dropdown.focusInEvent)
			# # view.dictionaries_dropdown.showPopup = (lambda old_method: (lambda: (self.__expand_right(view.left_field_input, view.dictionaries_dropdown), old_method())[-1]))(view.dictionaries_dropdown.showPopup)
			# # view.dictionaries_dropdown.showEvent = (lambda old_method: (lambda event: (self.__expand_right(view.left_field_input, view.dictionaries_dropdown), old_method(event))[-1]))(view.dictionaries_dropdown.showEvent)
			view.clear_right_field_button.clicked.connect(self.__on_clear_right_field_button_clicked)
			view.right_field_input.textEdited.connect(self.__on_right_field_input_edited)
			view.right_field_input.paintEvent = (lambda widget, previous_callback: (lambda event: (self.__on_right_field_input_painting(event, widget, previous_callback))))(view.right_field_input, view.right_field_input.paintEvent)
			# view.vocabulary_button.clicked.connect(self.__on_vocabulary_button_clicked)
			# view.right_field_input.focusInEvent = (lambda old_method: (lambda event: (self.__expand_left(view.right_field_input, view.vocabulary_dropdown), old_method(event))[-1]))(view.right_field_input.focusInEvent)
			# view.vocabulary_dropdown.activated.connect(self.__on_vocabulary_dropdown_activated)
			# view.vocabulary_dropdown.focusInEvent = (lambda old_method: (lambda event: (self.__expand_right(view.right_field_input, view.vocabulary_dropdown), old_method(event))[-1]))(view.vocabulary_dropdown.focusInEvent)
			# # view.vocabulary_dropdown.showPopup = (lambda old_method: (lambda: (self.__expand_right(view.right_field_input, view.vocabulary_dropdown), old_method())[-1]))(view.vocabulary_dropdown.showPopup)
			# # view.vocabulary_dropdown.showEvent = (lambda old_method: (lambda event: (self.__expand_right(view.right_field_input, view.vocabulary_dropdown), old_method(event))[-1]))(view.vocabulary_dropdown.showEvent)
			view.signs_browser.anchorClicked.connect(self.__on_article_browser_link_clicked)
			view.article_browser.anchorClicked.connect(self.__on_article_browser_link_clicked)

			if parent_view is None:
				self._restore_window_geometry()
				view.closeEvent = self._on_close
				view.keyPressEvent = self._on_key_pressed
				view.show()

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)
			data_model.changed.bind(self._on_model_updated)
			state_model.changed.bind(self._on_model_updated, invoke_in_main_thread=True)  # Invoke in main because the progress is running from thread

			""" Fill blank view by models """
			self._on_model_updated(settings_model)
			self._on_model_updated(data_model)
			self._on_model_updated(state_model)

			if '--test-dict' in sys.argv:
				import ipdb; ipdb.set_trace()  # FIXME: must be removed

			Caller.call_once_after(3.03, self.undo_redo_model.save)  # Wait for vocabularies and dictionaries are loaded
			Caller.call_once_after(3.03, self.double_undo_redo_model.save)  # Wait for vocabularies and dictionaries are loaded

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# print(self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80], file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		TopWidgetMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		data_model = self.data_model
		state_model = self.state_model
		view = self._view
		top_widget = self.top_widget

		if model is settings_model:
			if current[0] is None or 'dict_dictionaries' in current[0] and previous[0] != current[0]:
				state_model.dictionaries = list(filter(None, [data_model.dictionaries[x] for x in settings_model.dict_dictionaries]))

			if current[0] is None or 'dict_left_field' in current[0] and previous[0] != current[0]:
				state_model.left_field = settings_model.dict_left_field

			if current[0] is None or 'dict_variants_keywords' in current[0] and previous[0] != current[0]:
				state_model.variants_keywords = settings_model.dict_variants_keywords

				Caller.call_once_after(.1, self._fill_variants)
				Caller.call_once_after(.3, self._fill_articles)

			if current[0] is None or 'dict_font' in current[0] and previous[0] != current[0]:
				if settings_model.dict_font is not None:
					from styles import styles
					font = settings_model.dict_font
					styles.replace(view.signs_browser, {'* {font: ': self._font_to_style(font)[7:]})
					styles.replace(view.article_browser, {'* {font: ': self._font_to_style(font)[7:]})

			if current[0] is None or 'dict_variants_font' in current[0] and previous[0] != current[0]:
				Caller.call_once_after(.5, self._fill_article_browser)

			if current[0] is None or 'dict_variants_color' in current[0] and previous[0] != current[0]:
				Caller.call_once_after(.5, self._fill_article_browser)

			if current[0] is None or 'dict_show_punctuation_signs' in current[0] and previous[0] != current[0]:
				Caller.call_once_after(.5, self._fill_article_browser)

			if current[0] is None or 'dict_punctuation_signs_font' in current[0] and previous[0] != current[0]:
				Caller.call_once_after(.5, self._fill_article_browser)

			if current[0] is None or 'dict_clickable_words' in current[0] and previous[0] != current[0]:
				Caller.call_once_after(.5, self._fill_article_browser)

			if current[0] is None or 'dict_article_color' in current[0] and previous[0] != current[0]:
				Caller.call_once_after(.5, self._fill_article_browser)

		if model is data_model:
			if current[0] is None or 'vocabularies' in current[0] and previous[:2] != current[:2]:
				Caller.call_once_after(.4, self._fill_pair_from_vocabularies)

			if current[0] is None or 'dictionaries' in current[0] and previous[:2] != current[:2]:
				state_model.dictionaries = list(filter(None, [data_model.dictionaries[x] for x in settings_model.dict_dictionaries]))

		if model is state_model:
			if current[0] is None or 'vocabulary' in current[0] and previous[0] != current[0]:
				view.right_field_input.update()  # Repaint a sub-text in widget

			if current[0] is None or 'dictionaries' in current[0] and previous[0] != current[0]:
				tips = ''
				if not data_model.dictionaries:
					# Shows tip: how to select dictinaries
					tips += 'Click &nbsp;<img width="{0}" height="{0}" src="qrc:/images/dictionaries-dark.png">&nbsp; to download dictionaries.'.format(int(3 * view.physicalDpiX() / 25.4))
				elif not state_model.dictionaries:
					# Shows tip: how to select dictinaries
					tips += 'Click &nbsp;<img width="{0}" height="{0}" src="qrc:/images/dictionaries-dark.png">&nbsp; to select dictionaries.'.format(int(3 * view.physicalDpiX() / 25.4))
				state_model.tips = tips

			if current[0] is None or 'dictionaries' in current[0] and previous[0] != current[0]:
				view.left_field_input.update()  # Repaint a sub-text in widget

				Caller.call_once_after(.1, self._fill_variants)  # Long delay to allow dictionaries to start loading
				Caller.call_once_after(.3, self._fill_articles)  # Long delay to allow dictionaries to start loading

			if current[0] is None or 'left_field' in current[0] and previous[0] != current[0]:
				# If left_field was changed to something different
				if previous[0] is None or previous[0].get('left_field', None) is None or self._cut_insignificant_keywords_parts(previous[0]['left_field']) != self._cut_insignificant_keywords_parts(state_model.left_field):
					Caller.call_once_after(.3, self._fill_articles)
					Caller.call_once_after(.5, self._fill_article_browser)

					# Looks for existed pair in vocabularies
					Caller.call_once_after(.4, self._fill_pair_from_vocabularies)  # Slows during keyboard typing

				# Updates left_field_input
				if view.left_field_input.text() != state_model.left_field:
					view.left_field_input.setText(state_model.left_field)

				# Saves changed value
				settings_model.dict_left_field = state_model.left_field

			if current[0] is None or 'right_field' in current[0] and previous[0] != current[0]:
				if view.right_field_input.text() != state_model.right_field:
					view.right_field_input.setText(state_model.right_field)

			if current[0] is None or 'right_field_edited' in current[0] and previous[0] != current[0]:
				getattr(top_widget.save_button, 'show' if state_model.right_field_edited else 'hide')()

			if current[0] is None or 'variants_keywords' in current[0] and previous[0] != current[0]:
				settings_model.dict_variants_keywords = state_model.variants_keywords

				Caller.call_once_after(.1, self._fill_variants)
				# Caller.call_once_after(.3, self._fill_articles)

	""" View's event handlers """

	def __on_left_field_input_painting(self, event, widget, previous_callback):
		state_model = self.state_model

		previous_callback(event)

		text = 'From: {}'.format(', '.join([str(x) for x in state_model.dictionaries]) if state_model.dictionaries else 'not selected')
		self._paint_subtext(event, widget, text)

	def __on_right_field_input_painting(self, event, widget, previous_callback):
		state_model = self.state_model

		previous_callback(event)

		text = 'From: {}'.format(str(state_model.vocabulary) if state_model.vocabulary is not None else 'not found')
		self._paint_subtext(event, widget, text)

	def __on_menu_pressed(self, event):
		DictSettingsController(settings_model=self.settings_model).show()

	def _on_settings_button_clicked(self, event):
		DictSettingsController(settings_model=self.settings_model).show()

	def __on_dictionaries_button_clicked(self, event):
		state_model = self.state_model

		def on_dictionaries_selected(dictionaries):
			self.settings_model.dict_dictionaries = [str(x) for x in dictionaries]

			Caller.call_once_after(.03, self.undo_redo_model.save)
			Caller.call_once_after(.03, self.double_undo_redo_model.save)

		# def on_dictionary_renamed(item, name):
		#     item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

		DictionariesDialog(
			headers=['Dictionary'],
			settings_model=self.settings_model,
			data_model=self.data_model.dictionaries,
			model=state_model,
			key='dictionaries',
			path_attribute='decorated_path',
			path_separator='\\',
			path_prefix='dictionaries/',
			is_expanded=True,
			is_container_selectable=False,
			has_multiselection=True,
			selected=on_dictionaries_selected,
			# renamed=on_dictionary_renamed,
		).show()

	def __on_change_button_clicked(self, event=None):
		self._change_fields()

	def __on_undo_button_clicked(self, event=None):
		self.undo_redo_model.undo()

	def __on_redo_button_clicked(self, event=None):
		self.undo_redo_model.redo()

	def __on_double_undo_button_clicked(self, event=None):
		self.double_undo_redo_model.undo()

	def __on_double_redo_button_clicked(self, event=None):
		self.double_undo_redo_model.redo()

	def __on_clear_left_field_button_clicked(self, event=None):
		state_model = self.state_model

		state_model.left_field = ''
		state_model.variants_keywords = ''

		Caller.call_once_after(.03, self.undo_redo_model.save)

	def __on_clear_right_field_button_clicked(self, event=None):
		state_model = self.state_model

		state_model.right_field = ''
		state_model.right_field_edited = False

		Caller.call_once_after(.03, self.undo_redo_model.save)

	def __on_save_button_clicked(self, event=None):
		state_model = self.state_model

		self._save()

		# state_model.left_field = ''
		# state_model.variants_keywords = ''

		Caller.call_once_after(.03, self.undo_redo_model.save)
		Caller.call_once_after(.03, self.double_undo_redo_model.save)

	def __on_left_field_input_edited(self, text):
		state_model = self.state_model

		state_model.left_field = str(text)
		state_model.variants_keywords = str(text)

		Caller.call_once_after(.03, self.undo_redo_model.save)

	def __on_right_field_input_edited(self, text):
		self.state_model.right_field = str(text)
		self.state_model.right_field_edited = True

		Caller.call_once_after(.03, self.undo_redo_model.save)

	def __on_article_browser_link_clicked(self, url):
		self._open_url(str(url.toString()))

		Caller.call_once_after(.03, self.undo_redo_model.save)

	""" Helpers """

	def _change_fields(self):
		state_model = self.state_model

		state_model.left_field, state_model.right_field = state_model.right_field, state_model.left_field
		if state_model.right_field:
			self.state_model.right_field_edited = True

		Caller.call_once_after(.03, self.undo_redo_model.save)

	def _paint_subtext(self, event, widget, text):
		from styles import styles

		if event.rect() == widget.rect():
			with QtGui.QPainter(widget) as painter:
				painter.setRenderHint(painter.Antialiasing, True)
				default_pen = painter.pen()
				default_brush = painter.brush()
				default_font = painter.font()

				# # Draw rounded rectangle
				# painter.drawRoundedRect(event.rect().x() - .5, event.rect().y() - .5, event.rect().width(), event.rect().height(), 0, 0)

				# Calculate size of text
				font = QtGui.QFont(default_font)
				font.setPointSize(int(.5 * default_font.pointSize()))
				painter.setFont(font)
				painter.setPen(QtGui.QPen(QtCore.Qt.transparent))
				text_rect = painter.drawText(event.rect().x(), event.rect().y() + 1, event.rect().width() - styles.mm2px(1.), event.rect().height() + styles.mm2px(0.), QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom, text.upper())

				# Draw line
				painter.setRenderHint(painter.Antialiasing, False)
				painter.setPen(QtGui.QPen(default_pen))
				painter.drawLine(0, event.rect().height() - text_rect.height(), event.rect().width(), event.rect().height() - text_rect.height())

				# # Draw rounded rectangle
				# painter.setPen(QtCore.Qt.NoPen)
				# # painter.setBrush(QtGui.QBrush(QtGui.QColor('#ffcc66'), QtCore.Qt.SolidPattern))
				# painter.setBrush(QtGui.QBrush(QtGui.QColor(240, 240, 240, 200), QtCore.Qt.SolidPattern))
				# painter.drawRoundedRect(text_rect.x() - styles.mm2px(.4), text_rect.y() - styles.mm2px(.3), text_rect.width() + styles.mm2px(.8), text_rect.height() + styles.mm2px(.4), styles.mm2px(.6), styles.mm2px(.6))

				# Draw text
				color = QtGui.QColor(default_pen.color())
				color.setAlpha(150)
				painter.setPen(QtGui.QPen(color))
				painter.setBrush(default_brush)
				painter.drawText(text_rect, QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom, text.upper())

	def _fill_pair_from_vocabularies(self):
		state_model = self.state_model

		if not state_model.right_field_edited:
			with Timer('looking for pair'):
				left_field_without_insignificant_parts = self._cut_insignificant_keywords_parts(state_model.left_field)
				for vocabulary in self.data_model.vocabularies:
					for pairs in [getattr(vocabulary, mark) for mark in vocabulary.marks]:
						for pair in pairs:
							if self._cut_insignificant_keywords_parts(pair[0]) == left_field_without_insignificant_parts:
								state_model.vocabulary = vocabulary
								state_model.left_field = pair[0]
								state_model.right_field = pair[1]
								return
				else:
					state_model.vocabulary = None
					state_model.right_field = ''

	def _fill_variants(self):
		state_model = self.state_model

		keywords = self._cut_insignificant_keywords_parts(self.state_model.variants_keywords)

		def search():
			variants = []
			if len(keywords) > 1:
				max_results_count = self.settings_model.dict_variants_count

				try:
					state_model.progress = 'Loading...'
					response_tickets = [dictionary.search(keywords=keywords, method='startswith', limit=max_results_count) for dictionary in state_model.dictionaries]
					variants = [xx for x in [response_ticket() for response_ticket in response_tickets] for xx in x]
				finally:
					state_model.progress = None

				# Keeps only unique
				variants = list(set(variants))

				# Orders
				variants = sorted(variants, key=(state_model.dictionaries[0].normalize if state_model.dictionaries else str.lower))[:max_results_count]

			self._variants = variants
			Caller.call_once_after(.0, self._fill_article_browser)

		# self.thread = elf.make_breakpoint(threading.Thread(target=search))
		thread = threading.Thread(target=search)
		thread.setDaemon(True)
		thread.start()

	def _open_url(self, url):
		if url.startswith('?left_field='):
			self.state_model.left_field = urllib.parse.unquote(url.split('=')[1])
			Caller.call_once_after(.03, self.undo_redo_model.save)  # Save state after filling
			Caller.call_once_after(.03, self.double_undo_redo_model.save)  # Save state after filling

			Caller.call_once_after(.0, self._fill_articles)

		elif url.startswith('?right_field='):
			self._toggle_with_right_field(urllib.parse.unquote(url.split('=')[1]))

	def _fill_articles(self):
		state_model = self.state_model

		keywords = self._cut_insignificant_keywords_parts(state_model.left_field)

		def search():
			try:
				state_model.progress = 'Loading...'



				# for dictionary in state_model.dictionaries:
				#     logging.getLogger(__name__).warning('dictionary=' + '%s', dictionary)
				#     for x in [dictionary.get_articles(keywords=keywords)]:
				#         logging.getLogger(__name__).warning('x=' + '%s', x)
				#         for xx in x:
				#             logging.getLogger(__name__).warning('xx=' + '%s', xx)
				#             d = dict(text=str(xx), dictionary=dictionary)
				#             logging.getLogger(__name__).warning('d=' + '%s', d)



				self._articles = [
					dict(text=str(xx), dictionary=dictionary)
					for dictionary in state_model.dictionaries
					for x in [dictionary.get_articles(keywords=keywords)]
					for xx in x
				]
				Caller.call_once_after(.5, self._fill_article_browser)
			finally:
				state_model.progress = None

		# self.thread = elf.make_breakpoint(threading.Thread(target=search))
		thread = threading.Thread(target=search)
		thread.setDaemon(True)
		thread.start()

	# def _toggle_with_right_field(self, value):
	#     state_model = self.state_model

	#     opened_uninterrupted_signs = '"([|'
	#     closed_uninterrupted_signs = '",;]|)'

	#     if state_model.right_field.endswith(value):
	#         state_model.right_field = state_model.right_field[:-len(value)]
	#         if state_model.right_field and state_model.right_field[-1] not in opened_uninterrupted_signs and value[0] not in closed_uninterrupted_signs:
	#             state_model.right_field = state_model.right_field[:-1]
	#     else:
	#         if state_model.right_field and state_model.right_field[-1] not in opened_uninterrupted_signs and value[0] not in closed_uninterrupted_signs:
	#             state_model.right_field += ' '
	#         state_model.right_field += value

	def _toggle_with_right_field(self, value):
		state_model = self.state_model
		view = self._view

		# open_signs, close_signs = '"([|', '",;|])'
		# open_signs, close_signs = '"([|', ',;'
		leading_whitespace_signs, trailing_whitespace_signs = '', ',;'
		cursor_position = view.right_field_input.cursorPosition()
		left_part, right_part = state_model.right_field[:cursor_position].rstrip(), state_model.right_field[cursor_position:].lstrip()

		def check_if_needed_space_between(left, right):
			return (
				left and left[-1] in trailing_whitespace_signs or
				left and left[-1].isalnum() and right and right[0].isalnum()
			)

		if not left_part.endswith(value):
			# Adds value
			state_model.right_field = (
				left_part +
				(' ' if check_if_needed_space_between(left_part, value) else '') +
				value +
				(' ' if check_if_needed_space_between(value, right_part) else '') +
				right_part
			)
			cursor_position = len(
				left_part +
				(' ' if check_if_needed_space_between(left_part, value) else '') +
				value
			)
		else:
			# Removes value
			left_part = left_part[:-len(value)].rstrip()
			state_model.right_field = (
				left_part +
				(' ' if check_if_needed_space_between(left_part, right_part) else '') +
				right_part
			)
			cursor_position = len(
				left_part
			)
		state_model.right_field_edited = True
		view.right_field_input.setCursorPosition(cursor_position)

	def _save(self):
		state_model = self.state_model
		left_field, right_field = state_model.left_field, state_model.right_field

		if left_field:
			def on_vocabulary_selected(vocabulary):
				if vocabulary:
					# Removes previous copies
					pairs, pair = vocabulary.new, [left_field, right_field]
					for pairs, pair in ((vocabulary[mark], pair) for mark in vocabulary.marks for pair in vocabulary[mark] if pair[0] == left_field):
						pairs[pairs.index(pair)] = [left_field, right_field]
						break
					else:
						vocabulary.new.append([left_field, right_field])

					# Removes 'save'-button
					state_model.right_field_edited = False

					Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Action Save ' + self.__class__.__name__))

			def on_vocabulary_renamed(vocabulary, name):
				vocabulary.path = os.path.join(vocabulary.path.rsplit(os.sep, 1)[0], name)

			VocabulariesDialog(
				headers=['Vocabulary'],
				settings_model=self.settings_model,
				data_model=self.data_model.vocabularies,
				model=self.state_model,
				key='vocabulary',
				path_attribute='path',
				path_prefix='vocabularies/',
				buttons=('Save', ),
				is_expanded=True,
				is_container_selectable=False,
				renamed=on_vocabulary_renamed,
				selected=on_vocabulary_selected,
			).show()

	def _fill_article_browser(self):
		state_model = self.state_model
		settings_model = self.settings_model

		css = ''
		html = ''
		signs_html = ''

		with Timer('generating articles'):
			variants_font = settings_model.dict_variants_font
			punctuation_signs_font = settings_model.dict_punctuation_signs_font
			css += '''
				<style type="text/css">
					a.left_field {
						''' + (self._font_to_style(variants_font)[1:-1] if variants_font is not None else '') + '''
						color: ''' + (settings_model.dict_variants_color) + ''';
						text-decoration: none;
					}
					a.right_field {
						color: ''' + (settings_model.dict_article_color) + ''';
						text-decoration: none;
						background: rgba(0, 0, 0, 10);
					}
					a.sign {
						''' + (self._font_to_style(punctuation_signs_font)[1:-1] if punctuation_signs_font is not None else '') + '''
						color: white;
						background: rgba(102, 153, 102, 102);
						font-weight: bold;
						line-height: 10px;
					}
					a.audio {
						color: #ccc;
						text-decoration: none;
					}
					p.dictionary {
						color: #666;
						font-size: 14px;
					}
				</style>
				'''

			if self._variants:
				html += '<p>'
				html += ' &nbsp;|&nbsp; '.join(['<a class="left_field" href="?left_field={0}">{0}</a>'.format(x) for x in self._variants]).replace('\n', '<br/>\n')
				# html += '<hr width="100%" />'
				html += '</p>'
				html += '<p>&nbsp;</p>'

			# Adds panel with punctuation signs
			if settings_model.dict_show_punctuation_signs:
				signs_html += ''.join(['<p style="margin: 0 0 1px"><a class="right_field sign" href="?right_field={0}">&nbsp;{1}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></p>'.format(x, x.replace(' ', '·')) for x in self._punctuation_signs])

			if self._articles:
				articles = self._articles

				test_regex = False
				# test_regex = True

				# Parses articles if words are clickable
				if settings_model.dict_clickable_words:
					class Parser(HTMLParser):
						"""
						Link to XDXF standard: https://github.com/soshial/xdxf_makedict/blob/master/format_standard/xdxf_description.md
						rref - reference to a resource file (audio, video, gif, image files), which should be located in the same folder or any subfolder as the dictionary file is
						kref - is a reference to some <ar> or <def>, that is located in the same dictionary file. Links should be clickable
						"""
						@classmethod
						def parse(cls, src):
							self = cls()
							self.dst = []
							self._current_tags = []
							# Checks if there are meaning tags
							self._with_ex_or_dtrn_tags = '<ex>' in src or '<dtrn>' in src
							# Inserts apostrophs
							src = src.replace('<nu />', '').replace('[&apos;]', '').replace('[/&apos;]', '\u0301').replace('&apos;', '\'')
							# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'src=', src, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

							# Parses or returns plain text of exception occured
							try:
								self.feed(src)  # Parses string through handlers
							except SyntaxError as e:
								# raise e.__class__, '{}: {} [DEBUG: {}]'.format(e.__class__, e, dict(src=src)), sys.exc_info()[2]
								self.dst = src.replace('<', '&lt;').replace('>', '&gt;')

							return ''.join(self.dst)

						def handle_starttag(self, tag, attrs):
							self._current_tags.append(tag)
							self.dst.append('<' + tag + (' ' + ' '.join([k + '=' + v for k, v in attrs if v is not None]) if attrs else '') + '>')

						def handle_endtag(self, tag):
							try:
								list(iter(self._current_tags.pop, tag))  # Removes tag and everything from the right of it
							except IndexError:
								pass
							self.dst.append('</' + tag + '>')

						def handle_data(self, data):
							link_class = None
							data = data.replace('<', '〈').replace('>', '〉')
							# Cross-references
							if 'kref' in self._current_tags:
								link_class = 'left_field'
							# Internal data references
							elif 'rref' in self._current_tags:
								link_class = 'audio'
							# Secondary tags
							elif 'abr' in self._current_tags and len(data.strip()) <= 2:
								pass
							# Meaning tags
							elif not self._with_ex_or_dtrn_tags or 'dtrn' in self._current_tags or 'ex' in self._current_tags:
								link_class = 'right_field'
							if link_class is not None:
								data = re.sub(r'(\w[\w\u0301\'.-]*)', r'<a class="{0}" href="?{0}=\1">\1</a>'.format(link_class), data, flags=re.UNICODE)
							self.dst.append(data)
					articles = [dict(x, text=Parser.parse(x['text'])) for x in articles]

				# Adds dictionary name after each article
				articles = [dict(x, text='{0[text]}<p class="dictionary" align="right">{1}</p>'.format(x, 'From: {0[dictionary]}'.format(x).upper())) for x in articles]

				# Combines articles
				html += '<p>'
				html += '<hr/>\n'.join([x['text'] for x in articles]).replace('\n', '<br/>\n')
				html += '</p>'
				html += '<br/><br/>'

		if not settings_model.dict_show_punctuation_signs:
			self._view.signs_browser.hide()
		else:
			self._view.signs_browser.show()
			self._view.signs_browser.setHtml(css + signs_html)

		# Delays showing if the article is large enough
		Caller.call_once_after(0. if len(html) < 10000 else (1. if len(html) < 30000 else 2.), self._set_article_browser, css, html)

	def _set_article_browser(self, css, html, __state=dict(css='', html='')):
		view = self._view

		if css != __state['css'] or html != __state['html']:
			__state['css'], __state['html'] = css, html

			with Timer('filling article browser'):
				view.article_browser.setHtml(css + html)

	""" Animation """

	# def __jump(self, item):
	#     delta = -25
	#     animation = QtCore.QPropertyAnimation(item, 'geometry', item)
	#     animation.setEasingCurve(QtCore.QEasingCurve.OutBounce)
	#     animation.setDuration(2000)
	#     # animation.setStartValue(QtCore.QRect(x, y, width, height))
	#     animation.setKeyValueAt(.5, QtCore.QRect(
	#         item.x() - delta,
	#         item.y() - delta,
	#         item.width() + delta * 2,
	#         item.height() + delta * 2,
	#     ))
	#     # animation.setEndValue(QtCore.QRect(
	#     #     item.x(),
	#     #     item.y(),
	#     #     item.width(),
	#     #     item.height(),
	#     # ))
	#     # animation.setLoopCount(2)
	#     animation.start()

	# def __resize_width(self, item, to_x=None, to_y=None, to_width=None, to_height=None):
	#     # animation = QtCore.QPropertyAnimation(item, 'geometry', item)
	#     # animation.setEasingCurve(QtCore.QEasingCurve.OutBounce)
	#     # animation.setDuration(200)
	#     # # animation.setStartValue(QtCore.QRect(x, y, width, height))
	#     # # animation.setKeyValueAt(.5, 200)
	#     # animation.setEndValue(QtCore.QRect(
	#     #     to_x if to_x is not None else item.x(),
	#     #     to_y if to_y is not None else item.y(),
	#     #     to_width if to_width is not None else item.width(),
	#     #     to_height if to_height is not None else item.height(),
	#     # ))
	#     # # animation.setLoopCount(2)
	#     # animation.start()
	#     item.setGeometry(QtCore.QRect(
	#         to_x if to_x is not None else item.x(),
	#         to_y if to_y is not None else item.y(),
	#         to_width if to_width is not None else item.width(),
	#         to_height if to_height is not None else item.height(),
	#     ))

	# def __expand_left(self, left_item, right_item):
	#     # print("right_item.minimumContentsLength(),", right_item.minimumContentsLength(), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed
	#     if right_item.sizeAdjustPolicy() == QtWidgets.QComboBox.AdjustToContents:
	#         # right_item.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContentsOnFirstShow)
	#         # right_item.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToMinimumContentsLength)
	#         if self.settings_model.dict_save_space:
	#             right_item.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToMinimumContentsLengthWithIcon)
	#         # right_item.setMinimumContentsLength(1)
	#         # right_item.setMinimumWidth(right_item.width() * 4)
	#     # # if left_item.width() == left_item._collapsed_width:
	#     # if not getattr(left_item, '_expanded', False):
	#     #     width = left_item.width() + right_item.width()
	#     #     left_item._expanded, right_item._expanded = True, False
	#     #     self.__resize_width(left_item, to_width=width * 5 // 6)
	#     #     self.__resize_width(right_item, to_x=right_item.x() + (right_item.width() - width * 1 // 6), to_width=width * 1 // 6)

	# def __expand_right(self, left_item, right_item):
	#     # print("right_item.minimumContentsLength(),", right_item.minimumContentsLength(), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed
	#     if right_item.sizeAdjustPolicy() != QtWidgets.QComboBox.AdjustToContents:
	#         right_item.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)
	#         # right_item.setMinimumContentsLength(20)
	#     # if not getattr(right_item, '_expanded', False):
	#     #     right_item._expanded, left_item._expanded = True, False
	#     #     width = left_item.width() + right_item.width()
	#     #     self.__resize_width(left_item, to_width=width * 3 // 6)
	#     #     self.__resize_width(right_item, to_x=right_item.x() + (right_item.width() - width * 3 // 6), to_width=width * 3 // 6)


def run_init():
	from models.abstract import ObservableAttrDict
	data_model = ObservableAttrDict(
		# dictionaries=['DE-EN', 'EN-DE'],
		dictionaries=[],
		vocabularies={},
	)
	settings_model = ObservableAttrDict(
		# dict_vocabulary=None,
		dict_dictionaries=[],
		dict_left_field='',
		dict_variants_keywords='',
		dict_font=None,
		dict_variants_font=None,
		dict_variants_count=10,
		dict_variants_color='green',
		dict_show_punctuation_signs=True,
		dict_punctuation_signs_font=None,
		dict_clickable_words=True,
		dict_article_color='black',
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles; styles  # Load once the styles for QApplication

	controller = DictController(data_model=data_model, settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)

	# controller._view.left_field_input.setText('abb')
	# controller._view.left_field_input.textEdited.emit(controller._view.left_field_input.text())

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def run_left_field():
	from models.abstract import ObservableAttrDict
	data_model = ObservableAttrDict(
		dictionaries=['DE-EN', 'EN-DE'],
		vocabularies={},
	)
	settings_model = ObservableAttrDict(
		# dict_vocabulary=None,
		dict_dictionaries=[],
		dict_left_field='',
		dict_variants_keywords='',
		dict_font=None,
		dict_variants_font=None,
		dict_variants_count=10,
		dict_variants_color='green',
		dict_show_punctuation_signs=True,
		dict_punctuation_signs_font=None,
		dict_clickable_words=True,
		dict_article_color='black',
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles; styles  # Load once the styles for QApplication

	controller = DictController(data_model=data_model, settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)

	controller._view.left_field_input.setText('abb')
	controller._view.left_field_input.textEdited.emit(controller._view.left_field_input.text())

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def run_article():
	from models.abstract import ObservableAttrDict, ObservableList

	class Vocabularies(ObservableList):

		def get(self, key, default=None):
			try:
				return self[key]
			except:
				return default

		def __contains__(self, key):
			if key.__class__ == int and len(self) > key:
				return True
			return super(Vocabularies, self).__contains__(key)

	class Vocabulary(ObservableAttrDict):
		marks = ('new', 'good', 'fair', 'bad')

		def __init__(self, name, *args, **kwargs):
			self._name = name
			super(Vocabulary, self).__init__(*args, **kwargs)

		def __str__(self):
			return self._name

		@property
		def path(self):
			return 'root/subdirectory/' + self._name

	data_model = ObservableAttrDict(
		dictionaries=['de-en', 'en-de'],
		vocabularies=Vocabularies([
			Vocabulary('Körperteile', new=[['Glied', 'конечность'], ['Gelenk', 'сустав'], ['Knochen, Bein', 'кость'], ['Haut', 'кожа'], ['Nagel', 'ноготь'], ['Stirn', 'лоб']], good=[], fair=[], bad=[]),
			Vocabulary('Vocabulary #2', new=[], good=[], fair=[], bad=[]),
		]),
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles; styles  # Load once the styles for QApplication

	settings_model = ObservableAttrDict(
		# dict_vocabulary=None,
		dict_dictionaries=[],
		dict_left_field='',
		dict_variants_keywords='',
		dict_font=None,
		dict_variants_font=None,
		dict_variants_count=10,
		dict_variants_color='green',
		dict_show_punctuation_signs=True,
		dict_punctuation_signs_font=None,
		dict_clickable_words=True,
		dict_article_color='black',
	)

	controller = DictController(data_model=data_model, settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)

	variants = ['abbrechen', 'Abbruch', 'ableiten']
	articles = [
		dict(dictionary=None, text=(
			"""<k>abbrechen</k><br/>"""
			"""<b>I</b> <rref>abbrechen.wav</rref><br/>"""
			"""<b>1.</b> <i><c><co><abr>*</abr> <abr>vt</abr></co></c></i><br/>"""
			""" 1) <dtrn>обламывать, отламывать, (с)ломать</dtrn><br/>"""
			"""  <ex>eine Blume abbrechen — <abr><i><c><co>поэт.</co></c></i></abr> срывать цветок</ex><br/>"""
			""" 2) <dtrn><abr><i><c><co>горн.</co></c></i></abr> обрушать; отбивать</dtrn><br/>"""
			""" 3) <dtrn>сносить, разрушать, разбирать</dtrn><br/>"""
			"""  <ex>ein Lager abbrechen — снимать лагерь</ex><br/>"""
			"""  <ex>ein Zelt abbrechen — разбирать палатку</ex><br/>"""
			""" 4) <dtrn>прекращать, прерывать</dtrn><br/>"""
			"""  <ex>die diplomatischen Beziehungen abbrechen — порвать дипломатические отношения</ex><br/>"""
			"""  <ex>das Gefecht abbrechen — <abr><i><c><co>воен.</co></c></i></abr> выйти из боя</ex><br/>"""
			"""  <ex>eine Partie abbrechen — <abr><i><c><co>шахм.</co></c></i></abr> прервать партию; отложить партию</ex><br/>"""
			""" 5) <dtrn><abr><i><c><co>полигр.</co></c></i></abr> разделять <co>(<i>слово</i>)</co> при переносе</dtrn><br/>"""
			""" 6) <dtrn><abr><i><c><co>полигр.</co></c></i></abr> делать новый абзац</dtrn><br/>"""
			"""  <ex>den Satz abbrechen — сверстать набор</ex><br/>"""
			""" 7)<br/>"""
			"""  <ex>die Hufeisen abbrechen — расковывать лошадь</ex><br/>"""
			""" 8)<br/>"""
			"""  <ex>sich (<abr><i><c><co>D</co></c></i></abr>) <abr><i><c><co>etw.</co></c></i></abr> abbrechen — отказывать себе в <abr><i><c><co>чём-л.</co></c></i></abr></ex><br/>"""
			"""  <ex><abr><i><c><co>etw.</co></c></i></abr> am Lohne abbrechen — <abr><i><c><co>диал.</co></c></i></abr> удержать из заработной платы</ex><br/>"""
			"""  ••<br/>"""
			"""  <ex>brich dir nur keinen [keine Verzierung] ab! — <abr><i><c><co>разг.</co></c></i></abr> не задавайся!, не задирай нос!</ex><br/>"""
			"""<b>2.</b> <i><c><co><abr>*</abr> <abr>vi</abr></co></c></i><br/>"""
			""" 1) <dtrn><co>(<abr><i><c>s</c></i></abr>)</co> обламываться, отламываться</dtrn><br/>"""
			""" 2) <dtrn><co>(<abr><i><c>h</c></i></abr>)</co> прекращаться, обрываться, кончаться</dtrn><br/>"""
			"""  <ex>das Gespräch brach ab — разговор прервался</ex><br/>"""
			"""  <ex>die Musik brach ab — музыка смолкла</ex><br/>"""
			"""  <ex>der Pfad brach hier ab — здесь тропинка обрывалась</ex><br/>"""
			"""  <ex>er brach ab — он замолчал <co>(<i>не закончив начатого</i>)</co></ex><br/>"""
			"""  <ex>das bricht niemals ab — этому не будет конца</ex><br/>"""
			"""  <ex>in der Rede abbrechen — внезапно оборвать речь, замолчать; запнуться</ex><br/>"""
			""" 3) <dtrn><co>(<abr><i><c>h</c></i></abr>)</co> <abr><i><c><co>воен.</co></c></i></abr> перестраиваться из широкого строя в колонну</dtrn><br/>"""
			""" 4) <dtrn><co>(<abr><i><c>h</c></i></abr>)</co></dtrn><br/>"""
			"""  <ex><abr><i><c><co>j-m</co></c></i></abr> am Lohne abbrechen — <abr><i><c><co>диал.</co></c></i></abr> сократить [урезать] <abr><i><c><co>кому-л.</co></c></i></abr> заработную плату</ex><br/>"""
			"""<b>II</b> <rref>abbrechen.wav</rref><br/>"""
			"""<i><c><co><abr>*</abr> <abr>vt</abr> <abr>с.-х.</abr></co></c></i><br/>"""
			""" <dtrn>трепать <co>(<i>лён</i>)</co></dtrn>"""
			"""""".replace('\n', '')
		))]

	def init():
		controller._variants = variants
		controller._articles = articles
		controller._fill_article_browser()
	Caller.call_once_after(.3, init)

	sys.exit(app.exec_())


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
