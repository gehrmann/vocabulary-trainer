#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a cols-tab's settings

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtWidgets, uic

from helpers.timer import Timer
from controllers.abstract import ControllerMixture, CommonSettingsMixture, Network


class ColsSettingsController(ControllerMixture, CommonSettingsMixture):
	def __init__(self, settings_model):
		with Timer('initializing'):
			ControllerMixture.__init__(self)
			CommonSettingsMixture.__init__(self)

			# Models
			self.settings_model = settings_model

			# Views
			self._view = view = self._load_ui('views/cols_settings_widget.ui')
			self.dialog.settings_widget.layout().addWidget(view)

			view.font_button.clicked.connect(self.__on_font_button_clicked)
			view.leading_field_color_button.clicked.connect(self.__on_leading_field_color_button_clicked)
			view.trailing_field_color_button.clicked.connect(self.__on_trailing_field_color_button_clicked)
			view.trailing_field_background_color_button.clicked.connect(self.__on_trailing_field_background_color_button_clicked)
			view.hidden_trailing_field_background_color_button.clicked.connect(self.__on_hidden_trailing_field_background_color_button_clicked)
			view.column_background_color_button.clicked.connect(self.__on_column_background_color_button_clicked)
			view.failed_field_color_button.clicked.connect(self.__on_failed_field_color_button_clicked)
			view.guessed_field_color_button.clicked.connect(self.__on_guessed_field_color_button_clicked)
			view.group_by_field_dropdown.activated.connect(self.__on_group_by_field_dropdown_changed)
			view.group_method_dropdown.activated.connect(self.__on_group_method_dropdown_changed)
			view.group_filter_dropdown.activated.connect(self.__on_group_filter_dropdown_changed)
			view.column_field_separator_dropdown.activated.connect(self.__on_column_field_separator_dropdown_changed)

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Show Dialog ' + self.__class__.__name__))

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		CommonSettingsMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		view = self._view

		if model is settings_model:
			if current[0] is None or 'cols_font' in current[0] and previous[0] != current[0]:
				view.font_button.setText(self._font_to_text(settings_model.cols_font))

			if current[0] is None or 'cols_leading_field_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.leading_field_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cols_leading_field_color)})

			if current[0] is None or 'cols_trailing_field_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.trailing_field_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cols_trailing_field_color)})

			if current[0] is None or 'cols_trailing_field_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.trailing_field_background_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cols_trailing_field_background_color)})

			if current[0] is None or 'cols_hidden_trailing_field_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.hidden_trailing_field_background_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cols_hidden_trailing_field_background_color)})

			if current[0] is None or 'cols_column_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.column_background_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cols_column_background_color)})

			if current[0] is None or 'cols_failed_field_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.failed_field_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cols_failed_field_color)})

			if current[0] is None or 'cols_guessed_field_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.guessed_field_color_button, {'* {background-color: ': '{};}}'.format(settings_model.cols_guessed_field_color)})

			if current[0] is None or 'cols_group_by_field' in current[0] and previous[0] != current[0]:
				view.group_by_field_dropdown.setCurrentIndex(view.group_by_field_dropdown.findText(settings_model.cols_group_by_field))

			if current[0] is None or 'cols_group_method' in current[0] and previous[0] != current[0]:
				view.group_method_dropdown.setCurrentIndex(view.group_method_dropdown.findText(settings_model.cols_group_method))

			if current[0] is None or 'cols_group_filter' in current[0] and previous[0] != current[0]:
				view.group_filter_dropdown.setEditText(str(settings_model.cols_group_filter or ''))

			if current[0] is None or 'cols_column_field_separator' in current[0] and previous[0] != current[0]:
				view.column_field_separator_dropdown.setCurrentIndex(view.column_field_separator_dropdown.findText(settings_model.cols_column_field_separator))

	""" View's event handlers """

	def __on_font_button_clicked(self):
		self._show_font_dialog(model=self.settings_model, key='cols_font')

	def __on_leading_field_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cols_leading_field_color')

	def __on_trailing_field_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cols_trailing_field_color')

	def __on_trailing_field_background_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cols_trailing_field_background_color')

	def __on_hidden_trailing_field_background_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cols_hidden_trailing_field_background_color')

	def __on_column_background_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cols_column_background_color')

	def __on_failed_field_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cols_failed_field_color')

	def __on_guessed_field_color_button_clicked(self):
		self._show_color_dialog(model=self.settings_model, key='cols_guessed_field_color')

	def __on_group_by_field_dropdown_changed(self, current_index):
		self.settings_model.cols_group_by_field = str(self._view.group_by_field_dropdown.currentText())

	def __on_group_method_dropdown_changed(self, current_index):
		self.settings_model.cols_group_method = str(self._view.group_method_dropdown.currentText())

	def __on_group_filter_dropdown_changed(self, current_index):
		self.settings_model.cols_group_filter = float(self._view.group_filter_dropdown.currentText())

	def __on_column_field_separator_dropdown_changed(self, current_index):
		self.settings_model.cols_column_field_separator = str(self._view.column_field_separator_dropdown.currentText())


def run_init():
	from models.abstract import ObservableAttrDict, ObservableList

	class BackgroundsCategory(ObservableList):
		def __init__(self, name):
			self._name = name
			super(BackgroundsCategory, self).__init__()

		def __str__(self):
			return self._name

		def __repr__(self):
			return object.__repr__(self).replace(' object at ', ' name="{0._name}" object at '.format(self))

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	settings_model = ObservableAttrDict(
		font=dict(family='DejaVu Sans', size=.75 * styles.mm2px(2.6), weight=12, italic=False),
		button_size=6,
		close_on_double_back=True,
		current_tab=0,

		cols_vocabulary=None,
		cols_font=dict(family='Droid Sans', size=.75 * styles.mm2px(2.8), weight=50, italic=False),
		cols_leading_field_color='#000',
		cols_trailing_field_color='#666',
		cols_trailing_field_background_color='#eee',
		cols_hidden_trailing_field_background_color='#999',
		cols_column_background_color='#ffcccc',
		cols_failed_field_color='#c33',
		cols_guessed_field_color='#0c0',
		cols_group_by_field='left',
		cols_group_method='( ... )',
		cols_group_filter=3,
		cols_column_field_separator='new line',
	)

	controller = ColsSettingsController(settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)
	controller.show()


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
