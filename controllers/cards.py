#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for a cards-tab

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import random
import signal
import sys
import time

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtGui, QtWidgets, uic

from controllers.abstract import ControllerMixture, Network, BackgroundsCategoriesDialog
from controllers.cards_settings import CardsSettingsController
from controllers.top_widget_mixture import TopWidgetMixture
from helpers.caller import Caller
from helpers.timer import Timer
from models.abstract import ObservableAttrDict, ObservableSet


class _State(ObservableAttrDict):
	pass


class CardsController(ControllerMixture, TopWidgetMixture):
	def __init__(self, parent_view=None, test=False, data_model=None, settings_model=None):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# Models
			""" Current-State-Model for selected view's data"""
			state_model = self.state_model
			state_model.vocabulary = None
			state_model.backgrounds_categories = []
			state_model.background_image = None
			state_model.selected_cards_views = ObservableSet()
			# state_model.selected_cards_views.changed.bind(lambda model=None, previous=None, current=None: (state_model.changed(state_model, previous={'selected_cards_views': previous}, current={'selected_cards_views': current})))  # invoke parent's changed-event if is changed
			state_model.guessed_cards_views = ObservableSet()
			# state_model.guessed_cards_views.changed.bind(lambda model=None, previous=None, current=None: (state_model.changed(state_model, previous={'guessed_cards_views': previous}, current={'guessed_cards_views': current})))  # invoke parent's changed-event if is changed
			self.cards_views = []
			self.fields = []

			self.data_model = data_model

			self.settings_model = settings_model

			# Views
			self._view = view = self._load_ui('views/cards_view.ui')
			if parent_view is not None:
				parent_view.addWidget(view)

			TopWidgetMixture.__init__(self, parent_view=view.top_widget, with_vocabularies_button=True, with_backgrounds_button=True, with_marks_buttons=True, with_reload_button=True)
			self.top_widget.backgrounds_button.clicked.connect(self.__on_backgrounds_button_clicked)
			self.top_widget.reload_button.clicked.connect(self.__on_reload_button_clicked)
			view.cards_container.mousePressEvent = (lambda widget, previous_callback: (lambda event: (self.__on_cards_container_mouse_pressed(event, widget, previous_callback))))(view.cards_container, view.cards_container.mousePressEvent)
			view.cards_container.resizeEvent = (lambda widget, previous_callback: (lambda event: (self.__on_cards_container_resized(event, widget, previous_callback))))(view.cards_container, view.cards_container.resizeEvent)

			if parent_view is None:
				self._restore_window_geometry()
				view.closeEvent = self._on_close
				view.keyPressEvent = self._on_key_pressed
				view.show()

			""" Observe models by view """
			settings_model.changed.bind(self._on_model_updated)
			data_model.changed.bind(self._on_model_updated)
			state_model.changed.bind(self._on_model_updated)

			""" Fill blank view by models """
			self._on_model_updated(settings_model)
			self._on_model_updated(data_model)
			self._on_model_updated(state_model)

			if '--test-cards' in sys.argv:
				import ipdb; ipdb.set_trace()  # FIXME: must be removed

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		# print(self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80], file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		ControllerMixture._on_model_updated(self, model=model, previous=previous, current=current)
		TopWidgetMixture._on_model_updated(self, model=model, previous=previous, current=current)

		settings_model = self.settings_model
		data_model = self.data_model
		state_model = self.state_model
		view = self._view
		top_widget = self.top_widget

		if model is settings_model:
			# if current[0] is None or 'cards_vocabulary' in current[0] and previous[0] != current[0]:
			#     state_model.vocabulary = data_model.vocabularies.get(settings_model.cards_vocabulary, None)

			if current[0] is None or 'cards_selected_marks' in current[0] and previous[0] != current[0]:
				state_model.selected_marks = settings_model.cards_selected_marks

			if current[0] is None or 'cards_backgrounds_categories' in current[0] and previous[0] != current[0]:
				state_model.backgrounds_categories = list(filter(lambda x: (x is not None), [data_model.backgrounds_categories[x] for x in settings_model.get('cards_backgrounds_categories', [])]))
				Caller.call_once_after(0, self._fill)

			if current[0] is None or 'cards_vertical_rows_count' in current[0] and previous[0] != current[0]:
				if settings_model.cards_vertical_columns_count * settings_model.cards_vertical_rows_count != len(self.fields):
					Caller.call_once_after(0, self._fill)

			if current[0] is None or 'cards_vertical_columns_count' in current[0] and previous[0] != current[0]:
				if settings_model.cards_vertical_columns_count * settings_model.cards_vertical_rows_count != len(self.fields):
					Caller.call_once_after(0, self._fill)

			if current[0] is None or 'cards_background_arrangement' in current[0] and previous[0] != current[0]:
				Caller.call_once_after(0, self.__place_background_image)

			if current[0] is None or 'cards_font' in current[0] and previous[0] != current[0]:
				if settings_model.cards_font is not None:
					from styles import styles
					font = settings_model.cards_font
					styles.replace(view.cards_container, {'* {font: ': self._font_to_style(font)[7:]})

			if current[0] is None or 'cards_rounded_corners' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.cards_container, {'QLabel {border-top-left-radius: ': '{0}px; border-bottom-right-radius: {0}px;}}'.format(styles.mm2px(2.5) if settings_model.cards_rounded_corners else 0)})

			if current[0] is None or 'cards_left_card_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.cards_container, {'QLabel[left_field=true] {color: ': '{0};}}'.format(settings_model.cards_left_card_color)})

			if current[0] is None or 'cards_right_card_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.cards_container, {'QLabel[right_field=true] {color: ': '{0};}}'.format(settings_model.cards_right_card_color)})

			if current[0] is None or 'cards_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.cards_container, {'QLabel {background-color: ': '{0};}}'.format(settings_model.cards_card_background_color)})

			if current[0] is None or 'cards_selected_card_background_color' in current[0] and previous[0] != current[0]:
				from styles import styles
				styles.replace(view.cards_container, {'QLabel[selected=true] {background-color: ': '{0};}}'.format(settings_model.cards_selected_card_background_color)})

		if model is data_model:
			if current[0] is None or 'vocabularies' in current[0] and previous[:2] != current[:2]:
				state_model.vocabulary = data_model.vocabularies.get(settings_model.cards_vocabulary, None)

			if current[0] is None or 'backgrounds_categories' in current[0] and previous[:2] != current[:2]:
				state_model.backgrounds_categories = list(filter(lambda x: (x is not None), [data_model.backgrounds_categories[x] for x in settings_model.get('cards_backgrounds_categories', [])]))

		if model is state_model:
			# if current[0] is None or 'vocabulary' in current[0] and previous[0] != current[0]:
			#     settings_model.cards_vocabulary = state_model.vocabulary.path if state_model.vocabulary is not None else None

			if current[0] is None \
					or 'vocabulary' in current[0] and previous[0] != current[0] \
					or 'backgrounds_categories' in current[0] and previous[0] != current[0]:
				if state_model.vocabulary is not None:
					tips = ''
					if not state_model.backgrounds_categories:
						# Shows tip: how to select backgrounds categories
						tips += 'Click &nbsp;<img width="{0}" height="{0}" src="qrc:/images/backgrounds.png">&nbsp; to select backgrounds.'.format(int(3 * view.physicalDpiX() / 25.4))
					state_model.tips = tips

			if current[0] is None or 'selected_marks' in current[0] and previous[0] != current[0]:
				settings_model.cards_selected_marks = state_model.selected_marks

			# if current[0] is None or 'backgrounds_categories' in current[0] and previous[0] != current[0]:
			#     settings_model.cards_backgrounds_categories = [str(x) for x in state_model.backgrounds_categories]

			if current[0] is None or 'selected_cards_views' in current[0] and previous[:2] != current[:2]:
				for card_view in self.cards_views:
					# if card_view.card_label.property('selected') != card_view in state_model.selected_cards_views:
						card_view.card_label.setProperty('selected', card_view in state_model.selected_cards_views)
						card_view.card_label.style().unpolish(card_view.card_label)
						card_view.card_label.style().polish(card_view.card_label)

				self._check()

			if current[0] is None or 'guessed_cards_views' in current[0] and previous[:2] != current[:2]:
				for card_view in self.cards_views:
					card_view.card_label.setEnabled(card_view not in state_model.guessed_cards_views)

			if current[0] is None or 'background_image' in current[0] and previous[0] != current[0]:
				Caller.call_once_after(0, self.__place_background_image)

	""" View's event handlers """

	def _on_vocabulary_selected(self, vocabulary):
		self.settings_model.cards_vocabulary = vocabulary.path if vocabulary is not None else None

	def __on_cards_container_mouse_pressed(self, event, widget, previous_callback):
		if len(self.cards_views) == len(self.state_model.guessed_cards_views):
			Caller.call_once_after(0, self._fill)
		return previous_callback(event)

	def __on_cards_container_resized(self, event, widget, previous_callback):
		Caller.call_once_after(0, self.__place_background_image)
		return previous_callback(event)

	def __on_menu_pressed(self, event):
		CardsSettingsController(settings_model=self.settings_model).show()

	def _on_settings_button_clicked(self, event):
		CardsSettingsController(settings_model=self.settings_model).show()

	def __on_backgrounds_button_clicked(self, event):
		def on_backgrounds_categories_selected(backgrounds_categories):
			self.settings_model.cards_backgrounds_categories = [str(x) for x in backgrounds_categories]

		BackgroundsCategoriesDialog(
			headers=['Backgrounds'],
			settings_model=self.settings_model,
			data_model=self.data_model.backgrounds_categories,
			model=self.state_model,
			key='backgrounds_categories',
			path_attribute='path',
			# path_separator='\\',
			path_prefix='backgrounds/',
			is_expanded=True,
			is_container_selectable=False,
			has_multiselection=True,
			selected=on_backgrounds_categories_selected,
			# renamed=on_backgrounds_category_renamed,
		).show()


	def __on_reload_button_clicked(self, event):
		Caller.call_once_after(0, self._fill)

		Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Action Reload ' + self.__class__.__name__))

	def _on_card_label_clicked(self, card_view):
		self.state_model.selected_cards_views ^= set([card_view])

	""" Helpers """

	def _fill(self):
		# logging.getLogger(__name__).debug('Calling _fill()...')

		TopWidgetMixture._fill(self)

		state_model = self.state_model
		data_model = self.data_model
		settings_model = self.settings_model
		vocabulary = self.state_model.vocabulary
		view = self._view

		if vocabulary is not None:

			# Clean the board
			for row in range(view.cards_container.layout().rowCount()):
				for column in range(view.cards_container.layout().columnCount()):
					cell = view.cards_container.layout().takeAt(0)
					if cell is not None:
						cell.widget().deleteLater()

			rows_count = settings_model.cards_vertical_rows_count
			columns_count = settings_model.cards_vertical_columns_count
			fields = [pair for mark in state_model.selected_marks for pair in vocabulary[mark]]
			self.fields = fields = random.sample(fields, min(len(fields), rows_count * columns_count // 2))
			left_cards, right_cards = zip(*fields) if fields else ([], [])
			cards = left_cards + right_cards
			cards = random.sample(cards, len(cards))

			self.cards_views[:] = [self._load_ui('views/cards_item_view.ui') for i in range(len(cards))]  # Qt-resources have to be imported already

			for index, (card, card_view) in enumerate(zip(cards, self.cards_views)):
				if card is not None:
					row, column = index // columns_count, index % columns_count
					view.cards_container.layout().addWidget(card_view, row, column)

					card_view.card_label.setText(card)
					card_view.card_label.setProperty('left_field' if card in left_cards else 'right_field', True)

					card_view.card_label.mousePressEvent = (lambda card_view: (lambda event: (self._on_card_label_clicked(card_view))))(card_view)

			state_model.selected_cards_views.clear()

			state_model.guessed_cards_views.clear()

			Caller.call_once_after(.5, self._update_last_training_time)

		# Find background-image
		# sub 'landscape' if view.cards_container.width() > view.cards_container.height() else 'portrait'
		if not state_model.backgrounds_categories or not self.cards_views:
			state_model.background_image = None
		else:
			backgrounds_category = random.choice(state_model.backgrounds_categories)
			if backgrounds_category:
				state_model.background_image = random.choice(backgrounds_category)

	def _check(self):
		settings_model = self.settings_model
		state_model = self.state_model

		cards_views_to_check = list(state_model.selected_cards_views)[:2]

		if len(cards_views_to_check) > 1:
			fields_to_check = [card_view.card_label.text() for card_view in cards_views_to_check]
			for field in self.fields:
				if (
						field[0] == fields_to_check[0] and field[1] == fields_to_check[1] or
						field[0] == fields_to_check[1] and field[1] == fields_to_check[0]
				):
					state_model.guessed_cards_views |= set(cards_views_to_check)
					break

			state_model.selected_cards_views -= set(cards_views_to_check)

			for card_view in cards_views_to_check:
				widget = card_view.card_label
				if card_view not in state_model.guessed_cards_views:
					self._animate(widget, key='blurRadius', effect='blur', from_value=0., to_value=5., duration=.2, curve='SineCurve').start()
				else:
					# x, y, width, height = widget.x(), widget.y(), widget.width(), widget.height()
					# self._animate(widget, 'geometry', from_value=(x, y, width, height), to_value=(x + width // 2, y + height // 2, 0, 0), duration=.2, curve='OutCirc').start()

					from styles import styles
					styles.replace(widget, {'*:disabled {background-color: ': '{0};}}'.format(settings_model.cards_guessed_card_background_color)})

					self._animate_sequential(widget=widget, key='opacity', effect='opacity', duration=.2, curve='OutCirc', animations=(
						dict(from_value=1., to_value=float(settings_model.cards_guessed_card_delay_opacity)),
						settings_model.cards_guessed_card_delay_timeout,
						dict(from_value=float(settings_model.cards_guessed_card_delay_opacity), to_value=.0),
					)).start()

	def __place_background_image(self):
		state_model = self.state_model
		view = self._view

		from styles import styles
		styles.replace(view.cards_container, {'#cards_container {border-image: url(': '{0}) 0 0 0 0 stretch stretch; margin: {1};}}'.format(
			state_model.background_image or '',
			' '.join([str(x) for x in self.__get_image_margins(view.cards_container, state_model.background_image)])
		)})
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), '---------------------------------', file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'view.cards_container.styleSheet()=', view.cards_container.styleSheet(), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

	def __get_image_margins(self, widget, image_path):
		background_arrangement = self.settings_model.cards_background_arrangement

		if image_path is None or background_arrangement == 'fit':
			margins = 0, 0

		else:
			pixmap = QtGui.QPixmap(image_path)
			scale_x, scale_y = widget.width() / (pixmap.width() or widget.width()), widget.height() / (pixmap.height() or widget.height())
			if background_arrangement == 'contain':
				scale = min(scale_x, scale_y)
			elif background_arrangement == 'cover':
				scale = max(scale_x, scale_y)
			margins = int((widget.height() - pixmap.height() * scale) / 2), int((widget.width() - pixmap.width() * scale) / 2)

		return margins


def run_init():
	from models.abstract import ObservableAttrDict, ObservableList
	from models.data import _BackgroundsCategories

	class Vocabularies(ObservableList):
		def __contains__(self, key):
			if key.__class__ == int and len(self) > key:
				return True
			return super(Vocabularies, self).__contains__(key)

	class Vocabulary(ObservableAttrDict):
		def __init__(self, name, *args, **kwargs):
			self._name = name
			super(Vocabulary, self).__init__(*args, **kwargs)

		def __str__(self):
			return self._name

		@property
		def path(self):
			return 'root/subdirectory/' + self._name

	data_model = ObservableAttrDict(
		backgrounds_categories=_BackgroundsCategories(),
		vocabularies={
			0: Vocabulary('Körperteile', new=[['Glied', 'конечность'], ['Gelenk', 'сустав'], ['Knochen, Bein', 'кость'], ['Haut', 'кожа'], ['Nagel', 'ноготь'], ['Stirn', 'лоб']], good=[], fair=[], bad=[]),
		},
	)
	settings_model = ObservableAttrDict(
		cards_vocabulary=0,
		cards_backgrounds_categories=['my.pack'],
		cards_selected_marks=ObservableSet(['new', 'good', 'fair', 'bad']),
		cards_font=None,
		cards_left_card_color='#600',
		cards_right_card_color='#060',
		cards_card_background_color='#ddd',
		cards_selected_card_background_color='#bbb',
		cards_rounded_corners=True,
		cards_vertical_rows_count=4,
		cards_vertical_columns_count=3,
		cards_background_arrangement='cover',
		cards_guessed_card_background_color='#bbb',
		cards_guessed_card_delay_timeout=8,
		cards_guessed_card_delay_opacity=.3,
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	controller = CardsController(data_model=data_model, settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
