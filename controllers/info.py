#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides controller for an info-window

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore, QtWidgets, uic

from helpers.timer import Timer
from controllers.abstract import ControllerMixture, Network


class InfoController(ControllerMixture):
	def __init__(self, settings_model):
		with Timer('initializing'):
			ControllerMixture.__init__(self)

			# Models
			self.settings_model = settings_model

			# Views
			self.dialog = view = self._load_ui('views/info_dialog.ui', widget_name='QDialog')
			if hasattr(QtCore.Qt, 'MaximizeUsingFullscreenGeometryHint'):
				view.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)

			view.info_label.setText(
				str(view.info_label.text()).format(
					version=os.environ.get('VERSION', '??'),
					from_version=settings_model.get('from_version', '??'),
					email='gehrmann.apps@gmail.com',
				)
			)

			Network.send_for_google_analytics(settings_model=self.settings_model, event=dict(category='Common', action='Show Dialog ' + self.__class__.__name__))

	""" Helpers """

	def show(self):
		self.dialog.exec_()


def run_init():
	from models.abstract import ObservableAttrDict

	os.environ['VERSION'] = '2.0a'

	settings_model = ObservableAttrDict(
		from_version='1.0a',
	)

	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	controller = InfoController(settings_model=settings_model)
	# controller.dialog.setGeometry(850, 50, 480, 640)
	controller.show()


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
