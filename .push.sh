#!/usr/bin/env bash

if test -z "${SRC_PATH}"; then
	>&2 echo Empty variable SRC_PATH
	exit
fi

#set -x

#SRC_PATH=my_test_module.py
DST_PATH="/storage/emulated/0"
SDK_PATH="/home/vlad/-install/-programming/-android/sdk"

${SDK_PATH}/platform-tools/adb shell rm -rf "${DST_PATH}/Vocabulary\\ Trainer/${SRC_PATH}"
${SDK_PATH}/platform-tools/adb push ${SRC_PATH} "${DST_PATH}/Vocabulary Trainer/${SRC_PATH}"
