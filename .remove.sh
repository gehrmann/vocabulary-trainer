#!/usr/bin/env bash

DST_PATH="/storage/emulated/0"
SDK_PATH="/home/vlad/-install/-programming/-android/sdk"

if test -z "${SRC_PATH}"; then
	>&2 echo Empty variable SRC_PATH
	exit
fi

set -x

#${SDK_PATH}/platform-tools/adb shell mkdir "${DST_PATH}/Vocabulary\\ Trainer/${SRC_PATH}"
${SDK_PATH}/platform-tools/adb shell rm -rf "${DST_PATH}/Vocabulary\\ Trainer/${SRC_PATH}"
${SDK_PATH}/platform-tools/adb shell ls "${DST_PATH}/Vocabulary\\ Trainer"
