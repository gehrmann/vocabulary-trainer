#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

"""

*** Build ***

For Linux:
		python setup.py build

For Mac:
		python setup.py bdist_dmg
	or
		python setup.py bdist_mac

For Windows:
		python setup.py bdist_msi
	or
		python setup.py bdist_wininst

"""

import os
import sys
from cx_Freeze import setup, Executable

if not sys.argv[1:]:
	print("{} [ build | bdist_dmg | bdist_mac | bdist_msi | bdist_wininst ]".format(sys.argv[0]), file=sys.stderr);
	sys.exit()

with open("Vocabulary Trainer, trunk.pro") as src:
	for line in src:
		if line.startswith('APP_NAME='):
			tmp, name = line.rstrip().split('=', 1)
		elif line.startswith('VERSION='):
			tmp, version = line.rstrip().split('=', 1)
		elif line.startswith('DESCRIPTION='):
			tmp, description = line.rstrip().split('=', 1)
		elif line.startswith('AUTHOR='):
			tmp, author = line.rstrip().split('=', 1)
		elif line.startswith('EMAIL='):
			tmp, email = line.rstrip().split('=', 1)

setup(
	name=name,
	version=version,
	description=description,
	author=author,
	author_email=email,
	options=dict(
		build_exe=dict(
			packages=['controllers', 'models', 'helpers'],
			excludes=[],
			includes=["atexit"],
			include_files=[
				# "main_view.ui",
			],
		)
	),
	executables=[
		Executable('main.py', base=('Win32GUI' if sys.platform == 'win32' else None)),
	],
)
