#!/bin/sh

ARGS="${ARGS} --html"
#ARGS="${ARGS} --pdf"
ARGS="${ARGS} -v"
#ARGS="${ARGS} -v"
#ARGS="${ARGS} -v"
ARGS="${ARGS} --no-frames"
ARGS="${ARGS} --name Vocabulary_Trainer"
ARGS="${ARGS} --url https://bitbucket.org/gehrmann/vocabulary-trainer/"
#ARGS="${ARGS} --parse-only"
#ARGS="${ARGS} --introspect-only"
#ARGS="${ARGS} --docformat epytext"
ARGS="${ARGS} --docformat restructuredtext"
#ARGS="${ARGS} --graph classtree"
ARGS="${ARGS} --graph callgraph"
ARGS="${ARGS} --pstat profile.prof"
#ARGS="${ARGS} --graph all"
#ARGS="${ARGS} --inheritance grouped"
ARGS="${ARGS} --inheritance listed"
#ARGS="${ARGS} --inheritance included"
#ARGS="${ARGS} --css default"
#ARGS="${ARGS} --css white"
#ARGS="${ARGS} --css blue"
#ARGS="${ARGS} --css green"
#ARGS="${ARGS} --css black"
ARGS="${ARGS} --css grayscale"

MODULES="${MODULES} controllers"
MODULES="${MODULES} models"
MODULES="${MODULES} helpers"
MODULES="${MODULES} styles.py"

echo -n "Checking profile.prof..."
if [ ! -r "profile.prof" ]; then
	echo "ERROR!"
	echo
	echo "Unable to build call-graphs: profile.prof not exists."
	echo "Run the program with CPU-profiling enabled!"
	echo
	exit
fi
echo "DONE"

epydoc ${ARGS} -o .doc ${MODULES}
