# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
import logging
import os
import signal
import sys

__doc__ = """Proxy for transparent selection between PyQt4 and PyQt5

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

if __name__ == '__main__':
	# Runs in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

try:
	import PyQt5
except ImportError:
	from PyQt4 import QtCore, QtGui, uic
	try:
		from PyQt4 import QtSvg
	except ImportError:
		pass
	try:
		from PyQt4 import QtWebEngineWidgets
	except ImportError:
		pass
	logging.getLogger(__name__).debug('Using PyQt4: %s', QtCore.QT_VERSION_STR)
	# Force PyQt4 be alike PyQt5
	QtWidgets = QtGui
	QtCore.qInstallMessageHandler = QtCore.qInstallMsgHandler
else:
	from PyQt5 import QtCore, QtGui, QtWidgets, uic
	try:
		from PyQt5 import QtSvg
	except ImportError:
		pass
	try:
		from PyQt5 import QtWebEngineWidgets
	except ImportError:
		pass
	logging.getLogger(__name__).debug('Using PyQt5: %s', QtCore.QT_VERSION_STR)
	# Force PyQt5 be alike PyQt4
	QtCore.QString = lambda x: (x)
