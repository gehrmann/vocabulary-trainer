#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python3" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
"""

import os
import sys

if __name__ == '__main__':
	# Runs in application's working directory
	# os.chdir((os.path.dirname(sys.executable if getattr(sys, 'frozen', False) else __file__) or '.') + '/.'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	application_path = (os.path.dirname(sys.executable if getattr(sys, 'frozen', False) else __file__) or '.') + '/.'
	sys.path.insert(0, os.path.realpath(application_path))
	# os.chdir(application_path)

	data_path = os.path.join(os.environ.get('HOME', '.'), 'Vocabulary Trainer')
	if not os.path.exists(data_path):
		os.mkdir(data_path)
	os.chdir(data_path)


def main():
	import controllers
	controllers.loop(sys.argv)

if __name__ == '__main__':
	main()
