#!/bin/bash

# Registers images/*.png into images.qrc
echo "Creating images.qrc..."
echo >images.qrc '<RCC>'
echo >>images.qrc '	<qresource prefix="/">'
for IMAGE in $(cd $(dirname ${0}) && find images/ -type f -iname "*.png"); {
	echo >>images.qrc '		<file>'${IMAGE}'</file>'
}
echo >>images.qrc '	</qresource>'
echo >>images.qrc '</RCC>'
echo "Done."

# Generates images_rc.py
echo "Creating images_rc.py..."
bash -c "cd $(dirname ${0}) && python -m PyQt5.pyrcc_main images.qrc" > images_rc.py
echo "Done."
