#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides a model to store program's settings
"""

import logging
import os
import signal
import sys

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), 'WARNING')))

from models.abstract import Memento, ObservableAttrDict, ObservableList, ObservableSet


class Settings(ObservableAttrDict):
	"""Stores program's settings"""

	def __init__(self, path=None):
		super(Settings, self).__init__()

		from styles import styles

		# Protects from saving default values in settings.txt
		self.__dict__['defaults'] = ObservableAttrDict(
			font=ObservableAttrDict(family=styles.DEFAULT_FONT, size=styles.FONT_SCALE * 15, weight=12, italic=False),
			button_size=styles.DEFAULT_BUTTON_SIZE_MM,
			close_on_double_back=True,
			current_tab=0,
			unfreeze=None,
			show_banner_after=2.,
			# shared_vocabularies_storages=ObservableSet([ObservableAttrDict(name='Default shared storage')]),
			shared_vocabularies_storages=ObservableSet([
				ObservableAttrDict(name='Default shared storage'),
				# ObservableAttrDict(name='Custom shared storage', shared_id='0B_7-aHx_p3QTX3RQaFV4YlpqbE0'),
			]),

			dict_vocabulary=None,
			dict_dictionaries=ObservableList([]),
			dict_left_field='',
			dict_variants_keywords='',
			dict_font=ObservableAttrDict(family=styles.DEFAULT_FONT, size=styles.FONT_SCALE * 18, weight=50, italic=False),
			dict_variants_font=ObservableAttrDict(family=styles.DEFAULT_FONT, size=styles.FONT_SCALE * 15, weight=75, italic=False),
			dict_variants_count=20,
			dict_variants_color='green',
			dict_show_punctuation_signs=True,
			dict_punctuation_signs_font=ObservableAttrDict(family=styles.DEFAULT_FONT, size=styles.FONT_SCALE * 15, weight=75, italic=False),
			dict_clickable_words=True,
			dict_article_color='black',

			list_vocabulary=None,
			list_selected_marks=ObservableSet(['new', 'good', 'fair', 'bad']),
			list_font=ObservableAttrDict(family=styles.DEFAULT_FONT, size=styles.FONT_SCALE * 14, weight=50, italic=False),

			flash_vocabulary=None,
			flash_selected_marks=ObservableSet(['new', 'good', 'fair', 'bad']),
			flash_left_fields_are_hidden=True,
			flash_right_fields_are_hidden=False,
			flash_font=ObservableAttrDict(family=styles.DEFAULT_FONT, size=styles.FONT_SCALE * 21, weight=50, italic=False),
			flash_card_background_color='#ddd',
			flash_hidden_card_background_color='#bbb',
			flash_repeat_bad=False,
			flash_repeat_bad_from=None,
			flash_repeat_bad_to=None,
			flash_repeat_fair=False,
			flash_repeat_fair_from=None,
			flash_repeat_fair_to=None,
			flash_repeat_good=False,
			flash_repeat_good_from=None,
			flash_repeat_good_to=None,

			pairs_vocabulary=None,
			pairs_selected_marks=ObservableSet(['new', 'good', 'fair', 'bad']),
			pairs_left_fields_are_hidden=True,
			pairs_right_fields_are_hidden=False,
			pairs_font=ObservableAttrDict(family=styles.DEFAULT_FONT, size=styles.FONT_SCALE * 14, weight=50, italic=False),
			pairs_card_background_color='#ddd',
			pairs_hidden_card_background_color='#bbb',
			pairs_vertical_rows_count=8,

			cards_vocabulary=None,
			cards_selected_marks=ObservableSet(['new', 'good', 'fair', 'bad']),
			cards_backgrounds_categories=ObservableList(['miscellaneous']),
			cards_font=ObservableAttrDict(family=styles.DEFAULT_FONT, size=styles.FONT_SCALE * 14, weight=50, italic=False),
			cards_left_card_color='#600',
			cards_right_card_color='#060',
			cards_card_background_color='#ddd',
			cards_selected_card_background_color='#bbb',
			cards_rounded_corners=True,
			cards_vertical_rows_count=4,
			cards_vertical_columns_count=3,
			cards_background_arrangement='cover',
			cards_guessed_card_background_color='#bbb',
			cards_guessed_card_delay_timeout=8,
			cards_guessed_card_delay_opacity=.3,

			cols_vocabulary=None,
			cols_selected_marks=ObservableSet(['new', 'good', 'fair', 'bad']),
			cols_font=ObservableAttrDict(family=styles.DEFAULT_FONT, size=styles.FONT_SCALE * 14, weight=50, italic=False),
			cols_leading_field_color='#000',
			cols_trailing_field_color='#666',
			cols_trailing_field_background_color='#eee',
			cols_hidden_trailing_field_background_color='#999',
			cols_column_background_color='#ffcccc',
			cols_failed_field_color='#c33',
			cols_guessed_field_color='#0c0',
			cols_group_by_field='left',
			cols_group_method='( ... )',
			cols_group_filter=3,
			cols_column_field_separator='new line',

			speech_vocabulary=None,
			speech_selected_marks=ObservableSet(['new', 'good', 'fair', 'bad']),
			speech_font=ObservableAttrDict(family=styles.DEFAULT_FONT, size=styles.FONT_SCALE * 21, weight=50, italic=False),
			speech_pairs_count=10,
			speech_left_field_locale='English (en)',
			speech_right_field_locale='English (en)',
			speech_left_field_skip_braces=False,
			speech_right_field_skip_braces=False,
			speech_repeat_pair_count=2,
			speech_pause_between_fields=2,
			speech_pause_between_pairs=4,
		)

		if path is not None:
			self._memento = memento = Memento(path)
			self.update(memento.restore(parse_values=True))
			self.changed.bind(memento.save)

		self.defaults.changed.bind(self._on_model_updated)

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).warning('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:999], current and str(current)[:999])

		if model is self.defaults:
			if current[0] is not None:
				self.update(current[0])

	def __repr__(self):
		return object.__repr__(self)

	def __getattr__(self, key):
		try:
			value = super(Settings, self).__getattr__(key)
			# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "from settings:", key, '=', value, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		except KeyError:
			value = getattr(self.defaults, key)
			# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "from defaults:", key, '=', value, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		return value


def run_init():
	from PyQt import QtCore, QtGui, QtWidgets, uic
	app = QtWidgets.QApplication(sys.argv)
	from styles import styles  # Load once the styles for QApplication

	# settings = Settings()
	settings = Settings(path='settings.txt')

	storages = settings.shared_vocabularies_storages
	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'storages=', storages, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'storages.__class__=', storages.__class__, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

	storages.add(ObservableAttrDict(name='New storage', shared_id='test_id'))
	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'storages=', storages, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented


def main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
