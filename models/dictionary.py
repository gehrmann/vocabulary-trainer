#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python3" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides models of different dictionary formats

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )

Useful tags of extended dictionaries:
	<abr>vt</abr> -- transitive verb
	<abr>vi</abr> -- intransitive verb
	<k>...</k> -- keywords
	<dtrn>...</dtrn> -- determination
	<ex> -- example
"""

# from bisect import bisect_left
from colorama import (
	Fore as FG,
	Back as BG,
	Style as ST,
)
import datetime
import gzip
import logging
import os
# import pickle
import re
import signal
import string
import struct
import sys
import threading
import unicodedata
import zlib

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtCore  # helpers.caller detects which GUI-Toolkit is loaded

from helpers.caller import Caller
from helpers.worker import Worker
from helpers.timer import Timer


def _bisect_left(a, x, lo=0, hi=None, key=None):
	"""Customized function from module `bisect`"""
	assert lo >= 0, 'lo must be non-negative'
	hi = hi or len(a)

	x_key = key(x) if key is not None else x
	while lo < hi:
		mid = (lo + hi) // 2
		a_key = key(a[mid]) if key is not None else a[mid]
		if a_key < x_key:
			lo = mid + 1
		else:
			hi = mid
	return lo


class _DictZip(object):
	"""Dict-Zip (chunked zip-file)"""
	def __init__(self, path, cachesize=2):
		self._FTEXT, self._FHCRC, self._FEXTRA, self._FNAME, self._FCOMMENT = 1, 2, 4, 8, 16

		self._path = path
		self._cache_size = cachesize

		self._current_position = 0
		self._cache = {}
		self._cachekeys = []
		self._chunks = []
		self._chunk_size = 0

		self._parse_header()

	def _parse_header(self):
		with open(self._path, 'rb') as src:
			magic = src.read(2)
			if magic != b'\x1f\x8b':
				raise IOError('Not a gzipped file [DEBUG: {}]'.format(dict(path=self._path)))
			method = ord(src.read(1))
			if method != 8:
				raise IOError('Unknown compression method [DEBUG: {}]'.format(dict(path=self._path)))
			flag = ord(src.read(1))
			# modtime = src.read(4)
			# extraflag = src.read(1)
			# os = src.read(1)
			src.read(6)

			if flag & self._FEXTRA:
				# Read the extra field
				xlen = ord(src.read(1))
				xlen = xlen + 256 * ord(src.read(1))
				extra = src.read(xlen)
				while 1:
					l = (extra[2]) + 256 * (extra[3])
					e = extra[:(4 + l)]
					if e[:2] != b'RA':
						extra = extra[4 + l:]
						if not extra:
							raise Exception('Missing dictzip extension')
						continue
					else:
						break
				length = (extra[2]) + 256 * (extra[3])
				ver = (extra[4]) + 256 * (extra[5])
				self._chunk_size = (extra[6]) + 256 * (extra[7])
				chunks_count = (extra[8]) + 256 * (extra[9])

				p = 10
				chunks_lengths = []
				for i in range(chunks_count):
					chunk_length = (extra[p]) + 256 * (extra[p + 1])
					chunks_lengths.append(chunk_length)
					p = p + 2

				current_position = 0
				for chunk_length in chunks_lengths:
					self._chunks.append((current_position, chunk_length))
					current_position += chunk_length
				self._lastpos = current_position
			else:
				raise Exception('Missing dictzip extension')

			if flag & self._FNAME:
				# Read and discard a null-terminated string containing the filename
				while (1):
					s = src.read(1)
					if not s or s == b'\000':
						break
			if flag & self._FCOMMENT:
				# Read and discard a null-terminated string containing a comment
				while (1):
					s = src.read(1)
					if not s or s == b'\000':
						break
			if flag & self._FHCRC:
				src.read(2)     # Read & discard the 16-bit header CRC

			self._data_start = src.tell()

	def _read_chunk(self, index):
		if index >= len(self._chunks):
			return b''
		if index in self._cache:
			return self._cache[index]

		# Reads chunk data
		with open(self._path, 'rb') as src:
			src.seek(self._data_start + self._chunks[index][0])
			compressed_data = src.read(self._chunks[index][1])

		# Decompresses data
		dobj = zlib.decompressobj(-zlib.MAX_WBITS)
		data = dobj.decompress(compressed_data)
		del dobj

		# Caches data
		self._cache[index] = data
		self._cachekeys.append(index)
		# Deletes the oldest item from cache
		if len(self._cachekeys) > self._cache_size:
			try:
				del self._cache[self._cachekeys[0]]
				del self._cachekeys[0]
			except KeyError:
				pass

		return data

	def seek(self, pos, relative=0):
		self._current_position = (self._current_position if relative else 0) + pos

	def read(self, size=-1):
		from_chunk = self._current_position // self._chunk_size
		from_offset = self._current_position % self._chunk_size
		if size == -1:
			lastchunk = len(self._chunks) + 1
			finish = 0
			npos = sys.maxint
		else:
			lastchunk = (self._current_position + size) // self._chunk_size
			finish = from_offset + size
			npos = self._current_position + size
		buf = b''
		for index in range(from_chunk, lastchunk + 1):
			buf = buf + self._read_chunk(index)
		r = buf[from_offset:finish]
		self._current_position = npos
		return r


class DictdDictionary(object):
	"""Implements "dictd" dictionary format"""

	_number_size = 4  # Length of number (index, length) in bytes: [ 4 | 8 ]
	_numbers_offset = _number_size * 2 + 1  # Length of suffix with index + length
	_number_format = {4: b'!L', 8: b'!Q'}[_number_size]

	# @line_profiler
	def __init__(self, ifo_path, name=None, idx_path=None, dict_path=None):
		self._ifo_path = ifo_path

		if name is not None:
			self._name = name
		else:
			with open(ifo_path, 'r') as src:
				self._name = next((x[len('bookname='):].strip() for x in src if x.startswith('bookname=')), 'Unknown')

		self._idx_path = idx_path = idx_path if idx_path is not None else (ifo_path[:-len('.ifo')] + '.idx')
		self._cached_idx_path = cached_idx_path = idx_path + '.cached'
		self._dict_path = dict_path = dict_path if dict_path is not None else (ifo_path[:-len('.ifo')] + '.dict')
		if not os.path.exists(self._dict_path):
			self._dict_path = dict_path = dict_path + '.dz'

		self._index = None
		self._dict = _DictZip(dict_path)

		self._worker = None

	def start_process_once(self):
		"""Runs a separate process, pre-fills indices and waits in a loop"""
		if self._worker is None:
			self._worker = Worker(target=self._search_loop)

	# @staticmethod
	# def normalize(value):
	#     return value.lower()

	# New normalize method
	# @staticmethod
	# def normalize(value):
	#     # return bytes(''.join(filter(lambda x: (not unicodedata.combining(x)), unicodedata.normalize('NFKD', str(value, errors='ignore')))).lower())
	#     # return bytes(''.join([x for x in unicodedata.normalize('NFKD', str(value, errors='ignore')) if not unicodedata.combining(x)]).lower())
	#     return bytes(''.join(itertools_ifilterfalse(unicodedata.combining, unicodedata.normalize('NFKD', str(value, errors='ignore')))).lower())

	_combining_chars_table = {i: '' for i in range(65535) if unicodedata.combining(chr(i))}

	@classmethod
	def normalize(cls, value):
		"""Returns normalized string (lowercased, without combining characters)"""
		# return bytes(unicodedata.normalize('NFKD', (value if value.__class__ is unicode else str(value, errors='ignore'))).translate(cls._combining_chars_table).lower())
		return unicodedata.normalize('NFKD', (value if value.__class__ is str else str(value, errors='ignore'))).translate(cls._combining_chars_table).lower().encode()

	# @classmethod
	# def _normalize_index(cls, value):
	#     return bytes(unicodedata.normalize('NFKD', str(value[:-cls._numbers_offset], errors='ignore')).translate(cls._combining_chars_table).lower())

	# @classmethod
	# def _normalize_index(cls, value, _cache=dict(), _cache_length=4):
	#     value = str(value[:-cls._numbers_offset], errors='ignore')

	#     return unicodedata.normalize('NFKD', value).translate(cls._combining_chars_table).lower()

	@classmethod
	def _normalize_index(cls, value, _cache=dict()):
		value = str(value[:-cls._numbers_offset], errors='ignore')

		return [_cache[char] if char in _cache else _cache.setdefault(char, unicodedata.normalize('NFKD', char).translate(cls._combining_chars_table).lower()) for char in value]

	def _load_indices(self, with_cached=False):
		"""
		The .idx file is just a word list.

		The word list is a sorted list of word entries.

		Each entry in the word list contains three fields, one after the other: word_str; // a utf-8 string terminated by '\0'. word_data_offset; // word data's offset in .dict file word_data_size; // word data's total size in .dict file

		word_str gives the string representing this word. It's the string that is "looked up" by the StarDict.

		Two or more entries may have the same "word_str" with different word_data_offset and word_data_size. This may be useful for some dictionaries. But this feature is only well supported by StarDict-2.4.8 and newer.

		The length of "word_str" should be less than 256. In other words, (strlen(word) < 256).

		If the version is "3.0.0" and "idxoffsetbits=64", word_data_offset will be 64-bits unsigned number in network byte order. Otherwise it will be 32-bits. word_data_size should be 32-bits unsigned number in network byte order.

		It is possible the different word_str have the same word_data_offset and word_data_size, so multiple word index point to the same definition. But this is not recommended, for mutiple words have the same definition, you may create a ".syn" file for them, see section 4 below.

		The word list must be sorted by calling stardict_strcmp() on the "word_str" fields. If the word list order is wrong, StarDict will fail to function correctly!

		============ gint stardict_strcmp(const gchar *s1, const gchar *s2) { gint a; a = g_ascii_strcasecmp(s1, s2); if (a == 0) return strcmp(s1, s2); else return a;
		}

		g_ascii_strcasecmp() is a glib function: Unlike the BSD strcasecmp() function, this only recognizes standard ASCII letters and ignores the locale, treating all non-ASCII characters as if they are not letters.

		stardict_strcmp() works fine with English characters, but the other locale characters' sorting is not so good, in this case, you can enable the collation feature, see section 6. ```


		Cached entries are separated with \uFFFF.
		"""
		if not with_cached or not os.path.exists(self._cached_idx_path) or not os.path.getsize(self._cached_idx_path):
			with Timer('parsing index of "{}"'.format(self._name)):
				with open(self._idx_path, 'rb') as src:
					self._index = re.split(b'\x00[\d\D]{%d}' % (self._number_size * 2), src.read())

			with Timer('sorting index of "{}"'.format(self._name)):
				self._index.sort(key=self._normalize_index)

			if with_cached:
				with Timer('saving cached index of "{}"'.format(self._name)):
					with open(self._cached_idx_path, 'wb') as dst:
						# pickle.dump(self._index, file=dst, protocol=pickle.HIGHEST_PROTOCOL)
						dst.write(b'\uFFFF'.join(self._index))
		else:
			with Timer('restoring cached index of "{}"'.format(self._name)):
				with open(self._cached_idx_path, 'rb') as src:
					# self._index = pickle.load(src)
					self._index = src.read().split(b'\uFFFF')

	# @line_profiler
	def _search_loop(self, read, write):
		self._load_indices(with_cached=True)

		for work in iter(read, None):
			keywords = str(work['keywords'])
			# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'keywords=', keywords, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

			# with Timer(''):
			_start = _bisect_left(self._index, keywords, key=self.normalize)
			# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), '_start=', _start, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
			# sys.stderr.write('(bisect ' + str(timer) + ')'); sys.stderr.flush()  # FIXME: must be removed/commented

			results = []
			# for index in xrange(_start, len(self._index)):
			for index in range(_start, len(self._index)):
				# sys.stderr.write('.'); sys.stderr.flush()  # FIXME: must be removed/commented
				proposed_index = self._index[index]
				# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'proposed_index=', proposed_index, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
				proposed_keywords = proposed_index[: - self._numbers_offset].decode('utf-8', errors='replace')

				if (
						work['method'] == 'startswith' and self.normalize(proposed_keywords).startswith(self.normalize(keywords)) or
						work['method'] == 'equals' and self.normalize(proposed_keywords) == self.normalize(keywords)
				):
					# logging.getLogger(__name__).debug("%s: %s %s %s %s %s", index, work['method'] == 'startswith', work['method'] == 'equals', (keywords == proposed_keywords), keywords, proposed_keywords)
					if work['method'] == 'startswith' or work['method'] == 'equals' and (keywords == proposed_keywords):
						# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'found', file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
						offset, = struct.unpack_from(self._number_format, proposed_index[- 2 * self._number_size: - self._number_size])
						size, = struct.unpack_from(self._number_format, proposed_index[- self._number_size:])
						results.append(dict(keywords=proposed_keywords, offset=offset, size=size))
						if len(results) >= work['limit']:
							# sys.stderr.write('1\n'); sys.stderr.flush()  # FIXME: must be removed/commented
							break
				else:
					# sys.stderr.write('2\n'); sys.stderr.flush()  # FIXME: must be removed/commented
					break

			write(results)

	# @line_profiler
	def search(self, keywords, method='equals', limit=1):
		if self._worker is not None:
			with Timer('looking for "{}" in "{}"'.format(keywords, self._name)):
				self._worker.write(dict(keywords=keywords, method=method, limit=limit))

				def _response_ticket():
					result = self._worker.read()
					return [x['keywords'] for x in result]
				return _response_ticket

	# @line_profiler
	def get_articles(self, keywords, method='equals', limit=1):
		"""Searches for keywords, returns articles"""

		if self._worker is None:
			raise Exception('No workers!')

		else:
			with Timer('getting keywords for "{}" from "{}"'.format(keywords, self._name)):
				self._worker.write(dict(keywords=keywords, method=method, limit=limit))
				indices = self._worker.read()

			with Timer('getting articles for "{}" from "{}"'.format(keywords, self._name)):
				articles = []
				for index in indices:
					# If new dict object
					if self._dict is not None:
						self._dict.seek(index['offset'])
						article = self._dict.read(index['size']).decode('utf-8', errors='replace')
					# If previous dict object
					else:
						with (gzip.open if self._dict_path.endswith('.dz') else open)(self._dict_path, 'r') as dictionary_file:
							dictionary_file.seek(index['offset'])
							article = dictionary_file.read(index['size'])
					#
					articles.append(article)
				return articles


def run_init():
	import argparse
	parser = argparse.ArgumentParser(description='Searches articles by keywords in dictionaries.')
	parser.add_argument('-k', '--keywords', required=True, help='Keywords to search')
	parser.add_argument('-n', '--count', type=int, default=10, help='Count of indices to print')
	parser.add_argument('-d', '--dictionary', nargs='+', default=['dictionaries/DE-RU/stardict-GR-LingvoUniversal-2.4.2/GR-Universal.ifo'], help='Dictionaries')
	parser.add_argument('-m', '--method', default='equals', choices=['equals', 'startswith'], help='Search method')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	dictionaries = [DictdDictionary(ifo_path=x) for x in kwargs['dictionary']]
	for dictionary in dictionaries:
		dictionary.start_process_once()

	response_tickets = [dictionary.search(keywords=kwargs['keywords'], method=kwargs['method'], limit=kwargs['count']) for dictionary in dictionaries]
	results = [xx for x in [response_ticket() for response_ticket in response_tickets] for xx in x]
	if results:
		# articles = [dictionary.get_articles(results[0]) for dictionary in dictionaries]
		articles = [str(xx) for x in [dictionary.get_articles(results[0]) for dictionary in dictionaries] for xx in x]
		for article in articles:
			logging.getLogger(__name__).debug('Article: \n---\n%s\n---', article)


def run_dictionary_load():
	dictionary = DictdDictionary(
		name='Universal (Ge-Ru)',
		ifo_path='dictionaries/DE-RU/stardict-GR-LingvoUniversal-2.4.2/GR-Universal.ifo',
	)
	dictionary.start_process_once()

	response_ticket = dictionary.search(keywords='gare', method='startswith', limit=10)
	results = response_ticket()
	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'results=', results, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
	# if results:
	#     articles = dictionary.get_articles(results[0])
	#     logging.getLogger(__name__).debug('Articles: %s', articles)


def run_normalize():
	"""Checks keyword normalization"""
	value = 'Gäre'
	value = DictdDictionary.normalize(value)
	print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'value=', value, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented


def run_print_idx():
	"""Parses oft idx of dictionary"""
	import argparse
	parser = argparse.ArgumentParser(description='Parses idx of dictionary.')
	parser.add_argument('-n', '--count', type=int, default=40, help='Count of indices to print')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	dictionary = DictdDictionary(
		# name='Universal (Ge-Ru)',
		# ifo_path='dictionaries/DE-RU/stardict-GR-LingvoUniversal-2.4.2/GR-Universal.ifo',
		# idx_path='dictionaries/DE-RU/stardict-GR-LingvoUniversal-2.4.2/GR-Universal.idx.oft',
		name='LingvoUniversal EN-RU',
		ifo_path='dictionaries/EN-RU/stardict-ER-LingvoUniversal-2.4.2/ER-LingvoUniversal.ifo',
		idx_path='dictionaries/EN-RU/stardict-ER-LingvoUniversal-2.4.2/ER-LingvoUniversal.idx',
	)
	dictionary._load_indices()
	for keywords in dictionary._index[:kwargs['count']]:
		print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'keywords=', keywords, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented


def run_parse_ex():
	"""Parses examples from dictionary"""
	import argparse
	parser = argparse.ArgumentParser(description='Parses examples from dictionary.')
	parser.add_argument('-d', '--dictionary', required=True, help='Path to dictionary (*.ifo) to parse')
	# parser.add_argument('-d', '--dictionary', default='dictionaries/DE-RU/stardict-GR-LingvoUniversal-2.4.2/GR-Universal.ifo', help='Dictionary to parse')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	print('os.getcwd()=', os.getcwd(), file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

	dictionary = DictdDictionary(ifo_path=kwargs['dictionary'])
	dictionary.start_process_once()

	import HTMLParser

	class Parser(HTMLParser.HTMLParser):
		@classmethod
		def parse(cls, src):
			self = cls()
			self._dst = []
			self._current_tags = []
			# Inserts apostrophes
			src = src.replace('<nu />', '').replace('[&apos;]', '').replace('[/&apos;]', '\u0301').replace('&apos;', '\'')
			self.feed(src)  # Parses string through handlers
			return self._dst

		def handle_starttag(self, tag, attrs):
			self._current_tags.append(tag)
			if tag == 'ex':
				self._dst.append([])

		def handle_endtag(self, tag):
			try:
				list(iter(self._current_tags.pop, tag))  # Removes tag and everything from the right of it
			except IndexError:
				pass

		def handle_data(self, data):
			data = data.replace('<', '〈').replace('>', '〉')
			if 'ex' in self._current_tags:
				self._dst[-1].append(data)

	import time
	pairs = []
	pairs_to_check = []
	max_length = 72
	for article in dictionary.get_articles(keywords='', method='startswith', limit=sys.maxint):
		lines = Parser.parse(article)
		sys.stderr.write(' '); sys.stderr.flush()  # FIXME: must be removed/commented
		if not lines:
			continue

		sys.stderr.write('.'); sys.stderr.flush()  # FIXME: must be removed/commented
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), '----------------------', file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'article=', article, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		for line in lines:
			line = ''.join(line)
			for divider in (
					' — ',
					' ≈ ',
			):
				if divider in line:
					pair_to_check = line.split(divider)
					pair = line.split(divider, 1)
					break
			if len(pair_to_check) < 2:
				raise Exception('Can not divide into two parts: ' + line)
			elif len(pair_to_check) > 2 or len(pair[0]) > max_length or len(pair[1]) > max_length:
				pairs_to_check.append(pair)
			else:
				pairs.append(pair)

	from models.abstract import Memento
	Memento('examples-to-check.txt').save(pairs_to_check)
	Memento('examples.txt').save(pairs)


def run_parse_k_dtrn():
	"""Parses pairs "keywords - determinations" from dictionary"""
	import argparse
	parser = argparse.ArgumentParser(description='Parses pairs "keywords - determinations" from dictionary.')
	parser.add_argument('-d', '--dictionary', default='dictionaries/DE-RU/stardict-GR-LingvoUniversal-2.4.2/GR-Universal.ifo', help='Dictionary to parse')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	dictionary = DictdDictionary(ifo_path=kwargs['dictionary'])
	dictionary.start_process_once()

	import HTMLParser

	class Parser(HTMLParser.HTMLParser):
		@classmethod
		def parse(cls, src):
			self = cls()
			self._dst = ([], [])
			self._current_tags = []
			# Inserts apostrophes
			src = src.replace('<nu />', '').replace('[&apos;]', '').replace('[/&apos;]', '\u0301').replace('&apos;', '\'')
			self.feed(src)  # Parses string through handlers
			return self._dst

		def handle_starttag(self, tag, attrs):
			self._current_tags.append(tag)
			if tag == 'k':
				self._dst[0].append([])
			elif tag == 'dtrn':
				self._dst[-1].append([])

		def handle_endtag(self, tag):
			try:
				list(iter(self._current_tags.pop, tag))  # Removes tag and everything from the right of it
			except IndexError:
				pass

		def handle_data(self, data):
			data = data.replace('<', '〈').replace('>', '〉')
			if data and 'k' in self._current_tags:
				self._dst[0][-1].append(data)
			elif data and 'dtrn' in self._current_tags:
				self._dst[-1][-1].append(data)

	import time
	pairs = []
	pairs_to_check = []
	# max_length = 72
	for article in dictionary.get_articles(keywords='', method='startswith', limit=sys.maxint):
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), '----------------------', file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'article=', article, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), '----------------------', file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		sys.stderr.write('.'); sys.stderr.flush()  # FIXME: must be removed/commented

		pair = Parser.parse(article)
			# ', '.join([xx for xx in [' '.join(x) for x in pair[0] if x] if xx]), \
			# ', '.join([xx for xx in [' '.join(x) for x in pair[-1] if x] if xx]) \
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), \
		#     '\n\t\t\t\t'.join([xx for xx in [''.join(x) for x in pair[0] if x] if xx]), \
		#     '--', \
		#     '\n\t\t\t\t'.join([xx for xx in [''.join(x) for x in pair[1] if x] if xx]) \
		#     , file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		pair = (
			', '.join([xx for xx in [''.join(x) for x in pair[0] if x] if xx]),
			'; '.join([xx for xx in [''.join(x) for x in pair[1] if x] if xx]),
		)
		pairs.append(pair)
		# sys.stderr.write(' '); sys.stderr.flush()  # FIXME: must be removed/commented
		# if not lines:
		#     continue

		# sys.stderr.write('.'); sys.stderr.flush()  # FIXME: must be removed/commented
		# for line in lines:
		#     line = ''.join(line)
		#     for divider in (
		#             ' — ',
		#             ' ≈ ',
		#     ):
		#         if divider in line:
		#             pair_to_check = line.split(divider)
		#             pair = line.split(divider, 1)
		#             break
		#     if len(pair_to_check) < 2:
		#         raise Exception('Can not divide into two parts: ' + line)
		#     elif len(pair_to_check) > 2 or len(pair[0]) > max_length or len(pair[1]) > max_length:
		#         pairs_to_check.append(pair)
		#     else:
		#         pairs.append(pair)

	from models.abstract import Memento
	# Memento('.examples-to-check').save(pairs_to_check)
	Memento('.determinations').save(pairs)


def main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
