#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides a set of models
"""

import datetime
import logging
import os
import shutil
import signal
import sys
import time
import threading

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), 'WARNING')))

from helpers.caller import Caller
from models.abstract import Memento, ObservableAttrDict, ObservableList, ObservableSet
from models.dictionary import DictdDictionary


class _AbstractSharedItems(ObservableSet):
	# _shared_id = None  # Must be overridden
	# _shared_item_class = None  # Must be overridden
	# _path = None  # Must be overridden

	def __init__(self, shared_id=None):
		if shared_id is not None:
			self._shared_id = shared_id
		# self.preload_children(self._shared_item_class(id=self._shared_id, path=''))
		# self.preload_root()
		super(_AbstractSharedItems, self).__init__()

	def preload_root(self, repeat=False):
		self.preload_children(self._shared_item_class(id=self._shared_id, path=''), repeat=repeat)

	def preload_children(self, item, repeat=False):
		"""
		repeat -- time to repeat after, if no data received (disabled by default)
		"""
		from controllers.abstract import Network
		children = Network.get_shared_listing(shared_id=item._id, cache_interval=600)
		if children is None:
			if repeat:
				Caller.call_once_after(repeat, self.preload_children, item=item, repeat=repeat)
			return False

		# for child_id, child_name in children:
		for child_id, child_name in children:
			child = self._shared_item_class(id=child_id, path=item._path + child_name + ('' if '.' in child_name else '/'))
			self.add(child)

		return True

	def download(self, item):
		from controllers.abstract import Network
		for message in Network.download_shared_directory(shared_id=item._id, dst_path=(self._path + os.sep + item._path), autocreate_directories=True, replace=True, raise_exceptions=False):
			yield message


class _AbstractSharedItem(ObservableAttrDict):
	def __init__(self, id, path):
		self._id = id
		self._path = path
		super(_AbstractSharedItem, self).__init__()

	def __str__(self):
		return str(self._path)


class Data(ObservableAttrDict):
	"""Facade for all common models"""

	def __init__(self, fill_after_sec=None):
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'here Data from', self.__class__, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

		super(Data, self).__init__()

		self.backgrounds_categories = backgrounds_categories = _BackgroundsCategories(fill_after_sec=fill_after_sec)
		self.vocabularies = vocabularies = _Vocabularies(fill_after_sec=fill_after_sec)
		self.dictionaries = dictionaries = _Dictionaries(fill_after_sec=fill_after_sec)


class _Vocabularies(ObservableSet):
	def __init__(self, fill_after_sec=None):
		super(_Vocabularies, self).__init__()
		if fill_after_sec is None:
			self.refill()
		else:
			Caller.call_once_after(fill_after_sec + .2, self.refill)

	def refill(self):
		def _refill():
			items = set()
			for path, directories, files in os.walk('vocabularies'):
				for filename in files:
					if filename.endswith('.txt') and not filename.endswith('-bad.txt') and not filename.endswith('-fair.txt') and not filename.endswith('-good.txt'):
						try:
							items.add(_Vocabulary(os.path.join(path, filename[:- len('.txt')])))
						except ValueError:
							pass

			self.clear()
			self.update(items)

		_refill()
		# # Caller.call_once_after(.1, download)
		# # thread = elf.make_breakpoint(threading.Thread(target=send))
		# thread = threading.Thread(target=_refill)
		# thread.setDaemon(True)
		# thread.start()

	def get(self, key, default=None):
		try:
			return self[key]
		except:
			return default

	def __getitem__(self, key):
		if key.__class__ in (str, ):
			return next((x for x in self if x._path == key), None)
		raise KeyError('Vocabulary with path="{}" does not exist'.format(key))
		# return super(_Vocabularies, self).__getitem__(key)

	def __contains__(self, key):
		if key.__class__ in (str, ):
			return any(x._path == key for x in self)
		return super(_Vocabularies, self).__contains__(key)

	def add(self, key):
		if key in self:
			raise NameError('Already exists')
		else:
			if key.__class__ in (str, ):
				# vocabulary = _Vocabulary(path=None)  # Create empty
				# vocabulary.path = key  # Rename to create files
				vocabulary = _Vocabulary(path=key)
			else:
				vocabulary = key
			super(_Vocabularies, self).add(vocabulary)
			return vocabulary

	def remove(self, key):
		item = self[key] if key.__class__ in (str, ) else key

		item.path = None
		super(_Vocabularies, self).remove(item)


class _Vocabulary(ObservableAttrDict):
	_marks = ('new', 'good', 'fair', 'bad')
	_suffixes = ('.txt', '-good.txt', '-fair.txt', '-bad.txt')

	def __init__(self, path):
		self._path = path
		super(_Vocabulary, self).__init__()
		if path is not None:
			self._load()

	def __repr__(self):
		# return object.__repr__(self).replace(' object at ', ' name="{name}" object at '.format(name=str(self)))
		return object.__repr__(self).replace(' object at ', ' name="{name}" object at '.format(name=self.name))

	def __str__(self):
		return str(self.name)

	@property
	def name(self):
		return os.path.split(self._path)[-1] if self._path is not None else None

	# def __getitem__(self, key):
	#     if key not in self:
	#         if key in self._marks:
	#             self._load()

	#     return super(_Vocabulary, self).__getitem__(key)

	# __getattr__ = __getitem__

	def _load(self):
		items = {}
		for mark, path in self._generate_marks_with_paths(self._path):
			pairs = _Pairs(path=path)
			items[mark] = pairs
		self.update(items)

	@staticmethod
	def get_basepath(path):
		return path[:-len('.txt')] if path is not None and path.endswith('.txt') else path

	@classmethod
	def _generate_marks_with_paths(cls, path):
		for mark, suffix in zip(cls._marks, cls._suffixes):
			yield mark, '{}{}'.format(path, suffix) if path is not None else None

	@property
	def path(self):
		return self._path

	@path.setter
	def path(self, value):
		"""Changes path for all Pairs"""
		if self._path != value:
			# self._path = self.get_basepath(value)
			self._path = value

			for mark, path in self._generate_marks_with_paths(self._path):
				self[mark].path = path

	@property
	def marks(self):
		return self._marks


class _Pairs(ObservableList):
	def __init__(self, path):
		self._memento = memento = Memento(path)
		# self.extend(memento.restore(parse_values=False))
		# self.changed.bind(memento.save)
		self._initialized = False
		super(_Pairs, self).__init__()

	def __repr__(self):
		return object.__repr__(self).replace(' object at ', ' len="{}" object at '.format(len(self)))

	def __len__(self):
		self._preload()
		return super(_Pairs, self).__len__()

	def __iter__(self):
		self._preload()
		return super(_Pairs, self).__iter__()

	def _preload(self):
		if not self._initialized:
			self._initialized = True
			self.extend(self._memento.restore(parse_values=False))
			self.changed.bind(self._memento.save)

	@property
	def path(self):
		return self._memento.path

	@path.setter
	def path(self, value):
		self._memento.path = value


class _SharedVocabulariesStorage(ObservableAttrDict):
	def __init__(self, path):
		self._name = name
		self._path = path
		super(_SharedVocabulariesStorage, self).__init__()
		# self._load()

	def __str__(self):
		return self._name

	# def _load(self):
	#     items = {}
	#     for mark, path in self._generate_marks_with_paths(self._path):
	#         pairs = _Pairs(path=path)
	#         items[mark] = pairs
	#     self.update(items)


class _SharedVocabulary(_AbstractSharedItem):
	pass


class _SharedVocabularies(_AbstractSharedItems):
	_shared_id = '0B_7-aHx_p3QTaUkzaTJyOTNQUWs&resourcekey=0-tU9QZHJ_4EA3ESX2q22JDA#list'
	_shared_item_class = _SharedVocabulary
	_path = 'vocabularies'

	def download(self, item):
		from controllers.abstract import Network
		for message in Network.download_shared_file(shared_id=item._id, dst_path=(self._path + os.sep + item._path), autocreate_directories=True, replace=True, raise_exceptions=False):
			yield message


class _Dictionaries(ObservableList):
	def __init__(self, fill_after_sec=None):
		super(_Dictionaries, self).__init__()
		if fill_after_sec is None:
			self.refill()
		else:
			Caller.call_once_after(fill_after_sec + .4, self.refill)

	def refill(self):
		def _refill():
			items = []
			for path, directories, files in os.walk('dictionaries'):
				for filename in files:
					if filename.endswith('.ifo'):
						try:
							items.append(_Dictionary(os.path.join(path, filename[:-len('.ifo')])))
						except (IOError, ValueError) as e:
							logging.getLogger(__name__).error('Can not load dictionary: %s', e)

			self[:] = items

		_refill()
		# # Caller.call_once_after(.1, download)
		# # thread = elf.make_breakpoint(threading.Thread(target=send))
		# thread = threading.Thread(target=_refill)
		# thread.setDaemon(True)
		# thread.start()

	def __getitem__(self, key):
		if key.__class__ not in (str, ):
			AttributeError('Key-attribute must be "str" or "unicode", not {}({}).'.format(key.__class__, key))

		item = next((x for x in self if x._name == key), None)

		if item is not None:
			# Caller.call_once_after(.0, item.start_process_once)  # A item can be not pre-filled
			item.start_process_once()

		return item

	def remove(self, key):
		item = self[key] if key.__class__ in (str, ) else key

		try:
			shutil.rmtree(item.path)
		except OSError as e:
			# raise e.__class__, e.__class__(str(e) + ' [DEBUG: {}]'.format(dict(item=item, item_path=item.path))), sys.exc_info()[2]
			raise e.__class__(str(e) + ' [DEBUG: {}]'.format(dict(item=item, item_path=item.path))) from e

		super(_Dictionaries, self).remove(item)


class _Dictionary(DictdDictionary):
	def __init__(self, path):
		self._path = path
		DictdDictionary.__init__(self, ifo_path=path + '.ifo')

	def __repr__(self):
		return object.__repr__(self).replace(' object at ', ' name="{self._name}" object at '.format(self=self))

	def __str__(self):
		return self._name

	@property
	def path(self):
		path = self._path.split(os.path.sep)

		# Path to last directory if contents only one dictionary
		if len({x.split('.', 1)[0] for x in os.listdir(os.path.join(*path[:-1]))}) < 2:
			path.pop(-1)

		return os.path.join(*path)

	@property
	def decorated_path(self):
		directory_path = self._path.split(os.path.sep)[:-1]

		# Removes last directory name if contents only one dictionary
		if len({x.split('.', 1)[0] for x in os.listdir(os.path.join(*directory_path))}) < 2:
			directory_path.pop(-1)

		return ' ▸ '.join(directory_path + [self._name])


class _SharedDictionary(_AbstractSharedItem):
	pass


class _SharedDictionaries(_AbstractSharedItems):
	_shared_id = '0B_7-aHx_p3QTUG5vZkl6dEZDUWs&resourcekey=0-iyyQ4Ds8nL45H-HMDABnMg#list'
	_shared_item_class = _SharedDictionary
	_path = 'dictionaries'


class _BackgroundsCategories(ObservableList):
	def __init__(self, fill_after_sec=None):
		super(_BackgroundsCategories, self).__init__()
		if fill_after_sec is None:
			self.refill()
		else:
			Caller.call_once_after(fill_after_sec + .0, self.refill)

	def refill(self):
		def _refill():
			items = []
			try:
				items.extend([_BackgroundsCategory(path) for path in os.listdir('backgrounds')])
			except OSError:
				pass

			self[:] = items

		_refill()
		# # Caller.call_once_after(.1, download)
		# # thread = elf.make_breakpoint(threading.Thread(target=send))
		# thread = threading.Thread(target=_refill)
		# thread.setDaemon(True)
		# thread.start()

	def __getitem__(self, key):
		if key.__class__ in (str, ):
			item = next((x for x in self if x._name == key), None)
		else:
			item = super(_BackgroundsCategories, self).__getitem__(key)
		return item

	def remove(self, key):
		item = self[key] if key.__class__ in (str, ) else key

		shutil.rmtree(os.path.join(*os.path.split(item.path)[:-1]))
		super(_BackgroundsCategories, self).remove(item)


class _BackgroundsCategory(ObservableList):
	def __init__(self, name):
		self._name = name
		super(_BackgroundsCategory, self).__init__()

		path = os.path.join('backgrounds', self._name)
		try:
			# self.extend([os.path.join(path, x) for x in os.listdir(path) if x.endswith('.png')])
			self.extend([os.path.join(path, x) for x in os.listdir(path) if x.endswith('.jpg') or x.endswith('.png')])
		except (IndexError, OSError):
			pass

		self._path = path

	def __repr__(self):
		return object.__repr__(self).replace(' object at ', ' name="{self._name}" object at '.format(self=self))

	def __str__(self):
		return self._name

	# def __eq__(self, item):
	#     """Compares two objects by theirs ids, not by content"""
	#     return id(self) == id(item)

	# def __hash__(self):
	#     return id(self)

	@property
	def path(self):
		return self._path


class _SharedBackgroundsCategory(_AbstractSharedItem):
	pass


class _SharedBackgroundsCategories(_AbstractSharedItems):
	_shared_id = '0B_7-aHx_p3QTeHpScUhpX1V5ZFE&resourcekey=0-bS-Wvcjajg3OnYwl4rFnXg#list'
	_shared_item_class = _SharedBackgroundsCategory
	_path = 'backgrounds'


def run_dictionaries():
	for item in _Dictionaries():
		print('item,', item, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed


def run_vocabularies():
	for item in _Vocabularies():
		print('item,', item, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed


def run_shared_vocabularies():
	for item in _SharedVocabularies():
		print('item,', item, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed


def main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-r', '--run-function', required=True, choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
