#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals

__doc__ = """
This module provides default styles for Qt widgets

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import datetime
import logging
import os
import signal
import sys
import textwrap

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + ''); sys.path.insert(0, os.path.realpath(os.getcwd()))
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

from PyQt import QtGui, QtWidgets

from helpers.caller import Caller


class Styles(object):
	"""Default styles for application"""

	def __init__(self):
		self.DEFAULT_FONT = (set(('Droid Sans', 'Open Sans', 'Roboto', 'Droid Serif', 'Arial', 'Verdana', 'Tahoma', 'Serif')) & set(str(x) for x in QtGui.QFontDatabase().families())).pop()
		self.DEFAULT_BUTTON_SIZE_MM = 8
		self.FONT_SCALE = 1.
		# self.PX_IN_MM = 160 / 25.4
		# self.PX_IN_MM = QtWidgets.qApp.desktop().logicalDpiY() / 25.4
		self.PX_IN_MM = QtWidgets.qApp.desktop().physicalDpiY() / 25.4
		self.SCREEN_WIDTH_MM = QtWidgets.qApp.desktop().widthMM()
		self.SCREEN_HEIGHT_MM = QtWidgets.qApp.desktop().heightMM()
		self.AVAILABLE_WIDTH = QtWidgets.qApp.desktop().availableGeometry().width()
		self.AVAILABLE_HEIGHT = QtWidgets.qApp.desktop().availableGeometry().height()
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "PX_IN_MM,", self.PX_IN_MM, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "SCREEN_WIDTH_MM, SCREEN_HEIGHT_MM,", self.SCREEN_WIDTH_MM, self.SCREEN_HEIGHT_MM, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented
		# print('{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "AVAILABLE_WIDTH / PX_IN_MM, AVAILABLE_HEIGHT / PX_IN_MM,", self.AVAILABLE_WIDTH / self.PX_IN_MM, self.AVAILABLE_HEIGHT / self.PX_IN_MM, file=sys.stderr); sys.stderr.flush()  # FIXME: must be removed/commented

	def mm2px(self, value):
		"""Converts length (mm, float), returns pixels (int). Can be wrong on some devices."""
		return int(value * self.PX_IN_MM)

	# def replace(self, view, key, styles):
	def replace(self, view, *replacements):
		"""Replaces lines in view's stylesheet started from `key` with `key` + `styles`"""
		stylesheet = str(view.styleSheet()).split('\n')
		for replacement in replacements:
			for key, styles in replacement.items():
				stylesheet = [line for line in stylesheet if not line.startswith(key)] + [('' if key in styles else key) + styles]
		view.setStyleSheet('\n'.join(stylesheet))

	def set_button_size(self, button_size):
		self.replace(QtWidgets.qApp, {
			'QTreeWidget { qproperty-indentation: ': '{0}; }}'.format(self.mm2px(1. * button_size)),
			'#signs_browser { min-width: ': '{0}px; max-width: {0}px; }}'.format(self.mm2px(1 + button_size)),
			'QPushButton, QToolButton, QComboBox, QTreeWidget::item { min-width: ': '{0}px; min-height: {0}px; icon-size: {1}px; }}'.format(self.mm2px(button_size), self.mm2px(.6 * button_size)),
			'QTabBar::tab, QTableView > QHeaderView { min-width: ': '{0}px; min-height: {0}px; }}'.format(self.mm2px(.6 * button_size)),
			'QCheckBox::indicator, QGroupBox::indicator { width: ': '{0}px; height: {0}px; }}'.format(self.mm2px(button_size)),
			'QComboBox::drop-down { min-width: ': '{0}px; min-height: {0}px; }}'.format(self.mm2px(button_size)),
			'QComboBox::item { height: ': '{0}px; }}'.format(self.mm2px(button_size)),
			'QScrollBar:vertical { width: ': '{0}px; }}'.format(self.mm2px(button_size)),
			'QScrollBar:horizontal { height: ': '{0}px; }}'.format(self.mm2px(button_size)),
			'QScrollBar::handle:verical { min-height: ': '{0}px; }}'.format(self.mm2px(button_size)),
			'QScrollBar::handle:horizontal { min-width: ': '{0}px; }}'.format(self.mm2px(button_size)),
			'#play_button { min-width: ': '{0}px; max-width: {0}px; min-height: {0}px; max-height: {0}px; icon-size: {1}px; }}'.format(self.mm2px(3. * button_size), self.mm2px(2.4 * button_size)),
		})

	@property
	def stylesheet(self):
		return textwrap.dedent('''
		/*
			* { border: 1px solid red }
			#main_widget { border-image: url(images/background.jpg); }
		*/
			QScrollArea, QScrollArea > * > QWidget { background: transparent; }
			QPushButton { background: #696; color: #fff; }
			QToolButton { border: 0px; background: transparent; }
			QGroupBox { margin: 0ex 0px 20px; border: 0px solid #696; border-radius: 5px; background: rgba(0, 0, 0, 10); }
			QGroupBox[checkable=false] { padding-top: 4ex; }
			QGroupBox[checkable=true] { padding-top: 6ex; }
			QGroupBox::title { subcontrol-origin: border; subcontrol-position: top center; left: 0ex; top: 0ex; border-bottom: 1px solid  rgba(0, 0, 0, 20); padding: 5px 9999px; }
			QCheckBox::indicator::unchecked, QGroupBox::indicator::unchecked { image: url(:/images/checkbox-unchecked.png); }
			QCheckBox::indicator::checked, QGroupBox::indicator::checked { image: url(:/images/checkbox-checked.png); }
			QCheckBox::indicator, QGroupBox::indicator { width: 32px; height: 32px; }
			QTableView QLineEdit { padding: 0 ''' + str(self.mm2px(1)) + '''px; }

			QComboBox { border: 0px solid #000; border-radius: 5px; padding: ''' + str(self.mm2px(1)) + '''px; color: #000; background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #7a7, stop: .5 #696, stop:1 #585); }
			/* Background for item + dropdown (but dropdown is *black* by default, can not be transparent) */
			QComboBox::editable { background: #fff; }
			QComboBox::!editable { color: #fff; }
			QComboBox::drop-down { border-top-right-radius: 5px; border-bottom-right-radius: 5px; image: url(:/images/down-arrow.png); background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #7a7, stop: .5 #696, stop:1 #585); }
			QComboBox QAbstractItemView { min-width: 100px; min-height: 100px; border: 1px solid #696; padding: ''' + str(self.mm2px(1)) + '''px; background: #fff; }
			QComboBox::item { color: #000; }
			QComboBox::item:selected { color: #fff; background: #696; }

			#left_field_input, #right_field_input { margin: 0% 0 0%; border: 0px none transparent; background: transparent; }
			QTextBrowser { border: 0px none transparent; background: transparent; }
			QTableView QHeaderView { color: white; background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #ccc, stop: .5 #bbb, stop:1 #aaa); }
			#top_widget #new_checkbox, #top_widget #bad_checkbox, #top_widget #fair_checkbox, #top_widget #good_checkbox { color: black; font: 8pt "''' + self.DEFAULT_FONT + '''"; font-weight: 400; }
			#left_field_input, #right_field_input { padding-right: 28px; }
			#flash_view #left_field_visibility_button, #flash_view #right_field_visibility_button, #pairs_view #left_field_visibility_button, #pairs_view #right_field_visibility_button { border: none 0px transparent; outline: 0px; background: transparent; padding: 5px; }
			#cols_container #column { margin: ''' + str(self.mm2px(1)) + '''px ''' + str(self.mm2px(.5)) + '''px; border-radius: ''' + str(self.mm2px(2)) + '''px; background: #eee; }
			QDialogButtonBox { dialogbuttonbox-buttons-have-icons: true; }
			#tips_widget, #tips_widget * { background: #fc6; margin-bottom: 1px }
			#tips_image { min-width: ''' + str(self.mm2px(5)) + '''px; min-height: ''' + str(self.mm2px(5)) + '''px; max-width: ''' + str(self.mm2px(5)) + '''px; max-height: ''' + str(self.mm2px(5)) + '''px; }
			#wizard_button {background-color: #FFF; border-radius: 20%}
			#signs_browser { margin-left: -2px; }
			QProgressDialog { background: #696; color: white; }
			#show_rate_application_button { background: #b40; color: white; font-weight: bold; }
			#shared_id_entry { font-size: 14pt; }
			#shortened_field_label { padding: 10px; }
			#trailing_field_label { padding: 10px; }

		/*
			QTreeWidget { selection-color: transparent; selection-background-color: transparent; show-decoration-selected: 1; }
			QTreeWidget { selection-color: white; selection-background-color: #696; show-decoration-selected: 1; }
			QTreeWidget::item { padding: ''' + str(self.mm2px(1.4)) + '''px; }
			QTreeWidget::item:!has-children { border: 0px solid transparent; color: #666; image: url(:/images/checkbox-unchecked.png); image-position: right; padding-right: ''' + str(self.mm2px(1.)) + '''px; }
			QTreeWidget::item:!has-children:selected { color: #000; image: url(:/images/checkbox-checked.png); }
		*/
			QTreeWidget { selection-background-color: transparent; }
			QTreeWidget::item { border-radius: ''' + str(self.mm2px(.5)) + '''px; padding: 2px 4px 0px 0px; }
			QTreeWidget::item:has-children { background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 rgba(85, 136, 85, 16), stop: .5 rgba(85, 136, 85, 32), stop:1 rgba(85, 136, 85, 64)); background-clip: content; }
			QTreeWidget::item:!has-children { color: #999; }
			QTreeWidget::item:!has-children:selected { color: #060; }
			QTreeWidget::item:!has-children:selected #label { color: #060; }
			/* Branch */						QTreeWidget::branch { image-position: center; }
			/* Not-adjoined child */			QTreeWidget::branch:!adjoins-item:has-siblings { border-image: url(:/images/tree-not-adjoins-has-siblings.png) 0; background-repeat: repeat-y; background-position: center; }
			/* Not-adjoined child (last) */		QTreeWidget::branch:!adjoins-item:!has-siblings { }
			/* Adjoined child */				QTreeWidget::branch:adjoins-item:has-siblings { border-image: url(:/images/tree-adjoins-has-siblings.png) 0; background-repeat: repeat-y; background-position: center; }
			/* Adjoined child (last) */			QTreeWidget::branch:adjoins-item:!has-siblings { border-image: url(:/images/tree-adjoins-hasno-siblings.png); background-repeat: repeat-y; background-position: center; }
			/* Parent (closed) */				QTreeWidget::branch:has-children:!open { image: url(:/images/tree-has-children-closed.png); background-repeat: repeat-y; background-position: center; }
			/* Parent (opened) */				QTreeWidget::branch:has-children:open { image: url(:/images/tree-has-children-opened.png); background-repeat: repeat-y; background-position: center; }

			QScrollBar { margin: ''' + str(self.mm2px(1.)) + '''px; border-radius: ''' + str(self.mm2px(.5)) + '''px; padding: 0px; background: rgba(255, 255, 255, 192); }
			QScrollBar::handle { border-radius: ''' + str(self.mm2px(.5)) + '''px; background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #ccc, stop: .5 #bbb, stop:1 #aaa); }
			QScrollBar::add-line, QScrollBar::sub-line { width: 0; height: 0; }
			QScrollBar::add-page, QScrollBar::sub-page { background: transparent; }

			/* TAB WIDGET { */
				#tabs::pane { border: 0px none transparent; border-bottom: 1px solid #696; }
				#tabs::tab-bar { alignment: center; }
				#tabs > QTabBar::tab { border-top: .6ex solid transparent; border-bottom: .6ex solid transparent; padding: ''' + str(self.mm2px(1)) + '''px ''' + str(self.mm2px(1.2)) + '''px; }
				#tabs > QTabBar::tab:hover { border-top: ''' + str(self.mm2px(1)) + '''px solid #ccc; }
				#tabs > QTabBar::tab:selected { border-top: ''' + str(self.mm2px(1)) + '''px solid #696; }
			/* } TAB WIDGET */

		/*
			font-size: 12px;
			QComboBox QAbstractItemView { margin: 5px; border: none 0px transparent; outline: none; padding: 5px; color: #fff; selection-color: #fff; background: #696; selection-background-color: #363; }
			QComboBox { border: 1px solid #696; border-radius: 3px; padding: 1px 18px 1px 3px; min-width: 6em; margin: 0px; padding: 0px; min-height: 2.0; background-color: #696; color: #fff; }
			QComboBox::drop-down { width: 30px; }
			QComboBox QListView QScrollBar { background: rgb(90, 31, 0); color: rgb(253, 231, 146); width: 30px; }
			margin: 0px;
			padding: 0px;
			border: 0px;
			min-height: 2.0;
			background-color: rgb(90, 31, 0);
			color: rgb(253, 231, 146);
			QTreeWidget::branch { image: url(:/qt-project.org/styles/commonstyle/images/standardbutton-apply-16.png); }

			QComboBox { min-height:63px; max-height:63px; margin-right:47px; image:url(Resources/ComboBox_Center1.png); font-family:  "Franklin Gothic Medium"; font-size:  22px; }
			QComboBox::drop-down { width:47px; border:0px; margin:0px; margin-right:-47px; }
			QComboBox::down-arrow { image:url(Resources/ComboBox.png); }

		*/
		''')

try:
	import my_styles
	styles = my_styles.Styles()
except ImportError:
	styles = Styles()

if __name__ == "__main__":
	print("\nIn order to apply styles for the application insert\n>>> import styles\nright after the construction of the QApplication\n", file=sys.stderr);

else:
	# print([str(x) for x in QtWidgets.QStyleFactory.keys()])
	QtWidgets.qApp.setStyle('Fusion')
	QtWidgets.qApp.setStyleSheet(styles.stylesheet)
	styles.set_button_size(styles.DEFAULT_BUTTON_SIZE_MM)

	# Watches if file was updated
	if logging.getLogger(__name__).level == logging.DEBUG:
		def watch_if_module_was_updated(__internal={}):
			styles_module = sys.modules.get(__name__, None)
			if styles_module is not None and hasattr(styles_module, '__file__'):
				try:
					mtime = os.path.getmtime(styles_module.__file__)
				except OSError as e:
					# Ignores if getmtime is applicable only for a directory
					import errno
					if e.errno in (errno.ENOTDIR, errno.ENOENT):
						pass
					else:
						raise
				else:
					if 'mtime' not in __internal:
						__internal['mtime'] = mtime
					elif __internal['mtime'] < mtime:
						__internal['mtime'] = mtime
						logging.getLogger(__name__).info('Styles are updated, reloading')
						reload(styles_module)
						return
			Caller.call_once_after(2., watch_if_module_was_updated)
		watch_if_module_was_updated()
