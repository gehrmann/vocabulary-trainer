DEFINES += QT_DEPRECATED_WARNINGS
QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
android {
	QT += androidextras  # Enables QAndroidJniObject
}
#;QT += webview  # Support of QWebView
#;QT += bluetooth

TARGET = vocabulary_trainer_trunk  # Name of target file
TEMPLATE = app  # Qt project template, can be [ app | lib | subdirs | aux | vcapp | vclib ]

APP_NAME=Vocabulary Trainer
DESCRIPTION=
AUTHOR=gehrmann
EMAIL=gehrmann.mail@gmail.com
PACKAGE=gehrmann.vocabulary.learning.game.box
#;PACKAGE=org.qtproject.example.vocabulary_trainer_trunk
VERSION=$$system("python -B -c \"import changes; print changes.version\"")
VERSION_CODE=$$replace(VERSION, \.,  )
message(" ")
message(" ")
message("VERSION=$${VERSION}, VERSION_CODE=$${VERSION_CODE}")
message(" ")
message(" ")

CONFIG += c++11
qnx: target.path = /tmp/$${TARGET}/bin  # Path for 'make install'
else: unix:!android: target.path = /opt/$${TARGET}/bin  # Path for 'make install'
!isEmpty(target.path): INSTALLS += target  # Targets for 'make install'

;# Forces to cleanup working directory (avoid it and create right dependencies!)
;CONFIG += cleanup

;# Forces banner for all versions
CONFIG += banner

;# Enables in-app purchasing (in development)
;CONFIG += in_app_purchasing

CONFIG(debug, debug|release) {
	# Version with suffix '-debug'
	VERSION=	$${VERSION}-debug
} else {
	# Shows banner
	CONFIG += banner
	;# Adds AppsFlyer (receives INSTALL_REFERRER from Google Play, tracker for gowide)
	;CONFIG += appsflyer
	# Receives INSTALL_REFERRER from Google Play
	CONFIG += broadcast_receiver
	;CONFIG += full
	# Enables in-app billing (restricts app in some poor countries)
	;CONFIG += billing
}

;# Adds desktop widget (broken, can not launch python from DesktopWidgetReceiver)
;CONFIG += desktop_widget

;QMAKE_CFLAGS+=-fprofile-arcs -fprofile-dir=. -ftest-coverage
;QMAKE_CXXFLAGS+=-fprofile-arcs -fprofile-dir=. -ftest-coverage

HEADERS += \
				main.h \

SOURCES += \
				main.cpp \

FORMS    += \
				views/main_view.ui \
				\
				views/info_dialog.ui \
				views/release_notes_dialog.ui \
				views/first_start_wizard_dialog.ui \
				\
				views/top_widget.ui \
				\
				views/common_settings_dialog.ui \
				views/common_multiwindow_dialog.ui \
				views/common_tree_dialog.ui views/common_tree_entry_with_buttons_view.ui \
				\
				views/add_shared_vocabularies_storage_dialog.ui \
				\
				views/dict_view.ui views/dict_settings_widget.ui \
				views/list_view.ui views/list_settings_widget.ui \
				views/flash_view.ui views/flash_settings_widget.ui \
				views/pairs_view.ui views/pairs_item_view.ui views/pairs_settings_widget.ui \
				views/cards_view.ui views/cards_item_view.ui views/cards_settings_widget.ui \
				views/cols_view.ui views/cols_column_view.ui views/cols_settings_widget.ui \
				views/speech_view.ui views/speech_settings_widget.ui \

RESOURCES += \
	images.qrc \

CONFIG += mobility
MOBILITY = 

DISTFILES += \
    Android/AndroidManifest.xml \

ANDROID_JAVA_SOURCES += \
	Android/src/com/gehrmann/Activity.Helper.java \

CONFIG(banner) {
	DISTFILES += \
		Android/build.gradle \

}
CONFIG(in_app_purchasing) {
	QT += quick
	QT += qml
	QT += purchasing
	#; here must be some special qml-widget added
}

APP_BASENAME = $${APP_NAME}

CONFIG(debug, debug|release) {
#; FIXME  	PACKAGE=			$${PACKAGE}.debug
	APP_NAME+=			"(debug)"
}
CONFIG(banner) {
#; FIXME  #	PACKAGE=			$${PACKAGE}.free
	APP_NAME+=			"(free)"
} else {
	CONFIG(full) {
#; FIXME  		PACKAGE=			$${PACKAGE}.full
		APP_NAME+=			"(full)"
	} else {
#; FIXME  		PACKAGE=			$${PACKAGE}.pro
		APP_NAME+=			"(pro)"
	}
}

LIB_PREFIX="lib+"
LIB_SEPARATOR="+"
LIB_SUFFIX="+.so"
WRAP_SUFFIX=".elf-wrap"
;WRAP_LENGTH=4012  #; 4008 + 4
;WRAP_LENGTH=4090  #; 4008 + 4, previous stable value
WRAP_LENGTH=6420  #; working value for Android 8.x (the whole shared library in the beginning, read only protected from stripping)

ADMOB_APPLICATION_ID='ca-app-pub-9181265559842913~7303796384'

linux:!android {
	PYTHON_ROOT=$$PWD/.dependencies/python/.build/python2.7.2-linux-x86_64
	PYTHON_LIBS=$${PYTHON_ROOT}/build/lib.linux-x86_64-2.7
	PYTHON_ASSETS=$$PWD/.dependencies/python_assets/.build/.linux-x86_64
	PYQT5_ROOT=$$PWD/.dependencies/pyqt5/.build/.linux-x86_64-5.11.2
	PYQT5_HOST_ROOT=$$PWD/.dependencies/pyqt5/.build/.linux-x86_64-5.11.2

	LIB_DIRECTORY_PATH=""
	#;LIB_DIRECTORY_PATH=""
	#;LIB_PREFIX=""
	#;LIB_SEPARATOR="/"
	#;LIB_SUFFIX=""

	INCLUDEPATH += $${PYTHON_ROOT}
	INCLUDEPATH += $${PYTHON_ROOT}/Include
 	LIBS += -L$${PYTHON_ROOT} -lpython2.7
    ANDROID_EXTRA_LIBS += $${PYTHON_ROOT}/libpython2.7.so
}
android {
	PYTHON_ROOT=$$PWD/.dependencies/python/.build/python2.7.2-android-arm
	PYTHON_LIBS=$${PYTHON_ROOT}/build/lib.linux-x86_64-2.7
	PYTHON_ASSETS=$$PWD/.dependencies/python_assets/.build/.android-arm
	PYQT5_ROOT=$$PWD/.dependencies/pyqt5/.build/.android-arm-5.11.2
	PYQT5_HOST_ROOT=$$PWD/.dependencies/pyqt5/.build/.linux-x86_64-5.11.2
	PIL_ROOT=$$PWD/.dependencies/pil/.build/.android-arm

	LIB_DIRECTORY_PATH="lib/"

	contains(ANDROID_TARGET_ARCH,armeabi-v5) {
		# The ARMv5 archictecture is currently not supported by Qt?
	}
	contains(ANDROID_TARGET_ARCH,mips) {
		# The MIPS archictecture is currently not supported by Qt
	}
	contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
		INCLUDEPATH += $${PYTHON_ROOT}
		INCLUDEPATH += $${PYTHON_ROOT}/Include
		LIBS += -L$${PYTHON_ROOT} -lpython2.7
		ANDROID_EXTRA_LIBS += $${PYTHON_ROOT}/libpython2.7.so
	}
	contains(ANDROID_TARGET_ARCH,x86) {
	}
}

CONFIG(debug, debug|release) {
	DEFINES += "DEBUG=\"yes\""
}
DEFINES += "APP_NAME=\"\\\"$${APP_NAME}\\\"\""
DEFINES += "APP_BASENAME=\"\\\"$${APP_BASENAME}\\\"\""
DEFINES += "PACKAGE=\"\\\"$${PACKAGE}\\\"\""
DEFINES += "VERSION=\"\\\"$${VERSION}\\\"\""
DEFINES += "VERSION_CODE=\"\\\"$${VERSION_CODE}\\\"\""
DEFINES += "LIB_DIRECTORY_PATH=\"\\\"$${LIB_DIRECTORY_PATH}\\\"\""
DEFINES += "LIB_PREFIX=\"\\\"$${LIB_PREFIX}\\\"\""
DEFINES += "LIB_SEPARATOR=\"\\\"$${LIB_SEPARATOR}\\\"\""
DEFINES += "LIB_SUFFIX=\"\\\"$${LIB_SUFFIX}\\\"\""
DEFINES += "WRAP_SUFFIX=\"\\\"$${WRAP_SUFFIX}\\\"\""
DEFINES += "WRAP_LENGTH=$${WRAP_LENGTH}"
DEFINES += "ADMOB_BANNER_ID=\"\\\"$${ADMOB_BANNER_ID}\\\"\""
DEFINES += "ADMOB_INTERSTITIAL_ID=\"\\\"$${ADMOB_INTERSTITIAL_ID}\\\"\""

CONFIG(cleanup) {
	# Cleans working directory before building
	cleanup_target.target = cleanup
	cleanup_target.depends = FORCE
	cleanup_target.commands += @
	cleanup_target.commands += echo;
	cleanup_target.commands += echo Before cleaning; echo ===============; echo -n Files:\ ; ls -w0; echo;
	cleanup_target.commands += ls | grep -v Makefile | grep -v deployment-settings.json | xargs rm -rf {};
	cleanup_target.commands += echo After cleaning; echo ==============; echo -n Files:\ ; ls -w0; echo;
	PRE_TARGETDEPS += cleanup
	QMAKE_EXTRA_TARGETS += cleanup_target

	;first.depends = $(first) cleanup_target
	;export(first.depends)
	;export(cleanup_target.commands)
	;android:QMAKE_EXTRA_TARGETS += first cleanup_target
}

android {
	#; Example project: http://doc.qt.io/qt-5/qtandroidextras-notification-example.html
	ANDROID_PACKAGE_SOURCE_DIR = Android  #; Path (in a ".build"-directory) where to gather everything for custom android sources

	include($$PWD/.dependencies/QtJavaCaller/src/QtJavaCaller.pri)

 	# Custom java-sources
	android_package.target = $${ANDROID_PACKAGE_SOURCE_DIR}
#; FIXME	android_package.target = $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml
	android_package.depends += $$PWD/Android/AndroidManifest.xml
	android_package.depends += $(addprefix $$PWD/,$${ANDROID_JAVA_SOURCES})
	CONFIG(banner) {
		android_package.depends += $$PWD/Android/build.gradle
	}
	android_package.depends += $$PWD/Android/src/org/qtproject/qt5/android/bindings/MyApplication.java
#; FIXME #; FIXME  	android_package.depends += $$PWD/Android/src/QtActivity.java
#; FIXME #; FIXME  	android_package.depends += $$PWD/Android/src/QtLoader.java
#; FIXME #; FIXME  	android_package.depends += $$PWD/Android/src/QtHelper.java
 	android_package.commands += echo \'==============================\';
 	android_package.commands += echo \'$${CONFIG}\';
 	android_package.commands += echo \'$${PACKAGE}\';
 	android_package.commands += echo \'Building $$android_package.target\';
#; FIXME 	android_package.commands += rm -rf $${ANDROID_PACKAGE_SOURCE_DIR};
#; FIXME 	android_package.commands += mkdir -p $${ANDROID_PACKAGE_SOURCE_DIR};
#; FIXME 	android_package.commands += cp -R $$PWD/Android/AndroidManifest.xml $$PWD/Android/res $${ANDROID_PACKAGE_SOURCE_DIR}/;
#; FIXME 	android_package.commands += mkdir -p $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/;
	android_package.commands += rm -rf $@;
	android_package.commands += mkdir -p $@;
	android_package.commands += cp -r $$PWD/Android/* $@;
#; FIXME #; FIXME  	android_package.commands += cp $$PWD/Android/src/QtActivity.java $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/;
#; FIXME #; FIXME  	android_package.commands += cp $$PWD/Android/src/QtLoader.java $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/;
#; FIXME #; FIXME  	android_package.commands += cp $$PWD/Android/src/QtHelper.java $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/;
#; FIXME 	android_package.commands += mkdir $${ANDROID_PACKAGE_SOURCE_DIR}/libs/;
#; FIXME 	android_package.commands += ln -s $$PWD/.deps/android/linux/sdk/extras/android/support/v4/android-support-v4.jar $${ANDROID_PACKAGE_SOURCE_DIR}/libs/;
#; FIXME 	CONFIG(banner) | CONFIG(broadcast_receiver) | CONFIG(appsflyer) {
#; FIXME 		android_package.commands += sed -i.bak \'s^<!-- %%INSERT_GOOGLE_PLAY_SERVICES_LIBRARY_HERE%% -->^<!-- Google play services library -->\n\t\t<meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\"/>\n\t\t<activity android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" android:name=\"com.google.android.gms.ads.AdActivity\"/>\n\t\t<!-- Google play services library -->^\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;
#; FIXME 		android_package.commands += ln -s $$PWD/.dependencies/google-play-services $${ANDROID_PACKAGE_SOURCE_DIR}/google-play-services;
#; FIXME 		android_package.commands += echo \'android.library.reference.1=google-play-services\' >> $${ANDROID_PACKAGE_SOURCE_DIR}/project.properties;
#; FIXME 	}
#; FIXME 	CONFIG(appsflyer) {
#; FIXME 		android_package.commands += ln -s $$PWD/.dependencies/appsflyer/AF-Android-SDK.jar $${ANDROID_PACKAGE_SOURCE_DIR}/libs/AppsFlyerSDK.jar;
#; FIXME 		android_package.commands += sed -i.bak \'s^<!-- %%INSERT_APPSFLYER_BROADCAST_RECEIVER_HERE%% -->^<receiver android:name=\"com.appsflyer.MultipleInstallBroadcastReceiver\" android:exported=\"true\">\n\t\t\t<intent-filter>\n\t\t\t\t<action android:name=\"com.android.vending.INSTALL_REFERRER\" />  <!-- Delivers \"referrer=\" from Google-Play to broadcast receiver -->\n\t\t\t</intent-filter>\n\t\t</receiver>^\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;
#; FIXME 	} else {
#; FIXME #; FIXME  		android_package.commands += sed -i.bak \'s/.*\\/\\/ APPSFLYER//\' $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/QtActivity.java;
#; FIXME 	}
#; FIXME 	CONFIG(banner) {
#; FIXME 		android_package.commands += echo \'===> Adding Banner <===\';
#; FIXME 		android_package.depends += $$PWD/Android/src/QtBanner.java
#; FIXME 		android_package.commands += cp $$PWD/Android/src/QtBanner.java $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/;
#; FIXME 	} else {
#; FIXME #; FIXME  		android_package.commands += sed -i.bak \'s/.*\\/\\/ BANNER//\' $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/QtActivity.java;
#; FIXME #; FIXME  		android_package.commands += sed -i.bak \'s/.*\\/\\/ BANNER//\' $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/QtLoader.java;
#; FIXME 	}
#; FIXME 	CONFIG(broadcast_receiver) {
#; FIXME 		android_package.commands += echo \'===> Adding BroadcastReceiver <===\';
#; FIXME 		android_package.depends += $$PWD/Android/src/QtBroadcastReceiver.java
#; FIXME 		android_package.commands += sed -i.bak \'s^<!-- %%INSERT_QT_BROADCAST_RECEIVER_HERE%% -->^<receiver android:name=\"org.qtproject.qt5.android.bindings.QtBroadcastReceiver\" android:exported=\"true\">\n\t\t\t<intent-filter>\n\t\t\t\t<action android:name=\"com.android.vending.INSTALL_REFERRER\" />  <!-- Delivers \"referrer=\" from Google-Play to broadcast receiver -->\n\t\t\t</intent-filter>\n\t\t</receiver>^\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;
#; FIXME 		android_package.commands += cp $$PWD/Android/src/QtBroadcastReceiver.java $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/;
#; FIXME 	}
#; FIXME 	CONFIG(billing) {
#; FIXME 		android_package.commands += echo \'===> Adding Billing <===\';
#; FIXME 		android_package.depends += $$PWD/Android/src/QtBilling.java
#; FIXME 		android_package.commands += sed -i.bak \'s^<!-- %%INSERT_BILLING_PERMISSION_HERE%% -->^<uses-permission android:name=\"com.android.vending.BILLING\"/>^\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;
#; FIXME 		android_package.commands += cp $$PWD/Android/src/QtBilling.java $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/;
#; FIXME 	}
#; FIXME #; FIXME  	android_package.commands += sed -i.bak \'s/__INSERT_PACKAGE_NAME_HERE__/$${PACKAGE}/\' $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/QtActivity.java;
#; FIXME #; FIXME  	android_package.commands += sed -i.bak \'s/__INSERT_PACKAGE_NAME_HERE__/$${PACKAGE}/\' $${ANDROID_PACKAGE_SOURCE_DIR}/src/org/qtproject/qt5/android/bindings/QtHelper.java;
#; FIXME 	android_package.commands += sed -i.bak \'s/-- %%INSERT_PACKAGE_NAME%% --/$${PACKAGE}/\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;
#; FIXME 	android_package.commands += sed -i.bak \'s/-- %%INSERT_APP_VERSION_NAME%% --/$${VERSION}/\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;
#; FIXME 	android_package.commands += sed -i.bak \'s/-- %%INSERT_APP_VERSION_CODE%% --/$${VERSION_CODE}/\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;
#; FIXME 	android_package.commands += sed -i.bak \'s/-- %%INSERT_APP_NAME%% --/$${APP_NAME}/g\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;

	android_package.commands += sed -i.bak
	android_package.commands +=  -e \'s^-- %%INSERT_PACKAGE_NAME%% --^$${PACKAGE}^\'
	android_package.commands +=  -e \'s^-- %%INSERT_APP_VERSION_NAME%% --^$${VERSION}^\'
	android_package.commands +=  -e \'s^-- %%INSERT_APP_VERSION_CODE%% --^$${VERSION_CODE}^\'
	android_package.commands +=  -e \'s^-- %%INSERT_APP_NAME%% --^$${APP_NAME}^g\'
	android_package.commands +=  -e \'s^org.qtproject.qt5.android.bindings...Activity^com.gehrmann.Activity^\'
	android_package.commands +=   $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;

	CONFIG(banner) {
		;android_package.commands += sed -i.bak \'s^org.qtproject.qt5.android.bindings...Activity^org.dreamdev.QtAdMob.QtAdMobActivity^\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;

		android_package.commands += sed -i.bak \'s^\s*</application>^
		android_package.commands += \n\t\t<!-- Google play services library -->
		android_package.commands += \n\t\t<meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\"/>
		android_package.commands += \n\t\t<activity android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" android:name=\"com.google.android.gms.ads.AdActivity\"/>
		android_package.commands += \n\t\t<!-- Google play services library -->
		android_package.commands += \n
		android_package.commands += \n\t\t<meta-data android:name=\"com.google.android.gms.ads.APPLICATION_ID\" android:value=\"$${ADMOB_APPLICATION_ID}\"/>
		android_package.commands += \n
		android_package.commands += \n
		android_package.commands += \0^\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;

		android_package.commands += sed -i.bak \'s^\s*</manifest>^
		;# Attention: add permissions also into main.cpp!
		android_package.commands += \n\t<!-- Google play services library -->
		android_package.commands += \n\t<uses-permission android:name=\"android.permission.READ_EXTERNAL_STORAGE\"/>
		;#android_package.commands += \n\t<uses-permission android:name=\"android.permission.WRITE_EXTERNAL_STORAGE\"/>
		android_package.commands += \n\t<uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\"/>
		;#android_package.commands += \n\t<uses-permission android:name=\"android.permission.INTERNET\"/>
		android_package.commands += \n\t<!-- Google play services library -->
		android_package.commands += \n
		android_package.commands += \n
		android_package.commands += \0^\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;

		android_package.commands += echo MERGING JAVA-SOURCES;
		android_package.commands += find $${ANDROID_PACKAGE_SOURCE_DIR}/src -iname \"*.*.java\" | xargs $$PWD/mix_in_java_sources ;

		include($$PWD/.dependencies/qtadmob/QtAdMob.pri)
	}

	CONFIG(desktop_widget) {
		android_package.depends += $$PWD/Android/src/com/gehrmann/DesktopWidgetReceiver.java

		android_package.commands += echo \'===> Adding DesktopWidgetReceiver <===\';

		android_package.commands += sed -i.bak \'s^\s*</application>^
		android_package.commands += \n\t\t<!-- Desktop widget receiver -->
		android_package.commands += \n\t\t<receiver
		android_package.commands += \n\t\t\tandroid:icon=\"@drawable/icon\"
		android_package.commands += \n\t\t\tandroid:label=\"-- %%INSERT_APP_NAME%% --\"
		android_package.commands += \n\t\t\tandroid:name=\"com.gehrmann.DesktopWidgetReceiver\"
		android_package.commands += \n\t\t\tandroid:process=\":my_process\"
		android_package.commands += \n\t\t>
		android_package.commands += \n\t\t\t<intent-filter>
		android_package.commands += \n\t\t\t\t<action android:name=\"android.appwidget.action.APPWIDGET_UPDATE\" />
		android_package.commands += \n\t\t\t</intent-filter>
		android_package.commands += \n\t\t\t<meta-data
		android_package.commands += \n\t\t\t\tandroid:name=\"android.appwidget.provider\"
		android_package.commands += \n\t\t\t\tandroid:resource=\"@xml/desktop_widget_info\"
		android_package.commands += \n\t\t\t/>
		android_package.commands += \n\t\t</receiver>
		android_package.commands += \n\t\t\t\t
		android_package.commands += \n\t\t\t\t
		android_package.commands += \n\t\t\t\t
		android_package.commands += \n\t\t<!-- Desktop widget receiver -->
		android_package.commands += \n
		android_package.commands += \n
		android_package.commands += \0^\' $${ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml;

		android_package.commands += sed -i.bak \'s/__INSERT_PACKAGE_NAME_HERE__/$${PACKAGE}/\' $${ANDROID_PACKAGE_SOURCE_DIR}/src/com/gehrmann/DesktopWidgetReceiver.java;
	} else {
		android_package.commands += rm Android/src/com/gehrmann/DesktopWidgetReceiver.java;
	}

	android_package.commands += echo \'Contents of $@:\';
	android_package.commands += find $@;
	android_package.commands += echo Done;
	QMAKE_EXTRA_TARGETS += android_package
	PRE_TARGETDEPS += $$android_package.target
}

CONFIG(banner) {
	QtAdMobBanner.target = $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}QtAdMobBanner.so$${LIB_SUFFIX}
	QtAdMobBanner.depends += $$PWD/.dependencies/QtAdMobBanner/.build/.android-arm-5.11.2/QtAdMobBanner.so
	QtAdMobBanner.commands += mkdir -p lib;
	QtAdMobBanner.commands += rm -rf $@;
	QtAdMobBanner.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += QtAdMobBanner
	PRE_TARGETDEPS += $$QtAdMobBanner.target
	ANDROID_EXTRA_LIBS += $$QtAdMobBanner.target

	QtAdMobInterstitial.target = $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}QtAdMobInterstitial.so$${LIB_SUFFIX}
	QtAdMobInterstitial.depends += $$PWD/.dependencies/QtAdMobBanner/.build/.android-arm-5.11.2/QtAdMobInterstitial.so
	QtAdMobInterstitial.commands += mkdir -p lib;
	QtAdMobInterstitial.commands += rm -rf $@;
	QtAdMobInterstitial.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += QtAdMobInterstitial
	PRE_TARGETDEPS += $$QtAdMobInterstitial.target
	ANDROID_EXTRA_LIBS += $$QtAdMobInterstitial.target
}

android {
	QtJavaCaller.target = $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}QtJavaCaller.so
	QtJavaCaller.depends += $$PWD/.dependencies/QtJavaCaller/.build/.android-arm-5.11.2/QtJavaCaller.so
	QtJavaCaller.commands += mkdir -p lib;
	QtJavaCaller.commands += rm -rf $@;
	QtJavaCaller.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += QtJavaCaller
	PRE_TARGETDEPS += $$QtJavaCaller.target
	ANDROID_EXTRA_LIBS += $$QtJavaCaller.target

	AnimationWidgets.target = $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}AnimationWidgets.so
	AnimationWidgets.depends += $$PWD/.dependencies/QtJavaCaller/.build/.android-arm-5.11.2/AnimationWidgets.so
	AnimationWidgets.commands += mkdir -p lib;
	AnimationWidgets.commands += rm -rf $@;
	AnimationWidgets.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += AnimationWidgets
	PRE_TARGETDEPS += $$AnimationWidgets.target
	ANDROID_EXTRA_LIBS += $$AnimationWidgets.target

	libAnimationWidgets.target = $${LIB_DIRECTORY_PATH}libAnimationWidgets.so
	libAnimationWidgets.depends += $$PWD/.dependencies/QtJavaCaller/.build/.android-arm-5.11.2/libAnimationWidgets.so
	libAnimationWidgets.commands += mkdir -p lib;
	libAnimationWidgets.commands += rm -rf $@;
	libAnimationWidgets.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += libAnimationWidgets
	PRE_TARGETDEPS += $$libAnimationWidgets.target
	ANDROID_EXTRA_LIBS += $$libAnimationWidgets.target
}

# Pythons base
python.target = $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}.python27$${WRAP_SUFFIX}$${LIB_SUFFIX}
python.depends += $${PYTHON_ASSETS}/python27.zip
python.commands += mkdir -p lib;
python.commands += rm -rf $@;
python.commands += cp $^ $@;
python.commands += $$PWD/wrap_to_elf_container $@ $${WRAP_LENGTH};
QMAKE_EXTRA_TARGETS += python
PRE_TARGETDEPS += $$python.target
ANDROID_EXTRA_LIBS += $$python.target

_Makefile.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}.Makefile$${WRAP_SUFFIX}$${LIB_SUFFIX}
_Makefile.depends += $${PYTHON_ASSETS}/.Makefile
_Makefile.commands = mkdir -p $(dir $@);
_Makefile.commands += rm -rf $@;
_Makefile.commands += cp $^ $@;
_Makefile.commands += $$PWD/wrap_to_elf_container $@ $${WRAP_LENGTH};
QMAKE_EXTRA_TARGETS += _Makefile
PRE_TARGETDEPS += $$_Makefile.target
ANDROID_EXTRA_LIBS += $$_Makefile.target

_pyconfig_h.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}.pyconfig.h$${WRAP_SUFFIX}$${LIB_SUFFIX}
_pyconfig_h.depends += $${PYTHON_ASSETS}/.pyconfig.h
_pyconfig_h.commands = mkdir -p $(dir $@);
_pyconfig_h.commands += rm -rf $@;
_pyconfig_h.commands += cp $^ $@;
_pyconfig_h.commands += $$PWD/wrap_to_elf_container $@ $${WRAP_LENGTH};
QMAKE_EXTRA_TARGETS += _pyconfig_h
PRE_TARGETDEPS += $$_pyconfig_h.target
ANDROID_EXTRA_LIBS += $$_pyconfig_h.target

# Pythons modules
_collectionsmodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_collectionsmodule.so$${LIB_SUFFIX}
_collectionsmodule.depends += $${PYTHON_ROOT}/Modules/_collectionsmodule.so
_collectionsmodule.commands = mkdir -p $(dir $@);
_collectionsmodule.commands += rm -rf $@;
_collectionsmodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _collectionsmodule
PRE_TARGETDEPS += $$_collectionsmodule.target
ANDROID_EXTRA_LIBS += $$_collectionsmodule.target

_ctypes.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_ctypes.so$${LIB_SUFFIX}
_ctypes.depends += $${PYTHON_LIBS}/_ctypes.so
_ctypes.commands = mkdir -p $(dir $@);
_ctypes.commands += rm -rf $@;
_ctypes.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _ctypes
PRE_TARGETDEPS += $$_ctypes.target
ANDROID_EXTRA_LIBS += $$_ctypes.target

_elementtree.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_elementtree.so$${LIB_SUFFIX}
_elementtree.depends += $${PYTHON_ROOT}/Modules/_elementtree.so
_elementtree.commands = mkdir -p $(dir $@);
_elementtree.commands += rm -rf $@;
_elementtree.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _elementtree
PRE_TARGETDEPS += $$_elementtree.target
ANDROID_EXTRA_LIBS += $$_elementtree.target

_functoolsmodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_functoolsmodule.so$${LIB_SUFFIX}
_functoolsmodule.depends += $${PYTHON_ROOT}/Modules/_functoolsmodule.so
_functoolsmodule.commands = mkdir -p $(dir $@);
_functoolsmodule.commands += rm -rf $@;
_functoolsmodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _functoolsmodule
PRE_TARGETDEPS += $$_functoolsmodule.target
ANDROID_EXTRA_LIBS += $$_functoolsmodule.target

# _hashlib.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_hashlib.so$${LIB_SUFFIX}
# _hashlib.depends += $${PYTHON_LIBS}/_hashlib.so
# _hashlib.commands = mkdir -p $(dir $@);
# _hashlib.commands += rm -rf $@;
# _hashlib.commands += cp $^ $@;
# QMAKE_EXTRA_TARGETS += _hashlib
# PRE_TARGETDEPS += $$_hashlib.target
# ANDROID_EXTRA_LIBS += $$_hashlib.target

_io.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_io.so$${LIB_SUFFIX}
_io.depends += $${PYTHON_LIBS}/_io.so
_io.commands = mkdir -p $(dir $@);
_io.commands += rm -rf $@;
_io.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _io
PRE_TARGETDEPS += $$_io.target
ANDROID_EXTRA_LIBS += $$_io.target

_lsprof.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_lsprof.so$${LIB_SUFFIX}
_lsprof.depends += $${PYTHON_LIBS}/_lsprof.so
_lsprof.commands = mkdir -p $(dir $@);
_lsprof.commands += rm -rf $@;
_lsprof.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _lsprof
PRE_TARGETDEPS += $$_lsprof.target
ANDROID_EXTRA_LIBS += $$_lsprof.target

_md5module.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_md5module.so$${LIB_SUFFIX}
_md5module.depends += $${PYTHON_ROOT}/Modules/_md5module.so
_md5module.commands = mkdir -p $(dir $@);
_md5module.commands += rm -rf $@;
_md5module.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _md5module
PRE_TARGETDEPS += $$_md5module.target
ANDROID_EXTRA_LIBS += $$_md5module.target

_multiprocessing.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_multiprocessing.so$${LIB_SUFFIX}
_multiprocessing.depends += $${PYTHON_LIBS}/_multiprocessing.so
_multiprocessing.commands = mkdir -p $(dir $@);
_multiprocessing.commands += rm -rf $@;
_multiprocessing.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _multiprocessing
PRE_TARGETDEPS += $$_multiprocessing.target
ANDROID_EXTRA_LIBS += $$_multiprocessing.target

_randommodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_randommodule.so$${LIB_SUFFIX}
_randommodule.depends += $${PYTHON_ROOT}/Modules/_randommodule.so
_randommodule.commands = mkdir -p $(dir $@);
_randommodule.commands += rm -rf $@;
_randommodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _randommodule
PRE_TARGETDEPS += $$_randommodule.target
ANDROID_EXTRA_LIBS += $$_randommodule.target

_shamodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_shamodule.so$${LIB_SUFFIX}
_shamodule.depends += $${PYTHON_ROOT}/Modules/_shamodule.so
_shamodule.commands = mkdir -p $(dir $@);
_shamodule.commands += rm -rf $@;
_shamodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _shamodule
PRE_TARGETDEPS += $$_shamodule.target
ANDROID_EXTRA_LIBS += $$_shamodule.target

_sha256module.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_sha256module.so$${LIB_SUFFIX}
_sha256module.depends += $${PYTHON_ROOT}/Modules/_sha256module.so
_sha256module.commands = mkdir -p $(dir $@);
_sha256module.commands += rm -rf $@;
_sha256module.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _sha256module
PRE_TARGETDEPS += $$_sha256module.target
ANDROID_EXTRA_LIBS += $$_sha256module.target

_sha512module.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_sha512module.so$${LIB_SUFFIX}
_sha512module.depends += $${PYTHON_ROOT}/Modules/_sha512module.so
_sha512module.commands = mkdir -p $(dir $@);
_sha512module.commands += rm -rf $@;
_sha512module.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _sha512module
PRE_TARGETDEPS += $$_sha512module.target
ANDROID_EXTRA_LIBS += $$_sha512module.target

_socketmodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_socketmodule.so$${LIB_SUFFIX}
_socketmodule.depends += $${PYTHON_ROOT}/Modules/_socketmodule.so
_socketmodule.commands = mkdir -p $(dir $@);
_socketmodule.commands += rm -rf $@;
_socketmodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _socketmodule
PRE_TARGETDEPS += $$_socketmodule.target
ANDROID_EXTRA_LIBS += $$_socketmodule.target

# _sqlite3.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_sqlite3.so$${LIB_SUFFIX}
# _sqlite3.depends += $${PYTHON_ROOT}/Modules/_sqlite3.so
# _sqlite3.commands = mkdir -p $(dir $@);
# _sqlite3.commands += rm -rf $@;
# _sqlite3.commands += cp $^ $@;
# QMAKE_EXTRA_TARGETS += _sqlite3
# PRE_TARGETDEPS += $$_sqlite3.target
# ANDROID_EXTRA_LIBS += $$_sqlite3.target

_struct.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_struct.so$${LIB_SUFFIX}
_struct.depends += $${PYTHON_ROOT}/Modules/_struct.so
_struct.commands = mkdir -p $(dir $@);
_struct.commands += rm -rf $@;
_struct.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _struct
PRE_TARGETDEPS += $$_struct.target
ANDROID_EXTRA_LIBS += $$_struct.target

arraymodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}arraymodule.so$${LIB_SUFFIX}
arraymodule.depends += $${PYTHON_ROOT}/Modules/arraymodule.so
arraymodule.commands = mkdir -p $(dir $@);
arraymodule.commands += rm -rf $@;
arraymodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += arraymodule
PRE_TARGETDEPS += $$arraymodule.target
ANDROID_EXTRA_LIBS += $$arraymodule.target

binascii.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}binascii.so$${LIB_SUFFIX}
binascii.depends += $${PYTHON_ROOT}/Modules/binascii.so
binascii.commands = mkdir -p $(dir $@);
binascii.commands += rm -rf $@;
binascii.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += binascii
PRE_TARGETDEPS += $$binascii.target
ANDROID_EXTRA_LIBS += $$binascii.target

cPickle.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}cPickle.so$${LIB_SUFFIX}
cPickle.depends += $${PYTHON_ROOT}/Modules/cPickle.so
cPickle.commands = mkdir -p $(dir $@);
cPickle.commands += rm -rf $@;
cPickle.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += cPickle
PRE_TARGETDEPS += $$cPickle.target
ANDROID_EXTRA_LIBS += $$cPickle.target

cStringIO.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}cStringIO.so$${LIB_SUFFIX}
cStringIO.depends += $${PYTHON_ROOT}/Modules/cStringIO.so
cStringIO.commands = mkdir -p $(dir $@);
cStringIO.commands += rm -rf $@;
cStringIO.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += cStringIO
PRE_TARGETDEPS += $$cStringIO.target
ANDROID_EXTRA_LIBS += $$cStringIO.target

datetimemodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}datetimemodule.so$${LIB_SUFFIX}
datetimemodule.depends += $${PYTHON_ROOT}/Modules/datetimemodule.so
datetimemodule.commands = mkdir -p $(dir $@);
datetimemodule.commands += rm -rf $@;
datetimemodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += datetimemodule
PRE_TARGETDEPS += $$datetimemodule.target
ANDROID_EXTRA_LIBS += $$datetimemodule.target

fcntl.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}fcntl.so$${LIB_SUFFIX}
fcntl.depends += $${PYTHON_LIBS}/fcntl.so
fcntl.commands = mkdir -p $(dir $@);
fcntl.commands += rm -rf $@;
fcntl.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += fcntl
PRE_TARGETDEPS += $$fcntl.target
ANDROID_EXTRA_LIBS += $$fcntl.target

future_builtins.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}future_builtins.so$${LIB_SUFFIX}
future_builtins.depends += $${PYTHON_LIBS}/future_builtins.so
future_builtins.commands = mkdir -p $(dir $@);
future_builtins.commands += rm -rf $@;
future_builtins.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += future_builtins
PRE_TARGETDEPS += $$future_builtins.target
ANDROID_EXTRA_LIBS += $$future_builtins.target

itertoolsmodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}itertoolsmodule.so$${LIB_SUFFIX}
itertoolsmodule.depends += $${PYTHON_ROOT}/Modules/itertoolsmodule.so
itertoolsmodule.commands = mkdir -p $(dir $@);
itertoolsmodule.commands += rm -rf $@;
itertoolsmodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += itertoolsmodule
PRE_TARGETDEPS += $$itertoolsmodule.target
ANDROID_EXTRA_LIBS += $$itertoolsmodule.target

math.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}math.so$${LIB_SUFFIX}
math.depends += $${PYTHON_ROOT}/Modules/math.so
math.commands = mkdir -p $(dir $@);
math.commands += rm -rf $@;
math.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += math
PRE_TARGETDEPS += $$math.target
ANDROID_EXTRA_LIBS += $$math.target

mmap.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}mmap.so$${LIB_SUFFIX}
mmap.depends += $${PYTHON_LIBS}/mmap.so
mmap.commands = mkdir -p $(dir $@);
mmap.commands += rm -rf $@;
mmap.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += mmap
PRE_TARGETDEPS += $$mmap.target
ANDROID_EXTRA_LIBS += $$mmap.target

operator.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}operator.so$${LIB_SUFFIX}
operator.depends += $${PYTHON_ROOT}/Modules/operator.so
operator.commands = mkdir -p $(dir $@);
operator.commands += rm -rf $@;
operator.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += operator
PRE_TARGETDEPS += $$operator.target
ANDROID_EXTRA_LIBS += $$operator.target

parsermodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}parsermodule.so$${LIB_SUFFIX}
parsermodule.depends += $${PYTHON_ROOT}/Modules/parsermodule.so
parsermodule.commands = mkdir -p $(dir $@);
parsermodule.commands += rm -rf $@;
parsermodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += parsermodule
PRE_TARGETDEPS += $$parsermodule.target
ANDROID_EXTRA_LIBS += $$parsermodule.target

pyexpat.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}pyexpat.so$${LIB_SUFFIX}
pyexpat.depends += $${PYTHON_ROOT}/Modules/pyexpat.so
pyexpat.commands = mkdir -p $(dir $@);
pyexpat.commands += rm -rf $@;
pyexpat.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += pyexpat
PRE_TARGETDEPS += $$pyexpat.target
ANDROID_EXTRA_LIBS += $$pyexpat.target

selectmodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}selectmodule.so$${LIB_SUFFIX}
selectmodule.depends += $${PYTHON_ROOT}/Modules/selectmodule.so
selectmodule.commands = mkdir -p $(dir $@);
selectmodule.commands += rm -rf $@;
selectmodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += selectmodule
PRE_TARGETDEPS += $$selectmodule.target
ANDROID_EXTRA_LIBS += $$selectmodule.target

timemodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}timemodule.so$${LIB_SUFFIX}
timemodule.depends += $${PYTHON_ROOT}/Modules/timemodule.so
timemodule.commands = mkdir -p $(dir $@);
timemodule.commands += rm -rf $@;
timemodule.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += timemodule
PRE_TARGETDEPS += $$timemodule.target
ANDROID_EXTRA_LIBS += $$timemodule.target

unicodedata.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}unicodedata.so$${LIB_SUFFIX}
unicodedata.depends += $${PYTHON_ROOT}/Modules/unicodedata.so
unicodedata.commands = mkdir -p $(dir $@);
unicodedata.commands += rm -rf $@;
unicodedata.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += unicodedata
PRE_TARGETDEPS += $$unicodedata.target
ANDROID_EXTRA_LIBS += $$unicodedata.target

# zlibmodule.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}zlibmodule.so$${LIB_SUFFIX}
# zlibmodule.depends += $${PYTHON_ROOT}/Modules/zlibmodule.so
# zlibmodule.commands = mkdir -p $(dir $@);
# zlibmodule.commands += rm -rf $@;
# zlibmodule.commands += cp $^ $@;
# QMAKE_EXTRA_TARGETS += zlibmodule
# PRE_TARGETDEPS += $$zlibmodule.target
# ANDROID_EXTRA_LIBS += $$zlibmodule.target

_ssl.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_ssl.so$${LIB_SUFFIX}
_ssl.depends += $${PYTHON_ROOT}/Modules/_ssl.so
_ssl.commands = mkdir -p $(dir $@);
_ssl.commands += rm -rf $@;
_ssl.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += _ssl
PRE_TARGETDEPS += $$_ssl.target
ANDROID_EXTRA_LIBS += $$_ssl.target

android {
	# PIL _imaging
	_imaging.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_imaging.so$${LIB_SUFFIX}
	_imaging.depends += $${PIL_ROOT}/_imaging.so
	_imaging.commands = mkdir -p $(dir $@);
	_imaging.commands += rm -rf $@;
	_imaging.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _imaging
	PRE_TARGETDEPS += $$_imaging.target
	ANDROID_EXTRA_LIBS += $$_imaging.target

	_imagingft.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}_imagingft.so$${LIB_SUFFIX}
	_imagingft.depends += $${PIL_ROOT}/_imagingft.so
	_imagingft.commands = mkdir -p $(dir $@);
	_imagingft.commands += rm -rf $@;
	_imagingft.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _imagingft
	PRE_TARGETDEPS += $$_imagingft.target
	ANDROID_EXTRA_LIBS += $$_imagingft.target
}

pyqt5_sip.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}PyQt5$${LIB_SEPARATOR}sip.so$${LIB_SUFFIX}
pyqt5_sip.depends += $${PYQT5_ROOT}/PyQt5/sip.so
pyqt5_sip.commands = mkdir -p $(dir $@);
pyqt5_sip.commands += rm -rf $@;
pyqt5_sip.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += pyqt5_sip
PRE_TARGETDEPS += $$pyqt5_sip.target
ANDROID_EXTRA_LIBS += $$pyqt5_sip.target

pyqt5_init.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}PyQt5$${LIB_SEPARATOR}__init__.py$${WRAP_SUFFIX}$${LIB_SUFFIX}
pyqt5_init.depends += $${PYQT5_ROOT}/PyQt5/__init__.py
pyqt5_init.commands = mkdir -p $(dir $@);
pyqt5_init.commands += rm -rf $@;
pyqt5_init.commands += cp $^ $@;
pyqt5_init.commands += $$PWD/wrap_to_elf_container $@ $${WRAP_LENGTH};
QMAKE_EXTRA_TARGETS += pyqt5_init
PRE_TARGETDEPS += $$pyqt5_init.target
ANDROID_EXTRA_LIBS += $$pyqt5_init.target

pyqt5_uic_init.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}PyQt5$${LIB_SEPARATOR}uic$${LIB_SEPARATOR}__init__.py$${WRAP_SUFFIX}$${LIB_SUFFIX}
pyqt5_uic_init.depends += $${PYQT5_ROOT}/../../patches/PyQt5_uic___init__.py
pyqt5_uic_init.commands = mkdir -p $(dir $@);
pyqt5_uic_init.commands += rm -rf $@;
pyqt5_uic_init.commands += cp $^ $@;
pyqt5_uic_init.commands += $$PWD/wrap_to_elf_container $@ $${WRAP_LENGTH};
QMAKE_EXTRA_TARGETS += pyqt5_uic_init
PRE_TARGETDEPS += $$pyqt5_uic_init.target
ANDROID_EXTRA_LIBS += $$pyqt5_uic_init.target

;linux:!android {
;pyqt5_uic.target += PyQt5/uic
;pyqt5_uic.depends += $${PYQT5_ROOT}/PyQt5/uic
;pyqt5_uic.commands = mkdir -p $(dir $@);
;pyqt5_uic.commands += rm -rf $@;
;pyqt5_uic.commands += cp -r $^ $@;
;QMAKE_EXTRA_TARGETS += pyqt5_uic
;PRE_TARGETDEPS += $$pyqt5_uic.target
;ANDROID_EXTRA_LIBS += $$pyqt5_uic.target
;}

pyqt5_qt.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}PyQt5$${LIB_SEPARATOR}Qt.so$${LIB_SUFFIX}
pyqt5_qt.depends += $${PYQT5_ROOT}/PyQt5/Qt.so
pyqt5_qt.commands = mkdir -p $(dir $@);
pyqt5_qt.commands += rm -rf $@;
pyqt5_qt.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += pyqt5_qt
PRE_TARGETDEPS += $$pyqt5_qt.target
ANDROID_EXTRA_LIBS += $$pyqt5_qt.target

pyqt5_qtcore.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}PyQt5$${LIB_SEPARATOR}QtCore.so$${LIB_SUFFIX}
pyqt5_qtcore.depends += $${PYQT5_ROOT}/PyQt5/QtCore.so
pyqt5_qtcore.commands = mkdir -p $(dir $@);
pyqt5_qtcore.commands += rm -rf $@;
pyqt5_qtcore.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += pyqt5_qtcore
PRE_TARGETDEPS += $$pyqt5_qtcore.target
ANDROID_EXTRA_LIBS += $$pyqt5_qtcore.target

pyqt5_qtwidgets.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}PyQt5$${LIB_SEPARATOR}QtWidgets.so$${LIB_SUFFIX}
pyqt5_qtwidgets.depends += $${PYQT5_ROOT}/PyQt5/QtWidgets.so
pyqt5_qtwidgets.commands = mkdir -p $(dir $@);
pyqt5_qtwidgets.commands += rm -rf $@;
pyqt5_qtwidgets.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += pyqt5_qtwidgets
PRE_TARGETDEPS += $$pyqt5_qtwidgets.target
ANDROID_EXTRA_LIBS += $$pyqt5_qtwidgets.target

;pyqt5_qtsvg.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}PyQt5$${LIB_SEPARATOR}QtSvg.so$${LIB_SUFFIX}
;pyqt5_qtsvg.depends += $${PYQT5_ROOT}/PyQt5/QtSvg.so
;pyqt5_qtsvg.commands = mkdir -p $(dir $@);
;pyqt5_qtsvg.commands += rm -rf $@;
;pyqt5_qtsvg.commands += cp $^ $@;
;QMAKE_EXTRA_TARGETS += pyqt5_qtsvg
;PRE_TARGETDEPS += $$pyqt5_qtsvg.target
;ANDROID_EXTRA_LIBS += $$pyqt5_qtsvg.target

;pyqt5_qtbluetooth.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}PyQt5$${LIB_SEPARATOR}QtBluetooth.so$${LIB_SUFFIX}
;pyqt5_qtbluetooth.depends += $${PYQT5_ROOT}/PyQt5/QtBluetooth.so
;pyqt5_qtbluetooth.commands = mkdir -p $(dir $@);
;pyqt5_qtbluetooth.commands += rm -rf $@;
;pyqt5_qtbluetooth.commands += cp $^ $@;
;QMAKE_EXTRA_TARGETS += pyqt5_qtbluetooth
;PRE_TARGETDEPS += $$pyqt5_qtbluetooth.target
;ANDROID_EXTRA_LIBS += $$pyqt5_qtbluetooth.target

pyqt5_qtgui.target += $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}PyQt5$${LIB_SEPARATOR}QtGui.so$${LIB_SUFFIX}
pyqt5_qtgui.depends += $${PYQT5_ROOT}/PyQt5/QtGui.so
pyqt5_qtgui.commands = mkdir -p $(dir $@);
pyqt5_qtgui.commands += rm -rf $@;
pyqt5_qtgui.commands += cp $^ $@;
QMAKE_EXTRA_TARGETS += pyqt5_qtgui
PRE_TARGETDEPS += $$pyqt5_qtgui.target
ANDROID_EXTRA_LIBS += $$pyqt5_qtgui.target

app.target = $${LIB_DIRECTORY_PATH}$${LIB_PREFIX}.app$${WRAP_SUFFIX}$${LIB_SUFFIX}
app.depends += $$PWD/PyQt/*.py
app.depends += $$PWD/controllers/*.py
app.depends += $$PWD/models/*.py
app.depends += $$PWD/helpers/*.py
app.depends += $$PWD/views/*.ui
app.depends += $$PWD/widgets/*.py
app.depends += $$PWD/images/*.png
;app.depends += $$PWD/images_rc.py
app.depends += $$PWD/styles.py
app.depends += $$PWD/changes.py
app.commands += echo \'==============================\';
app.commands += echo Building $$app.target;
app.commands += rm -rf lib/app $$app.target;
app.commands += mkdir -p lib/app;
app.commands += cp -R $$PWD/PyQt lib/app;
app.commands += cp -R $$PWD/controllers lib/app;
app.commands += cp -R $$PWD/models lib/app;
app.commands += cp -R $$PWD/helpers lib/app;
app.commands += cp -R $$PWD/widgets lib/app;
app.commands += 
app.commands += mkdir lib/app/views && cp -R $$PWD/views/*.ui lib/app/views;
app.commands += touch lib/app/views/__init__.py;
app.commands += find lib/app/views -type f -name \"*.ui\" | xargs -I{} bash -c \'PYTHONPATH=\"$${PYQT5_HOST_ROOT}\" python -m PyQt5.uic.pyuic \$\$1 > \$\${1%.ui}.py\' -- {};
app.commands += rm -rf lib/app/views/*.ui;
app.commands += 
app.commands += $$PWD/images-update.sh;
app.commands += cp -R $$PWD/images_rc.py lib/app;
app.commands += 
app.commands += cp -R $$PWD/styles.py lib/app;
app.commands += cp -R $$PWD/changes.py lib/app;
app.commands += python -m compileall lib/app;
app.commands += find lib/app -type f -name '*.py' -o -name "*.pyo" -o -name "*.pyx" | xargs rm -rf;
app.commands += cd lib/app && zip -r ../app.zip . && cd ../..;
app.commands += rm -rf lib/app;
app.commands += mv lib/app.zip $@;
app.commands += $$PWD/wrap_to_elf_container $@ $${WRAP_LENGTH};
QMAKE_EXTRA_TARGETS += app
PRE_TARGETDEPS += $$app.target
ANDROID_EXTRA_LIBS += $$app.target
