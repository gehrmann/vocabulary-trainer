#ifndef MAIN_H
#	define MAIN_H

#	include <dirent.h>
#	include <stdbool.h>  // Provides true, false
#	include <errno.h>


#if defined(__ANDROID__)
#elif defined(__linux__)
#	include <libgen.h>
#elif defined(_WIN32)
#endif

#define MIN(a, b) (((a) < (b))? (a): (b))

//#	include <QTextStream>
#	include <QDebug>

#ifdef DEBUG
#	if defined(__ANDROID__)
	// #	define LOG(...) do { time(&__timer); __time = localtime(&__timer); struct timeval __tv; gettimeofday(&__tv, NULL); strftime(__time_buffer, 25, "%H:%M:%S", __time); fprintf(stderr, "%s.%d Native C:", __time_buffer, (int)__tv.tv_usec); fprintf(stderr, __VA_ARGS__), fprintf(stderr, "\n"); qDebug("Native C: " __VA_ARGS__); } while (0)
	// #	define LOG(...) do { time(&__timer); __time = localtime(&__timer); struct timeval __tv; gettimeofday(&__tv, NULL); strftime(__time_buffer, 25, "%H:%M:%S", __time); fprintf(stderr, "%s.%d Native C:", __time_buffer, (int)__tv.tv_usec); fprintf(stderr, __VA_ARGS__), fprintf(stderr, "\n"); qDebug().nospace() << __time_buffer << "." << (int)__tv.tv_usec << "Native C: " << __VA_ARGS__; } while (0)
	// #	define LOG(...) do { time(&__timer); __time = localtime(&__timer); struct timeval __tv; gettimeofday(&__tv, NULL); strftime(__time_buffer, 25, "%H:%M:%S", __time); fprintf(stderr, "%s.%d Native C:", __time_buffer, (int)__tv.tv_usec); fprintf(stderr, __VA_ARGS__), fprintf(stderr, "\n"); setenv("QT_MESSAGE_PATTERN", "[%{time yyyyMMdd h:mm:ss.zzz t} %{if-debug}D%{endif}%{if-info}I%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif}] %{file}:%{line} - %{message}", [>rewrite<]0); qDebug("Native C: " __VA_ARGS__); } while (0)
	// #	define LOG(...) do { time(&__timer); __time = localtime(&__timer); struct timeval __tv; gettimeofday(&__tv, NULL); strftime(__time_buffer, 25, "%H:%M:%S", __time); fprintf(stderr, "%s.%03d Native C: ", __time_buffer, ((int)__tv.tv_usec) / 1000); fprintf(stderr, __VA_ARGS__), fprintf(stderr, "\n"); setenv("QT_MESSAGE_PATTERN", "%{time h:mm:ss.zzz} %{if-debug}D%{endif}%{if-info}I%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif} %{message}", [>rewrite<]0); qDebug("Native C: " __VA_ARGS__); } while (0)
#		define LOG(...) do { time(&__timer); __time = localtime(&__timer); struct timeval __tv; gettimeofday(&__tv, NULL); strftime(__time_buffer, 25, "%H:%M:%S", __time); fprintf(stderr, "%s.%03d  ", __time_buffer, ((int)__tv.tv_usec) / 1000); fprintf(stderr, __VA_ARGS__), fprintf(stderr, "\n"); setenv("QT_MESSAGE_PATTERN", "%{time h:mm:ss.zzz} %{if-debug}D%{endif}%{if-info}I%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif} %{message}", /*rewrite*/0); qDebug("" __VA_ARGS__); } while (0)
	// #elif defined(__linux__)
#	else
#		define LOG(...) do { time(&__timer); __time = localtime(&__timer); struct timeval __tv; gettimeofday(&__tv, NULL); strftime(__time_buffer, 25, "%H:%M:%S", __time); fprintf(stderr, "%s.%03d Native C: ", __time_buffer, ((int)__tv.tv_usec) / 1000); fprintf(stderr, __VA_ARGS__), fprintf(stderr, "\n"); } while (0)
#	endif
//#	define LOG(...) qDebug("Native C: " __VA_ARGS__);
#else
#	define LOG(...)
#endif

#	define PY_SSIZE_T_CLEAN
#	include <Python.h>
#	ifndef Py_PYTHON_H
#		error Python headers needed to compile C extensions, please install development version of Python.
#	endif


// #	include <QObject>
// class Invoker: public QObject
// {
//     Q_OBJECT
//     public:
//         Invoker(QObject *parent=0);
//         // QPushButton *btn=new QPushButton("Browse");
//     public slots:
//         void eval();
// };

// Invoker invoker(NULL);


// int javaeval(const char* cls, const char* method, const char* types, char* serialized_kwargs);
char* javaeval(const char* cls, const char* method, char* serialized_kwargs);


#endif // MAIN_H
