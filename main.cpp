// Available ENV:
//	ANDROID_ASSETS => /system/app
//	ANDROID_BOOTLOGO => 1
//	ANDROID_DATA => /data
//	ANDROID_PROPERTY_WORKSPACE => 8,66560
//	ANDROID_ROOT => /system
//	ANDROID_SOCKET_zygote => 9
//	APK_PATH => /data/app/gehrmann.vocabulary.trainer.debug.banner-1.apk
//	APPLICATION_BASENAME => Vocabulary Trainer
//	APPLICATION_NAME => Vocabulary Trainer (debug) (banner)
//	APPLICATION_VERSION => 2.00
//	APPLICATION_VERSION_CODE => 200
//	ASEC_MOUNTPOINT => /mnt/asec
//	BOOTCLASSPATH => /system/framework/core.jar:/system/framework/core-junit.jar:/system/framework/bouncycastle.jar:/system/framework/ext.jar:/system/framework/framework.jar:/system/framework/framework2.jar:/system/framework/android.policy.jar:/system/framework/services.jar:/system/framework/apache-xml.jar:/system/framework/sec_edm.jar:/system/framework/seccamera.jar
//	EXTERNAL_STORAGE => /storage/sdcard0
//	HOME => /data/data/gehrmann.vocabulary.trainer.debug.banner/files
//	INSTALL_REFERRER => 
//	LD_LIBRARY_PATH => /system/lib:/lib:/usr/lib:/system/lib/ste_omxcomponents/:/vendor/lib
//	LOOP_MOUNTPOINT => /mnt/obb
//	MINISTRO_ANDROID_STYLE_PATH => /data/data/gehrmann.vocabulary.trainer.debug.banner/qt-reserved-files/android-style/240/
//	NECESSITAS_API_LEVEL => 2
//	OMX_BELLAGIO_LOADER_REGISTRY => /system/omxloaders
//	OMX_STE_ENS_COMPONENTS_DIR => /system/lib/ste_omxcomponents/
//	PACKAGE_NAME => gehrmann.vocabulary.trainer.debug.banner
//	PATH => /system/sbin:/system/bin:/system/xbin:/sbin:/bin:/vendor/bin
//	PYTHONHOME => /data/data/gehrmann.vocabulary.trainer.debug.banner/files/python27
//	PYTHONPATH => /data/data/gehrmann.vocabulary.trainer.debug.banner/files/app:/data/data/gehrmann.vocabulary.trainer.debug.banner/files:/data/data/gehrmann.vocabulary.trainer.debug.banner/files/python27:/data/data/gehrmann.vocabulary.trainer.debug.banner/files/python27/site-packages
//	QML2_IMPORT_PATH => /data/data/gehrmann.vocabulary.trainer.debug.banner/qt-reserved-files//qml
//	QML_IMPORT_PATH => /data/data/gehrmann.vocabulary.trainer.debug.banner/qt-reserved-files//imports
//	QT_ANDROID_APP_ICON_SIZE => 72
//	QT_ANDROID_FONTS => Roboto;Droid Sans;Droid Sans Fallback
//	QT_ANDROID_FONTS_MONOSPACE => Droid Sans Mono;Droid Sans;Droid Sans Fallback
//	QT_ANDROID_FONTS_SERIF => Droid Serif
//	QT_ANDROID_THEME => Theme_DeviceDefault_Light/
//	QT_ANDROID_THEMES_ROOT_PATH => /data/data/gehrmann.vocabulary.trainer.debug.banner/qt-reserved-files/android-style/
//	QT_ANDROID_THEME_DISPLAY_DPI => 240
//	QT_BLOCK_EVENT_LOOPS_WHEN_SUSPENDED => 1
//	QT_PLUGIN_PATH => /data/data/gehrmann.vocabulary.trainer.debug.banner/qt-reserved-files//plugins
//	QT_USE_ANDROID_NATIVE_DIALOGS => 1
//	QT_USE_ANDROID_NATIVE_STYLE => 1
//	SECONDARY_STORAGE => /storage/extSdCard:/storage/UsbDriveA:/storage/UsbDriveB:/storage/UsbDriveC:/storage/UsbDriveD:/storage/UsbDriveE:/storage/UsbDriveF
//	TMPDIR => /data/data/gehrmann.vocabulary.trainer.debug.banner/files
//	USER_AGENT => Dalvik/1.6.0 (Linux; U; Android 4.1.2; GT-I8190 Build/JZO54K)

// #if defined(Q_OS_ANDROID)
// #elif defined(Q_OS_BLACKBERRY)
// #elif defined(Q_OS_IOS)
// #elif defined(Q_OS_MAC)
// #elif defined(Q_OS_WINCE)
// #elif defined(Q_OS_WIN)
// #elif defined(Q_OS_LINUX)
// #elif defined(Q_OS_UNIX)
// #else
// #endif

#include "main.h"

time_t __timer;
struct tm* __time;
char __time_buffer[25];


#include <QDir>
#include <QFileInfo>
#if defined(__ANDROID__)
#	include <QtAndroid>
#endif

void
_redirect_stdout_stderr(QString directory_path, QString filename, const char* flags) {
	// Redirects stdout and stderr and changes current working directory

#if defined(__ANDROID__)
	// Asks for a permission to write into external storage


    //     PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS);
    //     if (info.requestedPermissions != null) {
    //         for (String p : info.requestedPermissions) {
    //             if (p.equals(permission)) {
    //                 return true;
    //             }
    //         }
    //     }


	// public static String[] retrievePermissions(Context context) {
	//     try {
	//         return context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS).requestedPermissions;
	//     } catch (PackageManager.NameNotFoundException e) {
	//         throw new RuntimeException ("This should have never happened.", e);
	//     }
	// }


	const QVector<QString> permissions({
			"android.permission.READ_EXTERNAL_STORAGE",
			"android.permission.WRITE_EXTERNAL_STORAGE",
			"android.permission.ACCESS_NETWORK_STATE",
			"android.permission.INTERNET",
			});
	for (const QString& permission: permissions) {
		auto result = QtAndroid::checkPermission(permission);
		if (result == QtAndroid::PermissionResult::Denied) {
			auto result = QtAndroid::requestPermissionsSync(QStringList({permission}));
			if (result[permission] == QtAndroid::PermissionResult::Denied) {
				LOG("Access to permission '%s' is DENIED", permission.toLatin1().data());
			}
		}
	}
#endif

	// Checks if path writable
	QDir parent_directory(QFileInfo(directory_path).dir().absolutePath());
	// LOG("Checking directory '%s'...", parent_directory.path().toLatin1().data());
	LOG("Checking directory '%s'...", parent_directory.absolutePath().toLatin1().data());
	if (!QFileInfo(parent_directory.absolutePath()).isDir()) {
		// LOG("Parent directory '%s' NOT EXISTS!", parent_directory.path().toLatin1().data());
		LOG("Parent directory '%s' NOT EXISTS!", parent_directory.absolutePath().toLatin1().data());
	} else if (!QFileInfo(parent_directory.absolutePath()).isWritable()) {
		// LOG("Parent directory '%s' IS NOT WRITABLE!", parent_directory.path().toLatin1().data());
		LOG("Parent directory '%s' IS NOT WRITABLE!", parent_directory.absolutePath().toLatin1().data());
	}

	// mkdir(directory_path.toLatin1().data(), 0700);
	LOG("Creating directory '%s'...", directory_path.toLatin1().data());
	QDir().mkpath(directory_path);
	// if (!QDir().mkdir(directory_path)) {
	//     LOG("Directory '%s' IS NOT CREATED! Maybe already exists?", directory_path.toLatin1().data());
	// }

	// Checks if directory was created
	if (!QDir(directory_path).exists()) {
		LOG("Directory '%s' NOT EXISTS!", directory_path.toLatin1().data());
	}

	// Changes current working directory
	chdir(directory_path.toLatin1().data());

	LOG("Redirecting stdout/stderr into: %s/%s...", directory_path.toLatin1().data(), filename.toLatin1().data());
	{
		freopen(filename.toLatin1().data(), flags, stdout);
		dup2(fileno(stdout), fileno(stderr));
	}
	LOG("--------------------------------------------------------------------------------");
}

#include <QDir>
#include <QFileInfo>
#include <QFileInfoList>
#include <QDateTime>

void
_list(QString path, int recursive, int level) {
	QDir directory(path);
	QFileInfoList entries = directory.entryInfoList();
	foreach(QFileInfo entry, entries) {
		LOG("# %10lld %s%s%s", entry.lastModified().toSecsSinceEpoch(), (path + (path.endsWith("/")? "": "/")).toLatin1().data(), entry.fileName().toLatin1().data(), entry.isDir()? "/": "");
		if (recursive && level < 64 && entry.isDir() && entry.fileName() != "." && entry.fileName() != "..") {
			_list(path + (path.endsWith("/")? "": "/") + entry.fileName(), recursive, level + 1);
		}
	}
}

#include <QApplication>
#include <QCoreApplication>
#include <QStandardPaths>  // http://doc.qt.io/qt-5/qstandardpaths.html

void
// #if defined(__ANDROID__)
// _init_python() {
// #elif defined(__linux__)
_init_python(int argc, char* argv[]) {
// #elif defined(_WIN32)
// #endif
	// char string[PATH_MAX];

	// Detects environment
	{
		// QApplication app();
		// QApplication::instance();
	}

#if defined(__ANDROID__)
#	define CACHE_PATH QStandardPaths::writableLocation(QStandardPaths::CacheLocation)
#	define SHARED_CACHE_PATH QStandardPaths::writableLocation(QStandardPaths::GenericCacheLocation)
#	define DATA_PATH QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)
#	define SHARED_DATA_PATH (QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) + "/" APP_BASENAME)
#if defined(__ANDROID__)
#endif
#else
#	define CACHE_PATH QString(".")
#	define SHARED_CACHE_PATH QString(".")
#	define DATA_PATH QString(".")
#	define SHARED_DATA_PATH QString(".")
#endif

// #if defined(__ANDROID__)
	QString libs_path = QFileInfo(argv[0]).dir().absolutePath();
	// QString current_path = QDir::currentPath();
	QString cache_path = CACHE_PATH;
	// QString data_path = DATA_PATH;

#ifdef DEBUG
	{ // Prints out contents of assets
		LOG("Libs path: %s", libs_path.toLatin1().data());
		LOG("Home path: %s", QDir::homePath().toLatin1().data());
		LOG("Cache path: %s", CACHE_PATH.toLatin1().data());
		LOG("Shared cache path: %s", SHARED_CACHE_PATH.toLatin1().data());
		LOG("Data path: %s", DATA_PATH.toLatin1().data());
		LOG("Shared data path: %s", SHARED_DATA_PATH.toLatin1().data());
		LOG("--");
		LOG("DesktopLocation: %s", QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).join(" , ").toLatin1().data());
		LOG("MusicLocation: %s", QStandardPaths::standardLocations(QStandardPaths::MusicLocation).join(" , ").toLatin1().data());
		LOG("MoviesLocation: %s", QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).join(" , ").toLatin1().data());
		LOG("PicturesLocation: %s", QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).join(" , ").toLatin1().data());
		LOG("TempLocation: %s", QStandardPaths::standardLocations(QStandardPaths::TempLocation).join(" , ").toLatin1().data());
		LOG("HomeLocation: %s", QStandardPaths::standardLocations(QStandardPaths::HomeLocation).join(" , ").toLatin1().data());
		LOG("DataLocation: %s", QStandardPaths::standardLocations(QStandardPaths::DataLocation).join(" , ").toLatin1().data());
		LOG("CacheLocation: %s", QStandardPaths::standardLocations(QStandardPaths::CacheLocation).join(" , ").toLatin1().data());
		LOG("GenericDataLocation: %s", QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation).join(" , ").toLatin1().data());
		LOG("RuntimeLocation: %s", QStandardPaths::standardLocations(QStandardPaths::RuntimeLocation).join(" , ").toLatin1().data());
		LOG("ConfigLocation: %s", QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).join(" , ").toLatin1().data());
		LOG("GenericConfigLocation: %s", QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation).join(" , ").toLatin1().data());
		LOG("DownloadLocation: %s", QStandardPaths::standardLocations(QStandardPaths::DownloadLocation).join(" , ").toLatin1().data());
		LOG("GenericCacheLocation: %s", QStandardPaths::standardLocations(QStandardPaths::GenericCacheLocation).join(" , ").toLatin1().data());
		LOG("AppDataLocation: %s", QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).join(" , ").toLatin1().data());
		LOG("AppConfigLocation: %s", QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation).join(" , ").toLatin1().data());
		LOG("AppLocalDataLocation: %s", QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation).join(" , ").toLatin1().data());
		LOG("--");
		
		// QStandardPaths::HomeLocation
		//
		// QStandardPaths::AppDataLocation
		// QStandardPaths::GenericDataLocation  // shared data
		//
		// QStandardPaths::AppConfigLocation
		// QStandardPaths::ConfigLocation  // user-specific configs
		//
		// QStandardPaths::CacheLocation
		// QStandardPaths::GenericCacheLocation  // shared cache
	}
#endif

#ifdef DEBUG
	{ // Prints out contents of assets
		_list(QString("assets:/"), /*recursive*/1, 0);
	}
#endif

	{ // Creates symlinks etc.
		// LOG("Creating symlinks");
		DIR* dir;
		struct dirent* directory_entity;

		if ((dir = opendir(libs_path.toLatin1().data())) == NULL) {
			LOG("Can not open '%s'", libs_path.toLatin1().data());
		} else {
			QString filename, src_path, dst_path;

			while ((directory_entity = readdir(dir)) != NULL) {
				filename = QString(directory_entity->d_name);
				// LOG("Entry: '%s'...", filename.toLatin1().data());

				// Trims prefix and suffix, replaces path separator, unwraps
				unsigned int trim_left = filename.startsWith(LIB_PREFIX)? strlen(LIB_PREFIX): 0;
				unsigned int trim_right = filename.endsWith(LIB_SUFFIX)? strlen(LIB_SUFFIX): 0;
				if (trim_left > 0 || trim_right > 0) {
					src_path = libs_path + "/" + filename;

					// Modifies filename: 1) remove 'lib+' and '+.so'; 2) replace '+' with '/' and create non-existent directories
					filename = filename.mid(trim_left, filename.length() - trim_left - trim_right).replace(LIB_SEPARATOR, "/");

					// Creates hierarchy of directories
					QString directory_path = filename.section("/", 0, -2);
					if (directory_path.length()) {
						LOG("Creating directory '%s'...", directory_path.toLatin1().data());
						QDir(cache_path).mkpath(directory_path);
					}

					dst_path = cache_path + "/" + filename;

					// Puts file to a right place
					// LOG("Moving file: %s => %s", src_path.toLatin1().data(), dst_path.toLatin1().data());
					// QFile(src_path).rename(dst_path);
					LOG("Creating symlink %s => %s", src_path.toLatin1().data(), dst_path.toLatin1().data());
					unlink(dst_path.toLatin1().data());
					QFile(src_path).link(dst_path);

					if (dst_path.endsWith(WRAP_SUFFIX)) {
						src_path = dst_path;
						dst_path = dst_path.replace(WRAP_SUFFIX, "");

						QFileInfo src_info(src_path);
						QFileInfo dst_info(dst_path);
						// LOG("Modified: src=%d, dst=%d", (int)src_info.lastModified().toSecsSinceEpoch(), (int)dst_info.lastModified().toSecsSinceEpoch());
						if (true) {
						// if (dst_info.exists() && src_info.lastModified().toSecsSinceEpoch() <= dst_info.lastModified().toSecsSinceEpoch()) {
						//     LOG("Unwrapped file exists and newer: %s", dst_path.toLatin1().data());
						// } else {
							LOG("Unwrapping %s => %s", src_path.toLatin1().data(), dst_path.toLatin1().data());
							unlink(dst_path.toLatin1().data());
							QFile(src_path).copy(dst_path);

							QFile dst(dst_path);
							if (!dst.open(QIODevice::ReadWrite)) {
								LOG("Can not open file \"%s\"", dst_path.toLatin1().data());

							} else {
								unsigned int genuine_part_length = MIN(dst.size() - WRAP_LENGTH, WRAP_LENGTH);

								// Reads genuine content from the end
								dst.seek(dst.size() - genuine_part_length);
								QByteArray genuine_part = dst.read(genuine_part_length);

								// Writes genuine content to the beginning
								dst.seek(0);
								dst.write(genuine_part);
								dst.close();

								// Trims genuine content from the end
								dst.resize(dst.size() - WRAP_LENGTH);
							}
						}
						unlink(src_path.toLatin1().data());
					}

					// Insert here extraction from asset or something else
					// if (dst_path.endsWith(ZIP_SUFFIX)) {
					//     src_path = dst_path;
					//     dst_path = dst_path.replace(ZIP_SUFFIX, "");

					//     QFile src(src_path);
					//     if (!src.open(QIODevice::ReadWrite)) {
					//         LOG("Can not open file \"%s\"", src_path.toLatin1().data());

					//     } else {

					//         // // src.rename(dst_path);
					//         // LOG("Unwrapping %s => %s", src_path.toLatin1().data(), dst_path.toLatin1().data());
					//         // unlink(dst_path.toLatin1().data());
					//         // // src.link(dst_path);
					//         // src.copy(dst_path);
					//         // unlink(src_path.toLatin1().data());
					//     }
					// }
				}
			}
			closedir(dir);
		}
	}
// #endif

	// { // Read listings of some directories
	//     const char* directories_list[] = {"lib", "files", ""};  // Empty-string-ended array
	//     DIR* dir;
	//     struct dirent* directory_entity;
	//     for (int i = 0; strlen(directories_list[i]); i ++) {
	//         snprintf(string, sizeof(string), "%s/../%s", getenv("APPLICATION_FILES"), directories_list[i]);
	//         LOG("Listing of %s", string);
	//         if ((dir = opendir(string)) == NULL) {
	//             LOG("\tcan not open");
	//         } else {
	//             while ((directory_entity = readdir(dir)) != NULL) {
	//                 LOG("\t%s", directory_entity->d_name);
	//             }
	//             closedir(dir);
	//         }
	//     }
	// }

#ifdef DEBUG
	{ // Prints out contents of cache path
		LOG("CACHE_PATH:");
		_list(CACHE_PATH, /*recursive*/1, 0);
		LOG("---");
	}
#endif

#ifdef DEBUG
	{ // Prints out contents of data path
		LOG("DATA_PATH:");
		_list(DATA_PATH, /*recursive*/1, 0);
		LOG("---");
	}
#endif

#ifdef DEBUG
	{ // Prints out contents of shared data path
		LOG("SHARED_DATA_PATH:");
		_list(SHARED_DATA_PATH, /*recursive*/0, 0);
		LOG("---");
	}
#endif

	// QString python_verbose = QString("5");
	// LOG("PYTHONVERBOSE=\"%s\"", python_verbose.toLatin1().data());
	// setenv("PYTHONVERBOSE", python_verbose.toLatin1().data(), [>rewrite<]1);

	QString python_home = cache_path + "/.python27";
	LOG("PYTHONHOME=\"%s\"", python_home.toLatin1().data());
	setenv("PYTHONHOME", python_home.toLatin1().data(), /*rewrite*/1);

	QString python_path = QString() + SHARED_DATA_PATH + ":" + cache_path + "/.app" ":" + cache_path + ":" + cache_path + "/.python27" ":" + cache_path + "/.python27/site-packages";
	LOG("PYTHONPATH=\"%s\"", python_path.toLatin1().data());
	setenv("PYTHONPATH", python_path.toLatin1().data(), /*rewrite*/1);

	setenv("APP_NAME", APP_NAME, /*rewrite*/1);
	setenv("PACKAGE", PACKAGE, /*rewrite*/1);
	setenv("VERSION", VERSION, /*rewrite*/1);
	setenv("VERSION_CODE", VERSION_CODE, /*rewrite*/1);

	LOG("Setting python's program name: '%s'", APP_NAME);
	char app_name[] = APP_NAME;
	Py_SetProgramName(app_name);

	LOG("Initializing python...");
	Py_Initialize();  // 130ms

	LOG("Initializing python threads...");
	PyEval_InitThreads();

	LOG("Setting python arguments...");
// #if defined(__ANDROID__)
//     int _argc = 1;
//     char package[] = PACKAGE;
//     char* _argv[] = { package, };
//     PySys_SetArgv(_argc, _argv);
// #elif defined(__linux__)
	PySys_SetArgv(argc, argv);
// #elif defined(_WIN32)
// #endif

}

#if defined(__ANDROID__)
#	include <jni.h>

#	include <QApplication>
#	include <QLabel>

extern "C" {
//     void Java_org_qtproject_qt5_android_bindings_QtActivity_PyEval(JNIEnv* jenv, jclass jcls, jstring jscript) {
//         jcls = jcls;

//         QString script(jenv->GetStringUTFChars(jscript, NULL));

//         qDebug("PyEval: %s", script.toStdString().c_str());
//         // QApplication::activeWindow()->findChild<QLabel*>("py_eval_widget")->setText(script);
//         // QMetaObject::invokeMethod(QApplication::activeWindow()->findChild<QLabel*>("py_eval_widget"), "setText", Qt::QueuedConnection, Q_ARG(QString, script));
//         // QApplication::activeWindow()->findChild<QLabel*>("py_eval_widget")->linkActivated(script);
//         QMetaObject::invokeMethod(QApplication::activeWindow()->findChild<QLabel*>("py_eval_widget"), "linkActivated", Qt::QueuedConnection, Q_ARG(QString, script));
//         // qDebug("PyEval: DONE");
//     }

//     void Java_org_qtproject_qt5_android_bindings_QtHelper_setenv(JNIEnv* jenv, jclass jcls, jstring jkey, jstring jvalue) {
//         jcls = jcls;

//         QString key(jenv->GetStringUTFChars(jkey, NULL));
//         QString value(jenv->GetStringUTFChars(jvalue, NULL));

//         // LOG("SETENV: %s => %s", key.toStdString().c_str(), value.toStdString().c_str());
//         // qDebug("SETENV: %s => %s", key.toStdString().c_str(), value.toStdString().c_str());

//         setenv(key.toStdString().c_str(), value.toStdString().c_str(), [>rewrite<]1);
//     }

//     void Java_org_qtproject_qt5_android_bindings_QtHelper_pyeval(JNIEnv* jenv, jclass jcls, jstring jscript) {
//         jcls = jcls;

//         QString script(jenv->GetStringUTFChars(jscript, NULL));

// #	if defined(__ANDROID__)
//         _redirect_stdout_stderr([>directory_path<]SHARED_DATA_PATH, QString("native.txt"), "a");
// #	endif

// //FIXME		_init_python();

//         PyRun_SimpleString(script.toStdString().c_str());  // 50ms

//         Py_Finalize();

//         // // Receives result through environment variable 'return'
//         // return (*jenv).NewStringUTF(getenv("return"));
//     }

	jstring Java_org_qtproject_qt5_android_bindings_QtHelper_pyrun(JNIEnv* jenv, jclass jcls, jstring jmodule, jstring jfunction, jstring jargument) {
		jcls = jcls;

		QString module_name(jenv->GetStringUTFChars(jmodule, NULL));
		QString function(jenv->GetStringUTFChars(jfunction, NULL));
		QString argument(jenv->GetStringUTFChars(jargument, NULL));

#	if defined(__ANDROID__)
		_redirect_stdout_stderr(/*directory_path*/SHARED_DATA_PATH, QString("native.txt"), "a");
#	endif

//FIXME		_init_python();

		PyObject* py_module_name = PyString_FromString((char*)module_name.toStdString().c_str());
		PyObject* py_module = PyImport_Import(py_module_name);
		
		PyObject* py_function = PyObject_GetAttrString(py_module, (char*)function.toStdString().c_str());

		PyObject* args = PyTuple_Pack(1, PyString_FromString(argument.toStdString().c_str()));

		PyObject* py_result = PyObject_CallObject(py_function, args);

		char* result = PyString_AsString(py_result);

		return (*jenv).NewStringUTF(result);
	}
};

// ALL IMPLEMENTED DIRECTLY IN QtJavaCaller.so !!!
// #include <QtAndroidExtras/qandroidjniobject.h>

// // This function is calling from Cython-wrapper "QtJavaCaller.so"
// char*
// // javaeval(const char* cls, const char* method, const char* types, char* serialized_kwargs) {
// javaeval(const char* cls, const char* method, char* serialized_kwargs) {
//     // // LOG("javaeval: %s/%s%s", cls, method, types);
//     // int return_code = QAndroidJniObject::callStaticMethod<int>(cls, method);
//     // // LOG("javaeval: DONE, %d", return_code);

//     // LOG("javaeval: %s/%s(): kwargs=%s", cls, method, serialized_kwargs);
//     // QAndroidJniObject _serialized_kwargs = QAndroidJniObject::fromString(serialized_kwargs);
//     // int return_code = QAndroidJniObject::callStaticMethod<int>(cls, method, "(Ljava/lang/String;)I", _serialized_kwargs.object<jstring>());
//     // LOG("javaeval: DONE, %d", return_code);

//     // LOG("javaeval: %s/%s(): kwargs=%s", cls, method, serialized_kwargs);
//     char* serialized_result = strdup(QAndroidJniObject::callStaticObjectMethod(
//             cls,
//             method,
//             "(Ljava/lang/String;)Ljava/lang/String;",
//             QAndroidJniObject::fromString(serialized_kwargs).object<jstring>()
//         ).toString().toUtf8().data());
//         // ).toString().toLatin1().data();
//     // LOG("javaeval: DONE, serialized_result=%s", serialized_result);
//     // printf("serialized_result=%s\n", serialized_result); fflush(stdout);

//     // QAndroidJniObject stringNumber = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/bindings/QtHelper", "setjenv", "()V");

//     // // the signature for the second function is "(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;"
//     // QAndroidJniObject string1 = QAndroidJniObject::fromString("String1");
//     // QAndroidJniObject string2 = QAndroidJniObject::fromString("String2");
//     // QAndroidJniObject stringArray = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/TestClass",
//     //         "stringArray"
//     //         "(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;"
//     //         string1.object<jstring>(),
//     //         string2.object<jstring>());

//     return serialized_result;
// }
#endif

#include <QApplication>
#include <QDir>
#include <QProcess>

#if defined(__ANDROID__)
#	include <android/log.h>
#endif

#include ".dependencies/qtadmob/QtAdMobBanner.h"
#include ".dependencies/qtadmob/QtAdMobInterstitial.h"

int
main(int argc, char* argv[]) {  //, char **env) {
	argc = argc; argv = argv;  // Suppress warnings

	for (int index = 0; index < argc; index ++) {
		LOG("ARGV[%d] = '%s'", index, argv[index]);
	}

	LOG("Dumping environment variables...");
	// for (char** key = env; key != NULL && *key != 0; key ++) {
	//     LOG("ENV: %s", *key);    
	// }
	LOG("\n\t\t%s", QProcess::systemEnvironment().join("\n\t\t").toLatin1().data());
	LOG("CWD: %s", QDir::currentPath().toLatin1().data());
	LOG("--------------------");

#if defined(__ANDROID__)
	_redirect_stdout_stderr(/*directory_path*/SHARED_DATA_PATH, QString("native.txt"), "w");
#elif defined(__linux__)
	// setenv("APPLICATION_FILES", dirname(argv[0]), [>rewrite<]0);
	// setenv("APPLICATION_BASENAME", argv[0], [>rewrite<]0);
#elif defined(_WIN32)
#else
#endif

// #if defined(__ANDROID__)
//     _init_python();
// #elif defined(__linux__)
	_init_python(argc, argv);
// #elif defined(_WIN32)
// #endif

	/// Найти setenv'ы и эти переменные в питон-коде и заменить на нормальные Qt-шные вызовы
	/// Починить logging в питоне, чтобы писал через qDebug
	/// Extract qrc:/python and qrc:/app в не сжатые папки

	LOG("Starting controllers");


#ifdef DEBUG
	fprintf(stdout, "Some dump into stdout to check if Qt writes into stdout and not in syslog or somewhere else...\n");
	fprintf(stderr, "Some dump into stderr to check if Qt writes into stderr and not in syslog or somewhere else...\n");
#	if defined(__ANDROID__)
	__android_log_write(ANDROID_LOG_DEBUG, APP_NAME, QString("Some android debug").toLocal8Bit().constData());
	__android_log_write(ANDROID_LOG_INFO, APP_NAME, QString("Some android info").toLocal8Bit().constData());
	__android_log_write(ANDROID_LOG_WARN, APP_NAME, QString("Some android warn").toLocal8Bit().constData());
	__android_log_write(ANDROID_LOG_ERROR, APP_NAME, QString("Some android error").toLocal8Bit().constData());
	__android_log_write(ANDROID_LOG_FATAL, APP_NAME, QString("Some android fatal").toLocal8Bit().constData());
#	endif
#endif

	QApplication app(argc, argv);



    // IQtAdMobBanner* banner = CreateQtAdMobBanner();
	// banner->setUnitId(QString("ca-app-pub-9181265559842913/2733995986"));
    // banner->setSize(IQtAdMobBanner::Banner);
	// banner->addTestDevice(QString("A5CE5C5520DA3A67A0E3776681CB7BDA"));
	// banner->setVisible(1);

	// PyRun_SimpleString("import QtJavaCaller; QtJavaCaller.createQtAdMobBanner()");  // 50ms

	// app.exec();




	QString args = QString("");
	for (uint8_t index = 0; index < argc; index ++) {
		args += "'" + QString(argv[index]) + "', ";
	}

	PyRun_SimpleString((QString() + "import controllers; controllers.loop([" + args + "])").toLatin1().data());  // 50ms

	Py_Finalize();

	LOG("_init() ends");

	return 0;
}

// --------------------------------------------------------------------------------

// #include "main_view.h"

// #include <QApplication>
// #include <QMovie>
// #include <QLabel>

// void
// test_qt(int argc, char* argv[]);

// int
// main(int argc, char* argv[]) {
	// QApplication a(argc, argv);
    //MainView w;
    //w.show();
    
	// QLabel* label = new QLabel();
	// QMovie* movie = new QMovie(":/views/images/splash.gif");
	// label->setMovie(movie);
	// label->show();
	// movie->start();

	// a.exec();

	// test_qt(argc, argv);

	// QApplication a(argc, argv);
    // //MainView w;
    // //w.show();
    
    // return a.exec();
// }

// #include <QCoreApplication>
// #include <QFile>
// #include <QLibraryInfo>
// #include <QTextStream>

// void
// test_qt(int argc, char* argv[]) {
//     QCoreApplication app(argc, argv);
//     QFile outf("/sdcard/Vocabulary Trainer/qtdetail.out");

//     if (!outf.open(QIODevice::WriteOnly|QIODevice::Truncate|QIODevice::Text)) {

//     } else {

//         QTextStream out(&outf);

//         out << QLibraryInfo::licensee() << '\n';

// #if defined(QT_SHARED) || defined(QT_DLL)
//         out << "shared\n";
// #else
//         out << "static\n";
// #endif

//         // Determine which features should be disabled.

// #if defined(QT_NO_ACCESSIBILITY)
//         out << "PyQt_Accessibility\n";
// #endif

// #if defined(QT_NO_SESSIONMANAGER)
//         out << "PyQt_SessionManager\n";
// #endif

// #if defined(QT_NO_SSL)
//         out << "PyQt_SSL\n";
// #endif

// #if defined(QT_NO_PRINTDIALOG)
//         out << "PyQt_PrintDialog\n";
// #endif

// #if defined(QT_NO_PRINTER)
//         out << "PyQt_Printer\n";
// #endif

// #if defined(QT_NO_PRINTPREVIEWDIALOG)
//         out << "PyQt_PrintPreviewDialog\n";
// #endif

// #if defined(QT_NO_PRINTPREVIEWWIDGET)
//         out << "PyQt_PrintPreviewWidget\n";
// #endif

// #if defined(QT_NO_RAWFONT)
//         out << "PyQt_RawFont\n";
// #endif

// #if defined(QT_NO_OPENGL)
//         out << "PyQt_OpenGL\n";
//         out << "PyQt_Desktop_OpenGL\n";
// #elif defined(QT_OPENGL_ES_2)
//         out << "PyQt_Desktop_OpenGL\n";
// #endif

// #if QT_VERSION < 0x050200
//         // This is the test used in qglobal.h in Qt prior to v5.2.  In v5.2 and later
//         // qreal is always double.
// #if defined(QT_NO_FPU) || defined(Q_PROCESSOR_ARM) || defined(Q_OS_WINCE)
//         out << "PyQt_qreal_double\n";
// #endif
// #endif

// #if defined(QT_NO_PROCESS)
//         out << "PyQt_Process\n";
// #endif
//     }
// }
