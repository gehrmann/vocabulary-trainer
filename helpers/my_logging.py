#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"

from __future__ import division, unicode_literals; del division, unicode_literals  # Imports features but removes them from documentation
import base64
import binascii
import contextlib
import logging
import logging.handlers
import os
import sys

__doc__ = """
Module to configure logging for python script.

Logging level raises (for a script) if found:
	a) --info or -v (as a command line argument)
	b) --debug or -vv (as a command line argument)
	c) LOGGING_<PATH_TO_MODULE> environment variable.
		For Ex.:
		1) LOGGING___MAIN__ -- for a running module or
		2) LOGGING_TCOS_LOGGING -- for tcos_logging.py or tcos/logging.py
		Values: DEBUG, INFO, WARNING or ERROR.
		Default: see attribute 'default_level' of init().
	d) LOGGING environment variable was found (will be applied for every module)
	e) 'logging.debug' (case insensitive) in application title was found

Doc-Tests
=========

	>>> _doctest_setup(namespace=locals())

How to use it
=============

Insert:
	>>> import tcos_logging
	>>> tcos_logging.init(logger=logging.getLogger(__name__))

or (to ignore exception if module is not available):
	>>> try:
	... 	import tcos_logging
	... 	tcos_logging.init(logger=logging.getLogger(__name__))
	... except ImportError:
	... 	# Attention: does not redirect stderr
	... 	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')

into your script's section where imports of 3rd-party modules are.
"""


def _doctest_setup(namespace):
	"""Prepares each doc-tests environment for execution"""
	import mock; mock

	sys.path.insert(0, (os.path.dirname(sys.argv[-1]) or '.') + '/')
	from helpers import tests; tests

	# Imports local variables into specified namespace
	namespace.update(locals())


def _init_logging_level(logger, default_level='WARNING'):
	"""Configures logging level for a module (where logger was created).

	How the logging level for a module will be set:
		1) Looking for an environment variable named like LOGGING_<PATH_TO_MODULE>
		2) If not found, looking for a environment variable LOGGING
		3) If not found, looking for a "logging.debug" or "logging.info" (case insensitive) in application title
	"""
	# Replaces level if specific environment variables found
	logger_key = (os.path.splitext(os.path.basename(sys.argv[0]))[0] if logger.name == '__main__' else logger.name).replace('.', '_').replace('-', '_').upper()
	level = os.environ.get('LOGGING_' + logger_key, os.environ.get('LOGGING', default_level))

	logger.setLevel(getattr(logging, level))
	# logging.getLogger(__name__).debug('Set logging level for module through %s (%s)', 'LOGGING_' + logger_key, level)


def _init_logging_stderr(logger):
	class StreamLogger(object):
		"""Redirects stream to logger"""
		def __init__(self, logger, level, previous_stream):
			self._logger = logger
			self._level = level
			self._buffer = []

			# Implements .fileno() (for modules and functions like subprocess which use sys.stderr.fileno()):
			#   * opens pipe and returns its descriptor for a writing as a result of .fileno()
			#   * listens to new data to read in the pipe (with select() system call)
			#   * sends it into .write() method
			pipe_to_read, pipe_to_write = os.pipe()

			def read_pipe(self, parent_thread):
				from select import select
				while True:
					# Waits for file events or timeout
					to_read, to_write, to_catch = select([pipe_to_read], [], [], .2)

					# Reads data from pipe
					if pipe_to_read in to_read:
						self.write(os.read(pipe_to_read, 1024))

					# Exits if parent thread not exists
					if not parent_thread.isAlive():
						return
			import threading
			self._read_pipe_thread = thread = threading.Thread(
				target=read_pipe,
				kwargs=dict(
					self=self,
					parent_thread=threading.current_thread(),
				),
			)
			thread.start()
			self.fileno = lambda: pipe_to_write

		def write(self, message):
			"""Writes message, line-buffered"""
			self._buffer.append(message)
			if '\n' in message:
				self.flush()

		def flush(self):
			if self._buffer:
				with self._patch_logging_currentframe():
					for line in ''.join(self._buffer).splitlines():
						self._logger.log(self._level, line.rstrip('\r\n'))
				self._buffer[:] = []

		@contextlib.contextmanager
		def _patch_logging_currentframe(self):
			"""Applies temporary monkey-patch for logging module: changes stack level to print out."""
			previous, logging.currentframe = logging.currentframe, lambda: (sys._getframe(5))
			yield
			logging.currentframe = previous
	sys.stderr.flush()
	sys.stderr = StreamLogger(logger, level=logging.ERROR, previous_stream=sys.stderr)


def init(
		logger,
		default_level='WARNING',
		with_stderr=True,
		force_stderr=False,
		with_first_argument=False,
		with_syslog=False,
		with_android_log=False,
		__state=dict(initialized=False),
):
	"""Initializes logging abilities for a module where was called.

	Arguments:
		logger -- logging object specific for a module. For Ex., logging.getLogger(__name__) .
		default_level (string) -- default logging level for a module (see logging.setLevel()).
			Values: DEBUG, INFO, WARNING or ERROR.
			Default: WARNING.
		with_stderr (bool) -- Enables redirection STDERR => logging.WARNING
	"""
	# Changes logging level from boot arguments
	try:
		import tcos_system
	except ImportError:
		boot_arguments = dict()
	else:
		boot_arguments = tcos_system.get_boot_arguments()
	logger_key = (os.path.splitext(os.path.basename(sys.argv[0]))[0] if logger.name == '__main__' else logger.name).replace('.', '_').replace('-', '_').upper()
	default_level = boot_arguments.get('LOGGING_' + logger_key, boot_arguments.get('LOGGING', default_level))

	# Configures logging (only once)
	if not __state['initialized']:
		# Looks for -v, -vv, --info, --debug in arguments
		import argparse
		parser = argparse.ArgumentParser(add_help=False)
		parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
		parser.add_argument('--info', action='store_true', help='Raises logging level to info')
		parser.add_argument('--debug', action='store_true', help='Raises logging level to debug')
		kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

		if with_first_argument:
			# Replaces default_level for script
			try:
				decoded_title = base64.b16decode(''.join(sys.argv[1:]))
			except (TypeError, binascii.Error):
				decoded_title = ''.join(sys.argv[1:])
			if 'logging.info' in decoded_title.lower():
				default_level = 'INFO'
			if 'logging.debug' in decoded_title.lower():
				default_level = 'DEBUG'
			if kwargs['info'] or kwargs['verbose'] == 1:
				default_level = 'INFO'
			if kwargs['debug'] or kwargs['verbose'] >= 2:
				default_level = 'DEBUG'

		# Configures logging module
		logging.basicConfig(
			level=logging.WARNING, datefmt='%H:%M:%S',
			format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
		)

		if with_syslog:
			# Configures sending to syslog (ignores if /dev/log not exists)
			try:
				syslog_handler = logging.handlers.SysLogHandler(address='/dev/log')
				syslog_handler.setFormatter(logging.Formatter('%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s'))
				logging.root.addHandler(syslog_handler)
			except IOError:
				pass

		if with_android_log:
			from controllers.abstract import ControllerMixture

			class _AndroidLogHandler(logging.StreamHandler):
				# def __init__(self):
				#     super(_AndroidLogHandler, self).__init__()
				#     self._messages = []

				def emit(self, event):
					"""
					'args', 'asctime', 'created', 'exc_info', 'exc_text', 'filename', 'funcName', 'getMessage', 'levelname', 'levelno', 'lineno',
					'message', 'module', 'msecs', 'msg', 'name', 'pathname', 'process', 'processName', 'relativeCreated', 'thread', 'threadName'
					"""
					ControllerMixture.java.call('log', ignore_wrong_platform_exception=True, level='<LEVEL>', message='<MESSAGE>')
					# ControllerMixture.java.call('log', ignore_wrong_platform_exception=True, level=event.levelname, message=event.message)
					# ControllerMixture.java.warning(event.message)

			android_log_handler = _AndroidLogHandler()
			# android_log_handler.setLevel(...)
			android_log_handler.setFormatter(logging.Formatter('%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s'))
			logging.root.addHandler(android_log_handler)

		# Redirects stderr to logger (if STDERR is not redirected to the console)
		if os.environ.get('TERM', '') in ('', 'linux') or force_stderr:
			if with_stderr or force_stderr:
				_init_logging_stderr(logger)

		# Configures logging level for self (__name__)
		_init_logging_level(logger=logging.getLogger(__name__))

	__state['initialized'] = True

	# Configures logging level for a module (logger.name)
	_init_logging_level(logger, default_level)


def run_init():
	"""Entry point to simulate initialization. Only for developing purposes."""
	import tcos_logging
	tcos_logging.init(logger=logging.getLogger(__name__))

	print('STDOUT')
	print('STDERR', file=sys.stderr);
	logging.getLogger(__name__).debug('DEBUG')
	logging.getLogger(__name__).info('INFO')
	logging.getLogger(__name__).warning('WARNING')
	logging.getLogger(__name__).error('ERROR')


def _main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	_main()
