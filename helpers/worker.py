#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division

__doc__ = """
This module provides tools for creation of slave processes with cross-processing interaction

Environment variables:
	LOGGING or LOGGING_<MODULE> -- Logging level ( NOTSET | DEBUG | INFO | WARNING | ERROR | CRITICAL )
"""

import ast
import errno
import functools
import logging
import multiprocessing
import threading
import os
import pickle
import sys
import time

if __name__ == '__main__':
	# # Sets utf-8 (instead of latin1) as default encoding for every IO
	# reload(sys); sys.setdefaultencoding('utf-8')
	# Runs in application's working directory
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), 'WARNING')))


class Worker(object):
	"""Implements slave process with a bidirectional queue.

	After .write() must be .read(): another .write() will be locked!
	"""
	def __init__(self, target):
		self._pipe, self._child_pipe = pipe, child_pipe = multiprocessing.Pipe(duplex=True)
		self.read = functools.partial(self._read, self, pipe)
		self.write = functools.partial(self._write, self, pipe)

		self._lock = threading.Lock()  # Blocks another one .write() after .write()

		def run_target(*args, **kwargs):
			def run_check_if_parent_alive():
				while True:
					try:
						os.kill(os.getppid(), 0)
					except OSError:
						logging.getLogger(__name__).warn('Can not find parent process. Terminating worker...')
						import signal
						os.kill(os.getpid(), signal.SIGTERM)
					else:
						if logging.getLogger(__name__).level == logging.DEBUG:
							sys.stderr.write(' C'); sys.stderr.flush()  # FIXME: must be removed/commented
					time.sleep(10.)
			thread = threading.Thread(target=run_check_if_parent_alive)
			thread.setDaemon(True)  # Terminate if main thread terminates
			thread.start()

			target(*args, **kwargs)
		self._process = process = multiprocessing.Process(target=run_target, kwargs=dict(read=functools.partial(self._read, None, child_pipe), write=functools.partial(self._write, None, child_pipe)))
		process.daemon = True  # Die if main process was finished
		process.start()
		logging.getLogger(__name__).info('Active workers: %s', len(multiprocessing.active_children()))

	@staticmethod
	def _read(self, pipe):
		"""
		self is not None if master.
		"""
		while True:
			try:
				if logging.getLogger(__name__).level == logging.DEBUG:
					sys.stderr.write('R' if self is not None else ('r')); sys.stderr.flush()  # FIXME: must be removed/commented
				time.sleep(.1)
				result = pipe.recv()
			except IOError as e:
				if e.errno == errno.EAGAIN:  # "Resource temporarily unavailable" error
					continue
				elif e.errno == errno.EINTR:  # "Interrupted system call" error
					logging.getLogger(__name__).warning('%s: %s', e.__class__, e)
					continue
				raise
			break

		if self is not None:  # If master
			self._lock.release()

		return result

	@staticmethod
	def _write(self, pipe, value):
		if self is not None:  # If master
			self._lock.acquire()

		pipe.send(value)


def run_init():
	pass


def main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Increases verbosity level (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs['verbose'] or 0, 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
