#!/usr/bin/env bash

SDK="/home/vlad/-install/-programming/-android/sdk"
DST="/storage/emulated/0"

echo ""
FILES=$(${SDK}/platform-tools/adb shell ls "${DST}/Vocabulary Trainer/" | grep "native_line_profile" | awk '{print "${DST}/Vocabulary Trainer/"$0}')
if [ "${FILES}" != "" ]; then
	IFS=$'\r\n'
	for FILE in ${FILES}
	do
		echo "${FILE}"
		${SDK}/platform-tools/adb pull "${FILE}"
		${SDK}/platform-tools/adb shell rm "${FILE}"
	done
fi
